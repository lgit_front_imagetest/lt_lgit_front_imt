//*****************************************************************************
// Filename	: 	MES_Base.h
// Created	:	2018/3/1 - 14:48
// Modified	:	2018/3/1 - 14:48
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef MES_Base_h__
#define MES_Base_h__

#pragma once
#include <afxwin.h>

//=============================================================================
// MES 결과 항목
//=============================================================================
typedef struct _tag_MES_ResultItem
{
	UINT		nReultItem_Index;	// MES 결과 항목 순서
	BOOL		bUse;				// 검사 항목 사용여부 (미사용시 -> :1,)

	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	CString		JUDGE;				// 판정

	_tag_MES_ResultItem()
	{
		nReultItem_Index = 0;
		bUse = TRUE;
	};

	_tag_MES_ResultItem& operator= (const _tag_MES_ResultItem& ref)
	{
		nReultItem_Index	= ref.nReultItem_Index;
		bUse				= ref.bUse;
		NAME				= ref.NAME;
		VALUE				= ref.VALUE;
		JUDGE				= ref.JUDGE;

		return *this;
	};

	void Reset()
	{
		bUse = FALSE;
		NAME.Empty();
		VALUE.Empty();
		JUDGE.Empty();
	};

	void Reset_ItemData()
	{
		VALUE.Empty();
		JUDGE.Empty();
	};

	void Set_Data_NotUse()
	{
		bUse = FALSE;
		VALUE.Empty();
		JUDGE = _T("1");
	};

	void Set_Data( __in DOUBLE dValue, __in BOOL bPass)
	{
		bUse = TRUE;
		VALUE.Format(_T("%f"), dValue);

		if (bPass)
			JUDGE = "1";
		else
			JUDGE = "0";
	};

	void Set_Data(__in LPCTSTR szName, __in DOUBLE dValue, __in BOOL bPass)
	{
		NAME = szName;
		Set_Data(dValue, bPass);
	};

	void Set_Data(__in FLOAT fValue, __in BOOL bPass)
	{
		bUse = TRUE;
		VALUE.Format(_T("%f"), fValue);

		if (bPass)
			JUDGE = "1";
		else
			JUDGE = "0";
	};
	void Set_Data(__in LPCTSTR szName, __in FLOAT fValue, __in BOOL bPass)
	{
		NAME = szName;
		Set_Data(fValue, bPass);
	};

	void Set_Data(__in LONG lValue, __in BOOL bPass)
	{
		bUse = TRUE;
		VALUE.Format(_T("%d"), lValue);

		if (bPass)
			JUDGE = "1";
		else
			JUDGE = "0";
	};

	void Set_Data(__in LPCTSTR szName, __in LONG lValue, __in BOOL bPass)
	{
		NAME = szName;
		Set_Data(lValue, bPass);
	};

	void Set_Data(__in LPCTSTR szValue, __in BOOL bPass)
	{
		bUse = TRUE;
		VALUE = szValue;

		if (bPass)
			JUDGE = "1";
		else
			JUDGE = "0";
	};

	void Set_Data(__in LPCTSTR szName, __in LPCTSTR szValue, __in BOOL bPass)
	{
		NAME = szName;
		Set_Data(szValue, bPass);
	};

	CString Get_MESData()
	{
		CString szData;

		// ex) 0.0:1, 1.1:0, example:1, 
		szData.Format(_T("%s:%s"), VALUE, JUDGE);

		return szData;
	};

	CString Get_LogData()
	{
		CString szData;

		if (VALUE.IsEmpty())
		{
			szData.Format(_T("%s"), JUDGE);
		}
		else{
			szData.Format(_T("%s,%s"), VALUE, JUDGE);
		}

		return szData;
	};

	CString Get_Name()
	{
		CString szData;

		// ex) 0.0:1, 1.1:0, example:1, 
		szData.Format(_T("%s"), NAME);

		return szData;
	};

}ST_MES_ResultItem, *PST_MES_ResultItem;

//=============================================================================
// CFileMES_Base
//=============================================================================
class CFileMES_Base
{
public:
	CFileMES_Base(){};
	~CFileMES_Base(){};

protected:

	CString		szPath;
	CString		szEquipmentID;
	CString		szBarcode;
	UINT		nLotTryCnt;
	BOOL		bJudgment;
	SYSTEMTIME	tmStart;

	CArray<ST_MES_ResultItem, ST_MES_ResultItem&> ItemList;

	//=============================================================================
	// Method		: Make_Dataz
	// Access		: virtual protected  
	// Returns		: CString
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	virtual CString Make_Dataz()
	{
		INT_PTR iCount = ItemList.GetCount();
		CString szDataz;



		for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
		{
			szDataz += ",";
			szDataz += ItemList.GetAt(nIdx).Get_MESData();
		}

		return szDataz;
	};

	virtual CString Make_NameHeader()
	{
		INT_PTR iCount = ItemList.GetCount();
		CString szDataz;

// 		for (INT_PTR nIdx = 0; nIdx < 3; nIdx++)
// 		{
// 			szDataz += ItemList.GetAt(nIdx).Get_Name();
// 			szDataz += ",";
// 		}

		for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
		{
			szDataz += ",";
			szDataz += ItemList.GetAt(nIdx).Get_Name();
		}

		return szDataz;
	};

	//=============================================================================
	// Method		: Make_Header
	// Access		: virtual protected  
	// Returns		: CString
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	virtual CString Make_Header()
	{
		CString szHeader;
		CString szItem;

		// 제품 바코드 정보
		szHeader += szBarcode + _T(",");

		// Try 회수
		szItem.Format(_T("%d,"), nLotTryCnt);
		szHeader += szItem;

		// PASS : 1, FAIL = 0
		if (TRUE == bJudgment)
		{
			szHeader += _T("1");
		}
		else
		{
			szHeader += _T("0");
		}

		return szHeader;
	};
	

	//=============================================================================
	// Method		: Get_MESFileData
	// Access		: virtual protected  
	// Returns		: CString
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	virtual CString Get_MESFileData()
	{
		CString szMESData;
		szMESData = Make_Header() + Make_Dataz() + _T("\r\n");

		return szMESData;
	};

	//=============================================================================
	// Method		: Get_MESFileData
	// Access		: virtual protected  
	// Returns		: BOOL
	// Parameter	: __out LPBYTE lpbyOutDATA
	// Parameter	: __out DWORD & dwOutDataSize
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	virtual BOOL Get_MESFileData(__out LPBYTE lpbyOutDATA, __out DWORD& dwOutDataSize)
	{
		USES_CONVERSION;
		CStringA szMESData = CT2A(Get_MESFileData().GetBuffer(0));

		dwOutDataSize = szMESData.GetLength();
		if (NULL == lpbyOutDATA)
		{
			lpbyOutDATA = new BYTE[dwOutDataSize];
		}

		memcpy(lpbyOutDATA, szMESData.GetBuffer(0), dwOutDataSize);

		return TRUE;
	};

	//=============================================================================
	// Method		: SaveMESFile_Unicode
	// Access		: virtual public  
	// Returns		: BOOL
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:59
	// Desc.		: 유니코드로 저장
	//=============================================================================
	virtual BOOL SaveMESFile_Unicode()
	{
		CString strPath;
		CString szFileName;
		szFileName.Format(_T("%s_%d_%04d%02d%02d%02d%02d%02d.txt"), szEquipmentID, nLotTryCnt,
																	tmStart.wYear, tmStart.wMonth, tmStart.wDay,
																	tmStart.wHour, tmStart.wMinute, tmStart.wSecond);

		CString strTempPath;
		CreateDirectory(_T("C:\\Temp\\"), NULL);

		strTempPath.Format(_T("C:\\Temp\\%s"), szFileName);
		strPath.Format(_T("%s\\%s"), szPath, szFileName);

		CString strMES_Raw = Get_MESFileData();

		CFile	File;
		CFileException e;

		::DeleteFile(strTempPath);
		if (!File.Open(strTempPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		CArchive arStore(&File, CArchive::store);

		arStore.WriteString(strMES_Raw.GetBuffer(0));

		arStore.Close();
		File.Close();

		// 기본 Temp 폴더에 저장하고 BMS_MES 폴더로 이동
		::DeleteFile(strPath);
		if (FALSE == ::MoveFile(strTempPath, strPath))
		{
			TRACE(_T("MES File Move Failed\n"));
			return FALSE;
		}

		return TRUE;
	};

	//=============================================================================
	// Method		: SaveMESFile_ANSI
	// Access		: virtual protected  
	// Returns		: BOOL
	// Qualifier	:
	// Last Update	: 2018/3/1 - 17:01
	// Desc.		: ANSI로 저장
	//=============================================================================
	virtual BOOL SaveMESFile_ANSI()
	{
		CString strPath;
		CString szFileName;
		szFileName.Format(_T("%s_%d_%04d%02d%02d%02d%02d%02d.txt"), szEquipmentID, nLotTryCnt,
			tmStart.wYear, tmStart.wMonth, tmStart.wDay,
			tmStart.wHour, tmStart.wMinute, tmStart.wSecond);


// 		szFileName.Format(_T("%s_%s_%d_%04d%02d%02d%02d%02d%02d.txt"), szEquipmentID, szBarcode, nLotTryCnt,
// 																	tmStart.wYear, tmStart.wMonth, tmStart.wDay,
// 																	tmStart.wHour, tmStart.wMinute, tmStart.wSecond);

		CString strTempPath;
		CreateDirectory(_T("c:\\Temp\\"), NULL);

		strTempPath.Format(_T("c:\\Temp\\%s"), szFileName);
		strPath.Format(_T("%s\\%s"), szPath, szFileName);

		CString strMES_Raw = Get_MESFileData();
		USES_CONVERSION;
		CStringA szWriteFile = T2A(strMES_Raw.GetBuffer(0));

		CFile	File;
		CFileException e;

		::DeleteFile(strTempPath);
		if (!File.Open(strTempPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		File.Write(szWriteFile.GetBuffer(0), szWriteFile.GetLength());
		File.Close();
		//CArchive arStore(&File, CArchive::store);
		//arStore.Write(szWriteFile.GetBuffer(0), szWriteFile.GetLength());
		//arStore.Close();

		// 기본 Temp 폴더에 저장하고 BMS_MES 폴더로 이동
		::DeleteFile(strPath);
		if (FALSE == ::MoveFile(strTempPath, strPath))
		{
			TRACE(_T("MES File Move Failed\n"));
			return FALSE;
		}

		return TRUE;
	};
	virtual BOOL SaveLogFile_ANSI(__in LPCTSTR szLogPath)
	{
		Temp_SaveLogFile_ANSI(szLogPath);

		CString strPath;
		CString szFileName;
		szFileName.Format(_T("%s_%s_%d_%04d%02d%02d%02d%02d%02d_LOG.txt"), szEquipmentID, szBarcode, nLotTryCnt,
			tmStart.wYear, tmStart.wMonth, tmStart.wDay,
			tmStart.wHour, tmStart.wMinute, tmStart.wSecond);

		CString strTempPath;
		CreateDirectory(szLogPath, NULL);

		strTempPath.Format(_T("c:\\Temp\\%s"), szFileName);
		strPath.Format(_T("%s\\%s"), szLogPath, szFileName);

		CString strMES_Raw = Get_LogFileData(TRUE);
		USES_CONVERSION;
		CStringA szWriteFile = T2A(strMES_Raw.GetBuffer(0));

		CFile	File;
		CFileException e;

		::DeleteFile(strTempPath);
		if (!File.Open(strTempPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		File.Write(szWriteFile.GetBuffer(0), szWriteFile.GetLength());
		File.Close();
		//CArchive arStore(&File, CArchive::store);
		//arStore.Write(szWriteFile.GetBuffer(0), szWriteFile.GetLength());
		//arStore.Close();

		// 기본 Temp 폴더에 저장하고 BMS_MES 폴더로 이동
		::DeleteFile(strPath);
		if (FALSE == ::MoveFile(strTempPath, strPath))
		{
			TRACE(_T("Log File Move Failed\n"));
			return FALSE;
		}

		return TRUE;
	};

	// 누적
	virtual BOOL Temp_SaveLogFile_ANSI(__in LPCTSTR szLogPath)
	{

		CString strPath;
		CString szFileName;
		szFileName.Format(_T("Total_Data_%04d%02d%02d.csv"), tmStart.wYear, tmStart.wMonth, tmStart.wDay);

		
		strPath.Format(_T("%s\\%s"), szLogPath, szFileName);

		CString strMES_Raw;
		USES_CONVERSION;
		CStringA szWriteFile;
// 
		CFile	File;
		CFileException e;


		if (!PathFileExists(strPath))
		{
			if (!File.Open(strPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
			{
				return FALSE;
			}
			strMES_Raw = Get_LogFileData(TRUE);
		}
		else
		{
			if (!File.Open(strPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
			{
				return FALSE;
			}

			strMES_Raw = Get_LogFileData(FALSE);

		}

		szWriteFile = T2A(strMES_Raw.GetBuffer(0));

		File.SeekToEnd();
		//File.Write(szBuff.GetBuffer(0), szBuff.GetLength() * sizeof(TCHAR));
		File.Write(szWriteFile.GetBuffer(0), szWriteFile.GetLength());

		File.Flush();
		File.Close();
		return TRUE;
	};




public:

	//=============================================================================
	// Method		: Reset
	// Access		: public  
	// Returns		: void
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	void Reset()
	{
		ItemList.RemoveAll();
	};

	//=============================================================================
	// Method		: Reset_ItemData
	// Access		: virtual public  
	// Returns		: void
	// Qualifier	:
	// Last Update	: 2018/3/1 - 17:06
	// Desc.		:
	//=============================================================================
	virtual void Reset_ItemData()
	{
		for (INT_PTR nIdx = 0; nIdx < ItemList.GetCount(); nIdx++)
		{
			ItemList[nIdx].Reset_ItemData();
		}
	};

	//=============================================================================
	// Method		: Set_Path
	// Access		: public  
	// Returns		: void
	// Parameter	: __in LPCTSTR szInPath
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	void Set_Path(__in LPCTSTR szInPath)
	{
		szPath = szInPath;
	};

	//=============================================================================
	// Method		: Set_EquipmnetID
	// Access		: public  
	// Returns		: void
	// Parameter	: __in LPCTSTR szInEqpID
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	void Set_EquipmnetID(__in LPCTSTR szInEqpID)
	{
		szEquipmentID = szInEqpID;
	};

	//=============================================================================
	// Method		: Set_Barcode
	// Access		: public  
	// Returns		: void
	// Parameter	: __in LPCTSTR szInBarcode
	// Parameter	: __in UINT nInRetryCnt
	// Parameter	: __in BOOL bInJudgment
	// Parameter	: __in const SYSTEMTIME * pstTime
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:36
	// Desc.		:
	//=============================================================================
	void Set_Barcode(__in LPCTSTR szInBarcode, __in UINT nInRetryCnt, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime)
	{
		szBarcode = szInBarcode;
		nLotTryCnt = nInRetryCnt;
		bJudgment = bInJudgment;

		if (NULL != pstTime)
		{
			memcpy(&tmStart, pstTime, sizeof(SYSTEMTIME));
		}
	};

	//=============================================================================
	// Method		: Set_Barcode
	// Access		: public  
	// Returns		: void
	// Parameter	: __in LPCTSTR szInBarcode
	// Parameter	: __in BOOL bInJudgment
	// Parameter	: __in const SYSTEMTIME * pstTime
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:37
	// Desc.		:
	//=============================================================================
	void Set_Barcode(__in LPCTSTR szInBarcode, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime)
	{
		szBarcode = szInBarcode;
		bJudgment = bInJudgment;

		if (NULL != pstTime)
		{
			memcpy(&tmStart, pstTime, sizeof(SYSTEMTIME));
		}
	};

	//=============================================================================
	// Method		: Add_ItemData
	// Access		: virtual public  
	// Returns		: BOOL
	// Parameter	: __in LPCTSTR szName
	// Parameter	: __in LPCTSTR szValue
	// Parameter	: __in BOOL bPass
	// Qualifier	:
	// Last Update	: 2018/3/1 - 16:37
	// Desc.		:
	//=============================================================================
	virtual BOOL Add_ItemData(__in LPCTSTR szName, __in LPCTSTR szValue, __in BOOL bPass = TRUE)
	{
		ST_MES_ResultItem stItem;

		stItem.nReultItem_Index = (UINT)ItemList.GetCount();
		stItem.Set_Data(szName, szValue, bPass);

		ItemList.Add(stItem);

		return TRUE;
	};

	//=============================================================================
	// Method		: SaveMESFile
	// Access		: virtual public  
	// Returns		: BOOL
	// Parameter	: __in LPCTSTR szInBarcode
	// Parameter	: __in UINT nInRetryCnt
	// Parameter	: __in BOOL bInJudgment
	// Parameter	: __in const SYSTEMTIME * pstTime
	// Qualifier	:
	// Last Update	: 2018/3/1 - 17:01
	// Desc.		:
	//=============================================================================
	virtual BOOL SaveMESFile(__in LPCTSTR szInBarcode, __in UINT nInRetryCnt, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime)
	{
		Set_Barcode(szInBarcode, nInRetryCnt, bInJudgment, pstTime);

		return SaveMESFile_ANSI();
		//return SaveMESFile_Unicode();
	};

	
// 	virtual BOOL Add_ItemFullData(__in LPCTSTR szName, __in LPCTSTR szFullData, int DataCount)
// 	{
// 		ST_MES_ResultItem stItem;
// 
// 		stItem.nReultItem_Index = (UINT)ItemList.GetCount();
// 	//	stItem.Set_Data(szName, szValue, bPass);
// 
// 		/*stItem.sz*/
// 		ItemList.Add(stItem);
// 
// 		return TRUE;
// 	};

	virtual CString Get_LogFileData(BOOL bHeaderMode = FALSE)
	{
		CString szMESData;
		if (bHeaderMode == FALSE)
		{
			szMESData =  Make_Header() + Make_LogDataz() + _T("\r\n");
		}else{

			szMESData = Make_NameLogHeader() + Make_Header() + Make_LogDataz() + _T("\r\n");
		}

		return szMESData;
	};

	virtual CString Make_LogDataz()
	{
		INT_PTR iCount = ItemList.GetCount();
		CString szDataz;



		for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
		{
			szDataz += ",";
			szDataz += ItemList.GetAt(nIdx).Get_LogData();
		}

		return szDataz;
	};

	virtual CString Make_NameLogHeader()
	{
		INT_PTR iCount = ItemList.GetCount();
		CString szDataz;

		szDataz += _T("Barcode,");
		szDataz += _T("차수,");
		szDataz += _T("Result");

		for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
		{
			if (!ItemList.GetAt(nIdx).Get_Name().IsEmpty())
			{
				szDataz += _T(",");
				szDataz += ItemList.GetAt(nIdx).Get_Name();
				if (ItemList.GetAt(nIdx).Get_Name() != _T("Reserved"))
				{
					szDataz += _T(",");
					szDataz += _T("R");
				}
			}		
		}
		szDataz += _T("\n");

		return szDataz;
	};

	virtual BOOL SaveLOGFile(__in LPCTSTR szLogPath, __in LPCTSTR szInBarcode, __in UINT nInRetryCnt, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime)
	{
		Set_Barcode(szInBarcode, nInRetryCnt, bInJudgment, pstTime);
		//Temp_SaveLogFile_ANSI();

		return SaveLogFile_ANSI(szLogPath);
		//return SaveMESFile_Unicode();
	};

};

#endif // MES_Base_h__
