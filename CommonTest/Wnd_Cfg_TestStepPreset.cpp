//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStepPreset.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestStepPreset.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestStepPreset.h"

// CWnd_Cfg_TestStepPreset

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestStepPreset, CWnd_BaseView)

CWnd_Cfg_TestStepPreset::CWnd_Cfg_TestStepPreset()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_TestStepPreset::~CWnd_Cfg_TestStepPreset()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestStepPreset, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestStepPreset::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Presets.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	// Preset 정보
	m_wnd_CfgPreset.SetOwner(GetOwner());
	m_wnd_CfgPreset.Create(NULL, _T("Preset"), dwStyle /*| WS_HSCROLL*/, rectDummy, &m_tc_Presets, nID_Index++);

	// 스텝 리스트
	for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		m_wnd_CfgPresetStep[nIdx].SetOwner(GetOwner());
		m_wnd_CfgPresetStep[nIdx].Create(NULL, _T("Preset"), dwStyle, rectDummy, &m_tc_Presets, nID_Index++);
	}

	// 검사기 별로 탭 컨트롤 정의
	UINT nTabIndex = 0;
	m_tc_Presets.AddTab(&m_wnd_CfgPreset, _T("Setting"), nTabIndex++, TRUE);

	for (UINT nIdx = 0; nIdx < m_nUsePresetCount; nIdx++)
	{
		if (m_szPresetName[nIdx].IsEmpty())
		{
			m_szPresetName[nIdx].Format(_T("Preset %d"), nIdx + 1);
		}
		
		m_tc_Presets.AddTab(&m_wnd_CfgPresetStep[nIdx], m_szPresetName[nIdx], nTabIndex++, TRUE);
	}
		
	if (m_tc_Presets.GetTabsNum() > 0)
	{
		m_tc_Presets.SetActiveTab(0);
		m_tc_Presets.EnableTabSwap(FALSE);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	m_tc_Presets.MoveWindow(0, 0, cx, cy);	
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Presets.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

	m_wnd_CfgPreset.SetSystemType(nSysType);
	for (UINT nItemCnt = 0; nItemCnt < MAX_TESTSTEP_PRESET; nItemCnt++)
	{
		m_wnd_CfgPresetStep[nItemCnt].SetSystemType(nSysType);
	}
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIndex
// Parameter	: __in const ST_StepInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2018/3/27 - 14:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Set_StepInfo(__in UINT nIndex, __in const ST_StepInfo* pstRecipeInfo)
{
	if (nIndex < m_nUsePresetCount)
	{
		m_wnd_CfgPresetStep[nIndex].Set_StepInfo(pstRecipeInfo);
	}
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIndex
// Parameter	: __out ST_StepInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2018/3/27 - 14:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Get_StepInfo(__in UINT nIndex, __out ST_StepInfo& stOutRecipInfo)
{
	if (nIndex < m_nUsePresetCount)
	{
		m_wnd_CfgPresetStep[nIndex].Get_StepInfo(stOutRecipInfo);
	}
}

//=============================================================================
// Method		: Set_PresetInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_PresetStepInfo * pstPresetInfo
// Qualifier	:
// Last Update	: 2018/4/5 - 11:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Set_PresetInfo(__in const ST_PresetStepInfo* pstPresetInfo)
{
	m_wnd_CfgPreset.Set_PresetInfo(pstPresetInfo);
	
	for (UINT nIdx = 0; nIdx < pstPresetInfo->nUsePresetFileCount; nIdx++)
	{
		Set_StepInfo(nIdx, &pstPresetInfo->stStepInfo[nIdx]);
	}

	for (UINT nIdx = pstPresetInfo->nUsePresetFileCount; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		//pstPresetInfo->stStepInfo[nIdx].RemoveAll();
	}
}

//=============================================================================
// Method		: Get_PresetInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_PresetStepInfo & stOutPresetInfo
// Qualifier	:
// Last Update	: 2018/4/5 - 11:46
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Get_PresetInfo(__out ST_PresetStepInfo& stOutPresetInfo)
{
	m_wnd_CfgPreset.Get_PresetInfo(stOutPresetInfo);

	for (UINT nIdx = 0; nIdx < stOutPresetInfo.nUsePresetFileCount; nIdx++)
	{
		Get_StepInfo(nIdx, stOutPresetInfo.stStepInfo[nIdx]);
	}

	for (UINT nIdx = stOutPresetInfo.nUsePresetFileCount; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		stOutPresetInfo.stStepInfo[nIdx].RemoveAll();
	}
}

//=============================================================================
// Method		: Set_UsePresetCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUseCount
// Qualifier	:
// Last Update	: 2018/4/5 - 16:11
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Set_UsePresetCount(__in UINT nUseCount)
{
	m_nUsePresetCount = (nUseCount < MAX_TESTSTEP_PRESET) ? nUseCount : MAX_TESTSTEP_PRESET;

	m_wnd_CfgPreset.Set_UsePresetCount(m_nUsePresetCount);
}

//=============================================================================
// Method		: Set_UsePreset
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUseCount
// Parameter	: __in CStringArray * szNames
// Qualifier	:
// Last Update	: 2018/3/27 - 11:28
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStepPreset::Set_UsePreset(__in UINT nUseCount, __in CStringArray* szNames)
{
	Set_UsePresetCount(nUseCount);

	if (NULL != szNames)
	{
		UINT nNameCount =  (m_nUsePresetCount < (UINT)szNames->GetCount()) ? m_nUsePresetCount : (UINT)szNames->GetCount();

		for (UINT nIdx = 0; nIdx < nNameCount; nIdx++)
		{
			m_szPresetName[nIdx] = szNames->GetAt(nIdx);
		}

		if (GetSafeHwnd())
		{
			// Tab Name 변경
			for (UINT nIdx = 0; nIdx < nNameCount; nIdx++)
			{
				m_tc_Presets.SetTabLabel(nIdx, m_szPresetName[nIdx]);
			}
		}
	}
}

//=============================================================================
// Method		: Get_PresetCount
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2018/3/27 - 13:33
// Desc.		:
//=============================================================================
UINT CWnd_Cfg_TestStepPreset::Get_PresetCount()
{
	return m_nUsePresetCount;
}
