//*****************************************************************************
// Filename	: 	Def_T_Foc.h
// Created	:	2018/3/4 - 10:29
// Modified	:	2018/3/4 - 10:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_Foc_h__
#define Def_T_Foc_h__

#include <afxwin.h>
#include "Def_UI_SFR.h"
#include "Def_UI_Defect_Black.h"
#include "Def_UI_Defect_White.h"
#include "Def_UI_OpticalCenter.h"
#include "Def_UI_Rotation.h"
#include "Def_UI_Ymean.h"
#include "Def_UI_LCB.h"
#include "Def_UI_BlackSpot.h"
#include "Def_UI_Current.h"
#include "Def_UI_Torque.h"
#include "Def_UI_ActiveAlign.h"

#pragma pack(push,1)

typedef enum enSpec_Foc
{
	
	Spec_Foc_Item_01,
	Spec_Foc_Item_02,
	Spec_Foc_Item_03,
	Spec_Foc_Item_04,
	Spec_Foc_Item_05,
	Spec_Foc_Item_06,
	Spec_Foc_Item_07,
	Spec_Foc_Item_08,
	Spec_Foc_Item_09,
	Spec_Foc_Item_10,
	Spec_Foc_Item_11,
	Spec_Foc_Item_12,
	Spec_Foc_MaxNum
};

static LPCTSTR	g_szSpecFoc[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};
typedef struct _tag_OpticalCenter_Data_Foc
{
	// 결과
	UINT nResult;
	UINT nDetectResult;

	// 양불
	UINT nResultX; //BOOL 형식으로 변경
	UINT nResultY;

	// 결과 값
	UINT nResultPosX; //Posx 결과 값
	UINT nResultPosY;

	UINT nPixX;
	UINT nPixY;

	int iStandPosDevX; //Offset 결과 값
	int iStandPosDevY;


	void Reset()
	{
		nResult = 1;
		nDetectResult = 0;
		nResultX = 1;
		nResultY = 1;
		nResultPosX = 0;
		nResultPosY = 0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	void FailData(){
		nResult = 0;
		nDetectResult = 0;
		nResultX = 0;
		nResultY = 0;
		nResultPosX = 0;
		nResultPosY = 0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	_tag_OpticalCenter_Data_Foc()
	{
		Reset();
	};

	_tag_OpticalCenter_Data_Foc& operator= (_tag_OpticalCenter_Data_Foc& ref)
	{
		nResult = ref.nResult;
		nResultX = ref.nResultX;
		nResultY = ref.nResultY;
		nResultPosX = ref.nResultPosX;
		nResultPosY = ref.nResultPosY;
		iStandPosDevX = ref.iStandPosDevX;
		iStandPosDevY = ref.iStandPosDevY;
		nDetectResult = ref.nDetectResult;
		nPixX = ref.nPixX;
		nPixY = ref.nPixY;

		return *this;
	};

}ST_OpticalCenter_Data_Foc, *PST_OpticalCenter_Data_Foc;

typedef struct _tag_Result_Rotation_Foc
{
	CRect		ptROI[ROI_Rotation_Max];		// 보정된 ROI

	double		dbRotation;
	BOOL		bRotation;

	_tag_Result_Rotation_Foc()
	{
		Reset();
	};

	void Reset()
	{
		dbRotation = 0.0;

		bRotation = TRUE;

		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI].left = 0;
			ptROI[nROI].right = 0;
			ptROI[nROI].top = 0;
			ptROI[nROI].bottom = 0;

		}
	};

	_tag_Result_Rotation_Foc& operator= (_tag_Result_Rotation_Foc& ref)
	{
		dbRotation = ref.dbRotation;

		bRotation = ref.bRotation;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];

			
		}

		return *this;
	};

}ST_Result_Rotation_Foc, *PST_Result_Rotation_Foc;

typedef struct _tag_Result_Ymean_Foc
{
	bool bYmeanResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[YMEAN_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_Ymean_Foc()
	{
		Reset();
	};

	void Reset()
	{
		bYmeanResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_Ymean_Foc& operator= (_tag_Result_Ymean_Foc& ref)
	{
		bYmeanResult = ref.bYmeanResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_Ymean_Foc, *PST_Result_Ymean_Foc;


typedef struct _tag_Result_LCB_Foc
{
	bool bLCBResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[LCB_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_LCB_Foc()
	{
		Reset();
	};

	void Reset()
	{
		bLCBResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_LCB_Foc& operator= (_tag_Result_LCB_Foc& ref)
	{
		bLCBResult = ref.bLCBResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_LCB_Foc, *PST_Result_LCB_Foc;


typedef struct _tag_Result_BlackSpot_Foc
{
	bool bBlackSpotResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[BlackSpot_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_BlackSpot_Foc()
	{
		Reset();
	};

	void Reset()
	{
		bBlackSpotResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_BlackSpot_Foc& operator= (_tag_Result_BlackSpot_Foc& ref)
	{
		bBlackSpotResult = ref.bBlackSpotResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_BlackSpot_Foc, *PST_Result_BlackSpot_Foc;



typedef struct _tag_Result_SFR_FOC
{
	CRect		rtROI[ROI_SFR_Max];		// 보정된 ROI

	BOOL		bResult[ROI_SFR_Max];	// 개별 결과
	double		dbValue[ROI_SFR_Max];	// 개별 결과 값

	_tag_Result_SFR_FOC()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI].SetRectEmpty();

			bResult[nROI] = TRUE;
			dbValue[nROI] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (FALSE == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_SFR_FOC& operator= (_tag_Result_SFR_FOC& ref)
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];

			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
		}

		return *this;
	};

}ST_Result_SFR_FOC, *PST_Result_SFR_FOC;

typedef struct _tag_Result_Defect_Black_FOC
{
	bool bDefect_BlackResult; // 판정 결과

	int nDefect_BlackCount; //
	CPoint	ptROI[Defect_Black_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_Black_FOC()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_BlackResult = true;
		nDefect_BlackCount=0; //


		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_Black_FOC& operator= (_tag_Result_Defect_Black_FOC& ref)
	{
		bDefect_BlackResult = ref.bDefect_BlackResult;
		nDefect_BlackCount = ref.nDefect_BlackCount;

	
		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_Black_FOC, *PST_Result_Defect_Black_FOC;


typedef struct _tag_Result_Defect_White_FOC
{
	bool bDefect_WhiteResult; // 판정 결과

	int nDefect_WhiteCount; //
	CPoint	ptROI[Defect_White_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_White_FOC()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_WhiteResult = true;
		nDefect_WhiteCount = 0; //


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_White_FOC& operator= (_tag_Result_Defect_White_FOC& ref)
	{
		bDefect_WhiteResult = ref.bDefect_WhiteResult;
		nDefect_WhiteCount = ref.nDefect_WhiteCount;


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_White_FOC, *PST_Result_Defect_White_FOC;


typedef struct _tag_Result_Torque_Foc
{
	UINT nResult;
	UINT nEachResult[Spec_Tor_Max];

	double dbValue[Spec_Tor_Max];

	enTorque_Status enStatus[Spec_Tor_Max];

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			nEachResult[nIdx] = 1;
			dbValue[nIdx] = 0.0;
			enStatus[nIdx] = Torque_Init;
		}
	};

	_tag_Result_Torque_Foc()
	{
		Reset();
	};

	_tag_Result_Torque_Foc& operator= (_tag_Result_Torque_Foc& ref)
	{
		nResult = ref.nResult;

		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			dbValue[nIdx] = ref.dbValue[nIdx];
			enStatus[nIdx] = ref.enStatus[nIdx];
		}

		return *this;
	};

}ST_Result_Torque_Foc, *PST_Result_Torquea_Foc;


typedef struct _tag_Result_ActiveAlign_Foc
{
	UINT nResult;
	UINT nResultX;
	UINT nResultY;
	UINT nResultR;

	int iOC_X;
	int iOC_Y;
	double dbRoatae;

	void Reset()
	{
		nResult = 1;
		nResultX = 1;
		nResultY = 1;
		nResultR = 1;

		iOC_X = 0;
		iOC_Y = 0;
		dbRoatae = 0.0;
	};

	_tag_Result_ActiveAlign_Foc()
	{
		Reset();
	};

	_tag_Result_ActiveAlign_Foc& operator= (_tag_Result_ActiveAlign_Foc& ref)
	{
		nResult = ref.nResult;

		nResultX = ref.nResultX;
		nResultY = ref.nResultY;
		nResultR = ref.nResultR;

		iOC_X = ref.iOC_X;
		iOC_Y = ref.iOC_Y;
		dbRoatae = ref.dbRoatae;

		return *this;
	};

}ST_Result_ActiveAlign_Foc, *PST_Result_ActiveAlign_Foc;


typedef struct _tag_Result_Current_Foc
{
	BOOL		bResult[Current_Max_Front];	// 개별 결과
	double		dbValue[Current_Max_Front];	// 개별 결과 값
	UINT		nEachResult[Current_Max_Front];	// 개별 결과

	_tag_Result_Current_Foc()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < Current_Max_Front; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
			nEachResult[nCH] = 0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < Current_Max_Front; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Current_Foc& operator= (_tag_Result_Current_Foc& ref)
	{
		for (UINT nROI = 0; nROI < 3; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
			nEachResult[nROI] = ref.nEachResult[nROI];
		}
	};


}ST_Result_Current_FOC, *PST_Result_Current_FOC;

//typedef struct _tag_Result_Current_FOC
//{
//	bool		bCurrentResult; // 판정 결과
//	double		dbValue;	// 개별 결과 값
//
//	_tag_Result_Current_FOC()
//	{
//		Reset();
//	};
//
//	void Reset()
//	{
//		bCurrentResult = true;
//		dbValue = 0.0;
//
//	};
//
//	_tag_Result_Current_FOC& operator= (_tag_Result_Current_FOC& ref)
//	{
//		bCurrentResult = ref.bCurrentResult;
//		dbValue = ref.dbValue;
//
//
//		return *this;
//	};
//
//}ST_Result_Current_FOC, *PST_Result_Current_FOC;

typedef struct _tag_Foc_Info
{
	ST_UI_Current			stCurrent;
	ST_UI_SFR				stSFR;
	ST_UI_Defect_Black		stDefect_Black;
	ST_UI_Defect_White		stDefect_White;
	ST_OpticalCenter_Opt	stOpticalCenter;
	ST_UI_Rotation			stRotation;
	ST_UI_Ymean				stYmean;
	ST_UI_BlackSpot			stBlackSpot;
	ST_UI_LCB				stLCB;
	ST_UI_Torque			stTorque;
	ST_UI_ActiveAlign		stActiveAlign;

	_tag_Foc_Info()
	{
		Reset();
	};

	_tag_Foc_Info& operator= (const _tag_Foc_Info& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Info, *PST_Foc_Info;

typedef struct _tag_Foc_Result_FOC
{

	CString szFileName;

	ST_Result_SFR_FOC			stSFR;
	ST_OpticalCenter_Data_Foc	stOpticalCenter;
	ST_Result_Rotation_Foc		stRotate;
	ST_Result_Ymean_Foc			stYmean;
	ST_Result_LCB_Foc			stLCB;
	ST_Result_BlackSpot_Foc		stBlackSpot;
	ST_Result_Current_FOC		stCurrent;
	ST_Result_Defect_Black_FOC	stDefect_Black;
	ST_Result_Defect_White_FOC	stDefect_White;
	ST_Result_Torque_Foc		stTorque;
	ST_Result_ActiveAlign_Foc	stActiveAlign;

	_tag_Foc_Result_FOC()
	{

		szFileName.Empty();

		Reset();
	};

	_tag_Foc_Result_FOC& operator= (const _tag_Foc_Result_FOC& ref)
	{
		szFileName = ref.szFileName;

		return *this;
	};

	void Reset()
	{
		stSFR.Reset();
	};

}ST_Foc_Result, *PST_Foc_Result;

typedef struct _tag_Foc_Spec
{
	_tag_Foc_Spec()
	{
		Reset();
	};

	_tag_Foc_Spec& operator= (const _tag_Foc_Spec& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Spec, *PST_Foc_Spec;



#pragma pack(pop)

#endif // Def_T_Foc_h__
