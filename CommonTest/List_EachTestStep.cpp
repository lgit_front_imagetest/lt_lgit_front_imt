//*****************************************************************************
// Filename	: 	List_EachTestStep.cpp
// Created	:	2017/9/24 - 16:35
// Modified	:	2017/9/24 - 16:35
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_EachTestStep.cpp : implementation file
//

#include "stdafx.h"
#include "List_EachTestStep.h"


// CList_EachTestStep

typedef enum enEachTestStepHeader
{
	ETSH_No,
	ETSH_TestItem,
	ETSH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszEachTestHeader[] =
{
	_T("No"),				// TSH_No
	_T("Test"),				// TSH_TestItem
	NULL
};

const int	iListEachTestAglin[] =
{
	LVCFMT_LEFT,	 // TSH_No
	LVCFMT_LEFT,	 // TSH_TestItem
};

// 540 기준
const int	iHeaderEachTestWidth[] =
{
	30, 	// TSH_No
	200,	// TSH_TestItem
};


IMPLEMENT_DYNAMIC(CList_EachTestStep, CListCtrl)

CList_EachTestStep::CList_EachTestStep()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
// 	VERIFY(m_Font.CreateFont(
// 		16,						// nHeight
// 		0,						// nWidth
// 		0,						// nEscapement
// 		0,						// nOrientation
// 		FW_BOLD,				// nWeight
// 		FALSE,					// bItalic
// 		FALSE,					// bUnderline
// 		0,						// cStrikeOut
// 		ANSI_CHARSET,			// nCharSet
// 		OUT_DEFAULT_PRECIS,		// nOutPrecision
// 		CLIP_DEFAULT_PRECIS,	// nClipPrecision
// 		ANTIALIASED_QUALITY,	// nQuality
// 		FIXED_PITCH,			// nPitchAndFamily
// 		_T("CONSOLAS")));		// lpszFacename

	m_pHeadWidth = iHeaderEachTestWidth;
	m_nSelectNum = 0;
}

CList_EachTestStep::~CList_EachTestStep()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_EachTestStep, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK,	&CList_EachTestStep::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()



// CList_EachTestStep message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_EachTestStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_EachTestStep::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	/*int iColWidth[TSH_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;
	int iMaxCol = TSH_MaxCol;

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iColDivide += iHeaderWidth[nCol];
	}

	//CRect rectClient;
	//GetClientRect(rectClient);

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iUnitWidth = (cx * m_pHeadWidth[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((cx * m_pHeadWidth[TSH_TestItem]) / iColDivide) + (cx - iMisc);
	SetColumnWidth(TSH_TestItem, iUnitWidth);*/
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_EachTestStep::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_EachTestStep::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	m_nSelectNum = index;

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_EachTestStep::InitHeader()
{
	if (FALSE == m_bIntiHeader)
	{
		m_bIntiHeader = TRUE;

		int iMaxCol = ETSH_MaxCol;

		for (int nCol = 0; nCol < iMaxCol; nCol++)
		{
			InsertColumn(nCol, g_lpszEachTestHeader[nCol], iListEachTestAglin[nCol], iHeaderEachTestWidth[nCol]);
		}
	}

	for (int nCol = 0; nCol < ETSH_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, m_pHeadWidth[nCol]);
	}
}

//=============================================================================
// Method		: ResetOrderingNumbers
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 16:05
// Desc.		:
//=============================================================================
void CList_EachTestStep::ResetOrderingNumbers()
{
	CString szText;

	for (int nIdx = 0; nIdx < GetItemCount(); nIdx++)
	{
		// TSH_No
		szText.Format(_T("%d"), nIdx + 1);
		SetItemText(nIdx, ETSH_No, szText);
	}
}

//=============================================================================
// Method		: SetSelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Qualifier	:
// Last Update	: 2017/9/25 - 18:28
// Desc.		:
//=============================================================================
void CList_EachTestStep::SetSelectItem(__in int nItem)
{
	if (nItem < GetItemCount())
	{
		SetHotItem(nItem);
		SetItemState(nItem, LVIS_FOCUSED, LVIS_FOCUSED);
		SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		SetFocus();
	}
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 19:57
// Desc.		:
//=============================================================================
void CList_EachTestStep::InsertTestStep(__in int nItem, __in const ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	if (GetItemCount() <= nItem)
		return;

	int iNewCount = nItem;

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, ETSH_No, szText);

	// TSH_TestItem
	if (pTestStep->bTest)
	{
		SetItemText(iNewCount, ETSH_TestItem, GetTestItemName(m_InspectionType, pTestStep->nTestItem));
	}
	else
	{
		SetItemText(iNewCount, ETSH_TestItem, _T("No Test"));
	}


	// 번호 재부여
	ResetOrderingNumbers();

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 14:22
// Desc.		:
//=============================================================================
void CList_EachTestStep::AddTestStep(const __in ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_No
	szText.Format(_T("%d"), GetItemCount());
	SetItemText(iNewCount, ETSH_No, szText);

	// TSH_TestItem
	if (pTestStep->bTest)
	{
		SetItemText(iNewCount, ETSH_TestItem, GetTestItemName(m_InspectionType, pTestStep->nTestItem));
	}
	else
	{
		SetItemText(iNewCount, ETSH_TestItem, _T("No Test"));
	}
	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/25 - 11:52
// Desc.		:
//=============================================================================
void CList_EachTestStep::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_pHeadWidth = iHeaderEachTestWidth;
// 	switch (m_InspectionType)
// 	{
// 	case Sys_Focusing:
// 		m_pHeadWidth = iHeaderWidth_Foc;
// 		break;
// 
// 	case Sys_Image_Test:
// 		m_pHeadWidth = iHeaderWidth_ImgT;
// 		break;
// 
// 	case Sys_Stereo_Cal:
// 		m_pHeadWidth = iHeaderWidth_Ste;
// 		break;
// 
// 	case Sys_2D_Cal:
// 		m_pHeadWidth = iHeaderWidth_2D;
// 		break;
// 
// 	case Sys_3D_Cal:
// 		m_pHeadWidth = iHeaderWidth_3D;
// 		break;
// 
// 	case Sys_MovableTestSys:
// 		m_pHeadWidth = iHeaderWidth_Mov;
// 		break;
// 
// 	default:
// 		m_pHeadWidth = iHeaderWidth;
// 		break;
// 	}
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:35
// Desc.		:
//=============================================================================
void CList_EachTestStep::Set_StepInfo(__in const ST_StepInfo* pstInStepInfo)
{
	m_stStepInfo.StepList.RemoveAll();
	DeleteAllItems();

	if (NULL != pstInStepInfo)
	{
		m_stStepInfo.StepList.Copy(pstInStepInfo->StepList);

		for (UINT nIdx = 0; nIdx < pstInStepInfo->StepList.GetCount(); nIdx++)
		{
			AddTestStep(&(pstInStepInfo->StepList[nIdx]));
		}
	}
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 23:27
// Desc.		:
//=============================================================================
void CList_EachTestStep::Get_StepInfo(__out ST_StepInfo& stOutStepInfo)
{
	stOutStepInfo.StepList.RemoveAll();
	stOutStepInfo.StepList.Copy(m_stStepInfo.StepList);
}

//=============================================================================
// Method		: Item_Add
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_EachTestStep::Item_Add(__in ST_StepUnit& stTestStep)
{
	POSITION posSel = GetFirstSelectedItemPosition();

	// 데이터 정상인가 확인
	if (m_stStepInfo.StepList.GetCount() != GetItemCount())
	{
		// 에러
		AfxMessageBox(_T("CList_EachTestStep::Item_Add() Data Count Error"));
		return;
	}

	if (MAX_STEP_COUNT < m_stStepInfo.StepList.GetCount())
	{
		// 에러
		AfxMessageBox(_T("Limit Max Step"));
		return;
	}

	m_stStepInfo.StepList.Add(stTestStep);
	AddTestStep(&stTestStep);
}

//=============================================================================
// Method		: Item_Insert
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_EachTestStep::Item_Insert(__in ST_StepUnit& stTestStep)
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		m_stStepInfo.StepList.InsertAt(iIndex, stTestStep);

		InsertTestStep(iIndex, &stTestStep);
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_EachTestStep::Item_Remove()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		DeleteItem(iIndex);
		ResetOrderingNumbers();

		m_stStepInfo.StepList.RemoveAt(iIndex);

		// 아이템 선택 활성화
		if (iIndex < GetItemCount())
		{
			SetSelectItem(iIndex);
		}
		else
		{
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_EachTestStep::Item_Up()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 0번 인덱스는 위로 이동 불가
		if ((0 < iIndex) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			InsertTestStep(iIndex - 1, &stStep);

			m_stStepInfo.StepList.RemoveAt(iIndex);
			m_stStepInfo.StepList.InsertAt(iIndex - 1, stStep);

			// 아이템 선택 활성화
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_EachTestStep::Item_Down()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 마지막 인덱스는 아래로 이동 불가
		if ((iIndex < (GetItemCount() - 1)) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			m_stStepInfo.StepList.RemoveAt(iIndex);

			// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
			if ((iIndex + 1) < (GetItemCount()))
			{
				InsertTestStep(iIndex + 1, &stStep);
				m_stStepInfo.StepList.InsertAt(iIndex + 1, stStep);
			}
			else
			{
				AddTestStep(&stStep);
				m_stStepInfo.StepList.Add(stStep);
			}

			// 아이템 선택 활성화
			SetSelectItem(iIndex + 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}