//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CommonTest.rc
//
#define IDD_DLG_BARCODE                 101
#define IDD_DLG_LOG                     107
#define IDD_DLG_MES_ONLINE              109
#define IDD_DLG_ACESS_MODE              199
#define IDB_BITMAP_CHECK                311
#define IDB_BITMAP_CHECKNO              312
#define IDI_ICON_LOGO                   317
#define IDI_ICON_Luritech               318
#define IDI_ICON_CONNECT                320
#define IDD_DLG_CHK_PASSWORD            323
#define IDD_DLG_MODULE_PROGRESS         324
#define IDB_SELECT                      339
#define IDB_SELECT_NO                   340
#define IDB_CHECKED                     341
#define IDB_UNCHECKED                   342
#define IDB_SELECT_16                   343
#define IDB_SELECTNO_16                 344
#define IDD_DLG_CHANGE_PASSWORD         366
#define IDB_CHECKED_16                  368
#define IDB_UNCHECKED_16                369
#define IDI_ICON_DEVICE                 374
#define IDI_ICON_TSP                    375
#define IDB_TEST_START                  378
#define IDB_TEST_START_R                380
#define IDB_LOAD_FILE                   382
#define IDB_LOAD_FILE_G                 383
#define IDB_START                       384
#define IDB_STOP                        385
#define IDB_BITMAP_Luritech             387
#define IDB_ARROW_UP                    388
#define IDB_ARROW_RIGHT                 389
#define IDB_ARROW_LEFT                  390
#define IDB_ARROW_DOWN                  391
#define IDC_ED_PASSWORD                 1012
#define IDC_LIST_DLG_TRANS_MSG          1013
#define IDC_ST_PASSWORD                 1014
#define IDC_ST_DLG_TRANS_STATE          1015
#define IDC_ED_OLD_PASSWORD             1027
#define IDC_ED_NEW_PASSWORD             1028
#define IDC_ED_CONFIRM_PW               1029
#define IDC_ST_OLD_PASSWORD             1030
#define IDC_ST_NEW_PASSWORD             1031
#define IDC_ST_CONFIRM_PW               1032
#define IDD_DLG_SEL_MODEL               1100
#define IDD_DLG_OPERATE_MODE            1101
#define IDD_DLG_ERR                     1103
#define IDD_DLG_POPUP                   1105

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
