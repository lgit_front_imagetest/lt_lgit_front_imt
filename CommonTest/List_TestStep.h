//*****************************************************************************
// Filename	: 	List_TestStep.h
// Created	:	2017/9/24 - 16:30
// Modified	:	2017/9/24 - 16:30
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef List_TestStep_h__
#define List_TestStep_h__

#pragma once

#include "Def_Enum_Cm.h"
#include "Def_TestItem_Cm.h"

//-----------------------------------------------------------------------------
// CList_TestStep
//-----------------------------------------------------------------------------
class CList_TestStep : public CListCtrl
{
	DECLARE_DYNAMIC(CList_TestStep)

public:
	CList_TestStep();
	virtual ~CList_TestStep();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CFont			m_Font;

	virtual void	InitHeader			();
	void			AdjustColWidth		();

	BOOL				m_bIntiHeader		= FALSE;
	const int*			m_pHeadWidth		= NULL;

	// 검사기 설정
	enInsptrSysType		m_InspectionType	= Sys_2D_Cal;//Sys_Focusing;

	// 스텝 정보 구조체
	ST_StepInfo			m_stStepInfo;

	// 검사기별 테스트 항목 명칭 반환
	//CString		GetTestItemName			(__in UINT nTestItem);

	// 넘버링 다시 설정
	void		ResetOrderingNumbers	();

	// 아이템 선택
	void		SetSelectItem			(__in int nItem);

	// 원하는 위치에 아이템 삽입
	void		InsertTestStep			(__in int nItem, __in const ST_StepUnit* pTestStep);
	// 아이템 추가
	void		AddTestStep				(__in const ST_StepUnit* pTestStep);

public:

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// 새로운 스텝 정보를 리스트에 넣음
	void		Set_StepInfo			(__in const ST_StepInfo* pstInStepInfo);	
	void		Get_StepInfo			(__out ST_StepInfo& stOutStepInfo);

	// 스텝 항목 추가
	void		Item_Add				(__in ST_StepUnit& stTestStep);
	// 스텝 항목 선택된 위치에 삽입
	void		Item_Insert				(__in ST_StepUnit& stTestStep);
	// 스텝 항목 삭제
	void		Item_Remove				();
	// 스텝 항목 위로 이동
	void		Item_Up					();
	// 스텝 항목 아래로 이동
	void		Item_Down				();
};

#endif // List_TestStep_h__


