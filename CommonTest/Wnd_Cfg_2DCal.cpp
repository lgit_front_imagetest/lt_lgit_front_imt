// Wnd_Cfg_2DCal.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_2DCal.h"

// CWnd_Cfg_2DCal
#define IDC_EDIT_PARAM	2000
#define IDC_EDIT_SPEC	2001
#define IDC_EDIT_ROI	2002

#define IDC_BTN_TEST	3000
#define IDC_BTN_RESULT	3001

#define IDC_LST_ROI		4000
#define	IDC_TB_OPTION	5000

static LPCTSTR	g_Param_bt2DCalROI[] =
{
	_T("Test"),
	_T("Result"),
	NULL		
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_2DCal, CWnd)

CWnd_Cfg_2DCal::CWnd_Cfg_2DCal()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_2DCal::~CWnd_Cfg_2DCal()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_2DCal, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_TEST, IDC_BTN_TEST + PROI_BT_MaxNum, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_2DCal 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
int CWnd_Cfg_2DCal::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_2DCal_MaxNum; nIdx++)
	{
		m_st_Parameter[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Parameter[nIdx].SetFont_Gdip(L"Arial", 9.0F, FontStyleRegular);
		m_st_Parameter[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Parameter[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Parameter[nIdx].Create(g_Param_2DCal[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_Parameter[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_PARAM + nIdx);
		m_ed_Parameter[nIdx].SetFont(&m_font);
		m_ed_Parameter[nIdx].SetValidChars(_T("0123456789.-"));
	}

	//m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TB_OPTION, CMFCTabCtrl::LOCATION_BOTTOM);

	// ROI
	m_lst_2DCalROI.Create(dwStyle | WS_BORDER | LVS_REPORT, rectDummy, this, IDC_LST_ROI);

	// ROI 버튼 
// 	for (UINT nIdex = 0; nIdex < PROI_BT_MaxNum; nIdex++)
// 	{
// 		m_bt_ParamROI[nIdex].Create(g_Param_bt2DCalROI[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_TEST + nIdex);
// 		m_bt_ParamROI[nIdex].SetFont(&m_font);
// 	}


	//m_tc_Option.AddTab(&m_lst_2DCalROI,  _T("ROI"),	 0, FALSE);

// 	m_tc_Option.SetActiveTab(0);
// 	m_tc_Option.EnableTabSwap(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iCtrlHeight = 32;
	int iTempWidth	= (iWidth - (iSpacing * 3)) / 4;
	int iNameWidth	= iTempWidth;

	int iLeftEdit	= iLeft + iNameWidth + iSpacing;
	int iLeftSub	= iLeftEdit + iTempWidth + iSpacing;
	int iLeftEditSub= iLeftSub + iNameWidth + iSpacing;

	int iListWidth	= iWidth;
	int iListHeight = 300;

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_2DCal_MaxNum; nIdx++)
	{
		if (0 == (nIdx % 2))
		{
			m_st_Parameter[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);
			m_ed_Parameter[nIdx].MoveWindow(iLeftEdit, iTop, iTempWidth, iCtrlHeight);
		}
		else
		{
			m_st_Parameter[nIdx].MoveWindow(iLeftSub, iTop, iNameWidth, iCtrlHeight);
			m_ed_Parameter[nIdx].MoveWindow(iLeftEditSub, iTop, iTempWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
		}
	}

	if ( 1 == (Param_2DCal_MaxNum % 2))
	{
		iTop += iCtrlHeight + iSpacing;
	}

	iListHeight = cy - iTop - iMargin;
	m_lst_2DCalROI.MoveWindow(iLeft, iTop, iListWidth, iListHeight);
	
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_2DCal::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nID
// Qualifier	:
// Last Update	: 2017/11/10 - 18:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::OnRangeBtnCtrl(__in UINT nID)
{
	UINT nIdex = nID - IDC_BTN_TEST;

	switch (nID)
	{
	case PROI_2DCal_Test:
		break;
	case PROI_2DCal_Result:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:34
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{

}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/11 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{

}

//=============================================================================
// Method		: Set_2DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __in ST_2DCal_Para * pst2DCal_Para
// Qualifier	:
// Last Update	: 2017/11/10 - 18:54
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::Set_2DCal_Para(__in ST_2DCal_Para* pst2DCal_Para)
{
	CString szText;

	UINT nIdx = Para_Left;	

	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nImgWidth);
	m_ed_Parameter[Param_2DCal_nImgWidth].SetWindowText(szText);
	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nImgHeight);
	m_ed_Parameter[Param_2DCal_nImgHeight].SetWindowText(szText);
	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nNumofCornerX);
	m_ed_Parameter[Param_2DCal_nNumofCornerX].SetWindowText(szText);
	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nNumofCornerY);
	m_ed_Parameter[Param_2DCal_nNumofCornerY].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fPatternSizeX);
	m_ed_Parameter[Param_2DCal_fPatternSizeX].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fPatternSizeY);
	m_ed_Parameter[Param_2DCal_fPatternSizeY].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fRepThres);
	m_ed_Parameter[Param_2DCal_fRepThres].SetWindowText(szText);
	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nMinNumImages);
	m_ed_Parameter[Param_2DCal_nMinNumImages].SetWindowText(szText);
	szText.Format(_T("%d"), pst2DCal_Para->IntrParam.nMinNumValidPnts);
	m_ed_Parameter[Param_2DCal_nMinNumValidPnts].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMinOriginOffset);
	m_ed_Parameter[Param_2DCal_fMinOriginOffset].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMaxOriginOffset);
	m_ed_Parameter[Param_2DCal_fMaxOriginOffset].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMinFocalLength);
	m_ed_Parameter[Param_2DCal_fMinFocalLength].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMaxFocalLength);
	m_ed_Parameter[Param_2DCal_fMaxFocalLength].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[0]);
	m_ed_Parameter[Param_2DCal_arfKK_0].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[1]);
	m_ed_Parameter[Param_2DCal_arfKK_1].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[2]);
	m_ed_Parameter[Param_2DCal_arfKK_2].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[3]);
	m_ed_Parameter[Param_2DCal_arfKK_3].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[0]);
	m_ed_Parameter[Param_2DCal_arfKc_0].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[1]);
	m_ed_Parameter[Param_2DCal_arfKc_1].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[2]);
	m_ed_Parameter[Param_2DCal_arfKc_2].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[3]);
	m_ed_Parameter[Param_2DCal_arfKc_3].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[4]);
	m_ed_Parameter[Param_2DCal_arfKc_4].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.fR2Max);
	m_ed_Parameter[Param_2DCal_fR2Max].SetWindowText(szText);
	szText.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fVersion);
	m_ed_Parameter[Param_2DCal_fVersion].SetWindowText(szText);

	m_lst_2DCalROI.InsertFullData(pst2DCal_Para);
}

//=============================================================================
// Method		: Get_2DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __out ST_2DCal_Para & stOut2DCal_Para
// Qualifier	:
// Last Update	: 2017/11/10 - 22:06
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::Get_2DCal_Para(__out ST_2DCal_Para& stOut2DCal_Para)
{
	CString szText;

	m_ed_Parameter[Param_2DCal_nImgWidth].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nImgWidth = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_nImgHeight].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nImgHeight = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_nNumofCornerX].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nNumofCornerX = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_nNumofCornerY].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nNumofCornerY = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fPatternSizeX].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fPatternSizeX = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fPatternSizeY].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fPatternSizeY = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fRepThres].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fRepThres = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_nMinNumImages].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nMinNumImages = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_nMinNumValidPnts].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.nMinNumValidPnts = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fMinOriginOffset].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fMinOriginOffset = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fMaxOriginOffset].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fMaxOriginOffset = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fMinFocalLength].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fMinFocalLength = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fMaxFocalLength].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fMaxFocalLength = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKK_0].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKK_1].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKK_2].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKK_3].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[3] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKc_0].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKc_1].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKc_2].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKc_3].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[3] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_arfKc_4].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[4] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fR2Max].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.fR2Max = (float)_ttof(szText.GetBuffer(0));

	m_ed_Parameter[Param_2DCal_fVersion].GetWindowText(szText);
	stOut2DCal_Para.IntrParam.fVersion = (float)_ttof(szText.GetBuffer(0));
	

	m_lst_2DCalROI.GetCellData(stOut2DCal_Para);

}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/11/15 - 2:41
// Desc.		:
//=============================================================================
void CWnd_Cfg_2DCal::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}
