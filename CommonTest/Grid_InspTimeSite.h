﻿//*****************************************************************************
// Filename	: 	Grid_InspTimeSite.h
// Created	:	2016/11/21 - 9:51
// Modified	:	2016/11/21 - 9:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_InspTimeSite_h__
#define Grid_InspTimeSite_h__

#pragma once

#include "Grid_Base.h"
#include "Def_DataStruct_Cm.h"

class CGrid_InspTimeSite :
	public CGrid_Base
{
public:
	CGrid_InspTimeSite();
	virtual ~CGrid_InspTimeSite();

protected:
	virtual void	OnSetup				();
	virtual int		OnHint				(int col, long row, int section, CString *string);
	virtual void	OnGetCell			(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect		(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline		();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline		();

	// 헤더를 초기화
	void			InitHeader			();

	CFont		m_font_Header;
	CFont		m_font_Data;

	UINT		m_nSiteCount;
	BOOL		m_bDisableInputTest;

	CString		m_szSiteName[MAX_SITE_CNT];

public:

	void		SetUseSiteCount			(__in UINT nCount);
	void		DisableInputTest		();
	void		SetSiteName				(__in UINT nSiteIdx, __in LPCTSTR szSiteName);
	void		SetCycleTime			(__in const ST_CycleTime* pstCycleTime);

};

#endif // Grid_InspTimeSite_h__

