//*****************************************************************************
// Filename	: 	Def_TestItem_Cm.h
// Created	:	2017/9/20 - 15:25
// Modified	:	2017/9/20 - 15:25
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_TestItem_Cm_h__
#define Def_TestItem_Cm_h__

#include <afxwin.h>
#include <afxdisp.h>
#include "Def_Enum_Cm.h"
#include "Def_Test_Cm.h"


#pragma pack(push,1)

// 검사 결과값 데이터 갯수
#define MAX_RESULT_CNT		100

//=============================================================================
// 개별 검사 항목
//=============================================================================
typedef enum enTestItem_Focusing
{
	TI_Foc_CaptureImage,
	TI_Foc_Motion_LockingCheck,
	TI_Foc_Motion_ReleaseScrew,
	TI_Foc_Motion_StageLoad,
	TI_Foc_Motion_StageUnLoad,
	TI_Foc_Motion_ParticleIn,
	TI_Foc_Motion_ParticleOut,
	TI_Foc_Motion_LockingScrew,
	TI_Foc_Initialize_Test,
	TI_Foc_Finalize_Test,
	TI_Foc_Fn_PreFocus,
	TI_Foc_Fn_ActiveAlign,
	TI_Foc_Fn_ECurrent,
	TI_Foc_Fn_OpticalCenter,
	TI_Foc_Fn_SFR,
	TI_Foc_Fn_Rotation,
	TI_Foc_Fn_Ymean,
	TI_Foc_Fn_BlackSpot,
	TI_Foc_Fn_LCB,
	TI_Foc_Fn_Defect_Black,
	TI_Foc_Fn_Defect_White,
	TI_Foc_Re_TorqueCheck,
	TI_Foc_Re_ECurrent,
	TI_Foc_Re_OpticalCenterX,
	TI_Foc_Re_OpticalCenterY,
	TI_Foc_Re_SFR,
	TI_Foc_Re_Rotation,
	TI_Foc_Re_Ymean,
	TI_Foc_Re_BlackSpot,
	TI_Foc_Re_LCB,
	TI_Foc_Re_Defect_Black,
	TI_Foc_Re_Defect_White,
	TI_Foc_MaxEnum,
};

//!SH _180913: Front Focus 수정
static LPCTSTR g_szTestItem_Focusing[] =
{
	_T("[PROC] Capture Image			"),				// TI_Foc_CaptureImage,
	_T("[MOTION] Module Locking Check	"),				// TI_Foc_Motion_LockingCheck,
	_T("[MOTION] Release Screw			"),				// TI_Foc_Motion_ReleaseScrew,
	_T("[MOTION] Stage Load				"),				// TI_Foc_Motion_StageLoad,
	_T("[MOTION] Stage UnLoad			"),				// TI_Foc_Motion_StageUnLoad,
	_T("[MOTION] Particle Lenz In		"),				// TI_Foc_Motion_ParticleIn,
	_T("[MOTION] Particle Lenz Out		"),				// TI_Foc_Motion_ParticleOut,
	_T("[MOTION] Locking Screw			"),				// TI_Foc_Motion_LockingScrew,
	_T("[PROC] Initialize				"),				// TI_Foc_Initialize_Test,
	_T("[PROC] Finalize					"),				// TI_Foc_Finalize_Test,
	_T("[PROC] Pre Focus				"),				// TI_Foc_Fn_PreFocus,
	_T("[PROC] Active Align				"),				// TI_Foc_Fn_ActiveAlign,
	_T("[PROC] Current					"),				// TI_Foc_Fn_ECurrent,
	_T("[PROC] Optical Center			"),				// TI_Foc_Fn_OpticalCenter,
	_T("[PROC] SFR						"),				// TI_Foc_Fn_SFR,
	_T("[PROC] Rotation					"),				// TI_Foc_Fn_Rotation,
	_T("[PROC] Ymean					"),				// TI_Foc_Fn_Ymean,
	_T("[PROC] BlackSpot				"),				// TI_Foc_Fn_BlackSpot,
	_T("[PROC] LCB						"),				// TI_Foc_Fn_LCB,
	_T("[PROC] DefectBlack				"),				// TI_Foc_Fn_Defect_Black,
	_T("[PROC] DefectWhite				"),				// TI_Foc_Fn_Defect_White,
	_T("[R] Torque						"),				// TI_Foc_Re_TorqueCheck,
	_T("[R] Current						"),				// TI_Foc_Re_ECurrent,
	_T("[R] OpticalCenter X				"),				// TI_Foc_Re_OpticalCenterX,
	_T("[R] OpticalCenter Y				"),				// TI_Foc_Re_OpticalCenterY,
	_T("[R] SFR							"),				// TI_Foc_Re_SFR,
	_T("[R] Rotation					"),				// TI_Foc_Re_Rotation,
	_T("[R] Ymean						"),				// TI_Foc_Re_Ymean,
	_T("[R] BlackSpot					"),				// TI_Foc_Re_BlackSpot,
	_T("[R] LCB							"),				// TI_Foc_Re_LCB,
	_T("[R] DefectBlack					"),				// TI_Foc_Re_Defect_Black,
	_T("[R] DefectWhite					"),				// TI_Foc_Re_Defect_White,
};

// 2D CAL 검사 -------------------------------------
typedef enum enTestItem_2D_CAL
{
	TI_2D_Initialize_Test,
	TI_2D_Finalize_Test,
	TI_2D_Set_CalibCornerParam,	// Parameter 전송
	TI_2D_Detect_CornerExt,		// Corner Extraction
	TI_2D_Exec_Intrinsic,		// LGE 라이브러리 이용하여 결과값 추출
	TI_2D_Re_KK0,				// TI_2D_Re_FocalLengthU
	TI_2D_Re_KK1,				// TI_2D_Re_FocalLengthV
	TI_2D_Re_KK2,				// TI_2D_Re_PrincipalPointU
	TI_2D_Re_KK3,				// TI_2D_Re_PrincipalPointV
	TI_2D_Re_Kc4,
	TI_2D_Re_R2Max,				// TI_2D_Re_IntrinsicFov
	TI_2D_Re_RepError,			// TI_2D_Re_ReprojectionError
	TI_2D_Re_EvalError,
	TI_2D_Re_OrgOffset,
	TI_2D_Re_DetectionFailureCnt,
	TI_2D_Re_InvalidPointCnt,
	TI_2D_Re_ValidPointCnt,
	TI_2D_Write_EEPROM,		// 결과값 전송
	TI_2D_MaxEnum,
};

static LPCTSTR g_szTestItem_2D_CAL[TI_2D_MaxEnum] =
{
	_T("[PROC] Initialize			"),		// TI_2D_Initialize_Test
	_T("[PROC] Finalize				"),		// TI_2D_Finalize_Test
	_T("[PROC] Set Parameters		"),		// TI_2D_Set_CalibCornerParam,	// Parameter 전송
	_T("[PROC] Corner Points		"),		// TI_2D_Detect_CornerExt,		// Corner Extraction
	_T("[PROC] Execute Intrinsic	"),		// TI_2D_Exec_Intrinsic,		// LGE 라이브러리 이용하여 결과값 추출
	_T("[R] Focal Length U			"),		// TI_2D_Re_KK0,
	_T("[R] Focal Length V			"),		// TI_2D_Re_KK1,
	_T("[R] Optical Center X		"),		// TI_2D_Re_KK2,
	_T("[R] Optical Center Y		"),		// TI_2D_Re_KK3,
	_T("[R] Cal Kc4					"),		// TI_2D_Re_Kc4,
	_T("[R] Intrinsc Fov			"),		// TI_2D_Re_R2Max,
	_T("[R] Reprojection Error		"),		// TI_2D_Re_RepError,
	_T("[R] Cal Eval Error			"),		// TI_2D_Re_EvalError,
	_T("[R] Cal Org Offset			"),		// TI_2D_Re_OrgOffset,
	_T("[R] Cal Detection FailureCnt"),		// TI_2D_Re_DetectionFailureCnt,
	_T("[R] Cal Invalid PointCnt	"),		// TI_2D_Re_InvalidPointCnt,
	_T("[R] Cal Valid PointCnt		"),		// TI_2D_Re_ValidPointCnt,
	_T("[PROC] Write EEPROM			"),		// TI_2D_Write_EEPROM,		// 결과값 전송
};

// 화질 검사 -------------------------------------
typedef enum enTestItem_ImgT
{
	TI_ImgT_CaptureImage,
	TI_ImgT_Motion_Load,
	TI_ImgT_Motion_Chart,
	TI_ImgT_Motion_Particle,
	TI_ImgT_Initialize_Test,
	TI_ImgT_Finalize_Test,
	TI_ImgT_Fn_ECurrent,
	TI_ImgT_Fn_OpticalCenter,
	TI_ImgT_Fn_SFR,
	TI_ImgT_Fn_Rotation,
 	TI_ImgT_Fn_Distortion,
 	TI_ImgT_Fn_FOV,
	TI_ImgT_Fn_DynamicBW,
// 	TI_ImgT_Fn_Stain,
// 	TI_ImgT_Fn_DefectPixel,
// 	TI_ImgT_Fn_Particle_SNR,		
// 	TI_ImgT_Fn_Intensity,
// 	TI_ImgT_Fn_SNR_Light,
 	TI_ImgT_Fn_Shading,
	TI_ImgT_Fn_Ymean,
	TI_ImgT_Fn_BlackSpot,
 	TI_ImgT_Fn_LCB,
 	TI_ImgT_Fn_Defect_Black,
	TI_ImgT_Fn_Defect_White,
// 	TI_ImgT_Re_TorqueCheck,
// 	//#if (LT_EQP == SYS_GJ_IMAGE_TEST)	
// 	TI_ImgT_Fn_HotPixel,			// Entry
// 	TI_ImgT_Fn_FPN,					// Entry
// 	TI_ImgT_Fn_3DDepth,				// Entry
// 	TI_ImgT_Fn_EEPROM_Verify,		// Entry
// 	//#endif
// 	TI_ImgT_Fn_TemperatureSensor,
// 	TI_ImgT_Fn_Particle_Entry,
	TI_ImgT_Re_ECurrent,
	TI_ImgT_Re_OpticalCenterX,
	TI_ImgT_Re_OpticalCenterY,
	TI_ImgT_Re_SFR,
// 	TI_ImgT_Re_G0_SFR,
// 	TI_ImgT_Re_G1_SFR,
// 	TI_ImgT_Re_G2_SFR,
// 	TI_ImgT_Re_G3_SFR,
// 	TI_ImgT_Re_G4_SFR,
// 	TI_ImgT_Re_X1Tilt_SFR,
// 	TI_ImgT_Re_X2Tilt_SFR,
// 	TI_ImgT_Re_Y1Tilt_SFR,
// 	TI_ImgT_Re_Y2Tilt_SFR,
	TI_ImgT_Re_Rotation,
	TI_ImgT_Re_Ymean,
	TI_ImgT_Re_BlackSpot,
	TI_ImgT_Re_LCB,
	TI_ImgT_Re_Defect_Black,
	TI_ImgT_Re_Defect_White,
 	TI_ImgT_Re_Distortion,
//	TI_ImgT_Re_FOV,
	TI_ImgT_Re_FOV_Dia,
 	TI_ImgT_Re_FOV_Hor,
 	TI_ImgT_Re_FOV_Ver,
// 	TI_ImgT_Re_Stain,
// 	TI_ImgT_Re_DefectPixel,
 	TI_ImgT_Re_DynamicRange,
 	TI_ImgT_Re_SNR_BW,
// 	TI_ImgT_Re_Intencity,
// 	TI_ImgT_Re_SNR_Light,
 	TI_ImgT_Re_Shading,
// 	
// 	//#if (LT_EQP == SYS_GJ_IMAGE_TEST)
// 	TI_ImgT_Re_Hot_Pixel,			// Entry
// 	TI_ImgT_Re_Fixed_Pattern,		// Entry
// 	TI_ImgT_Re_3D_Depth,			// Entry
// 	TI_ImgT_Re_EEPROM_Verify,		// Entry
// 	//#endif
// 	TI_ImgT_Re_TemperatureSensor,
// 	TI_ImgT_Re_Particle_Entry,
	TI_ImgT_MaxEnum

	
	
};

static LPCTSTR g_szTestItem_ImgT[TI_ImgT_MaxEnum] =
{
	_T("[PROC] Capture Image			"),				// TI_ImgT_CaptureImage,
	_T("[PROC] Move Stage Load			"),				// TI_ImgT_Motion_Load,
	_T("[PROC] Move Stage Chart			"),				// TI_ImgT_Motion_Chart,
	_T("[PROC] Move Stage Particle		"),				// TI_ImgT_Motion_Particle,
	_T("[PROC] Initialize				"),				// TI_ImgT_Initialize_Test,
	_T("[PROC] Finalize					"),				// TI_ImgT_Finalize_Test,
	_T("[PROC] Current					"),				// TI_ImgT_Fn_Current,					
	_T("[PROC] Optical Center			"),				// TI_ImgT_Fn_OpticalCenter,			
	_T("[PROC] SFR						"),				// TI_ImgT_Fn_SFR,						
	_T("[PROC] Rotation					"),				// TI_ImgT_Fn_Rotation,	
	_T("[PROC] Distortion				"),				// TI_ImgT_Fn_Distortion,				
 	_T("[PROC] FOV						"),				// TI_ImgT_Fn_FOV,		
	_T("[PROC] Dynamic Range, SNR_BW	"),				// Dynamic Range, SNR_BW
// 	_T("[PROC] PAR Stain				"),				// TI_ImgT_Fn_Stain,			
// 	_T("[PROC] PAR DefectPixel			"),				// TI_ImgT_Fn_DefectPixel,			
// 	_T("[PROC] PAR SNR (Dynamic Range)	"),				// TI_ImgT_Fn_Particle_SNR,		
// 	_T("[PROC] PAR Shading(RI)			"),				// TI_ImgT_Fn_Intensity,
// 	_T("[PROC] PAR SNR Light			"),				// TI_ImgT_Fn_SNR_Light,				
 	_T("[PROC] Shading					"),				// TI_ImgT_Fn_Shading,
	_T("[PROC] Ymean					"),				// TI_Foc_Fn_Ymean,
	_T("[PROC] BlackSpot				"),				// TI_Foc_Fn_BlackSpot,
	_T("[PROC] LCB						"),				// TI_Foc_Fn_LCB,
	_T("[PROC] DefectBlack				"),				// TI_Foc_Fn_Defect_Black,
	_T("[PROC] DefectWhite				"),				// TI_Foc_Fn_Defect_White,
// 	_T("[PROC] Hot Pixel				"),				// TI_ImgT_Hot Pixel,
// 	_T("[PROC] Fixed Pattern			"),				// TI_ImgT_Fixed Pattern,
// 	_T("[PROC] 3D Depth					"),				// TI_ImgT_3D Depth,
// 	_T("[PROC] EEPROM Verify			"),				// TI_ImgT_EEPROM Verify,
// 	_T("[PROC] TemperatureSensor		"),				// TI_ImgT_Fn_TemperatureSensor,
// 	_T("[PROC] Particle					"),				// TI_ImgT_Fn_Particle,			
	_T("[R] Current						"),				// TI_ImgT_Re_Current,					
	_T("[R] OpticalCenter X				"),				// TI_ImgT_Re_OpticalCenterX,			
	_T("[R] OpticalCenter Y				"),				// TI_ImgT_Re_OpticalCenterY,	
	_T("[R] SFR							"),				// TI_ImgT_Re_SFR,						
// 	_T("[R] SFR[G0]						"),				// TI_ImgT_Re_SFR,						
// 	_T("[R] SFR[G1]						"),				// TI_ImgT_Re_SFR,						
// 	_T("[R] SFR[G2] 					"),				// TI_ImgT_Re_SFR,						
// 	_T("[R] SFR[G3] 					"),				// TI_ImgT_Re_SFR,						
// 	_T("[R] SFR[G4]						"),				// TI_ImgT_Re_SFR,	
// 	_T("[R] SFR[X1_Tilt]				"),				// TI_ImgT_Re_X1Tilt_SFR,
// 	_T("[R] SFR[X2_Tilt]				"),				// TI_ImgT_Re_X2Tilt_SFR,
// 	_T("[R] SFR[Y1_Tilt]				"),				// TI_ImgT_Re_Y1Tilt_SFR,
// 	_T("[R] SFR[Y2_Tilt]				"),				// TI_ImgT_Re_Y2Tilt_SFR,
	_T("[R] Rotation					"),				// TI_ImgT_Re_Rotation,	
	_T("[R] Ymean						"),				// TI_Foc_Re_Ymean,
	_T("[R] BlackSpot					"),				// TI_Foc_Re_BlackSpot,
	_T("[R] LCB							"),				// TI_Foc_Re_LCB,
	_T("[R] DefectBlack					"),				// TI_Foc_Re_Defect_Black,
	_T("[R] DefectWhite					"),				// TI_Foc_Re_Defect_White,
 	_T("[R] Distortion					"),				// TI_ImgT_Re_Distortion,		
//	_T("[R] FOV							"),				// TI_ImgT_Re_FOV,	
	_T("[R] FOV Diagonal				"),				// TI_ImgT_Re_FOV_Dia,					
 	_T("[R] FOV Horizontal				"),				// TI_ImgT_Re_FOV_Hor,					
 	_T("[R] FOV Vertical				"),				// TI_ImgT_Re_FOV_Ver,					
// 	_T("[R] Stain						"),				// TI_ImgT_Re_Stain,					
// 	_T("[R] DefectPixel					"),				// TI_ImgT_Re_VeryHotPixel,
 	_T("[R] DynamicRange				"),				// TI_ImgT_Re_DynamicRange,		
 	_T("[R] SNR BW						"),				// TI_ImgT_Re_SNR_BW,					
// 	_T("[R] Shading [RI]				"),				// TI_ImgT_Re_Intencity,
// 	_T("[R] Light SNR					"),				// TI_ImgT_Re_Light_SNR,				
 	_T("[R] Shading	[SNR]				"),				// TI_ImgT_Re_Shading,
// 	_T("[R] Hot Pixel					"),				// TI_ImgT_Re_Hot_Pixel,
// 	_T("[R] Fixed Pattern				"),				// TI_ImgT_Re_Fixed_Pattern,
// 	_T("[R] 3D Depth					"),				// TI_ImgT_Re_3D_Depth,
// 	_T("[R] EEPROM_Verify			    "),				// TI_ImgT_Re_EEPROM Verify,
// 	_T("[R] TemperatureSensor			"),				// TI_ImgT_Re_TemperatureSensor,
// 	_T("[R] Particle					"),				// TI_ImgT_Re_Particle	,
};

//=============================================================================
// Method		: GetTestItemName
// Access		: public static  
// Returns		: CString
// Parameter	: __in enInsptrSysType nSysType
// Parameter	: __in UINT nTestItem
// Qualifier	:
// Last Update	: 2017/9/26 - 10:46
// Desc.		: 검사 항목 명칭 구하기 함수
//=============================================================================
static CString GetTestItemName (__in enInsptrSysType nSysType, __in UINT nTestItem)
{
	CString szText;

	switch (nSysType)
	{
	case Sys_Focusing:
		if (nTestItem < TI_Foc_MaxEnum)
		{
			szText = g_szTestItem_Focusing[nTestItem];
		}
		break;

	case Sys_2D_Cal:
		if (nTestItem < TI_2D_MaxEnum)
		{
			szText = g_szTestItem_2D_CAL[nTestItem];
		}
		break;

	case Sys_Image_Test:
		if (nTestItem < TI_ImgT_MaxEnum)
		{
			szText = g_szTestItem_ImgT[nTestItem];
		}
		break;

	default:
		break;
	}

	return szText;
};

//-----------------------------------------------------------------------------
// 검사 항목 기본 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_TestSpecUnit
{
	BOOL		bUseSpecMin;	// 검사 스펙 사용 여부 (Min 값이 없음)
	BOOL		bUseSpecMax;	// 검사 스펙 사용 여부 (Max 값이 없음)

	COleVariant	Spec_Min;		// 하한치	VARIANT TYPE	[MAX_RESULT_CNT] -> nResultCount 개수 만큼
	COleVariant	Spec_Max;		// 상한치	VARIANT TYPE	[MAX_RESULT_CNT] -> nResultCount 개수 만큼

	_tag_TestSpecUnit()
	{
		bUseSpecMin		= FALSE;
		bUseSpecMax		= FALSE;
	};

	_tag_TestSpecUnit& operator= (const _tag_TestSpecUnit& ref)
	{
		bUseSpecMin = ref.bUseSpecMin;
		bUseSpecMax = ref.bUseSpecMax;

		Spec_Min	= ref.Spec_Min;
		Spec_Max	= ref.Spec_Max;

		return *this;
	};
}ST_TestSpecUnit, *PST_TestSpecUnit;

typedef struct _tag_TestItemSpec
{
	UINT			nItemID;				// Item ID
	CString			szName;					// 검사 항목 명칭
	VARTYPE			vt;						// VARTYPE, 데이터 타입
	UINT			nResultCount;			// 결과 데이터 최대 갯수
	ST_TestSpecUnit	Spec[MAX_RESULT_CNT];
	
	BOOL			bUseMinMaxSpec;			// 검사 스펙 사용 여부 (Min / Max 값이 없음)
	BOOL			bUseMultiSpec;			// 검사 항목의 데이터가 1개 초과이면서, 각각 개별적인 스펙값을 가니는 경우

	PVOID			pOption;				// 검사 옵션
	DWORD			dwOptionSize;			// 검사 옵션 크기

	_tag_TestItemSpec()
	{
		nItemID			= 0;
		vt				= VARENUM::VT_R8;	// VT_BSTR
		nResultCount	= 1;

		bUseMinMaxSpec	= TRUE;
		bUseMultiSpec	= FALSE;

		pOption			= NULL;
		dwOptionSize	= 0;
	};

	_tag_TestItemSpec(UINT nID, LPCTSTR szInName, VARTYPE InVarType, long InSpecMin, long InSpecMax, long InResultCnt = 1, BOOL bUseSpecMin = TRUE, BOOL bUseSpecMax = TRUE, BOOL bUseArraySpec = FALSE)
	{
		vt				= InVarType;
		nItemID			= nID;
		szName			= szInName;
		nResultCount	= (InResultCnt <= MAX_RESULT_CNT) ? InResultCnt : MAX_RESULT_CNT;
		bUseMinMaxSpec	= ((FALSE == bUseSpecMin) && (FALSE == bUseSpecMax)) ? FALSE : TRUE;
		bUseMultiSpec	= bUseArraySpec;

		for (UINT nIdx = 0; nIdx < nResultCount; nIdx++)
		{
			Spec[nIdx].bUseSpecMin = bUseSpecMin;
			Spec[nIdx].bUseSpecMax = bUseSpecMax;
			Spec[nIdx].Spec_Min.ChangeType(vt);
			Spec[nIdx].Spec_Max.ChangeType(vt);
			Spec[nIdx].Spec_Min = InSpecMin;
			Spec[nIdx].Spec_Max = InSpecMax;
		}
		
		//pOption			= NULL;
		//dwOptionSize	= 0;
	};

	_tag_TestItemSpec(UINT nID, LPCTSTR szInName, VARTYPE InVarType, double InSpecMin, double InSpecMax, long InResultCnt = 1, BOOL bUseSpecMin = TRUE, BOOL bUseSpecMax = TRUE, BOOL bUseArraySpec = FALSE)
	{
		vt				= InVarType;
		nItemID			= nID;
		szName			= szInName;
		nResultCount	= (InResultCnt <= MAX_RESULT_CNT) ? InResultCnt : MAX_RESULT_CNT;
		bUseMinMaxSpec	= ((FALSE == bUseSpecMin) && (FALSE == bUseSpecMax)) ? FALSE : TRUE;
		bUseMultiSpec	= bUseArraySpec;
		
  		for (UINT nIdx = 0; nIdx < nResultCount; nIdx++)
  		{
			Spec[nIdx].bUseSpecMin = bUseSpecMin;
			Spec[nIdx].bUseSpecMax = bUseSpecMax;
			Spec[nIdx].Spec_Min.ChangeType(vt);
			Spec[nIdx].Spec_Max.ChangeType(vt);
			Spec[nIdx].Spec_Min = InSpecMin;
			Spec[nIdx].Spec_Max = InSpecMax;
  		}

		//pOption			= NULL;
		//dwOptionSize	= 0;
	};

	_tag_TestItemSpec& operator= (const _tag_TestItemSpec& ref)
	{
		nItemID			= ref.nItemID;
		szName			= ref.szName;
		vt				= ref.vt;
		nResultCount	= ref.nResultCount;

		for (UINT nIdx = 0; nIdx < nResultCount; nIdx++)
		{
			Spec[nIdx] = ref.Spec[nIdx];
		}

		bUseMinMaxSpec	= ref.bUseMinMaxSpec;
		bUseMultiSpec	= ref.bUseMultiSpec;
		
		pOption			= ref.pOption;
		dwOptionSize	= ref.dwOptionSize;
		
		return *this;
	};

	// 검사 항목의 스펙 매칭
	void Set_Spec		(__in const _tag_TestItemSpec& ref)
	{
		nItemID			= ref.nItemID;
		szName			= ref.szName;
		vt				= ref.vt;
		nResultCount	= ref.nResultCount;

		for (UINT nIdx = 0; nIdx < nResultCount; nIdx++)
		{
			Spec[nIdx] = ref.Spec[nIdx];
		}

		bUseMinMaxSpec	= ref.bUseMinMaxSpec;
		bUseMultiSpec	= ref.bUseMultiSpec;
		
		pOption			= ref.pOption;
		dwOptionSize	= ref.dwOptionSize;
	};

	void Set_Spec(__in UINT nArrIndex, long InSpecMin, long InSpecMax, BOOL bUseSpecMin = TRUE, BOOL bUseSpecMax = TRUE)
	{
		if (nArrIndex < nResultCount)
		{
			Spec[nArrIndex].bUseSpecMin = bUseSpecMin;
			Spec[nArrIndex].bUseSpecMax = bUseSpecMax;
			Spec[nArrIndex].Spec_Min	= InSpecMin;
			Spec[nArrIndex].Spec_Max	= InSpecMax;
		}
	};

	void Set_Spec(__in UINT nArrIndex, double InSpecMin, double InSpecMax, BOOL bUseSpecMin = TRUE, BOOL bUseSpecMax = TRUE)
	{
		if (nArrIndex < nResultCount)
		{
			Spec[nArrIndex].bUseSpecMin = bUseSpecMin;
			Spec[nArrIndex].bUseSpecMax = bUseSpecMax;
			Spec[nArrIndex].Spec_Min	= InSpecMin;
			Spec[nArrIndex].Spec_Max	= InSpecMax;
		}
	};

	void Set_Option		(__in PVOID pIN_Opt, __in DWORD pIN_OptSize)
	{
		pOption			= pIN_Opt;
		dwOptionSize	= pIN_OptSize;
	};	

	// 검사 항목 명칭 설정
	void SetName(__in LPCTSTR szInName)
	{
		szName			= szInName;
	};
}ST_TestItemSpec, *PST_TestItemSpec;

//-----------------------------------------------------------------------------
// 검사 항목 디폴트 설정 (szName에 문자열이 입력되면 메모리 누수 발생!!!!!!)
//-----------------------------------------------------------------------------
static ST_TestItemSpec	g_Def_TestItem_Foc[enTestItem_Focusing::TI_Foc_MaxEnum] = 
{
	//nItemID						szName		VARTYPE				Spec_Min	Spec_Max	nResultCount	bUseMinSpec		bUseMaxSpec		bUseArraySpec
	{TI_Foc_CaptureImage,			_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_LockingCheck,	_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_ReleaseScrew,	_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_StageLoad,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_StageUnLoad,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_ParticleIn,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_ParticleOut,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Motion_LockingScrew,	_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Initialize_Test,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Finalize_Test,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_PreFocus,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_ActiveAlign,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_ECurrent,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_OpticalCenter,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_SFR,					_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_Rotation,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_Ymean,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_BlackSpot,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_LCB,					_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_Defect_Black,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Fn_Defect_White,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_Foc_Re_TorqueCheck,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		8l,				TRUE,			TRUE,			TRUE  },
	{TI_Foc_Re_ECurrent,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		2l,				TRUE,			TRUE,			TRUE  },
	{TI_Foc_Re_OpticalCenterX,		_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_OpticalCenterY,		_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_SFR,					_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		30l,			TRUE,			TRUE,			TRUE  },
	{TI_Foc_Re_Rotation,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_Ymean,				_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_BlackSpot,			_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_LCB,					_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_Foc_Re_Defect_Black,		_T(""),		VARENUM::VT_I4,		0l,			0l,			8l,				TRUE,			TRUE,			TRUE  },
	{TI_Foc_Re_Defect_White,		_T(""),		VARENUM::VT_I4,		0l,			0l,			8l,				TRUE,			TRUE,			TRUE  },
};

static ST_TestItemSpec	g_Def_TestItem_2DCAL[enTestItem_2D_CAL::TI_2D_MaxEnum] = 
{
	//nItemID						szName		VARTYPE				Spec_Min	Spec_Max	nResultCount	bUseMinSpec		bUseMaxSpec		bUseArraySpec
	{TI_2D_Initialize_Test,			_T(""),		VARENUM::VT_BSTR,	0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
	{TI_2D_Finalize_Test,			_T(""),		VARENUM::VT_BSTR,	0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
	{TI_2D_Set_CalibCornerParam,	_T(""),		VARENUM::VT_I8,		0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
	{TI_2D_Detect_CornerExt,		_T(""),		VARENUM::VT_I8,		0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
	{TI_2D_Exec_Intrinsic,			_T(""),		VARENUM::VT_BSTR,	0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
	{TI_2D_Re_KK0,					_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_KK1,					_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_KK2,					_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_KK3,					_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_Kc4,					_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_R2Max,				_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			FALSE,			FALSE},
	{TI_2D_Re_RepError,				_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				FALSE,			TRUE,			FALSE},
	{TI_2D_Re_EvalError,			_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				FALSE,			TRUE,			FALSE},
	{TI_2D_Re_OrgOffset,			_T(""),		VARENUM::VT_R4,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE},
	{TI_2D_Re_DetectionFailureCnt,	_T(""),		VARENUM::VT_I4,		0l,			50l,		1l,				FALSE,			TRUE,			FALSE},
	{TI_2D_Re_InvalidPointCnt,		_T(""),		VARENUM::VT_I4,		0l,			50l,		1l,				FALSE,			TRUE,			FALSE},
	{TI_2D_Re_ValidPointCnt,		_T(""),		VARENUM::VT_I4,		0l,			40l,		1l,				FALSE,			TRUE,			FALSE},
	{TI_2D_Write_EEPROM,			_T(""),		VARENUM::VT_BSTR,	0l,			00l,		1l,				FALSE,			FALSE,			FALSE},
};

static ST_TestItemSpec	g_Def_TestItem_ImgT[enTestItem_ImgT::TI_ImgT_MaxEnum] = 
{
	//nItemID						szName		VARTYPE				Spec_Min	Spec_Max	nResultCount	bUseMinSpec		bUseMaxSpec		bUseArraySpec
	{TI_ImgT_CaptureImage,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Motion_Load,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Motion_Chart,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Motion_Particle,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Initialize_Test,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Finalize_Test,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_ECurrent,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_OpticalCenter,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_SFR,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_Rotation,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
 	{TI_ImgT_Fn_Distortion,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
 	{TI_ImgT_Fn_FOV,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_DynamicBW,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_Stain,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_DefectPixel,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_Particle_SNR,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_Intensity,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_SNR_Light,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
 	{TI_ImgT_Fn_Shading,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	//
	{TI_ImgT_Fn_Ymean,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_BlackSpot,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_LCB,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_Defect_Black,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Fn_Defect_White,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	//
// 	{TI_ImgT_Fn_HotPixel,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_FPN,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_3DDepth,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_EEPROM_Verify,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_TemperatureSensor,	_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
// 	{TI_ImgT_Fn_Particle_Entry,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				FALSE,			FALSE,			FALSE },
	{TI_ImgT_Re_ECurrent,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			TRUE  },
	{TI_ImgT_Re_OpticalCenterX,		_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_OpticalCenterY,		_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		30l,			TRUE,			TRUE,			TRUE  },
// 	{TI_ImgT_Re_G0_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_G1_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_G2_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_G3_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_G4_SFR,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_X1Tilt_SFR,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_X2Tilt_SFR,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_Y1Tilt_SFR,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_Y2Tilt_SFR,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_Rotation,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
	//
	{TI_ImgT_Re_Ymean,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_BlackSpot,			_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_LCB,				_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_Defect_Black,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_Defect_White,		_T(""),		VARENUM::VT_BSTR,	0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
	//
 	{TI_ImgT_Re_Distortion,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
//	{TI_ImgT_Re_FOV,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		3l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_FOV_Dia,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
	{TI_ImgT_Re_FOV_Hor,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
 	{TI_ImgT_Re_FOV_Ver,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_Stain,				_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_DefectPixel,		_T(""),		VARENUM::VT_I4,		0l,			0l,			8l,				TRUE,			TRUE,			TRUE  },
 	{TI_ImgT_Re_DynamicRange,		_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
 	{TI_ImgT_Re_SNR_BW,				_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_Intencity,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		5l,				TRUE,			TRUE,			TRUE  },
// 	{TI_ImgT_Re_SNR_Light,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			FALSE },
 	{TI_ImgT_Re_Shading,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		73l,			TRUE,			TRUE,			TRUE  },
// 	{TI_ImgT_Re_Hot_Pixel,			_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			FALSE },
// 	{TI_ImgT_Re_Fixed_Pattern,		_T(""),		VARENUM::VT_I4,		0l,			0l,			2l,				TRUE,			TRUE,			TRUE },
// 	{TI_ImgT_Re_3D_Depth,			_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		2l,				TRUE,			TRUE,			TRUE },
// 	{TI_ImgT_Re_EEPROM_Verify,		_T(""),		VARENUM::VT_I4,		0l,			0l,			4l,				TRUE,			TRUE,			TRUE },
// 	{TI_ImgT_Re_TemperatureSensor,	_T(""),		VARENUM::VT_R8,		0.0f,		0.0f,		1l,				TRUE,			TRUE,			TRUE },
// 	{TI_ImgT_Re_Particle_Entry,		_T(""),		VARENUM::VT_I4,		0l,			0l,			1l,				TRUE,			TRUE,			TRUE },
};

//-----------------------------------------------------------------------------
// 검사 항목 측정 데이터 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_TestItemMeas
{
	UINT				nItemID;						// Item ID
	CString				szName;							// 검사 항목 명칭
	
	VARTYPE				vt;								// 데이터 타입
	UINT				nResultCount;					// 결과 데이터 최대 갯수
	UINT				nMeasurmentCount;				// 실제 사용된 데이터 갯수
	COleVariant			Measurement[MAX_RESULT_CNT];	// 측정 값	VARIANT
	UINT				nJudgmentUnit[MAX_RESULT_CNT];	// 결과 판정
	UINT				nJudgmentAll;					// 결과 판정
	DWORD				dwElapTime;						// 검사 진행 시간

	ST_TestItemSpec*	pTestSpec;						// 테스트 스펙

	_tag_TestItemMeas()
	{
		nItemID			= 0;
		vt				= VARENUM::VT_R8;
		nResultCount	= 1;
		nMeasurmentCount= 1;
		for (UINT nIdx = 0; nIdx < MAX_RESULT_CNT; nIdx++)
		{
			Measurement[nIdx].ChangeType(vt);
			Measurement[nIdx]	= 0.0f;
			nJudgmentUnit[nIdx]	= TR_Pass;
		}
		nJudgmentAll	= TR_Pass;
		dwElapTime		= 0;

		pTestSpec		= NULL;
	};

	_tag_TestItemMeas& operator= (const _tag_TestItemMeas& ref)
	{
		nItemID			= ref.nItemID;
		szName			= ref.szName;
		vt				= ref.vt;
		nResultCount	= ref.nResultCount;
		nMeasurmentCount= ref.nMeasurmentCount;
		for (UINT nIdx = 0; nIdx < MAX_RESULT_CNT; nIdx++)
		{
			Measurement[nIdx].ChangeType(vt);
			Measurement[nIdx]	= ref.Measurement[nIdx];
			nJudgmentUnit[nIdx]	= ref.nJudgmentUnit[nIdx];
		}
		nJudgmentAll	= ref.nJudgmentAll;
		dwElapTime		= ref.dwElapTime;

		pTestSpec		= ref.pTestSpec;

		return *this;
	};

	// 측정 데이터를 입력
	void Set_Measurement		(__in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		if ( nResultCount < nMeasCount)
		{
			// 에러
			TRACE(_T("Error: Set_Measurement() -> nResultCount <= nMeasCount\n"));
			nMeasurmentCount = nResultCount; 
		}
		else
		{
			nMeasurmentCount = nMeasCount;
		}

		// 측정된 데이터
		for (UINT nIdx = 0; nIdx < nMeasurmentCount; nIdx++)
		{
			Measurement[nIdx] = pMeasurValue[nIdx];
		}

		// 검사 항목의 스펙이 설정되어 있으면 결과 판정을 한다.
		if (NULL != pTestSpec)
		{
			nJudgmentAll = TR_Pass;

			// 검사 항목이 Min/Max 값에 의한 판정을 하는 경우
			for (UINT nIdx = 0; nIdx < nMeasurmentCount; nIdx++)
			{
				if ((VT_I2 == vt) || (VT_I4 == vt) || ((VT_I1 <= vt) && (vt <= VT_UINT)))
				{
					// Spec Minimum
					if (pTestSpec->Spec[nIdx].bUseSpecMin)
					{
						if (pTestSpec->Spec[nIdx].Spec_Min.intVal <= Measurement[nIdx].intVal)
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
						}
						else
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_NG;
							nJudgmentAll = TR_Fail;
						}
					}
					else
					{
						nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
					}

					// Spec Maximum
					if ((pTestSpec->Spec[nIdx].bUseSpecMax) && (enJudgment::JUDGE_OK == nJudgmentUnit[nIdx]))
					{
						if (Measurement[nIdx].intVal <= pTestSpec->Spec[nIdx].Spec_Max.intVal)
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
						}
						else
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_NG;
							nJudgmentAll = TR_Fail;
						}
					}
				}
				else if ((VT_R4 == vt) || (VT_R8 == vt))
				{
					// Spec Minimum
					if (pTestSpec->Spec[nIdx].bUseSpecMin)
					{
						if (pTestSpec->Spec[nIdx].Spec_Min.dblVal <= Measurement[nIdx].dblVal)
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
						}
						else
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_NG;
							nJudgmentAll = TR_Fail;
						}
					}
					else
					{
						nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
					}

					// Spec Maximum
					if ((pTestSpec->Spec[nIdx].bUseSpecMax) && (enJudgment::JUDGE_OK == nJudgmentUnit[nIdx]))
					{
						if (Measurement[nIdx].dblVal <= pTestSpec->Spec[nIdx].Spec_Max.dblVal)
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_OK;
						}
						else
						{
							nJudgmentUnit[nIdx] = enJudgment::JUDGE_NG;
							nJudgmentAll = TR_Fail;			
						}
					}
				}
				else
				{
					nJudgmentUnit[nIdx] = enJudgment::JUDGE_NG;
					nJudgmentAll = TR_Fail;
				}
			} // End of for()
		} // End of if (NULL != pTestSpec)

		dwElapTime		= dwDuration;
	};

	// 측정 데이터 입력 (판정 미사용)
	void Set_Measurement_NoJudge(__in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		if (nResultCount <= nMeasCount)
		{
			// 에러
		}

		for (UINT nIdx = 0; nIdx < nMeasCount; nIdx++)
		{
			Measurement[nIdx] = pMeasurValue[nIdx];
		}

		dwElapTime = dwDuration;
	};

	// 검사 결과 판정 설정
	void Set_Judgment			(__in UINT nIN_Judgment)
	{
		nJudgmentAll	= nIN_Judgment;
	};

	// 진행 시간
	void Set_ElapTime(__in DWORD dwDuration = 0)
	{
		dwElapTime = dwDuration;
	};


	// 측정 데이터 초기화
	void Reset_MeasurementData	()
	{
		for (UINT nIdx = 0; nIdx < MAX_RESULT_CNT; nIdx++)
		{
			Measurement[nIdx]	= 0.0f;
			nJudgmentUnit[nIdx] = TR_Pass;
		}

		nJudgmentAll	= TR_Pass;
		dwElapTime		= 0;
	};

	// 검사 항목의 스펙 설정
	void SetTestItemSpec		(__in ST_TestItemSpec* pInTestSpec)
	{
		if (NULL != pInTestSpec)
		{
			pTestSpec	= pInTestSpec;
 			nItemID		= pInTestSpec->nItemID;
 			szName		= pInTestSpec->szName;
			nResultCount = pInTestSpec->nResultCount;
 			vt			= pInTestSpec->vt;
 			nResultCount= pInTestSpec->nResultCount;
 
 			for (UINT nIdx = 0; nIdx < MAX_RESULT_CNT; nIdx++)
 			{
 				Measurement[nIdx].ChangeType(vt);
 			}
		}
	};

}ST_TestItemMeas, *PST_TestItemMeas;

//-----------------------------------------------------------------------------
// 검사기별 검사 항목 설정값
//-----------------------------------------------------------------------------
typedef struct _tag_TestItemInfo
{
	CArray <ST_TestItemSpec, ST_TestItemSpec&> TestItemList;

	_tag_TestItemInfo()
	{

	};

 	_tag_TestItemInfo& operator= (const _tag_TestItemInfo& ref)
 	{		
 		TestItemList.RemoveAll();

 		TestItemList.Copy(ref.TestItemList);
 
 		return *this;
	};

	// 검사 항목 갯수
	INT_PTR GetCount		() const
	{
		return TestItemList.GetCount();
	};

	// 검사 항목 모두 삭제
	void RemoveAll			()
	{
		TestItemList.RemoveAll();
	};

	// 검사 항목 스펙 설정
	void Set_Spec			(__in UINT nItemIdx, __in ST_TestItemSpec* pstTestItem)
	{
		if (nItemIdx < TestItemList.GetCount())
		{
			TestItemList[nItemIdx].Set_Spec(*pstTestItem);
		}
	};

	// 검사 항목 옵션 설정
	void Set_Option			(__in UINT nItemIdx, __in PVOID pIN_Opt, __in DWORD pIN_OptSize)
	{
		if (nItemIdx < TestItemList.GetCount())
		{
			TestItemList[nItemIdx].Set_Option(pIN_Opt, pIN_OptSize);
		}
	};

	// 검사기 종류 설정
	void SetSystemType		(__in enInsptrSysType nSysType)
	{
		RemoveAll();

		switch (nSysType)
		{
		case Sys_Focusing:
			for (UINT nIdx = 0; nIdx < enTestItem_Focusing::TI_Foc_MaxEnum; nIdx++)
			{
				g_Def_TestItem_Foc[nIdx].SetName(GetTestItemName(nSysType, g_Def_TestItem_Foc[nIdx].nItemID));	// 메모리 누수 문제로 따로 입력함.
				TestItemList.Add(g_Def_TestItem_Foc[nIdx]);
			}
			break;

		case Sys_2D_Cal:
			for (UINT nIdx = 0; nIdx < enTestItem_2D_CAL::TI_2D_MaxEnum; nIdx++)
			{
				g_Def_TestItem_2DCAL[nIdx].SetName(GetTestItemName(nSysType, g_Def_TestItem_2DCAL[nIdx].nItemID));	// 메모리 누수 문제로 따로 입력함.
				TestItemList.Add(g_Def_TestItem_2DCAL[nIdx]);
			}
			break;

		case Sys_Image_Test:
			for (UINT nIdx = 0; nIdx < enTestItem_ImgT::TI_ImgT_MaxEnum; nIdx++)
			{
				g_Def_TestItem_ImgT[nIdx].SetName(GetTestItemName(nSysType, g_Def_TestItem_ImgT[nIdx].nItemID));	// 메모리 누수 문제로 따로 입력함.
				TestItemList.Add(g_Def_TestItem_ImgT[nIdx]);
			}
			break;
		}
	};

	// 검사 항목 얻기
	BOOL GetTestItem		(__in UINT TestItemID, __out ST_TestItemSpec& stOutTestItem)
	{
		for (UINT nIdx = 0; nIdx < TestItemList.GetCount(); nIdx++)
		{
			if (TestItemID == TestItemList[nIdx].nItemID)
			{
				stOutTestItem = TestItemList[nIdx];
				return TRUE;
			}
		}

		return FALSE;
	};

	// 검사 항목의 스펙 데이터 얻기
	ST_TestItemSpec* GetTestItem(__in UINT TestItemID)
	{
		for (UINT nIdx = 0; nIdx < TestItemList.GetCount(); nIdx++)
		{
			if (TestItemID == TestItemList[nIdx].nItemID)
			{
				//return &(TestItemList[nIdx]);
				return &TestItemList.GetAt(nIdx);
			}
		}

		return NULL;
	};

}ST_TestItemInfo, *PST_TestItemInfo;



//=============================================================================
// Step Info 
//=============================================================================

typedef enum enBoardCtrl
{
	BrdCtrl_NotUse,
	BrdCtrl_Initialize,
	BrdCtrl_Finalize,
	BrdCtrl_MaxEnum,
};

static LPCTSTR g_szBoard_Ctrl[] =
{
	_T("Not Use"),
	_T("Initialize"),
	_T("Finalize"),
};

typedef enum enTestLoop
{
	TestLoop_NotUse,
	TestLoop_Use,
	TestLoop_MaxEnum,
};

static LPCTSTR g_szTestLoop[] =
{
	_T("Not Use"),
	_T("Use"),
};

//-----------------------------------------------------------------------------
// 검사 스텝 설정용 기본 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_StepUnit
{
	UINT		nRetryCnt;			// 오류나 NG판정이 추가적으로 재시도하는 횟수
	DWORD		dwDelay;			// 스텝과 스텝 사이의 딜레이

	BOOL		bTest;				// 테스트 진행 여부
	UINT		nTestItem;			// 테스트 항목 구분

	BOOL		bUseMoveY;			// 안착부 전/후진 사용여부
	UINT		nMoveY;				// 안착부 전/후진 거리
	BOOL		bUseMoveX;			// 안착부 좌/우 이동 사용여부
	INT			iMoveX;				// 안착부 좌/우 이동거리
	BOOL		bUseChart_Rot;		// 차트 회전 사용여부
	double		dChart_Rot;			// 차트 회전 각도
	UINT		nChart_Idx;			// 차트 번호
	BOOL		bChart_Check;		// 차트 체크

	BOOL		bUseChart_Move_X;	// 차트 X
	INT			iChart_Move_X;	
	BOOL		bUseChart_Move_Z;	// 차트 Z
	INT			iChart_Move_Z;
	BOOL		bUseChart_Tilt_X;	// 차트 Tilt X
	INT			iChart_Tilt_X;	
	BOOL		bUseChart_Tilt_Z;	// 차트 Tilt Z
	INT			iChart_Tilt_Z;	
	
	WORD		wAlpha;				// Alpha 값
	WORD		wAlpha_2nd;			// Alpah 2번째 값
	enBoardCtrl	nBoardCtrl;			// Initialize, Finalize 
	enTestLoop	nTestLoop;			// TestLoop
	// 영상 캡쳐

	_tag_StepUnit()
	{
		Reset();
	};

	_tag_StepUnit& operator= (const _tag_StepUnit& ref)
	{
		nRetryCnt			= ref.nRetryCnt;
		dwDelay				= ref.dwDelay;
		bTest				= ref.bTest;
		nTestItem			= ref.nTestItem;
		bUseMoveY			= ref.bUseMoveY;
		nMoveY				= ref.nMoveY;
		bUseMoveX			= ref.bUseMoveX;
		iMoveX				= ref.iMoveX;
		bUseChart_Rot		= ref.bUseChart_Rot;
		dChart_Rot			= ref.dChart_Rot;
		nChart_Idx			= ref.nChart_Idx;

		bUseChart_Move_X	= ref.bUseChart_Move_X;
		iChart_Move_X		= ref.iChart_Move_X;
		bUseChart_Move_Z	= ref.bUseChart_Move_Z;
		iChart_Move_Z		= ref.iChart_Move_Z;
		bUseChart_Tilt_X	= ref.bUseChart_Tilt_X;
		iChart_Tilt_X		= ref.iChart_Tilt_X;
		bUseChart_Tilt_Z	= ref.bUseChart_Tilt_Z;
		iChart_Tilt_Z		= ref.iChart_Tilt_Z;
		
		wAlpha				= ref.wAlpha;
		wAlpha_2nd			= ref.wAlpha_2nd;
		nBoardCtrl			= ref.nBoardCtrl;
		nTestLoop			= ref.nTestLoop;

		bChart_Check		= ref.bChart_Check;

		return *this;
	};

	void Reset()
	{
		nRetryCnt			= 0;
		dwDelay				= 0;
		bTest				= TRUE;
		nTestItem			= 0;
		bUseMoveY			= FALSE;
		nMoveY				= 0;
		bUseMoveX			= FALSE;
		iMoveX				= 0;
		bUseChart_Rot		= FALSE;
		dChart_Rot			= 0;
		nChart_Idx			= 0;

		bUseChart_Move_X	= FALSE;
		iChart_Move_X		= 0;
		bUseChart_Move_Z	= FALSE;
		iChart_Move_Z		= 0;
		bUseChart_Tilt_X	= FALSE;
		iChart_Tilt_X		= 0;
		bUseChart_Tilt_Z	= FALSE;
		iChart_Tilt_Z		= 0;

		wAlpha				= 0;
		wAlpha_2nd			= 0;
		nBoardCtrl			= enBoardCtrl::BrdCtrl_NotUse;
		nTestLoop			= enTestLoop::TestLoop_NotUse;

		bChart_Check		 = FALSE;
	};

}ST_StepUnit, *PST_StepUnit;

//-----------------------------------------------------------------------------
// 스텝의 한계 수치 설정 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_StepSpec
{
	UINT nMoveZ_SpecMin;	// 100
	UINT nMoveZ_SpecMax;	// 800
	INT  iChartRot_SpecMin;	// - 30
	INT  iChartRot_SpecMax;	// 30

	_tag_StepSpec()
	{
		nMoveZ_SpecMin		= 100;
		nMoveZ_SpecMax		= 800;
		iChartRot_SpecMin	= -30;
		iChartRot_SpecMax	= 30;
	};

	_tag_StepSpec& operator= (const _tag_StepSpec& ref)
	{
		nMoveZ_SpecMin		= ref.nMoveZ_SpecMin;	
		nMoveZ_SpecMax		= ref.nMoveZ_SpecMax;	
		iChartRot_SpecMin	= ref.iChartRot_SpecMin;
		iChartRot_SpecMax	= ref.iChartRot_SpecMax;

		return *this;
	};

	void Reset()
	{
		nMoveZ_SpecMin		= 100;
		nMoveZ_SpecMax		= 800;
		iChartRot_SpecMin	= -30;
		iChartRot_SpecMax	= 30;
	};

}ST_StepSpec, *PST_StepSpec;

//-----------------------------------------------------------------------------
// 검사 스텝 설정용 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_StepInfo
{
	ST_StepSpec								StepSpec;	// 스텝에 대한 한계값(현재 : 미사용)
	CArray <ST_StepUnit, ST_StepUnit&>		StepList;	// 검사 스텝 목록

	_tag_StepInfo()
	{

	};

 	_tag_StepInfo& operator= (const _tag_StepInfo& ref)
 	{
 		StepSpec = ref.StepSpec;
 		StepList.RemoveAll();
 
 		StepList.Copy(ref.StepList);
 
 		return *this;
 	};

	// 스텝 갯수
	INT_PTR GetCount() const
	{
		return StepList.GetCount();
	};

	// 모든 스텝 삭제
	virtual void RemoveAll()
	{
		StepList.RemoveAll();
	};

	// 스텝 한계값 설정
	void Set_StepSpec			(__in const ST_StepSpec* pstStepSpec)
	{
		if (NULL != pstStepSpec)
		{
			StepSpec = *pstStepSpec;
		}
	};

	// 스텝 추가
	virtual void Step_Add		(__in ST_StepUnit stTestStep)
	{
		StepList.Add(stTestStep);
	};
	// 스텝 삽입
	virtual void Step_Insert	(__in UINT nIdex, __in ST_StepUnit stTestStep)
	{
		if (0 < GetCount())
		{
			StepList.InsertAt(nIdex, stTestStep);
		}
	};
	// 스텝 삭제
	virtual void Step_Remove	(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			StepList.RemoveAt(nIdex);
		}
	};
	// 스텝 위로 이동
	virtual void Step_Up		(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 0번 인덱스는 위로 이동 불가
			if ((0 < nIdex) && (1 < GetCount()))
			{
				ST_StepUnit stStep = StepList.GetAt(nIdex);

				StepList.RemoveAt(nIdex);
				StepList.InsertAt(nIdex - 1, stStep);
			}
		}
	};
	// 스텝 아래로 이동
	virtual void Step_Down		(__in UINT nIdex)
	{
		if (0 < GetCount())
		{
			// 마지막 인덱스는 아래로 이동 불가
			if ((nIdex < (GetCount() - 1)) && (1 < GetCount()))
			{
				ST_StepUnit stStep = StepList.GetAt(nIdex);

				StepList.RemoveAt(nIdex);

				// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
				if ((nIdex + 1) < (GetCount()))
				{
					StepList.InsertAt(nIdex + 1, stStep);
				}
				else
				{
					StepList.Add(stStep);
				}
			}
		}
	};

}ST_StepInfo, *PST_StepInfo;

//-----------------------------------------------------------------------------
// Step 정보 + Step의 검사 측정 정보
//-----------------------------------------------------------------------------
//typedef struct _tag_StepMeasInfo : public ST_StepInfo
typedef struct _tag_StepMeasInfo
{
	const ST_StepInfo*		pStepInfo;
	const ST_TestItemInfo*	pTestItemInfo;

	CArray <ST_TestItemMeas, ST_TestItemMeas&>	TestMeasList;	// 검사 스텝의 검사 항목 측정값

	_tag_StepMeasInfo()
	{
		pStepInfo		= NULL;
		pTestItemInfo	= NULL;
	};

	_tag_StepMeasInfo& operator= (const _tag_StepMeasInfo& ref)
	{
		pStepInfo		= ref.pStepInfo;
		pTestItemInfo	= ref.pTestItemInfo;

		TestMeasList.RemoveAll();
		TestMeasList.Copy(ref.TestMeasList);

		return *this;
	};

	INT_PTR GetCount() const
	{
		return TestMeasList.GetCount();
	};

	virtual void RemoveAll()
	{
		TestMeasList.RemoveAll();
	};

	void ResetData()
	{
		for (UINT nStepIdx = 0; nStepIdx < TestMeasList.GetCount(); nStepIdx++)
		{
			TestMeasList[nStepIdx].Reset_MeasurementData();
		}
	};

	// 스텝 정보와 검사 아이템 스펙 정보를 이용해서 측정 데이터 기본값 설정
	void UpdateStepInfo_TestItemInfo()
	{
		RemoveAll();

		if ((NULL != pStepInfo) && (NULL != pTestItemInfo))
		{
			for (UINT nStepIdx = 0; nStepIdx < pStepInfo->GetCount(); nStepIdx++)
			{
				ST_TestItemMeas stMeas;

				if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
				{
					stMeas.SetTestItemSpec(((ST_TestItemInfo*)pTestItemInfo)->GetTestItem(pStepInfo->StepList[nStepIdx].nTestItem));
				}

				TestMeasList.Add(stMeas);
			}
		}
		else
		{
			TRACE(_T("pStepInfo == NULL or pTestItemInfo == NULL\n"));
		}
	};

	// 스텝 정보와 검사 아이템 스펙 정보 설정
	void SetStepInfo_TestItemInfo(__in const ST_StepInfo* pInStepInfo, __in const ST_TestItemInfo* pInTestItemInfo)
	{
		pStepInfo		= pInStepInfo;
		pTestItemInfo	= pInTestItemInfo;

		UpdateStepInfo_TestItemInfo();
	};

	// 측정 데이터 입력
	void Set_Measurement		(__in UINT nStepIdx, __in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		if (nStepIdx < TestMeasList.GetCount())
		{
			TestMeasList[nStepIdx].Set_Measurement(nMeasCount, pMeasurValue, dwDuration);
		}
	};

	// 측정 데이터 입력 (결과 판정 안함)
	void Set_Measurement_NoJudge(__in UINT nStepIdx, __in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		if (nStepIdx < TestMeasList.GetCount())
		{
			TestMeasList[nStepIdx].Set_Measurement_NoJudge(nMeasCount, pMeasurValue, dwDuration);
		}
	};

	// 결과 판정
	void Set_Judgment			(__in UINT nStepIdx, __in UINT nIN_Judgment)
	{
		if (nStepIdx < TestMeasList.GetCount())
		{
			TestMeasList[nStepIdx].Set_Judgment(nIN_Judgment);
		}
	};

	// 진행 시간
	void Set_ElapTime(__in UINT nStepIdx, __in DWORD dwDuration = 0)
	{
		if (nStepIdx < TestMeasList.GetCount())
		{
			TestMeasList[nStepIdx].Set_ElapTime(dwDuration);
		}
	};

	// 측정 데이터 초기화
	void Reset_MeasurementData	()
	{
		for (UINT nIdx = 0; nIdx < TestMeasList.GetCount(); nIdx++)
		{
			TestMeasList[nIdx].Reset_MeasurementData();
		}
	};

	// 측정 데이터 구하기
	ST_TestItemMeas* GetMeasurmentData(__in UINT nTestItem)
	{
		BOOL bFind = FALSE;

		for (UINT nIdx = 0; nIdx < TestMeasList.GetCount(); nIdx++)
		{
			if (nTestItem == TestMeasList[nIdx].nItemID)
			{
				return &TestMeasList.GetAt(nIdx);
			}
		}

		return NULL;
	};

	// 불량 판정된 측정 데이터 구하기
	ST_TestItemMeas* GetMeasurmentData_Fail(__in UINT nTestItem, __out UINT& nIndex)
	{
		BOOL bFind = FALSE;

		for (UINT nIdx = 0; nIdx < TestMeasList.GetCount(); nIdx++)
		{
			if (nTestItem == TestMeasList[nIdx].nItemID)
			{
				TestMeasList[nIdx].nJudgmentAll;

				return &TestMeasList.GetAt(nIdx);
			}
		}

		return NULL;
	};

}ST_StepMeasInfo, *PST_StepMeasInfo;

//-----------------------------------------------------------------------------
// 프리셋 모드 정보 - 유형별 검사 스텝정보 적용
//-----------------------------------------------------------------------------
#define		MAX_TESTSTEP_PRESET		10	// Preset 모드 StepInfo 개수
typedef struct _tag_PresetLink
{
	// 불량 유형별 설정 프리셋
	CString			szAlias;		// 명칭
	UINT			nPresetType;	// 프리셋 유형
	//UINT			nStepInfoIndex;	// 프리셋에 적용될 스텝정보 배열 인덱스

	_tag_PresetLink()
	{
		nPresetType		= 0;
		//nStepInfoIndex	= 0;
	};

	_tag_PresetLink& operator= (const _tag_PresetLink& ref)
	{
		szAlias			= ref.szAlias;
		nPresetType		= ref.nPresetType;
		//nStepInfoIndex	= ref.nStepInfoIndex;
		
		return *this;
	};

	void Reset()
	{
		szAlias.Empty();
		nPresetType		= 0;
		//nStepInfoIndex	= 0;
	};

// 	void Set_StepInfoIndex(UINT nIndex)
// 	{
// 		nStepInfoIndex	= __min(nIndex, MAX_TESTSTEP_PRESET - 1);
// 	};
}ST_PresetLink;

typedef struct _tag_PresetStepInfo
{
	BOOL			bUsePresetMode;	// 프리셋 사용여부

	ST_StepInfo		stStepInfo[MAX_TESTSTEP_PRESET];

	// 불량 유형별 설정 프리셋
	ST_PresetLink	stPresetLink[MAX_TESTSTEP_PRESET];

	UINT			nUsePresetFileCount;

	_tag_PresetStepInfo()
	{
		bUsePresetMode		= FALSE;
		nUsePresetFileCount = MAX_TESTSTEP_PRESET;
	};

	_tag_PresetStepInfo& operator= (const _tag_PresetStepInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
		{
			stStepInfo[nIdx]	= ref.stStepInfo[nIdx];
			stPresetLink[nIdx]	= ref.stPresetLink[nIdx];
		}

		bUsePresetMode		= ref.bUsePresetMode;
		nUsePresetFileCount = ref.nUsePresetFileCount;

		return *this;
	};

	void Set_UsePresetFileCount(UINT nCount)
	{
		nUsePresetFileCount = __min(nCount, MAX_TESTSTEP_PRESET);
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
		{
			stStepInfo[nIdx].RemoveAll();
			stPresetLink[nIdx].Reset();
		}

		bUsePresetMode		= FALSE;
		nUsePresetFileCount = MAX_TESTSTEP_PRESET;
	};

}ST_PresetStepInfo;



#pragma pack(pop)

#endif // Def_TestItem_Cm_h__
