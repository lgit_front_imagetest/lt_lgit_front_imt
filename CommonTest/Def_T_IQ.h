//*****************************************************************************
// Filename	: 	Def_T_IQ.h
// Created	:	2017/11/13 - 11:51
// Modified	:	2017/11/13 - 11:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#ifndef Def_T_IQ_h__
#define Def_T_IQ_h__

#include <afxwin.h>
#include "Def_UI_SFR.h"
#include "Def_UI_Defect_Black.h"
#include "Def_UI_Defect_White.h"
#include "Def_UI_OpticalCenter.h"
#include "Def_UI_Rotation.h"
#include "Def_UI_Ymean.h"
#include "Def_UI_LCB.h"
#include "Def_UI_BlackSpot.h"
#include "Def_UI_Current.h"
#include "Def_UI_Torque.h"
#include "Def_UI_ActiveAlign.h"
#include "Def_UI_Shading.h"
#include "Def_UI_FOV.h"

#pragma pack(push,1)


typedef enum enSpec_IQ
{

	Spec_IQ_Item_01,
	Spec_IQ_Item_02,
	Spec_IQ_Item_03,
	Spec_IQ_Item_04,
	Spec_IQ_Item_05,
	Spec_IQ_Item_06,
	Spec_IQ_Item_07,
	Spec_IQ_Item_08,
	Spec_IQ_Item_09,
	Spec_IQ_Item_10,
	Spec_IQ_Item_11,
	Spec_IQ_Item_12,
	Spec_IQ_MaxNum
};

static LPCTSTR	g_szSpecIQ[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};
typedef struct _tag_OpticalCenter_Data_IQ
{
	// 결과
	UINT nResult;
	UINT nDetectResult;

	// 양불
	UINT nResultX; //BOOL 형식으로 변경
	UINT nResultY;

	// 결과 값
	UINT nResultPosX; //Posx 결과 값
	UINT nResultPosY;

	UINT nPixX;
	UINT nPixY;

	int iStandPosDevX; //Offset 결과 값
	int iStandPosDevY;


	void Reset()
	{
		nResult = 1;
		nDetectResult = 0;
		nResultX = 1;
		nResultY = 1;
		nResultPosX = 0;
		nResultPosY = 0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	void FailData(){
		nResult = 0;
		nDetectResult = 0;
		nResultX = 0;
		nResultY = 0;
		nResultPosX = 0;
		nResultPosY = 0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	_tag_OpticalCenter_Data_IQ()
	{
		Reset();
	};

	_tag_OpticalCenter_Data_IQ& operator= (_tag_OpticalCenter_Data_IQ& ref)
	{
		nResult = ref.nResult;
		nResultX = ref.nResultX;
		nResultY = ref.nResultY;
		nResultPosX = ref.nResultPosX;
		nResultPosY = ref.nResultPosY;
		iStandPosDevX = ref.iStandPosDevX;
		iStandPosDevY = ref.iStandPosDevY;
		nDetectResult = ref.nDetectResult;
		nPixX = ref.nPixX;
		nPixY = ref.nPixY;

		return *this;
	};

}ST_OpticalCenter_Data_IQ, *PST_OpticalCenter_Data_IQ;

typedef struct _tag_Result_Rotation_IQ
{
	CRect		ptROI[ROI_Rotation_Max];		// 보정된 ROI

	double		dbRotation;
	BOOL		bRotation;

	_tag_Result_Rotation_IQ()
	{
		Reset();
	};

	void Reset()
	{
		dbRotation = 0.0;

		bRotation = TRUE;

		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI].left = 0;
			ptROI[nROI].right = 0;
			ptROI[nROI].top = 0;
			ptROI[nROI].bottom = 0;

		}
	};

	_tag_Result_Rotation_IQ& operator= (_tag_Result_Rotation_IQ& ref)
	{
		dbRotation = ref.dbRotation;

		bRotation = ref.bRotation;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];


		}

		return *this;
	};

}ST_Result_Rotation_IQ, *PST_Result_Rotation_IQ;


typedef struct _tag_Result_Distortion_IQ
{
	CRect		ptROI[ROI_Rotation_Max];		// 보정된 ROI

	double		dbDistortion;
	BOOL		bDistortion;

	_tag_Result_Distortion_IQ()
	{
		Reset();
	};

	void Reset()
	{
		dbDistortion = 0.0;

		bDistortion = TRUE;

		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI].left = 0;
			ptROI[nROI].right = 0;
			ptROI[nROI].top = 0;
			ptROI[nROI].bottom = 0;

		}
	};

	_tag_Result_Distortion_IQ& operator= (_tag_Result_Distortion_IQ& ref)
	{
		dbDistortion = ref.dbDistortion;
		bDistortion = ref.bDistortion;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Distortion_IQ, *PST_Result_Distortion_IQ;

typedef struct _tag_Result_FOV_IQ
{
	CRect		ptROI[ROI_Rotation_Max];		// 보정된 ROI

	double		dbDFOV;
	double		dbHFOV;
	double		dbVFOV;
	BOOL		bFOV_All;
	BOOL		bDFOV;
	BOOL		bHFOV;
	BOOL		bVFOV;

	_tag_Result_FOV_IQ()
	{
		Reset();
	};

	void Reset()
	{
		dbDFOV		= 0.0;
		dbHFOV		= 0.0;
		dbVFOV		= 0.0;

		bFOV_All	= TRUE;
		bDFOV		= TRUE;
		bHFOV		= TRUE;
		bVFOV		= TRUE;
		
		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI].left = 0;
			ptROI[nROI].right = 0;
			ptROI[nROI].top = 0;
			ptROI[nROI].bottom = 0;

		}
	};

	_tag_Result_FOV_IQ& operator= (_tag_Result_FOV_IQ& ref)
	{
		dbDFOV = ref.dbDFOV;
		dbHFOV = ref.dbHFOV;
		dbVFOV = ref.dbVFOV;

		bFOV_All = ref.bFOV_All;
		bDFOV = ref.bDFOV;
		bHFOV = ref.bHFOV;
		bVFOV = ref.bVFOV;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_FOV_IQ, *PST_Result_FOV_IQ;

typedef struct _tag_Result_Ymean_IQ
{
	bool bYmeanResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[YMEAN_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_Ymean_IQ()
	{
		Reset();
	};

	void Reset()
	{
		bYmeanResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_Ymean_IQ& operator= (_tag_Result_Ymean_IQ& ref)
	{
		bYmeanResult = ref.bYmeanResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_Ymean_IQ, *PST_Result_Ymean_IQ;


typedef struct _tag_Result_LCB_IQ
{
	bool bLCBResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[LCB_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_LCB_IQ()
	{
		Reset();
	};

	void Reset()
	{
		bLCBResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_LCB_IQ& operator= (_tag_Result_LCB_IQ& ref)
	{
		bLCBResult = ref.bLCBResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_LCB_IQ, *PST_Result_LCB_IQ;


typedef struct _tag_Result_BlackSpot_IQ
{
	bool bBlackSpotResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[BlackSpot_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_BlackSpot_IQ()
	{
		Reset();
	};

	void Reset()
	{
		bBlackSpotResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_BlackSpot_IQ& operator= (_tag_Result_BlackSpot_IQ& ref)
	{
		bBlackSpotResult = ref.bBlackSpotResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_BlackSpot_IQ, *PST_Result_BlackSpot_IQ;



typedef struct _tag_Result_SFR_IQ
{
	CRect		rtROI[ROI_SFR_Max];		// 보정된 ROI

	BOOL		bResult[ROI_SFR_Max];	// 개별 결과
	double		dbValue[ROI_SFR_Max];	// 개별 결과 값

	_tag_Result_SFR_IQ()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI].SetRectEmpty();

			bResult[nROI] = TRUE;
			dbValue[nROI] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (FALSE == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_SFR_IQ& operator= (_tag_Result_SFR_IQ& ref)
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];

			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
		}

		return *this;
	};

}ST_Result_SFR_IQ, *PST_Result_SFR_IQ;

typedef struct _tag_Result_Defect_Black_IQ
{
	bool bDefect_BlackResult; // 판정 결과

	int nDefect_BlackCount; //
	CPoint	ptROI[Defect_Black_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_Black_IQ()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_BlackResult = true;
		nDefect_BlackCount = 0; //


		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_Black_IQ& operator= (_tag_Result_Defect_Black_IQ& ref)
	{
		bDefect_BlackResult = ref.bDefect_BlackResult;
		nDefect_BlackCount = ref.nDefect_BlackCount;


		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_Black_IQ, *PST_Result_Defect_Black_IQ;


typedef struct _tag_Result_Defect_White_IQ
{
	bool bDefect_WhiteResult; // 판정 결과

	int nDefect_WhiteCount; //
	CPoint	ptROI[Defect_White_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_White_IQ()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_WhiteResult = true;
		nDefect_WhiteCount = 0; //


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_White_IQ& operator= (_tag_Result_Defect_White_IQ& ref)
	{
		bDefect_WhiteResult = ref.bDefect_WhiteResult;
		nDefect_WhiteCount = ref.nDefect_WhiteCount;


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_White_IQ, *PST_Result_Defect_White_IQ;


typedef struct _tag_Result_Current_IQ
{
	BOOL		bResult[Current_Max_Front];	// 개별 결과
	double		dbValue[Current_Max_Front];	// 개별 결과 값
	UINT		nEachResult[Current_Max_Front];	// 개별 결과

	_tag_Result_Current_IQ()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < Current_Max_Front; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
			nEachResult[nCH] = 0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < Current_Max_Front; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Current_IQ& operator= (_tag_Result_Current_IQ& ref)
	{
		for (UINT nROI = 0; nROI < 3; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
			nEachResult[nROI] = ref.nEachResult[nROI];
		}
	};


}ST_Result_Current_IQ, *PST_Result_Current_IQ;

typedef struct _tag_Result_DynamicBW_IQ
{
	CPoint		ptROI[ROI_DnyBW_Max];		// 보정된 ROI

	double		dbDynamic;
	double		dbSNR_BW;

	BOOL		bDynamic;
	BOOL		bSNR_BW;

	double dbVarianceValue[ROI_DnyBW_Max]; //분산
	double dbAverageValue[ROI_DnyBW_Max];  //평균

	_tag_Result_DynamicBW_IQ()
	{
		Reset();
	};

	void Reset()
	{
		dbDynamic = 0.0;
		dbSNR_BW = 0.0;

		bDynamic = TRUE;
		bSNR_BW = TRUE;

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;

			dbVarianceValue[nROI] = 0.0;
			dbAverageValue[nROI] = 0.0;
		}
	};

	_tag_Result_DynamicBW_IQ& operator= (_tag_Result_DynamicBW_IQ& ref)
	{
		dbDynamic = ref.dbDynamic;
		dbSNR_BW = ref.dbSNR_BW;

		bDynamic = ref.bDynamic;
		bSNR_BW = ref.bSNR_BW;

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];

			dbVarianceValue[nROI] = ref.dbVarianceValue[nROI];
			dbAverageValue[nROI] = ref.dbAverageValue[nROI];
		}

		return *this;
	};

}ST_Result_DynamicBW_IQ, *PST_Result_DynamicBW_IQ;

typedef struct _tag_Result_Shading_IQ
{
	CPoint		ptHorROI[ROI_Shading_Max];			// 보정된 ROI
	CPoint		ptVerROI[ROI_Shading_Max];			// 보정된 ROI
	CPoint		ptDiaAROI[ROI_Shading_Max];			// 보정된 ROI
	CPoint		ptDiaBROI[ROI_Shading_Max];			// 보정된 ROI
	double		dbHorizonValue[ROI_Shading_Max];	// 개별 결과 값
	double		dbVerticalValue[ROI_Shading_Max];	// 개별 결과 값
	double		dbDiaAValue[ROI_Shading_Max];		// 개별 결과 값
	double		dbDiaBValue[ROI_Shading_Max];		// 개별 결과 값

	//@SH _180808: LGIT dll 결과가 아니라 UI에서 판별한 결과값 저장
	bool		bHorizonResult[ROI_Shading_Max];	// 개별 결과
	bool		bVerticalResult[ROI_Shading_Max];	// 개별 결과
	bool		bDiaAResult[ROI_Shading_Max];		// 개별 결과
	bool		bDiaBResult[ROI_Shading_Max];		// 개별 결과

	_tag_Result_Shading_IQ()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nROI = 0; nROI < ROI_Shading_Max; nROI++)
		{
			ptHorROI[nROI].x = 0;
			ptHorROI[nROI].y = 0;

			ptVerROI[nROI].x = 0;
			ptVerROI[nROI].y = 0;

			ptDiaAROI[nROI].x = 0;
			ptDiaAROI[nROI].y = 0;

			ptDiaBROI[nROI].x = 0;
			ptDiaBROI[nROI].y = 0;

			bHorizonResult[nROI] = true;
			dbHorizonValue[nROI] = 0.0;

			bVerticalResult[nROI] = true;
			dbVerticalValue[nROI] = 0.0;

			bDiaAResult[nROI] = true;
			dbDiaAValue[nROI] = 0.0;

			bDiaBResult[nROI] = true;
			dbDiaBValue[nROI] = 0.0;

		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < ROI_Shading_Max; nROI++)
		{
			if (false == bHorizonResult[nROI])
			{
				return FALSE;
			}
			if (false == bVerticalResult[nROI])
			{
				return FALSE;
			}
			if (false == bDiaAResult[nROI])
			{
				return FALSE;
			}
			if (false == bDiaBResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Shading_IQ& operator= (_tag_Result_Shading& ref)
	{
		for (UINT nROI = 0; nROI < ROI_Shading_Max; nROI++)
		{
			ptHorROI[nROI] = ref.ptHorROI[nROI];
			ptVerROI[nROI] = ref.ptVerROI[nROI];
			ptDiaAROI[nROI] = ref.ptDiaAROI[nROI];
			ptDiaBROI[nROI] = ref.ptDiaBROI[nROI];

			bHorizonResult[nROI] = true;
			dbHorizonValue[nROI] = 0.0;

			bVerticalResult[nROI] = true;
			dbVerticalValue[nROI] = 0.0;

			bDiaAResult[nROI] = true;
			dbDiaAValue[nROI] = 0.0;

			bDiaBResult[nROI] = true;
			dbDiaBValue[nROI] = 0.0;
		}

		return *this;
	};

}ST_Result_Shading_IQ, *PST_Result_Shading_IQ;
//typedef struct _tag_Result_Current_IQ
//{
//	bool		bCurrentResult; // 판정 결과
//	double		dbValue;	// 개별 결과 값
//
//	_tag_Result_Current_IQ()
//	{
//		Reset();
//	};
//
//	void Reset()
//	{
//		bCurrentResult = true;
//		dbValue = 0.0;
//
//	};
//
//	_tag_Result_Current_IQ& operator= (_tag_Result_Current_IQ& ref)
//	{
//		bCurrentResult = ref.bCurrentResult;
//		dbValue = ref.dbValue;
//
//
//		return *this;
//	};
//
//}ST_Result_Current_IQ, *PST_Result_Current_IQ;

typedef struct _tag_IQ_Info
{
	ST_UI_Current			stCurrent;
	ST_UI_SFR				stSFR;
	ST_UI_Defect_Black		stDefect_Black;
	ST_UI_Defect_White		stDefect_White;
	ST_OpticalCenter_Opt	stOpticalCenter;
	ST_UI_Rotation			stRotation;
	ST_UI_Ymean				stYmean;
	ST_UI_BlackSpot			stBlackSpot;
	ST_UI_LCB				stLCB;
	//ST_UI_Torque			stTorque;
	ST_UI_ActiveAlign		stActiveAlign;
	ST_UI_Rotation			stDistortion;
	ST_UI_FOV				stFOV;
	ST_UI_DynamicBW			stDynamicBW;
	ST_UI_Shading			stShading;

	_tag_IQ_Info()
	{
		Reset();
	};

	_tag_IQ_Info& operator= (const _tag_IQ_Info& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_IQ_Info, *PST_IQ_Info;

typedef struct _tag_IQ_Result_IQ
{

	CString szFileName;

	ST_Result_SFR_IQ			stSFR;
	ST_OpticalCenter_Data_IQ	stOpticalCenter;
	ST_Result_Rotation_IQ		stRotate;
	ST_Result_Ymean_IQ			stYmean;
	ST_Result_LCB_IQ			stLCB;
	ST_Result_BlackSpot_IQ		stBlackSpot;
	ST_Result_Current_IQ		stCurrent;
	ST_Result_Defect_Black_IQ	stDefect_Black;
	ST_Result_Defect_White_IQ	stDefect_White;
	ST_Result_FOV_IQ			stFOV;
	ST_Result_Distortion_IQ		stDistortion;
	ST_Result_DynamicBW_IQ		stDynamicBW;
	ST_Result_Shading_IQ		stShading;

	_tag_IQ_Result_IQ()
	{
		szFileName.Empty();

		Reset();
	};

	_tag_IQ_Result_IQ& operator= (const _tag_IQ_Result_IQ& ref)
	{
		szFileName = ref.szFileName;

		return *this;
	};

	void Reset()
	{
		stSFR.Reset();
	};

}ST_IQ_Result, *PST_IQ_Result;

//typedef struct _tag_IQ_Spec
//{
//	_tag_IQ_Spec()
//	{
//		Reset();
//	};
//
//	_tag_IQ_Spec& operator= (const _tag_IQ_Spec& ref)
//	{
//
//		return *this;
//	};
//
//	void Reset()
//	{
//		
//	};
//
//}ST_IQ_Spec, *PST_IQ_Spec;
//
//typedef struct _tag_IQ_Info
//{
//	_tag_IQ_Info()
//	{
//		Reset();
//	};
//
//	_tag_IQ_Info& operator= (const _tag_IQ_Info& ref)
//	{
//		return *this;
//	};
//
//	void Reset()
//	{
//		
//	};
//
//}ST_IQ_Info, *PST_IQ_Info;




#pragma pack(pop)

#endif // Def_T_IQ_h__
