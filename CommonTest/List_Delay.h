﻿#ifndef List_Delay_h__
#define List_Delay_h__

#pragma once

#include "Def_DataStruct_Cm.h"



typedef enum enListItemNum_Delay
{
	DelOp_ItemNum = 100,
};


// List_SFRInfo

class CList_Delay : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Delay)

public:
	CList_Delay();
	virtual ~CList_Delay();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	ST_RecipeInfo_Base*  m_pstRecipeInfo;
	void SetPtr_RecipeInfo (ST_RecipeInfo_Base* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstRecipeInfo = pstRecipeInfo;
	};


protected:

	DECLARE_MESSAGE_MAP()

	CFont	m_Font;
	CEdit	m_ed_CellEdit;
	UINT	m_nEditCol;
	UINT	m_nEditRow;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData		(UINT nRow, UINT nCol, double dbValue);

public:

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusECpOpellEdit();
};

#endif // List_SFRInfo_h__
