// CList_3DCal_ROI
#ifndef List_3DCal_ROI_h__
#define List_3DCal_ROI_h__

#pragma once


#include "Def_DataStruct_Cm.h"
#include "Def_T_3DCal.h"

typedef enum enListNum_3DCal_ROI
{
	ROI_3D_Object = 0,
	ROI_3D_U,
	ROI_3D_V,
	ROI_3D_W,
	ROI_3D_H,
	ROI_3D_MaxCol,
};

static LPCTSTR	g_lpszHeader_3DCal_ROI[] =
{
	_T("No"),	// ROI_3D_Object
	_T("U"),	// ROI_3D_U,
	_T("V"),	// ROI_3D_V,
	_T("W"),	// ROI_3D_W,
	_T("H"),	// ROI_3D_H,
	NULL
};

typedef enum enListItemNum_3DCal_ROI
{
	ROI_3D_ItemNum = MAX_3DCAL_ROI,
};

const int	iListAglin_3DCal_ROI[] =
{
	LVCFMT_LEFT,		// ROI_3D_Object
	LVCFMT_CENTER,		// ROI_3D_U,
	LVCFMT_CENTER,		// ROI_3D_V,
	LVCFMT_CENTER,		// ROI_3D_W,
	LVCFMT_CENTER,		// ROI_3D_H,
};

const int	iHeaderWidth_3DCal_ROI[] =
{
	95,		// ROI_3D_Object
	95,		// ROI_3D_U,
	95,		// ROI_3D_V,
	95,		// ROI_3D_W,
	95,		// ROI_3D_H,
};

class CList_3DCal_ROI : public CListCtrl
{
	DECLARE_DYNAMIC(CList_3DCal_ROI)

public:
	CList_3DCal_ROI();
	virtual ~CList_3DCal_ROI();

	void		InsertFullData			(__in ST_3DCal_Depth_Para* pst3DCal_Para);
	void		GetCellData				(__out ST_3DCal_Depth_Para& stOut3DCal_Para);

protected:

	ST_3DCal_Depth_Para	m_st3DCal_Para;

	CFont	m_Font;
	CEdit	m_ed_CellEdit;
	UINT	m_nEditCol;
	UINT	m_nEditRow;

	BOOL	UpdateCellData(UINT nRow, UINT nCol, int  iValue);

	void	InitHeader();
	void	SetRectRow(UINT nRow);

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void	OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusECpOpellEdit();
};

#endif // List_3DCal_ROI_h__