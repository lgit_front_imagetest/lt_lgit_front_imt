﻿#ifndef List_DynamicBW_h__
#define List_DynamicBW_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_UI_DynamicBW.h"

typedef enum enListItemNum_DynamicOp 
{
	DyOp_ItemNum = ROI_DnyBW_Max,
};

//-----------------------------------------------------------------------------
// List_DynamicInfo
//-----------------------------------------------------------------------------
class CList_DynamicBW : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_DynamicBW)

public:
	CList_DynamicBW();
	virtual ~CList_DynamicBW();

protected:

	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow				(CREATESTRUCT& cs);

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel				(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusComboColor		();
	afx_msg void	OnEnSelectComboColor		();
	
	afx_msg void	OnEnKillFocusComboOverlay	();
	afx_msg void	OnEnSelectComboOverlay		();

	afx_msg void	OnEnKillFocusEdit			();

	BOOL			UpdateCellData				(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData			(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;

	UINT				m_nEditCol		= 0;
	UINT				m_nEditRow		= 0;

	CMFCMaskedEdit		m_ed_CellEdit;
	CComboBox			m_cb_Color;
	CComboBox			m_cb_Overlay;

	ST_UI_DynamicBW*	m_pstConfig = NULL;
	
public:

	void SetPtr_ConfigInfo(ST_UI_DynamicBW* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};


#endif // List_DynamicInfo_h__
