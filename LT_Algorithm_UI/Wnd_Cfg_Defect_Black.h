﻿#ifndef Wnd_Cfg_Defect_Black_h__
#define Wnd_Cfg_Defect_Black_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Defect_Black.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Defect_Black
//-----------------------------------------------------------------------------
class CWnd_Cfg_Defect_Black : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Defect_Black)

public:
	CWnd_Cfg_Defect_Black();
	virtual ~CWnd_Cfg_Defect_Black();

	enum enDefect_BlackStatic
	{
		STI_DB_INSPECT = 0,
		STI_DB_SPEC,
		STI_DB_PARAMETER,
		STI_DB_DataForamt,
		STI_DB_OutMode,
		STI_DB_SensorType,
		STI_DB_BlackLevel, //공통
		STI_DB_BlockSize,
		STI_DB_NormalDefectRatio,
		STI_DB_VeryDefectRatio,
		STI_DB_MaxNormalDefectNum,
		STI_DB_MaxVeryDefectNum,
		STI_DB_NormalDefectCount,
		STI_DB_VeryDefectCount,
		STI_DB_8bitUse,
		STI_DB_Type,
		STI_DB_MAX,
	};

	enum enDefect_BlackButton
	{
		BTN_DB_MAX = 1,
	};

	enum enDefect_BlackComobox
	{
		CMB_DB_DataForamt = 0,
		CMB_DB_OutMode,
		CMB_DB_SensorType,
		CMB_DB_EnableCircle,
		CMB_DB_8bitUse,
		CMB_DB_Type,
		CMB_DB_MAX,
	};

	enum enDefect_BlackEdit
	{
		EDT_DB_BlackLevel = 0,
		EDT_DB_BlockSize,
		EDT_DB_NormalDefectRatio,
		EDT_DB_VeryDefectRatio,
		EDT_DB_MaxNormalDefectNum,
		EDT_DB_MaxVeryDefectNum,
		EDT_DB_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_DB_MAX];
	CButton						m_bn_Item[BTN_DB_MAX];
	CComboBox					m_cb_Item[CMB_DB_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DB_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Defect_Black_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Defect_Black_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Defect_Black_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Defect_Black_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Defect_Black_MAX];

	ST_UI_Defect_Black*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Defect_Black* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Defect_Black_h__
