﻿#ifndef Wnd_Cfg_Ymean_h__
#define Wnd_Cfg_Ymean_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Ymean.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Ymean
//-----------------------------------------------------------------------------
class CWnd_Cfg_Ymean : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Ymean)

public:
	CWnd_Cfg_Ymean();
	virtual ~CWnd_Cfg_Ymean();

	enum enYmeanStatic
	{
		STI_BLS_INSPECT = 0,
		STI_BLS_SPEC,
		STI_BLS_DataForamt,
		STI_BLS_OutMode,
		STI_BLS_SensorType,
		STI_BLS_BlackLevel,
		STI_BLS_DefectBlockSize,
		STI_BLS_EdgeSize,
		STI_BLS_CenterThreshold,
		STI_BLS_EdgeThreshold,
		STI_BLS_CornerThreshold,
		STI_BLS_LscBlockSize,
		STI_BLS_EnableCircle,
		STI_BLS_PosOffsetX,
		STI_BLS_PosOffsetY,
		STI_BLS_RadiusRationX,
		STI_BLS_RadiusRationY,
		STI_BLS_Count,
		STI_BLS_PARAM,
		STI_BLS_8bitUse,
		STI_BLS_MAX,
	};

	enum enYmeanButton
	{
		BTN_BLS_MAX = 1,
	};

	enum enYmeanComobox
	{
		CMB_BLS_DataForamt = 0,
		CMB_BLS_OutMode,
		CMB_BLS_SensorType,
		CMB_BLS_EnableCircle,
		CMB_BLS_8bitUse,
		CMB_BLS_MAX,
	};

	enum enYmeanEdit
	{
		EDT_BLS_BlackLevel = 0,
		EDT_BLS_DefectBlockSize,
		EDT_BLS_EdgeSize,
		EDT_BLS_CenterThreshold,
		EDT_BLS_EdgeThreshold,
		EDT_BLS_CornerThreshold,
		EDT_BLS_LscBlockSize,
		EDT_BLS_PosOffsetX,
		EDT_BLS_PosOffsetY,
		EDT_BLS_RadiusRationX,
		EDT_BLS_RadiusRationY,
		EDT_BLS_Count,
		EDT_BLS_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_BLS_MAX];
	CButton						m_bn_Item[BTN_BLS_MAX];
	CComboBox					m_cb_Item[CMB_BLS_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_BLS_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Ymean_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Ymean_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Ymean_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Ymean_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Ymean_MAX];

	ST_UI_Ymean*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Ymean* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
	void	Enable_Setting		(__in BOOL bEnable);
};

#endif // Wnd_Cfg_Ymean_h__
