// Wnd_TorqueData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_TorqueData.h"


// CWnd_TorqueData

IMPLEMENT_DYNAMIC(CWnd_Rst_Torque, CWnd)

CWnd_Rst_Torque::CWnd_Rst_Torque()
{
}

CWnd_Rst_Torque::~CWnd_Rst_Torque()
{
}


BEGIN_MESSAGE_MAP(CWnd_Rst_Torque, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_TorqueData 메시지 처리기입니다.

int CWnd_Rst_Torque::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

 	for (int t = 0; t < (int)m_nHeaderCnt; t++)
 	{
 		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Header[t].Create(g_szTorData[t], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 
 	}

	for (int i = 0; i < enTorData_inputmax; i++)
	{
		m_st_Data[i].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Data[i].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[i].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Data[i].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_Rst_Torque::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 0;
	int iSpacing	= 0;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	int HeaderW		= (iWidth + 2) / 2;

	int HeaderH = (iHeight - m_nHeaderCnt + 1) / 6;

	iLeft = iMargin;
	m_st_Header[enTorData_Header_A].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

	iLeft += HeaderW - 1;
	m_st_Header[enTorData_Header_B].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

	iLeft = iMargin;
	iTop += (HeaderH - 1);
	m_st_Data[enTorData_A].MoveWindow(iLeft, iTop, HeaderW, HeaderH * 2);

	iLeft += HeaderW - 1;
	m_st_Data[enTorData_B].MoveWindow(iLeft, iTop, HeaderW, HeaderH * 2);

	//iLeft = iMargin;
	//iTop += (HeaderH * 2 - 1);
	//m_st_Header[enTorData_Header_C].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

	//iLeft += HeaderW - 1;
	//m_st_Header[enTorData_Header_D].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

	//iLeft = iMargin;
	//iTop += (HeaderH - 1);
	//m_st_Data[enTorData_C].MoveWindow(iLeft, iTop, HeaderW, HeaderH * 2);

	//iLeft += HeaderW - 1;
	//m_st_Data[enTorData_D].MoveWindow(iLeft, iTop, HeaderW, HeaderH * 2);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Torque::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Torque::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: DataSetting
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Parameter	: BOOL MODE
// Parameter	: double dData
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_Rst_Torque::DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData)
{

// 	if (MODE == TRUE)
// 	{
// 		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	}
// 	else{
// 		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
// 	}
// 
// 	CString strData;
// 
// 
// 	strData.Format(_T("%6.3f"), dData);
// 
// 	m_st_Data[nDataIdx][nIdx].SetWindowText(strData);
}

//=============================================================================
// Method		: DataEachReset
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_Rst_Torque::DataEachReset(UINT nDataIdx, UINT nIdx)
{

// 	
// 	m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Data[nDataIdx][nIdx].SetWindowText(_T(""));

}

//=============================================================================
// Method		: DataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_Rst_Torque::DataReset()
{

// 	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
// 	{
// 		m_st_Data[0][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 		m_st_Data[0][nIdx].SetWindowText(_T(""));
// 	}
}

//=============================================================================
// Method		: DataDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_Torque_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_Rst_Torque::Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in double dbValue, __in double dbValueCnt)
{
	CString szText;

	switch (nResultIdx)
	{
	case Torque_Init:
	case Torque_Lock:
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;

	case Torque_Rele:
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case Torque_Pass:
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	default:
		break;
	}

	szText.Format(_T("%.2f / %.f"), dbValue, dbValueCnt);
	m_st_Data[nIdx].SetText(szText);

}

