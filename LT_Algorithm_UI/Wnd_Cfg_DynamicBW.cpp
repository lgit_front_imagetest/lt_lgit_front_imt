﻿// Wnd_Cfg_DynamicBW.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_DynamicBW.h"
#include "resource.h"

static LPCTSTR	g_szDynamic_Static[] =
{
	_T("INSPECT				"),
	_T("SPEC				"),
	_T("ROI					"),
	_T("Data Foramt			"),
	_T("Out Mode			"),
	_T("Sensor Type			"),
	_T("Black Level			"),
	_T("ROI Max				"),
	_T("Offset Dynamic		"),
	_T("Offset BW			"),
	_T("ROI Width			"),
	_T("ROI Height			"),
	_T("DR					"),
	_T("SNR					"),
	_T("PARAMETER			"),
	//_T("DRThreshold			"),
	//_T("SNRThreshold		"),
	_T("8BitUse				"),
	NULL
};

static LPCTSTR	g_szDynamic_Button[] =
{
	NULL
};

// CWnd_Cfg_DynamicBW
typedef enum DynamicOptID
{
	IDC_BTN_ITEM		= 1001,
	IDC_CMB_ITEM		= 2001,
	IDC_EDT_ITEM		= 3001,
	IDC_LIST_ITEM		= 4001,


	IDC_ED_SPEC_MIN		= 5001,
	IDC_ED_SPEC_MAX		= 6001,
	IDC_CHK_SPEC_MIN	= 7001,
	IDC_CHK_SPEC_MAX	= 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_DynamicBW, CWnd_Cfg_VIBase)

CWnd_Cfg_DynamicBW::CWnd_Cfg_DynamicBW()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_DynamicBW::~CWnd_Cfg_DynamicBW()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_DynamicBW, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_DynamicBW 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_DynamicBW::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

		DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_DYN_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szDynamic_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_DYN_INSPECT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_DYN_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_DYN_ROI].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_DYN_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_DYN_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szDynamic_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_DYN_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_DYN_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	for (UINT nIdex = 0; NULL < g_szDataForamt_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_DYN_DataForamt].InsertString(nIdex, g_szDataForamt_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szOutMode_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_DYN_OutMode].InsertString(nIdex, g_szOutMode_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szSensorType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_DYN_SensorType].InsertString(nIdex, g_szSensorType_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szEnable_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_DYN_8bitUse].InsertString(nIdex, g_szEnable_Static[nIdex]);
	}

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Spec_DynamicBW_MAX; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szDynamic_Static[nIdx + STI_DYN_DR], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	m_List.SetOwner(GetOwner());
	m_List.SetChang_Message(m_wm_ChangeOption);
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_DynamicBW::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	
	int iStWidth	= (iWidth - iSpacing - iSpacing ) / 6;
	int iEdWidth	= iStWidth * 2;
	int iStHeight	= 25;

	// Comm
	m_st_Item[STI_DYN_INSPECT].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	// Comm 1
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_DataForamt].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_DYN_DataForamt].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 2
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_OutMode].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_DYN_OutMode].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 3
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_SensorType].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_DYN_SensorType].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 4
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_BlackLevel].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_BlackLevel].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	//Param
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_PARAM].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_8bitUse].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_DYN_8bitUse].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_Offset_DY].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_Offset_DY].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_Offset_BW].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_Offset_BW].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_ROI_Width].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_ROI_Width].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_ROI_Height].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_ROI_Height].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_ROI_Max].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_DYN_ROI_Max].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	//iLeft = iMargin;
	//iTop += iStHeight + iSpacing;
	//m_st_Item[STI_DYN_DRThreshold].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	//iLeft += iStWidth + 2;
	//m_ed_Item[EDT_DYN_DRThreshold].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	//iLeft = iMargin;
	//iTop += iStHeight + iSpacing;
	//m_st_Item[STI_DYN_SNRThreshold].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	//iLeft += iStWidth + 2;
	//m_ed_Item[EDT_DYN_SNRThreshold].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Spec
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_DYN_SPEC].MoveWindow(iLeft, iTop, iWidth, iStHeight);


	iLeft = iMargin;
	iTop += iStHeight + iSpacing;

	int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;

	iEdWidth = iCtrlWidth - iSpacing - iStHeight;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iStHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iStHeight);

	for (UINT nIdx = 0; nIdx < Spec_DynamicBW_MAX; nIdx++)
	{
		iTop += iStHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iStHeight, iStHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iStHeight + iSpacing, iTop, iEdWidth, iStHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iStHeight, iStHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iStHeight + iSpacing, iTop, iEdWidth, iStHeight);
	}

	iLeft = iMargin;
	iTop += (iStHeight + iSpacing);
	m_st_Item[STI_DYN_ROI].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop  + iSpacing);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DynamicBW::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_DynamicBW::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfig)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_DynamicBW::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_DynamicBW::SetUpdateData()
{
	if ( NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("%d"), m_pstConfig->iMaxROICount);
	m_ed_Item[EDT_DYN_ROI_Max].SetWindowText(szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->dbOffsetDR);
	m_ed_Item[EDT_DYN_Offset_DY].SetWindowText(szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->dbOffsetSNR);
	m_ed_Item[EDT_DYN_Offset_BW].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->iMaxROIWidth);
	m_ed_Item[EDT_DYN_ROI_Width].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->iMaxROIHeight);
	m_ed_Item[EDT_DYN_ROI_Height].SetWindowText(szValue);

// 	szValue.Format(_T("%.2f"), m_pstConfig->dbDRThreshold);
// 	m_ed_Item[EDT_DYN_DRThreshold].SetWindowText(szValue);
// 
// 	szValue.Format(_T("%.2f"), m_pstConfig->dbSNRThreshold);
// 	m_ed_Item[EDT_DYN_SNRThreshold].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInspect.iBlackLevel);
	m_ed_Item[EDT_DYN_BlackLevel].SetWindowText(szValue);

	m_cb_Item[CMB_DYN_DataForamt].SetCurSel(m_pstConfig->stInspect.eDataForamt);

	m_cb_Item[CMB_DYN_OutMode].SetCurSel(m_pstConfig->stInspect.eOutMode);

	m_cb_Item[CMB_DYN_SensorType].SetCurSel(m_pstConfig->stInspect.eSensorType);

	m_cb_Item[CMB_DYN_8bitUse].SetCurSel(m_pstConfig->b8BitUse);

	for (UINT nSpec = 0; nSpec < Spec_DynamicBW_MAX; nSpec++)
	{
		szValue.Format(_T("%.2f"), m_pstConfig->stSpecMin[nSpec].dbValue);
		m_ed_SpecMin[nSpec].SetWindowText(szValue);

		szValue.Format(_T("%.2f"), m_pstConfig->stSpecMax[nSpec].dbValue);
		m_ed_SpecMax[nSpec].SetWindowText(szValue);

		m_chk_SpecMin[nSpec].SetCheck(m_pstConfig->stSpecMin[nSpec].bEnable);
		m_chk_SpecMax[nSpec].SetCheck(m_pstConfig->stSpecMax[nSpec].bEnable);
	}
		


	m_List.SetPtr_ConfigInfo(m_pstConfig);
	m_List.InsertFullData();
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_DynamicBW::GetUpdateData()
{
	if ( NULL == m_pstConfig)
		return;

	CString szValue;

	m_ed_Item[EDT_DYN_BlackLevel].GetWindowText(szValue);
	m_pstConfig->stInspect.iBlackLevel = _ttoi(szValue);

	m_ed_Item[EDT_DYN_ROI_Max].GetWindowText(szValue);
	m_pstConfig->iMaxROICount = _ttoi(szValue);

	m_ed_Item[EDT_DYN_Offset_DY].GetWindowText(szValue);
	m_pstConfig->dbOffsetDR = _ttof(szValue);

	m_ed_Item[EDT_DYN_Offset_BW].GetWindowText(szValue);
	m_pstConfig->dbOffsetSNR = _ttof(szValue);

	m_ed_Item[EDT_DYN_ROI_Width].GetWindowText(szValue);
	m_pstConfig->iMaxROIWidth = _ttoi(szValue);

	m_ed_Item[EDT_DYN_ROI_Height].GetWindowText(szValue);
	m_pstConfig->iMaxROIHeight = _ttoi(szValue);

// 	m_ed_Item[EDT_DYN_DRThreshold].GetWindowText(szValue);
// 	m_pstConfig->dbDRThreshold = _ttof(szValue);
// 
// 	m_ed_Item[EDT_DYN_SNRThreshold].GetWindowText(szValue);
// 	m_pstConfig->dbSNRThreshold = _ttof(szValue);


	for (UINT nSpec = 0; nSpec < Spec_DynamicBW_MAX; nSpec++)
	{
		m_ed_SpecMin[nSpec].GetWindowText(szValue);
		m_pstConfig->stSpecMin[nSpec].dbValue = _ttof(szValue);

		m_ed_SpecMax[nSpec].GetWindowText(szValue);
		m_pstConfig->stSpecMax[nSpec].dbValue = _ttof(szValue);

		m_pstConfig->stSpecMin[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
		m_pstConfig->stSpecMax[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
	}

	

	m_pstConfig->stInspect.eDataForamt	= (enDataForamt)m_cb_Item[CMB_DYN_DataForamt].GetCurSel();
	m_pstConfig->stInspect.eOutMode		= (enOutMode)m_cb_Item[CMB_DYN_OutMode].GetCurSel();
	m_pstConfig->stInspect.eSensorType	= (enSensorType)m_cb_Item[CMB_DYN_SensorType].GetCurSel();
	m_pstConfig->b8BitUse				= (1 == m_cb_Item[CMB_DYN_8bitUse].GetCurSel()) ? true : false;
}
