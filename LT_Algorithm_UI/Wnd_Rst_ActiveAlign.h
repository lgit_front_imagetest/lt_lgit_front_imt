#ifndef Wnd_ActiveAlignData_h__
#define Wnd_ActiveAlignData_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_ActiveAlign.h"

// CWnd_ActiveAlignData
typedef enum
{
	enAAData_OC_X		= 0,
	enAAData_OC_Y,
	enAAData_Rotate,
	enAAData_MAX,

}enumHeaderAAData;

static	LPCTSTR	g_szAAData[] =
{
	_T("Offset X"),
	_T("Offset Y"),
	_T("Rotate"),
	NULL
};

class CWnd_Rst_ActiveAlign : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_ActiveAlign)

public:
	CWnd_Rst_ActiveAlign();
	virtual ~CWnd_Rst_ActiveAlign();

protected:

	CString			m_SzHeaderName[enAAData_MAX];

	CVGStatic		m_st_Header[enAAData_MAX];
	CVGStatic		m_st_Data[enAAData_MAX];

	BOOL			m_bMode			= FALSE;
	UINT			m_nHeaderCnt	= enAAData_MAX;

	void SetDisplayMode(BOOL bMode)
	{
		m_bMode = bMode;
	};

	DECLARE_MESSAGE_MAP()

public:

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	void	DataReset					();
	void	Result_Reset				();
	void	Result_Display_OC			(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void	Result_Display_Rotate		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue);
};

#endif // Wnd_ActiveAlignData_h__
