// Wnd_ActiveAlignData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_ActiveAlign.h"

// CWnd_ActiveAlignData

IMPLEMENT_DYNAMIC(CWnd_Rst_ActiveAlign, CWnd)

CWnd_Rst_ActiveAlign::CWnd_Rst_ActiveAlign()
{
}

CWnd_Rst_ActiveAlign::~CWnd_Rst_ActiveAlign()
{
}


BEGIN_MESSAGE_MAP(CWnd_Rst_ActiveAlign, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CWnd_ActiveAlignData 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2018/3/19 - 10:08
// Desc.		:
//=============================================================================
int CWnd_Rst_ActiveAlign::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

 	for (UINT nItem = 0; nItem < m_nHeaderCnt; nItem++)
 	{
 		m_st_Header[nItem].SetStaticStyle(CVGStatic::StaticStyle_Title);
 		m_st_Header[nItem].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[nItem].SetFont_Gdip(L"Arial", 11.0F);
		m_st_Header[nItem].Create(g_szAAData[nItem], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 
		m_st_Data[nItem].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Data[nItem].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nItem].SetFont_Gdip(L"Arial", 11.0F);
		m_st_Data[nItem].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/3/19 - 10:09
// Desc.		:
//=============================================================================
void CWnd_Rst_ActiveAlign::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin	= 0;

	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth + 2 ) / 2;
	int HeaderH = (iHeight - m_nHeaderCnt + 1) / m_nHeaderCnt;

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW - 1;
		m_st_Data[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			
		iTop += (HeaderH - 1);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/3/19 - 10:10
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_ActiveAlign::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/3/19 - 10:10
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_ActiveAlign::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: DataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:12
// Desc.		:
//=============================================================================
void CWnd_Rst_ActiveAlign::DataReset()
{
	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nIdx].SetWindowText(_T(""));
	}

}

void CWnd_Rst_ActiveAlign::Result_Reset()
{
	for (UINT nIdx = 0; nIdx < enAAData_MAX; nIdx++)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nIdx].SetWindowText(_T(""));

		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nIdx].SetWindowText(_T(""));
	}
}

//=============================================================================
// Method		: Result_Display_OC
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in int nValue
// Qualifier	:
// Last Update	: 2018/10/25 - 22:12
// Desc.		:
//=============================================================================
void CWnd_Rst_ActiveAlign::Result_Display_OC(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%d"), nValue);

	m_st_Data[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: Result_Display_Rotate
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/10/23 - 15:06
// Desc.		:
//=============================================================================
void CWnd_Rst_ActiveAlign::Result_Display_Rotate(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%.2f"), dbValue);

	m_st_Data[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: DataDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_ActiveAlign_Data stData
// Qualifier	:
// Last Update	: 2018/3/19 - 10:13
// Desc.		:
//=============================================================================
//void CWnd_ActiveAlignData::DataDisplay(ST_ActiveAlign_Data stData)
//{
//	CString szValue;
//	
//	if (TRUE == stData.nResultX)
//	{
//		m_st_Data[enAAData_OC_X].SetColorStyle(CVGStatic::ColorStyle_Default);
//	}
//	else
//	{
//		m_st_Data[enAAData_OC_X].SetColorStyle(CVGStatic::ColorStyle_Red);
//	}
//
//	if (TRUE == stData.nResultY)
//	{
//		m_st_Data[enAAData_OC_Y].SetColorStyle(CVGStatic::ColorStyle_Default);
//	}
//	else
//	{
//		m_st_Data[enAAData_OC_Y].SetColorStyle(CVGStatic::ColorStyle_Red);
//	}
//
//	if (TRUE == stData.nResultR)
//	{
//		m_st_Data[enAAData_Rotate].SetColorStyle(CVGStatic::ColorStyle_Default);
//	}
//	else
//	{
//		m_st_Data[enAAData_Rotate].SetColorStyle(CVGStatic::ColorStyle_Red);
//	}
//
//	szValue.Format(_T("%d"), stData.iOC_X);
//	m_st_Data[enAAData_OC_X].SetWindowText(szValue);
//
//	szValue.Format(_T("%d"), stData.iOC_Y);
//	m_st_Data[enAAData_OC_Y].SetWindowText(szValue);
//
//	szValue.Format(_T("%.2f"), stData.dbRoatae);
//	m_st_Data[enAAData_Rotate].SetWindowText(szValue);
//}
