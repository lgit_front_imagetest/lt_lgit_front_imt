// Wnd_Rst_Shading.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_Shading.h"

// CWnd_Rst_Shading

IMPLEMENT_DYNAMIC(CWnd_Rst_Shading, CWnd)

CWnd_Rst_Shading::CWnd_Rst_Shading()
{
}

CWnd_Rst_Shading::~CWnd_Rst_Shading()
{
}

BEGIN_MESSAGE_MAP(CWnd_Rst_Shading, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_Rst_Shading 메시지 처리기입니다.

int CWnd_Rst_Shading::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	CString szText;

	for (UINT nIdx = 0; nIdx < enShadingData_Header; nIdx++)
	{
		szText.Format(_T("%s"), g_szSHData[nIdx]);
		m_st_Header[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Header[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Header[nIdx].Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		
	}

	for (UINT ndata = 0; ndata < enShadingEachResultMax; ndata++)
	{
		szText.Format(_T("%d"), ndata);
		m_st_NumHeader[ndata].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_NumHeader[ndata].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_NumHeader[ndata].SetFont_Gdip(L"Arial", 12.0F);
		m_st_NumHeader[ndata].Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	}
	for (UINT nResultdata = 0; nResultdata < enShadingData_MAX; nResultdata++)
	{
		m_st_Result[nResultdata].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Result[nResultdata].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nResultdata].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Result[nResultdata].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/7/29 - 13:11
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 0;
	int iSpacing	= 0;

	int iLeft		= iMargin;
	int iTop		= iMargin;

	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int HeaderW		= (iWidth) / enShadingData_Header;
	int HeaderH		= iHeight / (20);

	//Shading Header
	iLeft = iMargin;
	for (int nIdx = 0; nIdx < enShadingData_Header; nIdx++)
	{
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft += HeaderW;
	}

	//Shading 데이터 순서(0~18)
	iLeft = iMargin;
	iTop += HeaderH;

	for (int nIdx = 0; nIdx < enShadingEachResultMax; nIdx++)
	{
		m_st_NumHeader[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft = iMargin;
		iTop += HeaderH;
	}
	//Shading Horizontal
	iTop = iMargin;
	iTop += HeaderH;

	for (int nIdx = 0; nIdx < enShadingEachResultMax; nIdx++)
	{
		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft = iMargin;
		iTop += HeaderH;
	}

	//Shading Vertical
	iLeft = iMargin + HeaderW;
	iTop = iMargin;
	iTop += HeaderH;
	int nVerticalnum = enShadingEachResultMax * 2;

	for (int nIdx = enShadingEachResultMax; nIdx < nVerticalnum; nIdx++)
	{
		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft = iMargin + HeaderW;
		iTop += HeaderH;
	}

	//Shading DiagonalA
	iLeft = iMargin + HeaderW * 2;
	iTop = iMargin;
	iTop += HeaderH;

	int nDiagonalAnum = enShadingEachResultMax * 3;

	for (int nIdx = nVerticalnum; nIdx < nDiagonalAnum; nIdx++)
	{
		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft = iMargin + HeaderW * 2;
		iTop += HeaderH;
	}
	
	//Shading DiagonalB
	iLeft = iMargin + HeaderW * 3;
	iTop = iMargin;
	iTop += HeaderH;

	for (int nIdx = nDiagonalAnum; nIdx < enShadingData_MAX; nIdx++)
	{
		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft = iMargin + HeaderW * 3;
		iTop += HeaderH;
	}
// 	iLeft += HeaderW - 1;
// 	m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW / 2, HeaderH);

	iTop += HeaderH - 1;

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/7/29 - 13:12
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Shading::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/7/29 - 13:12
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Shading::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: Result_Display
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in UINT nValue
// Qualifier	:
// Last Update	: 2018/7/29 - 12:11
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%.3f"), dbValue);

	m_st_Result[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: Result_Reset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/29 - 12:10
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::Result_Reset()
{
	for (UINT nIdx = 0; nIdx < enShading_Max; nIdx++)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));

		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));
	}
}

