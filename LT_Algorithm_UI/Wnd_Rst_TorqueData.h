#ifndef Wnd_TorqueData_h__
#define Wnd_TorqueData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
//#include "Def_T_Foc.h"
#include "Def_UI_Torque.h"

// CWnd_TorqueData
typedef enum
{
	enTorData_Header_A	= 0,
	enTorData_Header_B,
	enTorData_MAX,

}enumHeaderTorData;

typedef enum
{
	enTorData_A = 0,
	enTorData_B,
	enTorData_inputmax,

}enumDataTorData;

static	LPCTSTR	g_szTorData[] =
{
	_T("Torque A "),
	_T("Torque B "),
	NULL
};

class CWnd_Rst_Torque : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Torque)

public:
	CWnd_Rst_Torque();
	virtual ~CWnd_Rst_Torque();

	UINT m_nHeaderCnt = enTorData_MAX;
	CString m_SzHeaderName[enTorData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt)
	{

		if (nHeaderCnt > enTorData_MAX)
		{
			nHeaderCnt = enTorData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader)
	{
		if (nCnt > enTorData_MAX)
		{
			nCnt = enTorData_MAX - 1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enTorData_MAX];
	CVGStatic		m_st_Data[enTorData_inputmax];

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd			(CDC* pDC);

	void DataSetting			(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset			(UINT nDataIdx, UINT nIdx);
	void DataReset				();
	//!SH _180913: 현재 상태 알수있어야됨
	//void DataDisplay			(ST_Result_Torque_Foc stData);

	void Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in double dbValue, __in double dbValueCnt);
	void Result_Reset();
};

#endif // Wnd_TorqueData_h__
