﻿#ifndef Def_UI_SFR_h__
#define Def_UI_SFR_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_SFR_ROI
{
	ROI_SFR_FST = 0,
	ROI_SFR_LST = 29,
	ROI_SFR_Max,
};

typedef struct _tag_Input_SFR
{
	CRect		rtROI;

	BOOL		bEnable;				
	UINT		nOverlayDir;
	double		dbOffset;
	
	ST_Spec		stSpecMin;
	ST_Spec		stSpecMax;

	int			iEdgeDir;				// Edge의 방향. 0: Vertical, 1 : Horizontal
	double		dbFrequencyLists;		// SFR을 구하고자 하는 공간 주파수. 0.0 ~1.0 사이의 값.nFrequencyNum 만큼의 배열.
	double		dbSFR;					// 공간 주파수를 구할 때 사용되는 기준 SFR값. 0.0 ~ 1.0 사이의 값
	int			iFrequencyNum;			// SFR을 구하고자 하는 공간 주파수 개수. 1 ~3 사이의 값

	
	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right  = rtROI.left + rtTemp.Width();
		rtROI.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_Input_SFR()
	{
		Reset();
	};

	void Reset()
	{
		rtROI.SetRectEmpty();

		bEnable				= FALSE;
		nOverlayDir			= OverlayDir_Top;
		dbOffset			= 0.0;

		stSpecMin.Reset();
		stSpecMax.Reset();

		iEdgeDir			= _EdgeDir_Vertical;
		dbFrequencyLists	= 0.0;
		dbSFR				= 0.0;
		iFrequencyNum		= 1;
	};

	_tag_Input_SFR& operator= (_tag_Input_SFR& ref)
	{
		rtROI				= ref.rtROI;
		
		bEnable				= ref.bEnable;
		nOverlayDir			= ref.nOverlayDir;
		dbOffset			= ref.dbOffset;

		stSpecMin			= ref.stSpecMin;
		stSpecMax			= ref.stSpecMax;

		iEdgeDir			= ref.iEdgeDir;
		dbFrequencyLists	= ref.dbFrequencyLists;
		dbSFR				= ref.dbSFR;
		iFrequencyNum		= ref.iFrequencyNum;
		
		return *this; 
	};

}ST_Input_SFR, *PST_Input_SFR;

// SFR 영역에 대한 옵션
typedef struct _tag_UI_SFR
{
	// 1. SetAlgorithmConfig
	double				dbMaxEdgeAngle;			// ° 허용되는 최대 Slanted Edge 기울기(default: 45.0). 0.0 초과 ~45.0 이하
	double				dbPixelSize;			// μm Pixel의 가로 크기
	enSFRType			eType;					// SFR algorithm.ESFRAlgorithm_ISO12233로 고정
	enSFRMethod			eMethod;
	bool				bEnableLog;				// SFR 디버깅을 위한 Log 생성 유무. false or true
	UINT				nAverageCount;			// MAX = 15 번
	enSFRFrequencyUnit	eConverse;				// 단위 뭐로 할 껀지

	// 2. SetDataSpec
	ST_Inspect			stInspect;

	// 3. CalcSFR
	ST_Input_SFR		stInput[ROI_SFR_Max];

	UINT nCaptureCnt;
	UINT nCaptureDelay;

	_tag_UI_SFR()
	{
		dbMaxEdgeAngle	= 45.0;
		dbPixelSize		= 1.0;
		eType			= _ESFR_ISO12233;
		eMethod			= _ESFRMethod_Freq2SFR;
		bEnableLog		= false;
		nAverageCount	= 0;
		eConverse		 = _ESFRFreq_LinePairPerMilliMeter;

		nCaptureCnt		= 0;
		nCaptureDelay	= 0;

		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			stInput[nROI].Reset();
		}
	};

	_tag_UI_SFR& operator= (_tag_UI_SFR& ref)
	{
		dbMaxEdgeAngle	= ref.dbMaxEdgeAngle;
		dbPixelSize		= ref.dbPixelSize;
		eType			= ref.eType;
		bEnableLog		= ref.bEnableLog;
		nAverageCount	= ref.nAverageCount;
		eConverse		= ref.eConverse;

		nCaptureCnt		= ref.nCaptureCnt;
		nCaptureDelay	= ref.nCaptureDelay;

		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			stInput[nIdx] = ref.stInput[nIdx];
		}

		return *this;
	};

}ST_UI_SFR, *PST_UI_SFR;

#pragma pack(pop)

#endif // Def_SFR_h__