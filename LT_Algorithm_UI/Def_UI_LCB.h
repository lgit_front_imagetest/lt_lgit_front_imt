﻿#ifndef Def_UI_LCB_h__
#define Def_UI_LCB_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

#define LCB_COUNT_MAX 500

typedef enum enLCB_Spec
{
	Spec_LCB = 0,
	Spec_LCB_MAX,
};

typedef struct _tag_UI_LCB
{
	// Inspect
	ST_Inspect		stInspect;

	// Spec
	double	 dCenterThreshold;
	double	 dEdgeThreshold;
	double	 dCornerThreshold;
	int		 nMaxSingleDefectNum;
	int		 nMinDefectWidthHeight;


	bool			bEnableCircle;						// 4개의 corner 영역을 검사 영역에서 제외하기 위한 parameter 0 : Main, Narrow Camera 1 : Wide Camera
	int				iPosOffsetX;						// Wide 영상 내 타원 검출 시 사용하는 이미지 영역의 X offset 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	int				iPosOffsetY;						// 이미지 내 타원을 찾을 때 사용하고자 하는 좌, 우 픽셀 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	double			dbRadiusRatioX;						// Wide 영상에서 검출된 타원의 가로 반지름 비율
	double			dbRadiusRatioY;						// Wide 영상에서 검출된 타원의 세로 반지름 비율

	ST_Spec			stSpecMin[Spec_LCB_MAX];
	ST_Spec			stSpecMax[Spec_LCB_MAX];

	bool					b8BitUse;

	_tag_UI_LCB()
	{
		stInspect.Reset();

		for (int nidx = 0; nidx < Spec_LCB_MAX; nidx++)
		{
			stSpecMin[nidx].Reset();
			stSpecMax[nidx].Reset();
		}
		

		dCenterThreshold = 1.0;
		dEdgeThreshold = 1.0;
		dCornerThreshold = 1.0;
		nMaxSingleDefectNum = 0;
		nMinDefectWidthHeight = 0;

		bEnableCircle		= false;
		iPosOffsetX			= 5;
		iPosOffsetY			= 5;
		dbRadiusRatioX		= 0.5;
		dbRadiusRatioY		= 0.5;
		b8BitUse			= false;

	};

	_tag_UI_LCB& operator= (_tag_UI_LCB& ref)
	{
		stInspect.Reset();

		dCenterThreshold = ref.dCenterThreshold;
		dEdgeThreshold = ref.dEdgeThreshold;
		dCornerThreshold = ref.dCornerThreshold;
		nMaxSingleDefectNum = ref.nMaxSingleDefectNum;
		nMinDefectWidthHeight = ref.nMinDefectWidthHeight;

		bEnableCircle		= ref.bEnableCircle;		
		iPosOffsetX			= ref.iPosOffsetX;			
		iPosOffsetY			= ref.iPosOffsetY;			
		dbRadiusRatioX		= ref.dbRadiusRatioX;	
		dbRadiusRatioY		= ref.dbRadiusRatioY;
		b8BitUse			= ref.b8BitUse;

		for (int nidx = 0; nidx < Spec_LCB_MAX; nidx++)
		{
			stSpecMin[nidx] = ref.stSpecMin[nidx];
			stSpecMax[nidx] = ref.stSpecMax[nidx];
		}
		
				
		return *this;
	};

}ST_UI_LCB, *PST_UI_LCB;

#pragma pack(pop)

#endif // Def_LCB_h__
