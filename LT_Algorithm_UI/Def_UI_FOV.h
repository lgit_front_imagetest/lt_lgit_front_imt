#ifndef Def_UI_FOV_h__
#define Def_UI_FOV_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_FOV_ROI
{

	ROI_FOV_FST = 0,
	//ROI_FOV_LST = 3,
	ROI_FOV_LST = 8,
	ROI_FOV_Max,
};


typedef enum enFOV_Spec
{
	//Spec_FOV = 0,
	Spec_FOV_D=0,
	Spec_FOV_H,
	Spec_FOV_V,
	Spec_FOV_MAX,
};

typedef enum enROI_FOV
{
	/*
	ROI_Rot_LT = 0,
	ROI_Rot_RT,
	ROI_Rot_LB,
	ROI_Rot_RB,
	*/
	ROI_FOV_1 = 0,
	ROI_FOV_2,
	ROI_FOV_3,
	ROI_FOV_4,
	ROI_FOV_5,
	ROI_FOV_6,
	ROI_FOV_7,
	ROI_FOV_8,
	ROI_FOV_9,
	ROI_FOVROI_Max,
};

static LPCTSTR g_szROI_Fov[] =
{
	//_T("LT"),
	//_T("RT"),
	//_T("LB"),
	//_T("RB"),
	_T("1"),
	_T("2"),
	_T("3"),
	_T("4"),
	_T("5"),
	_T("6"),
	_T("7"),
	_T("8"),
	_T("9"),
	NULL
};


typedef struct _tag_Input_FOV
{
	CRect		rtROI;
	UINT		nMarkType;
	UINT		nMarkColor;
	UINT		nBrightness;
	UINT		nSharpness;
	BOOL		bEnable;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right = rtROI.left + rtTemp.Width();
		rtROI.top = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};


	_tag_Input_FOV()
	{
		Reset();
	};

	void Reset()
	{
		rtROI.left = 0;
		rtROI.right = 0;
		rtROI.top = 0;
		rtROI.bottom = 0;

		nMarkType = MTyp_Circle;
		nMarkColor = MCol_Black;
		nBrightness = 0;
		nSharpness = 100;
		bEnable = FALSE;
	};

	_tag_Input_FOV& operator= (_tag_Input_FOV& ref)
	{
		rtROI = ref.rtROI;
		nMarkType = ref.nMarkType;
		nMarkColor = ref.nMarkColor;
		nBrightness = ref.nBrightness;
		nSharpness = ref.nSharpness;

		bEnable = ref.bEnable;

		return *this;
	};

}ST_Input_FOV, *PST_Input_FOV;

typedef struct _tag_UI_FOV
{
	// Inspect
	ST_Inspect				stInspect;
	ST_Input_FOV		stInput[ROI_FOV_Max];


	//!SH _181106: UI 변경 사항
	/*
	Combo CString ChartType;
	Combo int	 nFiducialmarkType;
	ListROI  int	 nFiducialMarkNum;
	Edit  double dModuleCartDistance;
	Edit  double dRealGapX;
	Edit  double dRealGapY;
	*/
	// Spec
	int			nChartType;		   //UI Combo 입력
	int			nFiducialMarkType; //UI Combo 입력
	int			nFiducialMarkNum;  //UI ListROI 갯수
	double		dXFromCenter;
	double		dYFromCenter;
	double		dModuleChartDistance;//UI Edit 입력
	double		dRealGapX;			//UI Edit 입력
	double		dRealGapY;			//UI Edit 입력
	int			nROIBoxSize;
	int			nMaxROIBoxSize;
	double		dRadius;
	int			nDistortionAlrotithmType;

	double		dbOffsetRot;


	ST_Spec			stSpecMin[Spec_FOV_MAX];
	ST_Spec			stSpecMax[Spec_FOV_MAX];

	_tag_UI_FOV()
	{
		stInspect.Reset();

		nROIBoxSize = 40;
		nMaxROIBoxSize = 50;
		dRadius = 1.0;
		dRealGapX = 1.0;
		dRealGapY = 1.0;
		nFiducialMarkNum = 4;
		nFiducialMarkType = 0;
		dModuleChartDistance = 1.0;
		nDistortionAlrotithmType = 0;
		dbOffsetRot = 1.0;
		dXFromCenter = 1.0;
		dYFromCenter = 1.0;

		for (UINT nIdx = 0; nIdx < Spec_FOV_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}

		for (UINT nROI = 0; nROI < ROI_FOV_Max; nROI++)
		{
			stInput[nROI].Reset();
		}
	}

	_tag_UI_FOV& operator= (_tag_UI_FOV& ref)
	{
		nROIBoxSize = ref.nROIBoxSize;
		nMaxROIBoxSize = ref.nMaxROIBoxSize;
		dRadius = ref.dRadius;
		dRealGapX = ref.dRealGapX;
		dRealGapY = ref.dRealGapY;
		nFiducialMarkNum = ref.nFiducialMarkNum;
		nFiducialMarkType = ref.nFiducialMarkType;
		dModuleChartDistance = ref.dModuleChartDistance;
		nDistortionAlrotithmType = ref.nDistortionAlrotithmType;
		dbOffsetRot = ref.dbOffsetRot;
		dXFromCenter = ref.dXFromCenter;
		dYFromCenter = ref.dYFromCenter;
		for (UINT nROI = 0; nROI < ROI_FOV_Max; nROI++)
		{
			stInput[nROI] = ref.stInput[nROI];
		}

		for (UINT nIdx = 0; nIdx < Spec_FOV_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_FOV, *PST_UI_FOV;

#pragma pack(pop)

#endif // Def_FOV_h__

