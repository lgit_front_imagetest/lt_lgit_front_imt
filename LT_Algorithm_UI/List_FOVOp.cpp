// List_RotateOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_FOVOp.h"

typedef enum enListNum_FOVOp
{
	FOVOp_Object = 0,
	FOVOp_PosX,
	FOVOp_PosY,
	FOVOp_Width,
	FOVOp_Height,
	FOVOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_FOVOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	NULL
};

static LPCTSTR	g_lpszItem_FOVOp[] =
{
	NULL
};

const int	iListAglin_FOVOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_FOVOp[] =
{
	105,
	105,
	105,
	105,
	105,
};

#define IDC_EDT_CELLEDIT		1000
#define IDC_COM_CELLCOMBO_TYPE	2000
#define IDC_COM_CELLCOMBO_COLOR	3000

// CList_RotateOp

IMPLEMENT_DYNAMIC(CList_FOVOp, CList_Cfg_VIBase)

CList_FOVOp::CList_FOVOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstConfigInfo = NULL;
}

CList_FOVOp::~CList_FOVOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_FOVOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_FOVOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_FOVOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IDC_EDT_CELLEDIT, &CList_FOVOp::OnEnKillFocusEdit)
	ON_CBN_KILLFOCUS(IDC_COM_CELLCOMBO_TYPE, &CList_FOVOp::OnEnKillFocusComboType)
	ON_CBN_SELCHANGE(IDC_COM_CELLCOMBO_TYPE, &CList_FOVOp::OnEnSelectComboType)
	ON_CBN_KILLFOCUS(IDC_COM_CELLCOMBO_COLOR, &CList_FOVOp::OnEnKillFocusComboColor)
	ON_CBN_SELCHANGE(IDC_COM_CELLCOMBO_COLOR, &CList_FOVOp::OnEnSelectComboColor)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_RotateOp 메시지 처리기입니다.
int CList_FOVOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkType[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szMarkType[nIdex]);
	}

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkColor[nIdex]; nIdex++)
	{
		m_cb_Color.InsertString(nIdex, g_szMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FOVOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FOVOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FOVOp::InitHeader()
{
	for (int nCol = 0; nCol < FOVOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_FOVOp[nCol], iListAglin_FOVOp[nCol], iHeaderWidth_FOVOp[nCol]);
	}

	for (int nCol = 0; nCol < FOVOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_FOVOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_FOVOp::InsertFullData()
{
	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < FOVOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_FOVOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szROI_Fov[nRow]);
	SetItemText(nRow, FOVOp_Object, strValue);

	if (m_pstConfigInfo->stInput[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, FOVOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, FOVOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Width());
	SetItemText(nRow, FOVOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Height());
	SetItemText(nRow, FOVOp_Height, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_FOVOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->stInput[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stInput[pNMItemActivate->iItem].bEnable = TRUE;

		GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_FOVOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{

		if (pNMItemActivate->iSubItem == FOVOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfigInfo->stInput[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfigInfo->stInput[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < FOVOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}

		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_FOVOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_FOVOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

	switch (nCol)
	{
	case FOVOp_PosX:
		m_pstConfigInfo->stInput[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case FOVOp_PosY:
		m_pstConfigInfo->stInput[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case FOVOp_Width:
		m_pstConfigInfo->stInput[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case FOVOp_Height:
		m_pstConfigInfo->stInput[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.left = 0;
		m_pstConfigInfo->stInput[nRow].rtROI.right = m_pstConfigInfo->stInput[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stInput[nRow].rtROI.left = m_pstConfigInfo->stInput[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.top = 0;
		m_pstConfigInfo->stInput[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stInput[nRow].rtROI.top;
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stInput[nRow].rtROI.top = m_pstConfigInfo->stInput[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.Height() <= 0)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_pstConfigInfo->stInput[nRow].rtROI.Width(), 1);

	if (m_pstConfigInfo->stInput[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_pstConfigInfo->stInput[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stInput[nRow].rtROI.Width() <= 0)
		m_pstConfigInfo->stInput[nRow].RectPosWH(1, m_pstConfigInfo->stInput[nRow].rtROI.Height());

	if (m_pstConfigInfo->stInput[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stInput[nRow].rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case FOVOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().x);
		break;
	case FOVOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().y);
		break;
	case FOVOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Width());
		break;
	case FOVOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FOVOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FOVOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FOVOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dbValue = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);

	}
	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_FOVOp::OnEnKillFocusComboType()
{
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_FOVOp::OnEnSelectComboType()
{
	m_pstConfigInfo->stInput[m_nEditRow].nMarkType = m_cb_Type.GetCurSel();
}

//=============================================================================
// Method		: OnEnKillFocusComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_FOVOp::OnEnKillFocusComboColor()
{
}

//=============================================================================
// Method		: OnEnSelectComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_FOVOp::OnEnSelectComboColor()
{
	m_pstConfigInfo->stInput[m_nEditRow].nMarkColor = m_cb_Color.GetCurSel();
}