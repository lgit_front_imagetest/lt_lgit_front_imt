﻿// Wnd_Cfg_Rotate.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_Rotate.h"
#include "resource.h"

static LPCTSTR	g_szRotate_Static[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	_T("INSPECT				"),
	_T("Data Foramt			"),
	_T("Out Mode			"),
	_T("Sensor Type			"),
	_T("Black Level			"),
// 	_T("ROIBoxSize"),
// 	_T("MaxROIBoxSize"),
//	_T("Radius"),
	_T("ChartType"),
	_T("FiducialMarkType"),
	_T("RealGapx"),
	_T("RealGapY"),
	_T("ModuleChartDistance"),
// 	_T("DistanceXFromCenter"),
// 	_T("DistanceYFromCenter"),
	_T("RotateDegree"),
	_T("Offset"),
	NULL
};

static LPCTSTR	g_szRotate_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

static LPCTSTR	g_szRotateCommoBox[] =
{
	NULL
};

// CWnd_Cfg_Rotate
typedef enum RotateOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,

	IDC_ED_SPEC_MIN = 5001,
	IDC_ED_SPEC_MAX = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_Rotate, CWnd_Cfg_VIBase)

CWnd_Cfg_Rotate::CWnd_Cfg_Rotate()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Rotate::~CWnd_Cfg_Rotate()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Rotate, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_Rotate 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_Rotate::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_ROT_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szRotate_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}
	m_st_Item[STI_ROT_INSPECT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_ROT_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_ROT_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_ROT_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szRotate_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_ROT_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_ROT_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	for (UINT nIdex = 0; NULL < g_szDataForamt_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_ROT_DataForamt].InsertString(nIdex, g_szDataForamt_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szOutMode_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_ROT_OutMode].InsertString(nIdex, g_szOutMode_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szSensorType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_ROT_SensorType].InsertString(nIdex, g_szSensorType_Static[nIdex]);
	}
	//!SH _1811006: ComboBox 추가
	for (UINT nIdex = 0; NULL < g_szChartType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_ROT_ChartType].InsertString(nIdex, g_szChartType_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szFiducialMarkType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_ROT_FiducialMarkType].InsertString(nIdex, g_szFiducialMarkType_Static[nIdex]);
	}
	m_List.SetOwner(GetOwner());
	m_List.SetWindowMessage(m_wm_ChangeOption);
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szRotate_Static[nIdx + STI_ROT_RotateDegree], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_Rotate::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt		= RoOp_ItemNum;
	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iHeaderH	= 40;
	int iList_H		= iHeaderH + iIdxCnt * 20;
	int iCtrl_W		= iWidth / 5;
	int iCtrl_H		= 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;
	int iTopTemp = 0;

	int iStWidth = (iWidth - iSpacing - iSpacing) / 6;
	int iEdWidth = iStWidth * 2;
	int iStHeight = 25;
	// Comm
	m_st_Item[STI_ROT_INSPECT].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	// Comm 1
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_ROT_DataForamt].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_ROT_DataForamt].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 2
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_ROT_OutMode].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_ROT_OutMode].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 3
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_ROT_SensorType].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_ROT_SensorType].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 4
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_ROT_BlackLevel].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_ROT_BlackLevel].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);


	//Param
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_ROT_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// BUTTON 
// 	iTopTemp = iTop + iSTHeight + iSpacing;
// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_ROT_RESET].MoveWindow(iLeft, iTopTemp, iSTWidth, iSTHeight);
// 
// 	iLeft = cx - iMargin - iSTWidth;
// 	iTopTemp += iSTHeight + iSpacing;
// 	m_bn_Item[BTN_ROT_TEST].MoveWindow(iLeft, iTopTemp, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_Offset].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ROT_Offset].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_ChartType].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_ROT_ChartType].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_FiducialMarkType].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_ROT_FiducialMarkType].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

// 	iLeft = iMargin;
// 	iTop += iSTHeight + iSpacing;
// 	m_st_Item[STI_ROT_Radius].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft += iSTWidth + 2;
// 	m_ed_Item[EDT_ROT_Radius].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_RealGapx].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ROT_RealGapx].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_RealGapY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ROT_RealGapY].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ROT_ModuleChartDistance].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ROT_ModuleChartDistance].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

// 	iLeft = iMargin;
// 	iTop += iSTHeight + iSpacing;
// 	m_st_Item[STI_ROT_DistanceXFromCenter].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft += iSTWidth + 2;
// 	m_ed_Item[EDT_ROT_DistanceXFromCenter].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
// 
// 	iLeft = iMargin;
// 	iTop += iSTHeight + iSpacing;
// 	m_st_Item[STI_ROT_DistanceYFromCenter].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft += iSTWidth + 2;
// 	m_ed_Item[EDT_ROT_DistanceYFromCenter].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

// 	iLeft = iMargin;
// 	iTop += iSTHeight + iSpacing;
// 	m_st_Item[STI_ROT_ROIBoxSize].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft += iSTWidth + 2;
// 	m_ed_Item[EDT_ROT_ROIBoxSize].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
// 
// 	iLeft = iMargin;
// 	iTop += iSTHeight + iSpacing;
// 	m_st_Item[STI_ROT_MaxROIBoxSize].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft += iSTWidth + 2;
// 	m_ed_Item[EDT_ROT_MaxROIBoxSize].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	// 리스트
	iLeft = iMargin;
	iTop += (iCtrl_H + iSpacing) * 2;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);

	iLeft = iMargin;
	iTop += iList_H + iSpacing + iSpacing + iSpacing;
	m_st_Item[STI_ROT_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidthsp = iCtrlWidth - iSpacing - iSTHeight;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidthsp, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidthsp, iSTHeight);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Rotate::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_Rotate::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Rotate::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_ROT_RESET:
		break;
	case BTN_ROT_TEST:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_Rotate::SetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_List.SetPtr_ConfigInfo(m_pstConfigInfo);
	m_List.InsertFullData();

	strValue.Format(_T("%d"), m_pstConfigInfo->stInspect.iBlackLevel);
	m_ed_Item[EDT_ROT_BlackLevel].SetWindowText(strValue);

// 	strValue.Format(_T("%d"), m_pstConfigInfo->nROIBoxSize);
// 	m_ed_Item[EDT_ROT_ROIBoxSize].SetWindowText(strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->nMaxROIBoxSize);
// 	m_ed_Item[EDT_ROT_MaxROIBoxSize].SetWindowText(strValue);

// 	strValue.Format(_T("%.3f"), m_pstConfigInfo->dRadius);
// 	m_ed_Item[EDT_ROT_Radius].SetWindowText(strValue);

	strValue.Format(_T("%.3f"), m_pstConfigInfo->dRealGapX);
	m_ed_Item[EDT_ROT_RealGapx].SetWindowText(strValue);

	strValue.Format(_T("%.3f"), m_pstConfigInfo->dRealGapY);
	m_ed_Item[EDT_ROT_RealGapY].SetWindowText(strValue);

	strValue.Format(_T("%.3f"), m_pstConfigInfo->dModuleChartDistance);
	m_ed_Item[EDT_ROT_ModuleChartDistance].SetWindowText(strValue);

// 	strValue.Format(_T("%.3f"), m_pstConfigInfo->dXFromCenter);
// 	m_ed_Item[EDT_ROT_DistanceXFromCenter].SetWindowText(strValue);
// 
// 	strValue.Format(_T("%.3f"), m_pstConfigInfo->dYFromCenter);
// 	m_ed_Item[EDT_ROT_DistanceYFromCenter].SetWindowText(strValue);

	strValue.Format(_T("%.3f"), m_pstConfigInfo->dbOffsetRot);
	m_ed_Item[EDT_ROT_Offset].SetWindowText(strValue);

	m_cb_Item[CMB_ROT_DataForamt].SetCurSel(m_pstConfigInfo->stInspect.eDataForamt);
				
	m_cb_Item[CMB_ROT_OutMode].SetCurSel(m_pstConfigInfo->stInspect.eOutMode);
			
	m_cb_Item[CMB_ROT_SensorType].SetCurSel(m_pstConfigInfo->stInspect.eSensorType);

	m_cb_Item[CMB_ROT_ChartType].SetCurSel(m_pstConfigInfo->nChartType);

	m_cb_Item[CMB_ROT_FiducialMarkType].SetCurSel(m_pstConfigInfo->nFiducialMarkType);

	for (UINT nSpec = 0; nSpec < Spec_Rotation_MAX; nSpec++)
	{
		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpecMin[nSpec].dbValue);
		m_ed_SpecMin[nSpec].SetWindowText(strValue);

		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpecMax[nSpec].dbValue);
		m_ed_SpecMax[nSpec].SetWindowText(strValue);

		m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpecMin[nSpec].bEnable);
		m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpecMax[nSpec].bEnable);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_Rotate::GetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_ed_Item[EDT_ROT_BlackLevel].GetWindowText(strValue);
	m_pstConfigInfo->stInspect.iBlackLevel = _ttoi(strValue);

// 	m_ed_Item[EDT_ROT_ROIBoxSize].GetWindowText(strValue);
// 	m_pstConfigInfo->nROIBoxSize = _ttoi(strValue);
// 
// 	m_ed_Item[EDT_ROT_MaxROIBoxSize].GetWindowText(strValue);
// 	m_pstConfigInfo->nMaxROIBoxSize = _ttoi(strValue);

// 	m_ed_Item[EDT_ROT_Radius].GetWindowText(strValue);
// 	m_pstConfigInfo->dRadius = _ttof(strValue);

	m_ed_Item[EDT_ROT_RealGapx].GetWindowText(strValue);
	m_pstConfigInfo->dRealGapX = _ttof(strValue);

	m_ed_Item[EDT_ROT_RealGapY].GetWindowText(strValue);
	m_pstConfigInfo->dRealGapY = _ttof(strValue);

	m_ed_Item[EDT_ROT_ModuleChartDistance].GetWindowText(strValue);
	m_pstConfigInfo->dModuleChartDistance = _ttof(strValue);

// 	m_ed_Item[EDT_ROT_DistanceXFromCenter].GetWindowText(strValue);
// 	m_pstConfigInfo->dXFromCenter = _ttof(strValue);
// 
// 	m_ed_Item[EDT_ROT_DistanceYFromCenter].GetWindowText(strValue);
// 	m_pstConfigInfo->dYFromCenter = _ttof(strValue);

	m_ed_Item[EDT_ROT_Offset].GetWindowText(strValue);
	m_pstConfigInfo->dbOffsetRot = _ttof(strValue);

	for (UINT nSpec = 0; nSpec < Spec_Rotation_MAX; nSpec++)
	{
		m_ed_SpecMin[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpecMin[nSpec].dbValue = _ttof(strValue);

		m_ed_SpecMax[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpecMax[nSpec].dbValue = _ttof(strValue);

		m_pstConfigInfo->stSpecMin[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
		m_pstConfigInfo->stSpecMax[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
	}

	m_pstConfigInfo->stInspect.eDataForamt = (enDataForamt)m_cb_Item[CMB_ROT_DataForamt].GetCurSel();
	m_pstConfigInfo->stInspect.eOutMode = (enOutMode)m_cb_Item[CMB_ROT_OutMode].GetCurSel();
	m_pstConfigInfo->stInspect.eSensorType = (enSensorType)m_cb_Item[CMB_ROT_SensorType].GetCurSel();
	
	m_pstConfigInfo->nChartType = (enOutMode)m_cb_Item[CMB_ROT_ChartType].GetCurSel();
	m_pstConfigInfo->nFiducialMarkType = (enSensorType)m_cb_Item[CMB_ROT_FiducialMarkType].GetCurSel();

}

