#ifndef Wnd_Rst_FOV_h__
#define Wnd_Rst_FOV_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_FOV.h"

// CWnd_Rst_DRotate
typedef enum
{
	enFOV_Result_D = 0,
	enFOV_Result_H,
	enFOV_Result_V,
	enFOV_Result_Max,
}enFOV_Result;

static	LPCTSTR	g_szFOV_Result[] =
{
	_T("FOV_Diagonal"),
	_T("Fov_Horizontal"),
	_T("Fov_Vertical"),
	NULL
};

class CWnd_Rst_FOV : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_FOV)

public:
	CWnd_Rst_FOV();
	virtual ~CWnd_Rst_FOV();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd(CDC* pDC);

	CVGStatic	m_st_Header[enFOV_Result_Max];
	CVGStatic	m_st_Result[enFOV_Result_Max];

public:

	void Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue);
	void Result_Reset();
};


#endif // Wnd_Rst_Rotate_h__
