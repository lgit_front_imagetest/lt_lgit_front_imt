﻿#ifndef Wnd_Cfg_Rotate_h__
#define Wnd_Cfg_Rotate_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Rotation.h"
#include "List_RotateOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Rotate
//-----------------------------------------------------------------------------

static LPCTSTR	g_szChartType_Static[] =
{
	_T("DOT"),
	_T("CROSSDOT"),
	_T("GRID"),
	NULL
};

static LPCTSTR	g_szFiducialMarkType_Static[] =
{
	_T("Dot"),
	_T("Cross Dot"),
	_T("Grid"),
	NULL
};


class CWnd_Cfg_Rotate : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Rotate)

public:
	CWnd_Cfg_Rotate();
	virtual ~CWnd_Cfg_Rotate();

	//!SH _181106: UI 변경 사항
	/*
	Combo CString ChartType; 
	Combo int	 nFiducialmarkType; 
	Edit  int	 nFiducialMarkNum;
	Edit  double dModuleCartDistance;
	Edit  double dRealGapX;
	Edit  double dRealGapY;
	*/
	enum enRotateStatic
	{
		STI_ROT_PARAM = 0,
		STI_ROT_SPEC,
		STI_ROT_INSPECT,
		STI_ROT_DataForamt,
		STI_ROT_OutMode,
		STI_ROT_SensorType,
		STI_ROT_BlackLevel,
// 		STI_ROT_ROIBoxSize,
// 		STI_ROT_MaxROIBoxSize,
//		STI_ROT_Radius,
		STI_ROT_ChartType,
		STI_ROT_FiducialMarkType,
		STI_ROT_RealGapx, 
		STI_ROT_RealGapY,
		STI_ROT_ModuleChartDistance,
//		STI_ROT_DistanceXFromCenter,
//		STI_ROT_DistanceYFromCenter,
		STI_ROT_RotateDegree,
		STI_ROT_Offset,
		STI_ROT_MAX,
	};


	enum enRotateButton
	{
		BTN_ROT_RESET = 0,
		BTN_ROT_TEST,
		BTN_ROT_MAX,
	};

	enum enRotateComobox
	{
		CMB_ROT_DataForamt = 0,
		CMB_ROT_OutMode,
		CMB_ROT_SensorType,
		CMB_ROT_ChartType,
		CMB_ROT_FiducialMarkType,
		CMB_ROT_MAX,
	};

	enum enRotateEdit
	{
		EDT_ROT_BlackLevel = 0,
// 		EDT_ROT_ROIBoxSize,
// 		EDT_ROT_MaxROIBoxSize,
//		EDT_ROT_Radius,
		EDT_ROT_RealGapx,
		EDT_ROT_RealGapY,
		EDT_ROT_ModuleChartDistance,
//		EDT_ROT_DistanceXFromCenter,
//		EDT_ROT_DistanceYFromCenter,
		EDT_ROT_Offset,
		EDT_ROT_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	ST_UI_Rotation*		m_pstConfigInfo = NULL;
	CList_RotateOp		m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_ROT_MAX];
	CButton				m_bn_Item[BTN_ROT_MAX];
	CComboBox			m_cb_Item[CMB_ROT_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_ROT_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_Rotation_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Rotation_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Rotation_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_Rotation_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_Rotation_MAX];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_RecipeInfo(ST_UI_Rotation* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();

	//void Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_Cfg_Rotate_h__
