﻿// Wnd_Cfg_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_SFR.h"
#include "resource.h"

static LPCTSTR	g_szSFR_Static[] =
{
	_T("INSPECT			"),
	_T("SPEC			"),
	_T("ROI				"),
	_T("Data Foramt		"),
	_T("Out Mode		"),
	_T("Sensor Type		"),
	_T("Black Level		"),
	_T("Max EdgeAngle	"),
	_T("PixelSize		"),
//	_T("Type			"),
	_T("Method			"),
//	_T("EnableLog		"),
	_T("Average Count	"),
	NULL
};

static LPCTSTR	g_szSFR_Button[] =
{
	NULL
};

// CWnd_Cfg_SFR
typedef enum SFROptID
{
	IDC_BTN_ITEM	= 1001,
	IDC_CMB_ITEM	= 2001,
	IDC_EDT_ITEM	= 3001,
	IDC_LIST_ITEM	= 4001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_SFR, CWnd_Cfg_VIBase)

CWnd_Cfg_SFR::CWnd_Cfg_SFR()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_SFR::~CWnd_Cfg_SFR()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_SFR, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_SFR 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_SFR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_SFR_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szSFR_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_SFR_INSPECT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SFR_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SFR_ROI].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_SFR_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szSFR_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_SFR_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_SFR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	for (UINT nIdex = 0; NULL < g_szDataForamt_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SFR_DataForamt].InsertString(nIdex, g_szDataForamt_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szOutMode_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SFR_OutMode].InsertString(nIdex, g_szOutMode_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szSensorType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SFR_SensorType].InsertString(nIdex, g_szSensorType_Static[nIdex]);
	}
// 
// 	for (UINT nIdex = 0; NULL < g_szSFRType_Static[nIdex]; nIdex++)
// 	{
// 		m_cb_Item[CMB_SFR_Type].InsertString(nIdex, g_szSFRType_Static[nIdex]);
// 	}

	for (UINT nIdex = 0; NULL < g_szSFRMethod_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SFR_Method].InsertString(nIdex, g_szSFRMethod_Static[nIdex]);
	}

// 	for (UINT nIdex = 0; NULL < g_szEnableLog_Static[nIdex]; nIdex++)
// 	{
// 		m_cb_Item[CMB_SFR_EnableLog].InsertString(nIdex, g_szEnableLog_Static[nIdex]);
// 	}

	m_List.SetOwner(GetOwner());
	m_List.SetChang_Message(m_wm_ChangeOption);
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iStWidth	= (iWidth - iSpacing - iSpacing ) / 6;
	int iEdWidth	= iStWidth * 2;
	int iStHeight	= 25;

	// Comm
	m_st_Item[STI_SFR_INSPECT].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	// Comm 1
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_DataForamt].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SFR_DataForamt].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 2
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_OutMode].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SFR_OutMode].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 3
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_SensorType].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SFR_SensorType].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 4
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_BlackLevel].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SFR_BlackLevel].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Spec
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_SPEC].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_AverageCount].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SFR_AverageCount].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_EdgeAngle].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SFR_EdgeAngle].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_PixelSize].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SFR_PixelSize].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_SFR_Type].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_cb_Item[CMB_SFR_Type].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_Method].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SFR_Method].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_SFR_EnableLog].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_cb_Item[CMB_SFR_EnableLog].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SFR_ROI].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop + iSpacing);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_SFR::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfig)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::SetUpdateData()
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("%d"), m_pstConfig->stInspect.iBlackLevel);
	m_ed_Item[EDT_SFR_BlackLevel].SetWindowText(szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->dbMaxEdgeAngle);
	m_ed_Item[EDT_SFR_EdgeAngle].SetWindowText(szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->dbPixelSize);
	m_ed_Item[EDT_SFR_PixelSize].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->nAverageCount);
	m_ed_Item[EDT_SFR_AverageCount].SetWindowText(szValue);


	m_cb_Item[CMB_SFR_DataForamt].SetCurSel(m_pstConfig->stInspect.eDataForamt);

	m_cb_Item[CMB_SFR_OutMode].SetCurSel(m_pstConfig->stInspect.eOutMode);

	m_cb_Item[CMB_SFR_SensorType].SetCurSel(m_pstConfig->stInspect.eSensorType);

	//m_cb_Item[CMB_SFR_Type].SetCurSel(m_pstConfig->eType);

	m_cb_Item[CMB_SFR_Method].SetCurSel(m_pstConfig->eMethod);

	//m_cb_Item[CMB_SFR_EnableLog].SetCurSel(m_pstConfig->bEnableLog);

	m_List.SetPtr_ConfigInfo(m_pstConfig);
	m_List.InsertFullData();

	switch (m_pstConfig->eMethod)
	{
	case _ESFRMethod_Freq2SFR:
		// SfOp_SFR,
		m_List.DeleteHeader(7);
		break;
	case _ESFRMethod_SFR2Freq:
		// SfOp_FLists,
		m_List.DeleteHeader(6);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::GetUpdateData()
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	m_ed_Item[EDT_SFR_BlackLevel].GetWindowText(szValue);
	m_pstConfig->stInspect.iBlackLevel = _ttoi(szValue);

	m_ed_Item[EDT_SFR_EdgeAngle].GetWindowText(szValue);
	m_pstConfig->dbMaxEdgeAngle = _ttof(szValue);

	m_ed_Item[EDT_SFR_PixelSize].GetWindowText(szValue);
	m_pstConfig->dbPixelSize = _ttof(szValue);

	m_ed_Item[EDT_SFR_AverageCount].GetWindowText(szValue);
	m_pstConfig->nAverageCount = _ttoi(szValue);

	m_pstConfig->stInspect.eDataForamt	= (enDataForamt)m_cb_Item[CMB_SFR_DataForamt].GetCurSel();
	m_pstConfig->stInspect.eOutMode		= (enOutMode)m_cb_Item[CMB_SFR_OutMode].GetCurSel();
	m_pstConfig->stInspect.eSensorType  = (enSensorType)m_cb_Item[CMB_SFR_SensorType].GetCurSel();
//	m_pstConfig->eType					= (enSFRType)m_cb_Item[CMB_SFR_Type].GetCurSel();
	m_pstConfig->eMethod				= (enSFRMethod)m_cb_Item[CMB_SFR_Method].GetCurSel();
//	m_pstConfig->bEnableLog				= (1 == m_cb_Item[CMB_SFR_EnableLog].GetCurSel()) ? true : false;

	return;
}
