﻿#ifndef Wnd_Cfg_OpticalCenter_h__
#define Wnd_Cfg_OpticalCenter_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_OpticalCenter.h"
#include "List_OpticalCenterOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_OpticalCenter
//-----------------------------------------------------------------------------
class CWnd_Cfg_OpticalCenter : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_OpticalCenter)

public:
	CWnd_Cfg_OpticalCenter();
	virtual ~CWnd_Cfg_OpticalCenter();

	enum enOpticalCenter_Static
	{
		STI_CTP_PARAM = 0,
		STI_CTP_SPEC,
		STI_CTP_STANDARD_X,
		STI_CTP_STANDARD_Y,
		STI_CTP_OFFSET_X,
		STI_CTP_OFFSET_Y,
		STI_CTP_MAX,
	};

	enum enOpticalCenter_Button
	{
		BTN_CTP_RESET = 0,
		BTN_CTP_TEST,
		BTN_CTP_MAX,
	};

	enum enOpticalCenter_Comobox
	{
		CMB_CTP_MAX = 1,
	};

	enum enOpticalCenter_Edit
	{
		EDT_CTP_STANDARD_X = 0,
		EDT_CTP_STANDARD_Y,
		EDT_CTP_OFFSET_X,
		EDT_CTP_OFFSET_Y,
		EDT_CTP_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_OpticalCenterOp		m_List;

	CVGStatic					m_st_Item[STI_CTP_MAX];
	CButton						m_bn_Item[BTN_CTP_MAX];
	CComboBox					m_cb_Item[CMB_CTP_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_CTP_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_OC_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_OC_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_OC_Max];
	CMFCButton					m_chk_SpecMin[Spec_OC_Max];
	CMFCButton					m_chk_SpecMax[Spec_OC_Max];

	ST_OpticalCenter_Opt*		m_pstConfigInfo		= NULL;

public:

	void	SetPtr_RecipeInfo (ST_OpticalCenter_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_OpticalCenter_h__
