#ifndef Wnd_Rst_LCB_h__
#define Wnd_Rst_LCB_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_LCB.h"

// CWnd_Rst_LCB
typedef enum
{
	enLCB_Result_Count = 0,
	enLCB_Result_Max,

}enLCB_Result;

static	LPCTSTR	g_szLCB_Result[] =
{
	_T("Count"),
	NULL
};

class CWnd_Rst_LCB : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_LCB)

public:
	CWnd_Rst_LCB();
	virtual ~CWnd_Rst_LCB();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enLCB_Result_Max];
	CVGStatic	m_st_Result[enLCB_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_LCB_h__
