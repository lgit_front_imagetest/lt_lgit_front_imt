﻿#ifndef Def_UI_Defect_White_h__
#define Def_UI_Defect_White_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#define Defect_White_COUNT_MAX 500

#pragma pack(push,1)

typedef enum enDefect_White_Spec
{
	Spec_Defect_White_NormalCount = 0,
	Spec_Defect_White_VeryCount,
	Spec_Defect_White_MAX,
};

typedef struct _tag_UI_Defect_White
{
	// Inspect
	ST_Inspect				stInspect;

	// PARAMETER

	int		nBlockSize;
	double	dbNormalDefectRatio;
	int		nMaxNormalDefectNum;
	double	dbVeryDefectRatio;
	int		nMaxVeryDefectNum;

	ST_Spec			stSpecMin[Spec_Defect_White_MAX];
	ST_Spec			stSpecMax[Spec_Defect_White_MAX];

	bool					b8BitUse;
	bool					bType;

	_tag_UI_Defect_White()
	{
		stInspect.Reset();

		nBlockSize = 0;
		dbNormalDefectRatio = 0;
		nMaxNormalDefectNum = 0;
		dbVeryDefectRatio = 0;
		nMaxVeryDefectNum = 0;
		b8BitUse = false;
		bType = false;

		for (UINT nIdx = 0; nIdx < Spec_Defect_White_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}

		
	}

	_tag_UI_Defect_White& operator= (_tag_UI_Defect_White& ref)
	{
		nBlockSize = ref.nBlockSize;
		dbNormalDefectRatio = ref.dbNormalDefectRatio;
		nMaxNormalDefectNum = ref.nMaxNormalDefectNum;
		dbVeryDefectRatio = ref.dbVeryDefectRatio;
		nMaxVeryDefectNum = ref.nMaxVeryDefectNum;
		b8BitUse = ref.b8BitUse;
		bType = ref.bType;

		for (UINT nIdx = 0; nIdx < Spec_Defect_White_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Defect_White, *PST_UI_Defect_White;

#pragma pack(pop)

#endif // Def_Defect_Black_h__

