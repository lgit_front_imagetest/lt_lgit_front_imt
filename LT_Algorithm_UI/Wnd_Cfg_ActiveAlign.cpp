﻿// Wnd_ActiveAlign.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_ActiveAlign.h"
#include "resource.h"

static LPCTSTR	g_szActiveAlign_Static[] =
{
	_T("PARAMETER		"),
	_T("TARGET TRY CNT	"),
	_T("TARGET R		"),
	_T("TARGET X		"),
	_T("TARGET Y		"),
	_T("TARGET X [R]	"),
	_T("TARGET Y [R]	"),
	NULL
};

static LPCTSTR	g_szActiveAlign_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

// CWnd_Cfg_ActiveAlign
typedef enum ActiveAlignOptID
{
	IDC_BTN_ITEM		= 1001,
	IDC_CMB_ITEM		= 2001,
	IDC_EDT_ITEM		= 3001,
	IDC_LIST_ITEM		= 4001,

	IDC_ED_SPEC_MIN		= 5001,
	IDC_ED_SPEC_MAX		= 6001,
	IDC_CHK_SPEC_MIN	= 7001,
	IDC_CHK_SPEC_MAX	= 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_ActiveAlign, CWnd_Cfg_VIBase)

CWnd_Cfg_ActiveAlign::CWnd_Cfg_ActiveAlign()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_ActiveAlign::~CWnd_Cfg_ActiveAlign()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_ActiveAlign, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_ActiveAlign 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_ActiveAlign::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_AA_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szActiveAlign_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_AA_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_AA_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szActiveAlign_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_AA_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_AA_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_ActiveAlign::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt  = 0;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 20;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;
	int iTopTemp = 0;

	m_st_Item[STI_AA_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	m_st_Item[STI_AA_TARGET_X_R].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_AA_TARGET_X_R].MoveWindow(0, 0, 0, 0);

	m_st_Item[STI_AA_TARGET_Y_R].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_AA_TARGET_Y_R].MoveWindow(0, 0, 0, 0);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_AA_TARGET_CNT].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_AA_TARGET_CNT].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_AA_TARGET_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_AA_TARGET_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	// 옵셋 
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_AA_TARGET_X_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_AA_TARGET_X_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_AA_TARGET_Y_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_AA_TARGET_Y_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	CString szText;

	if (1 == m_nModelType)
	{
		szText.Format(_T("%s [L]"), g_szActiveAlign_Static[STI_AA_TARGET_X_L]);
		m_st_Item[STI_AA_TARGET_X_L].SetText(szText);

		szText.Format(_T("%s [L]"), g_szActiveAlign_Static[STI_AA_TARGET_Y_L]);
		m_st_Item[STI_AA_TARGET_Y_L].SetText(szText);

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_AA_TARGET_X_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_AA_TARGET_X_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_AA_TARGET_Y_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_AA_TARGET_Y_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
	}
	else
	{
		szText.Format(_T("%s"), g_szActiveAlign_Static[STI_AA_TARGET_X_L]);
		m_st_Item[STI_AA_TARGET_X_L].SetText(szText);

		szText.Format(_T("%s"), g_szActiveAlign_Static[STI_AA_TARGET_Y_L]);
		m_st_Item[STI_AA_TARGET_Y_L].SetText(szText);
	}

	// 리스트
// 	iLeft = iMargin;
// 	iTop += (iCtrl_H + iSpacing) * 2;
// 	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ActiveAlign::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_ActiveAlign::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);
	
	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_ActiveAlign::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_ActiveAlign::SetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%d"), m_pstConfigInfo->nTargetCnt);
	m_ed_Item[EDT_AA_TARGET_CNT].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->dbTargetR);
	m_ed_Item[EDT_AA_TARGET_R].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iTargetX[0]);
	m_ed_Item[EDT_AA_TARGET_X_L].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iTargetY[0]);
	m_ed_Item[EDT_AA_TARGET_Y_L].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iTargetX[1]);
	m_ed_Item[EDT_AA_TARGET_X_R].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iTargetY[1]);
	m_ed_Item[EDT_AA_TARGET_Y_R].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_ActiveAlign::GetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_ed_Item[EDT_AA_TARGET_CNT].GetWindowText(strValue);
	m_pstConfigInfo->nTargetCnt = _ttoi(strValue);

	m_ed_Item[EDT_AA_TARGET_R].GetWindowText(strValue);
	m_pstConfigInfo->dbTargetR = _ttof(strValue);

	m_ed_Item[EDT_AA_TARGET_X_L].GetWindowText(strValue);
	m_pstConfigInfo->iTargetX[0] = _ttoi(strValue);

	m_ed_Item[EDT_AA_TARGET_Y_L].GetWindowText(strValue);
	m_pstConfigInfo->iTargetY[0] = _ttoi(strValue);

	m_ed_Item[EDT_AA_TARGET_X_R].GetWindowText(strValue);
	m_pstConfigInfo->iTargetX[1] = _ttoi(strValue);

	m_ed_Item[EDT_AA_TARGET_Y_R].GetWindowText(strValue);
	m_pstConfigInfo->iTargetY[1] = _ttoi(strValue);
}
