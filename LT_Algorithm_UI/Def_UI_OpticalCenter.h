﻿#ifndef Def_T_OpticalCenter_h__
#define Def_T_OpticalCenter_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enOpticalCenter_Spec
{
	Spec_OC_X = 0,
	Spec_OC_Y,
	Spec_OC_Max,
};

static LPCTSTR	g_szOpticalCenter_Spec[] =
{
	_T("Optical Center X"),
	_T("Optical Center Y"),
	NULL
};

typedef struct _tag_OpticalCenter_Opt
{
	UINT nStandarX;
	UINT nStandarY;

	// 옵셋 광축
	int iOffsetX;
	int iOffsetY;

	CRect		rtROI;
	UINT		nMarkType;
	UINT		nMarkColor;
	UINT		nBrightness;
	UINT		nSharpness;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right = rtROI.left + rtTemp.Width();
		rtROI.top = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	ST_Spec stSpec_Min[Spec_OC_Max];
	ST_Spec stSpec_Max[Spec_OC_Max];

	_tag_OpticalCenter_Opt()
	{
		nStandarX	= 0;
		nStandarY	= 0;
		
		iOffsetX	= 0;
		iOffsetY	= 0;

		rtROI.SetRectEmpty();

		nMarkType = MTyp_Circle;
		nMarkColor = MCol_Black;
		nBrightness = 0;
		nSharpness = 100;

		for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_OpticalCenter_Opt& operator= (_tag_OpticalCenter_Opt& ref)
	{
		nStandarX	= ref.nStandarX;
		nStandarY	= ref.nStandarY;

		iOffsetX	= ref.iOffsetX;
		iOffsetY	= ref.iOffsetY;

		rtROI		= ref.rtROI;
		nMarkType	= ref.nMarkType;
		nMarkColor	= ref.nMarkColor;
		nBrightness = ref.nBrightness;
		nSharpness	= ref.nSharpness;


		for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_OpticalCenter_Opt, *PST_OpticalCenter_Opt;



typedef struct _tag_TI_OpticalCenter
{
	ST_OpticalCenter_Opt	stCenterOpt;	// 검사 기준 데이터

	_tag_TI_OpticalCenter()
	{
	};

	void Reset()
	{
	};

	_tag_TI_OpticalCenter& operator= (_tag_TI_OpticalCenter& ref)
	{
		stCenterOpt		= ref.stCenterOpt;

		return *this;
	};

}ST_TI_OpticalCenter, *PST_TI_OpticalCenter;

#pragma pack(pop)

#endif // Def_OpticalCenter_h__