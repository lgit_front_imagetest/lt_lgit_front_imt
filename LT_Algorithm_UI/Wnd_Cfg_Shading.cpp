﻿// Wnd_Cfg_Shading.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_Shading.h"
#include "resource.h"

static LPCTSTR	g_szShading_Static[] =
{
	_T("INSPECT				"),
	_T("SPEC				"),
	_T("ROI					"),
	_T("Data Foramt			"),
	_T("Out Mode			"),
	_T("Sensor Type			"),
	_T("Black Level			"),
	_T("ROI Width			"),
	_T("ROI Height			"),
	_T("ROI Max				"),
	_T("NormlizeIndex		"),
	NULL
};

static LPCTSTR	g_szShading_Button[] =
{
	NULL
};

// CWnd_Cfg_Shading
typedef enum ShadingOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
	IDC_TB_OPTION = 5001,

};

IMPLEMENT_DYNAMIC(CWnd_Cfg_Shading, CWnd_Cfg_VIBase)

CWnd_Cfg_Shading::CWnd_Cfg_Shading()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Shading::~CWnd_Cfg_Shading()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Shading, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM,		IDC_BTN_ITEM	+ 999,					OnRangeBtnCtrl	)
END_MESSAGE_MAP()

// CWnd_Cfg_Shading 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_Shading::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	for (UINT nIdx = 0; nIdx < STI_SHA_MAX; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szShading_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_SHA_INSPECT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_ROI].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdx = 0; nIdx < BTN_SHA_MAX; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szShading_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < EDT_SHA_MAX; nIdx++)
	{
		m_ed_Item[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdx = 0; nIdx < CMB_SHA_MAX; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdx);
	}

	for (UINT nIdex = 0; NULL < g_szDataForamt_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SHA_DataForamt].InsertString(nIdex, g_szDataForamt_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szOutMode_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SHA_OutMode].InsertString(nIdex, g_szOutMode_Static[nIdex]);
	}

	for (UINT nIdex = 0; NULL < g_szSensorType_Static[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SHA_SensorType].InsertString(nIdex, g_szSensorType_Static[nIdex]);
	}

	// Tab
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TB_OPTION, CMFCTabCtrl::LOCATION_BOTTOM);

	m_ListHor.SetOwner(GetOwner());
	m_ListHor.SetChang_Message(m_wm_ChangeOption);
	m_ListHor.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, IDC_LIST_ITEM);

	m_ListVer.SetOwner(GetOwner());
	m_ListVer.SetChang_Message(m_wm_ChangeOption);
	m_ListVer.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, IDC_LIST_ITEM);

	m_ListDirA.SetOwner(GetOwner());
	m_ListDirA.SetChang_Message(m_wm_ChangeOption);
	m_ListDirA.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, IDC_LIST_ITEM);

	m_ListDirB.SetOwner(GetOwner());
	m_ListDirB.SetChang_Message(m_wm_ChangeOption);
	m_ListDirB.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Option, IDC_LIST_ITEM);

	UINT nTabIndex = 0;

	m_tc_Option.AddTab(&m_ListHor,		_T("Horizontal"),	nTabIndex++,	FALSE);
	m_tc_Option.AddTab(&m_ListVer,		_T("Vertical  "),	nTabIndex++,	FALSE);
	m_tc_Option.AddTab(&m_ListDirA,		_T("Diagonal A"),	nTabIndex++,	FALSE);
	m_tc_Option.AddTab(&m_ListDirB,		_T("Diagonal B"),	nTabIndex++,	FALSE);

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	
	int iStWidth	= (iWidth - iSpacing - iSpacing ) / 6;
	int iEdWidth	= iStWidth * 2;
	int iStHeight	= 25;

	// Comm
	m_st_Item[STI_SHA_INSPECT].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	// Comm 1
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_DataForamt].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SHA_DataForamt].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 2
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_OutMode].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SHA_OutMode].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 3
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_SensorType].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_SHA_SensorType].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Comm 4
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_BlackLevel].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SHA_BlackLevel].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	// Spec
	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_SPEC].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_ROI_Width].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SHA_ROI_Width].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_ROI_Height].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SHA_ROI_Height].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_ROI_Max].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SHA_ROI_Max].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_SHA_NormlizeIndex].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_ed_Item[EDT_SHA_NormlizeIndex].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

	iLeft = iMargin;
	iTop += (iStHeight + iSpacing) * 4;
	m_st_Item[STI_SHA_ROI].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop + iSpacing);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Shading::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfig)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}


//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::SetUpdateData()
{
	if (m_pstConfig == NULL)
		return;

	CString szValue;

	szValue.Format(_T("%d"), m_pstConfig->stInspect.iBlackLevel);
	m_ed_Item[EDT_SHA_BlackLevel].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->iNormalizeIndex);
	m_ed_Item[EDT_SHA_NormlizeIndex].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->iMaxROIWidth);
	m_ed_Item[EDT_SHA_ROI_Width].SetWindowText(szValue);

	szValue.Format(_T("%d"), m_pstConfig->iMaxROIHeight);
	m_ed_Item[EDT_SHA_ROI_Height].SetWindowText(szValue);


	if (m_pstConfig->iMaxROICount > ROI_Shading_Max)
	{
		m_pstConfig->iMaxROICount = ROI_Shading_Max;
	}

	szValue.Format(_T("%d"), m_pstConfig->iMaxROICount);
	m_ed_Item[EDT_SHA_ROI_Max].SetWindowText(szValue);

	m_cb_Item[CMB_SHA_DataForamt].SetCurSel(m_pstConfig->stInspect.eDataForamt);

	m_cb_Item[CMB_SHA_OutMode].SetCurSel(m_pstConfig->stInspect.eOutMode);

	m_cb_Item[CMB_SHA_SensorType].SetCurSel(m_pstConfig->stInspect.eSensorType);

	m_ListHor.SetMaxROI((UINT)m_pstConfig->iMaxROICount);
	m_ListHor.SetPtr_ConfigInfo(m_pstConfig->stInputHor);
	m_ListHor.InsertFullData();

	m_ListVer.SetMaxROI((UINT)m_pstConfig->iMaxROICount);
	m_ListVer.SetPtr_ConfigInfo(m_pstConfig->stInputVer);
	m_ListVer.InsertFullData();
	
	m_ListDirA.SetMaxROI((UINT)m_pstConfig->iMaxROICount);
	m_ListDirA.SetPtr_ConfigInfo(m_pstConfig->stInputDirA);
	m_ListDirA.InsertFullData();

	m_ListDirB.SetMaxROI((UINT)m_pstConfig->iMaxROICount);
	m_ListDirB.SetPtr_ConfigInfo(m_pstConfig->stInputDirB);
	m_ListDirB.InsertFullData();
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::GetUpdateData()
{
	if (m_pstConfig == NULL)
		return;

	CString szValue;

	m_ed_Item[EDT_SHA_BlackLevel].GetWindowText(szValue);
	m_pstConfig->stInspect.iBlackLevel = _ttoi(szValue);

	m_ed_Item[EDT_SHA_NormlizeIndex].GetWindowText(szValue);
	m_pstConfig->iNormalizeIndex = _ttoi(szValue);

	m_ed_Item[EDT_SHA_ROI_Width].GetWindowText(szValue);
	m_pstConfig->iMaxROIWidth = _ttoi(szValue);

	m_ed_Item[EDT_SHA_ROI_Height].GetWindowText(szValue);
	m_pstConfig->iMaxROIHeight = _ttoi(szValue);

	m_ed_Item[EDT_SHA_ROI_Max].GetWindowText(szValue);
	m_pstConfig->iMaxROICount = _ttoi(szValue);

	if (m_pstConfig->iMaxROICount > ROI_Shading_Max)
	{
		m_pstConfig->iMaxROICount = ROI_Shading_Max;
	}

	m_pstConfig->stInspect.eDataForamt	= (enDataForamt)m_cb_Item[CMB_SHA_DataForamt].GetCurSel();
	m_pstConfig->stInspect.eOutMode		= (enOutMode)m_cb_Item[CMB_SHA_OutMode].GetCurSel();
	m_pstConfig->stInspect.eSensorType	= (enSensorType)m_cb_Item[CMB_SHA_SensorType].GetCurSel();
}
