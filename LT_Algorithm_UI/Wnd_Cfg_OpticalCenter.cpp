﻿// Wnd_Cfg_OpticalCenter.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_OpticalCenter.h"
#include "resource.h"

static LPCTSTR	g_szOpticalCenter_Static[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	_T("STANDARD X"),
	_T("STANDARD Y"),
	_T("OFFSET X"),
	_T("OFFSET Y"),
	NULL
};

static LPCTSTR	g_szOpticalCenter_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

// CWnd_Cfg_OpticalCenter
typedef enum OpticalCenter_OptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,

	IDC_ED_SPEC_MIN  = 5001,
	IDC_ED_SPEC_MAX  = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_OpticalCenter, CWnd_Cfg_VIBase)

CWnd_Cfg_OpticalCenter::CWnd_Cfg_OpticalCenter()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_OpticalCenter::~CWnd_Cfg_OpticalCenter()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_OpticalCenter, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_OpticalCenter 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_OpticalCenter::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_CTP_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szOpticalCenter_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_CTP_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_CTP_SPEC ].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_CTP_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szOpticalCenter_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_CTP_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_CTP_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}
	m_List.SetOwner(GetOwner());
	m_List.SetWindowMessage(m_wm_ChangeOption);
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szOpticalCenter_Spec[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_OpticalCenter::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt		= OptOp_ItemNum;
	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iHeaderH	= 40;
	int iList_H		= iHeaderH + iIdxCnt * 20;
	int iCtrl_W		= iWidth / 5;
	int iCtrl_H		= 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth	= iWidth / 5;
	int iSTHeight	= 25;
	int iTopTemp	= 0;

	int iStWidth	= (iWidth - iSpacing - iSpacing) / 6;
	int iStHeight	= 25;

	m_st_Item[STI_CTP_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);
	
	// BUTTON 
// 	iTopTemp = iTop + iSTHeight + iSpacing;
// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_CTP_RESET].MoveWindow(iLeft, iTopTemp, iSTWidth, iSTHeight);
// 
// 	iLeft = cx - iMargin - iSTWidth;
// 	iTopTemp += iSTHeight + iSpacing;
// 	m_bn_Item[BTN_CTP_TEST].MoveWindow(iLeft, iTopTemp, iSTWidth, iSTHeight);

	// ITEM
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_CTP_STANDARD_X].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_CTP_STANDARD_X].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_CTP_STANDARD_Y].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_CTP_STANDARD_Y].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	// 
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_CTP_OFFSET_X].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_CTP_OFFSET_X].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	// 
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_CTP_OFFSET_Y].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_CTP_OFFSET_Y].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	// 리스트
	iLeft = iMargin;
	iTop += (iCtrl_H + iSpacing) * 2;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);

	// 이름
	iLeft = iMargin;
	iTop += iList_H + iSpacing + iSpacing + iSpacing;
	m_st_Item[STI_CTP_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth	= (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd	= iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd	= iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidth	= iCtrlWidth - iSpacing - iSTHeight;
	
	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_OpticalCenter::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_OpticalCenter::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay); 
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_OpticalCenter::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	//switch (nIdex)
	//{
	//case BTN_CTP_RESET:
	//	GetOwner()->SendMessage(m_wm_ShowWindow, 0, -1);
	//	break;

	//case BTN_CTP_TEST:
	//	GetOwner()->SendMessage(m_wm_ShowWindow, 0, VTID_OpticalCenter);
	//	break;
	//default:
	//	break;
	//}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_OpticalCenter::SetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_List.SetPtr_ConfigInfo(m_pstConfigInfo);
	m_List.InsertFullData();

	strValue.Format(_T("%d"), m_pstConfigInfo->nStandarX);
	m_ed_Item[EDT_CTP_STANDARD_X].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->nStandarY);
	m_ed_Item[EDT_CTP_STANDARD_Y].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iOffsetX);
	m_ed_Item[EDT_CTP_OFFSET_X].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->iOffsetY);
	m_ed_Item[EDT_CTP_OFFSET_Y].SetWindowText(strValue);

	for (UINT nSpec = 0; nSpec < Spec_OC_Max; nSpec++)
	{
		strValue.Format(_T("%d"), m_pstConfigInfo->stSpec_Min[nSpec].iValue);
		m_ed_SpecMin[nSpec].SetWindowText(strValue);

		strValue.Format(_T("%d"), m_pstConfigInfo->stSpec_Max[nSpec].iValue);
		m_ed_SpecMax[nSpec].SetWindowText(strValue);

		m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Min[nSpec].bEnable);
		m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Max[nSpec].bEnable);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_OpticalCenter::GetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_List.GetCellData();

	m_ed_Item[EDT_CTP_STANDARD_X].GetWindowText(strValue);
	m_pstConfigInfo->nStandarX = _ttoi(strValue);

	m_ed_Item[EDT_CTP_STANDARD_Y].GetWindowText(strValue);
	m_pstConfigInfo->nStandarY = _ttoi(strValue);

	m_ed_Item[EDT_CTP_OFFSET_X].GetWindowText(strValue);
	m_pstConfigInfo->iOffsetX = _ttoi(strValue);

	m_ed_Item[EDT_CTP_OFFSET_Y].GetWindowText(strValue);
	m_pstConfigInfo->iOffsetY = _ttoi(strValue);

	for (UINT nSpec = 0; nSpec < Spec_OC_Max; nSpec++)
	{
		m_ed_SpecMin[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Min[nSpec].iValue = _ttoi(strValue);

		m_ed_SpecMax[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Max[nSpec].iValue = _ttoi(strValue);

		m_pstConfigInfo->stSpec_Min[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
		m_pstConfigInfo->stSpec_Max[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
	}
}
