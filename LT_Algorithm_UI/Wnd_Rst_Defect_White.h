#ifndef Wnd_Rst_Defect_White_h__
#define Wnd_Rst_Defect_White_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_Defect_White.h"

// CWnd_Rst_Defect_White
typedef enum
{
	enDefect_White_Result_Count = 0,
	enDefect_White_Result_Max,

}enDefect_White_Result;

static	LPCTSTR	g_szDefect_White_Result[] =
{
	_T("Count"),
	NULL
};

class CWnd_Rst_Defect_White : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Defect_White)

public:
	CWnd_Rst_Defect_White();
	virtual ~CWnd_Rst_Defect_White();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enDefect_White_Result_Max];
	CVGStatic	m_st_Result[enDefect_White_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Defect_Black_h__
