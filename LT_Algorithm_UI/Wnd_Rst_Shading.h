#ifndef Wnd_Rst_Shading_h__
#define Wnd_Rst_Shading_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_UI_Shading.h"

// CWnd_Rst_Shading

static	LPCTSTR	g_szSHData[] =
{
	_T("NUM"),
	_T("Horizontal"),
	_T("Vertical"),
	_T("DiagonalA"),
	_T("DiagonalB"),
	NULL
};

typedef enum
{
	enShadingData_Header = 5,
	enShadingEachResultMax=19,
	enShadingData_MAX = 76,
}enumHeaderShadingData;

typedef enum
{
	enShading_CT = 0,
	enShading_CB,
	enShading_LC,
	enShading_RC,
	enShading_D_LT,
	enShading_D_LB,
	enShading_D_RT,
	enShading_D_RB,
	enShading_C,
	enShading_Max,

}enShading_Result;

class CWnd_Rst_Shading : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Shading)

public:
	CWnd_Rst_Shading();
	virtual ~CWnd_Rst_Shading();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enShadingEachResultMax];
	CVGStatic	m_st_Result[enShadingData_MAX];
	CVGStatic	m_st_NumHeader[enShadingData_MAX];

public:

	void Result_Display				(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue);
	void Result_Reset				();
};


#endif // Wnd_Rst_Shading_h__
