﻿#ifndef Wnd_Cfg_Shading_h__
#define Wnd_Cfg_Shading_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Shading.h"
#include "List_ShadingOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Shading
//-----------------------------------------------------------------------------
class CWnd_Cfg_Shading : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Shading)

public:
	CWnd_Cfg_Shading();
	virtual ~CWnd_Cfg_Shading();

	enum enShadingStatic
	{
		STI_SHA_INSPECT = 0,
		STI_SHA_SPEC,
		STI_SHA_ROI,
		STI_SHA_DataForamt,
		STI_SHA_OutMode,
		STI_SHA_SensorType,
		STI_SHA_BlackLevel,
		STI_SHA_ROI_Width,
		STI_SHA_ROI_Height,
		STI_SHA_ROI_Max,
		STI_SHA_NormlizeIndex,
		STI_SHA_MAX,
	};

	enum enShadingButton
	{
		BTN_SHA_MAX = 1,
	};

	enum enShadingComobox
	{
		CMB_SHA_DataForamt = 0,
		CMB_SHA_OutMode,
		CMB_SHA_SensorType,
		CMB_SHA_MAX,
	};

	enum enShadingEdit
	{
		EDT_SHA_BlackLevel = 0,
		EDT_SHA_ROI_Width,
		EDT_SHA_ROI_Height,
		EDT_SHA_ROI_Max,
		EDT_SHA_NormlizeIndex,
		EDT_SHA_MAX,
	};

protected:

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CFont				m_font;

	CMFCTabCtrl			m_tc_Option;

	CList_ShadingOp		m_ListHor;
	CList_ShadingOp		m_ListVer;
	CList_ShadingOp		m_ListDirA;
	CList_ShadingOp		m_ListDirB;

	CVGStatic			m_st_Item[STI_SHA_MAX];
	CButton				m_bn_Item[BTN_SHA_MAX];
	CComboBox			m_cb_Item[CMB_SHA_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SHA_MAX];

	ST_UI_Shading*		m_pstConfig = NULL;


public:

	void	SetPtr_RecipeInfo (ST_UI_Shading* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_Cfg_Shading_h__
