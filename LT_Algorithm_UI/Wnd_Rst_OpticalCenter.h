#ifndef Wnd_Rst_OpticalCenter_h__
#define Wnd_Rst_OpticalCenter_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_UI_OpticalCenter.h"
// CWnd_OpticalData
typedef enum
{
	enOpticalCenter_PosX = 0,
	enOpticalCenter_PosY,
	enOpticalCenter_OffsetX,
	enOpticalCenter_OffsetY,
	enOpticalCenter_Result_Max,
}enOpticalCenter_Result;



static	LPCTSTR	g_szOpticalCenter_Result[] =
{
	_T("Pos X"),
	_T("Pos Y"),
	_T("Offset X"),
	_T("Offset Y"),
	NULL
};

class CWnd_Rst_OpticalCenter : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_OpticalCenter)
public:
	CWnd_Rst_OpticalCenter();
	virtual ~CWnd_Rst_OpticalCenter();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd(CDC* pDC);

	CVGStatic	m_st_Header[enOpticalCenter_Result_Max];
	CVGStatic	m_st_Result[enOpticalCenter_Result_Max];


public:

	void Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset();
};


#endif // Wnd_OpticalData_h__
