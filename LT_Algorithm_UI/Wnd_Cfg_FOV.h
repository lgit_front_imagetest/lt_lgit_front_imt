#ifndef Wnd_Cfg_FOV_h__
#define Wnd_Cfg_FOV_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_FOV.h"
#include "List_FOVOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Rotate
//-----------------------------------------------------------------------------

static LPCTSTR	g_szChartType_Static_FOV[] =
{
	_T("DOT"),
	_T("CROSSDOT"),
	_T("GRID"),
	NULL
};

static LPCTSTR	g_szFiducialMarkType_Static_FOV[] =
{
	_T("Dot"),
	_T("Cross Dot"),
	_T("Grid"),
	NULL
};


class CWnd_Cfg_FOV : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_FOV)

public:
	CWnd_Cfg_FOV();
	virtual ~CWnd_Cfg_FOV();

	//!SH _181106: UI ���� ����
	/*
	Combo CString ChartType;
	Combo int	 nFiducialmarkType;
	Edit  int	 nFiducialMarkNum;
	Edit  double dModuleCartDistance;
	Edit  double dRealGapX;
	Edit  double dRealGapY;
	*/
	enum enFOVStatic
	{
		STI_FOV_PARAM = 0,
		STI_FOV_SPEC,
		STI_FOV_INSPECT,
		STI_FOV_DataForamt,
		STI_FOV_OutMode,
		STI_FOV_SensorType,
		STI_FOV_BlackLevel,
		// 		STI_ROT_ROIBoxSize,
		// 		STI_ROT_MaxROIBoxSize,
		//		STI_ROT_Radius,
		STI_FOV_ChartType,
		STI_FOV_FiducialMarkType,
		STI_FOV_RealGapx,
		STI_FOV_RealGapY,
		STI_FOV_ModuleChartDistance,
		//		STI_ROT_DistanceXFromCenter,
		//		STI_ROT_DistanceYFromCenter,
		STI_FOV_D,
		STI_FOV_H,
		STI_FOV_V,

		STI_FOV_Offset,
		STI_FOV_MAX,
	};


	enum enFOVButton
	{
		BTN_FOV_RESET = 0,
		BTN_FOV_TEST,
		BTN_FOV_MAX,
	};

	enum enFOVComobox
	{
		CMB_FOV_DataForamt = 0,
		CMB_FOV_OutMode,
		CMB_FOV_SensorType,
		CMB_FOV_ChartType,
		CMB_FOV_FiducialMarkType,
		CMB_FOV_MAX,
	};

	enum enFOVEdit
	{
		EDT_FOV_BlackLevel = 0,
		// 		EDT_ROT_ROIBoxSize,
		// 		EDT_ROT_MaxROIBoxSize,
		//		EDT_ROT_Radius,
		EDT_FOV_RealGapx,
		EDT_FOV_RealGapY,
		EDT_FOV_ModuleChartDistance,
		//		EDT_ROT_DistanceXFromCenter,
		//		EDT_ROT_DistanceYFromCenter,
		EDT_FOV_Offset,
		EDT_FOV_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	ST_UI_FOV*			m_pstConfigInfo = NULL;
	CList_FOVOp		m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_FOV_MAX];
	CButton				m_bn_Item[BTN_FOV_MAX];
	CComboBox			m_cb_Item[CMB_FOV_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_FOV_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_FOV_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_FOV_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_FOV_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_FOV_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_FOV_MAX];

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl(UINT nID);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

public:

	void	SetPtr_RecipeInfo(ST_UI_FOV* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetUpdateData();
	void GetUpdateData();

	//void Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_Cfg_Rotate_h__
