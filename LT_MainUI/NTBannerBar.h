﻿//*****************************************************************************
// Filename	: 	NTBannerBar.h
//
// Created	:	2010/03/18
// Modified	:	2010/03/18 - 17:09
//
// Author	:	PiRing
//	
// Purpose	:	배너 & 메뉴 버튼 & 시계 출력용 윈도우
//*****************************************************************************
#ifndef __NTBannerBar_H__
#define __NTBannerBar_H__

#pragma once

//#include "ImageMenuButton.h"
#include "NTClockWnd.h"
#include "NTStyle.h"

static const UINT uiBannerBarID = AFX_IDW_DIALOGBAR +2;

//#define SC_OPTION	0xF200

#define USE_CLOCK		// 시계 표시
#define USE_BANNER		// 배너 영역 표시 

//=============================================================================
// CNTBannerBar
//=============================================================================
class CNTBannerBar : public CPane
{
	DECLARE_DYNAMIC(CNTBannerBar)

public:
	CNTBannerBar();
	virtual ~CNTBannerBar();

	// Overrides
protected:
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual void	DoPaint					(CDC* pDCPaint);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	virtual UINT	HitTest					(const CPoint& pt) const;

public:
	virtual BOOL	Create					(CWnd* pParentWnd, UINT nID = uiBannerBarID);
	virtual BOOL	CreateEx				(CWnd* pParentWnd, UINT nID = uiBannerBarID);
	virtual CSize	CalcFixedLayout			(BOOL, BOOL);

protected:
	afx_msg LRESULT OnSetText				(WPARAM, LPARAM lParam);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnExitProgram			();
	afx_msg void	OnOption				();
	DECLARE_MESSAGE_MAP()

	// Context 팝업 메뉴 동작을 막음..
	//virtual BOOL	OnShowControlBarMenu(CPoint point) {return FALSE;};


protected:

	CString				m_strCaption;

	HICON				m_hIcon;
	CSize				m_szIcon;

	BOOL				m_bParentActive;
	BOOL				m_bParentMaximize;

	int					m_SystemHeight;
	int					m_CaptionHeight;
	CFont				m_CaptionFont;

	CSize				m_szBtnSize;
	//CImageMenuButton	m_BtnOption;
	//CImageMenuButton	m_BtnExit;

#ifdef USE_CLOCK
	CNTClockWnd			m_wndClock;
#endif

	UINT				m_nID_BnExit;
	UINT				m_nID_BnOption;

	UINT				m_wmOption;

public:
	virtual void		SetIcon					(HICON hIcon);

	void				SetCaptionHeight		(int nHeight);
	int					GetCaptionHeight		() const;
	void				SetCaptionFont			(const LOGFONT& lf);
	HFONT				GetCaptionFont			() const;

	virtual COLORREF	GetCaptionTextColor		() const;

	void				SetParentActive			(BOOL bParentActive = true);
	BOOL				IsParentActive			() const;
	void				SetParentMaximize		(BOOL bParentMaximize = true);
	BOOL				IsParentMaximize		() const;

	void				SetButtonImage			(UINT nIDExit, UINT nIDOption)
	{
		m_nID_BnExit	= nIDExit;
		m_nID_BnOption	= nIDOption;
	};

	void				SetWM_Option			(UINT nWindowMessage) {m_wmOption = nWindowMessage;};
};

inline
int CNTBannerBar::GetCaptionHeight () const
{
	return m_CaptionHeight;
}

inline
HFONT CNTBannerBar::GetCaptionFont () const
{
	return (HFONT)m_CaptionFont.GetSafeHandle ();
}

inline
BOOL CNTBannerBar::IsParentActive () const
{
	return m_bParentActive;
}

inline
BOOL CNTBannerBar::IsParentMaximize () const
{
	return m_bParentMaximize;
}

#endif // __NTBannerBar_H__


