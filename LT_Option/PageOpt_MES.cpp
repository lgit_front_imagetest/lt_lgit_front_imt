﻿//*****************************************************************************
// Filename	: PageOpt_MES.cpp
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 15:33
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_MES.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"
#include <memory>
#include "MFCPropertyGridProperties.h"

#define ID_PROPGRID_IPADDR (21)

IMPLEMENT_DYNAMIC(CPageOpt_MES, CPageOption)

//=============================================================================
// 생성자
//=============================================================================
CPageOpt_MES::CPageOpt_MES(void)
{
}

CPageOpt_MES::CPageOpt_MES(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
// 소멸자
//=============================================================================
CPageOpt_MES::~CPageOpt_MES(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_MES, CPageOption)	
END_MESSAGE_MAP()

//=============================================================================
// CPageOpt_MES 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CPageOpt_MES::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_MES::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_MES::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_MES::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_MES::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_MES::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("MES Setting")));

	CMFCPropertyGridProperty* pProp = NULL;

	// 설비코드
	apGroup_Comm->AddSubItem(new CMFCPropertyGridProperty(_T("Equipment ID"), (COleVariant)_T(""), _T("Equipment ID")));

	// IP Address	
	in_addr addr;
	addr.s_addr = inet_addr("10.224.6.60");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);
	apGroup_Comm->AddSubItem(pProp);

	// Port
	pProp = new CMFCPropertyGridProperty(_T("Port"), (_variant_t)41000l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);
	apGroup_Comm->AddSubItem(pProp);

	// MES 저장 경로
	apGroup_Comm->AddSubItem(new CMFCPropertyGridFileProperty(_T("MES Log Path"), _T("C:\\MES\\")));

	// NIC IP Address
	addr.s_addr = inet_addr("10.224.6.60");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("NIC IP Address"), addr, OPT_DESC_NIC_IP_ADDRESS, ID_PROPGRID_IPADDR);
	apGroup_Comm->AddSubItem(pProp);

	m_wndPropList.AddProperty(apGroup_Comm.release());
}

//=============================================================================
// Method		: CPageOpt_MES::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_MES::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_MES(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_MES::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_MES::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_MES(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: GetOption
// Access		: protected  
// Returns		: Luritech_Option::stOpt_MES
// Qualifier	:
// Last Update	: 2016/5/18 - 16:22
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_MES CPageOpt_MES::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	USES_CONVERSION;

	// 설비 코드
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szEquipmentID = strValue;

	// 서버 IP Address ---------------------------
	m_stOption.Address.dwAddress = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	// 서버 IP Port
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_I4);
	m_stOption.Address.dwPort = varData.intVal;

	// MES 저장 경로
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_MESLog = strValue;

	// NIC IP Address
	m_stOption.dwNIC_Address = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	return m_stOption;
}

//=============================================================================
// Method		: SetOption
// Access		: protected  
// Returns		: void
// Parameter	: stOpt_MES stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:22
// Desc.		:
//=============================================================================
void CPageOpt_MES::SetOption( stOpt_MES stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 그룹 1 통신 종류
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	//Equipment ID
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szEquipmentID);
	
	// 서버 IP Address ----------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.Address.dwAddress));
	
	// 서버 IP Port
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.Address.dwPort);

	// MES 저장 경로
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_MESLog);

	// NIC IP Address
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.dwNIC_Address));

}
