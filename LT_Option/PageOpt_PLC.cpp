//*****************************************************************************
// Filename	: PageOpt_PLC.cpp
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 15:33
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_PLC.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"
#include <memory>
#include "MFCPropertyGridProperties.h"

#define ID_PROPGRID_IPADDR (21)

IMPLEMENT_DYNAMIC(CPageOpt_PLC, CPageOption)

//=============================================================================
// 생성자
//=============================================================================
CPageOpt_PLC::CPageOpt_PLC(void)
{
}

CPageOpt_PLC::CPageOpt_PLC(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
// 소멸자
//=============================================================================
CPageOpt_PLC::~CPageOpt_PLC(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_PLC, CPageOption)	
END_MESSAGE_MAP()

//=============================================================================
// CPageOpt_PLC 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CPageOpt_PLC::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_PLC::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_PLC::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_PLC::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_PLC::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_PLC::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("Comm Setting")));
	CMFCPropertyGridProperty* pProp = NULL;

	// PLC 사용여부
// 	pProp = new CMFCPropertyGridProperty(_T("Use PLC"), lpszUsableTable[0], OPT_DESC_PLC_USE);
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Comm->AddSubItem(pProp);

	// PLC IP Address	
	in_addr addr;
	addr.s_addr=inet_addr("192.168.0.20");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("PLC IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);	
	apGroup_Comm->AddSubItem(pProp);

	// PLC Port
	pProp = new CMFCPropertyGridProperty(_T("PLC IP Port"), (_variant_t) 1296l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);	
	apGroup_Comm->AddSubItem(pProp);

	// NIC IP Address	
	addr.s_addr = inet_addr("192.168.0.20");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("NIC IP Address"), addr, OPT_DESC_NIC_IP_ADDRESS, ID_PROPGRID_IPADDR);
	apGroup_Comm->AddSubItem(pProp);

	m_wndPropList.AddProperty(apGroup_Comm.release());
	
	//--------------------------------------------------------
	// 그외 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Misc(new CMFCPropertyGridProperty(_T("PLC Setting")));

	// Monitor Cycle
	pProp = new CMFCPropertyGridProperty(_T("PLC Monitoring Cycle (ms)"), (_variant_t) 500l, OPT_DESC_PLC_MONITOR_CYCLE);
	pProp->EnableSpinControl(TRUE, 150, 1000);	
	apGroup_Misc->AddSubItem(pProp);

	// Device Code;
	pProp = new CMFCPropertyGridProperty(_T("Device Code"), lpszDeviceCodeTable[0], OPT_DESC_PLC_DEVICE_CODE);
	for (int iIndex = 0; NULL != lpszDeviceCodeTable[iIndex]; iIndex++)
	{
		pProp->AddOption(lpszDeviceCodeTable[iIndex]);
	}	
	pProp->AllowEdit(FALSE);
	apGroup_Misc->AddSubItem(pProp);

	// 읽기용 Head Device;
	pProp = new CMFCPropertyGridProperty(_T("PLC Read Head Device"), (_variant_t) 50l, OPT_DESC_PLC_HEAD_DEVICE_READ);
	pProp->EnableSpinControl(TRUE, 0, 99999);	
	apGroup_Misc->AddSubItem(pProp);

	// 쓰기용 Head Device;
	pProp = new CMFCPropertyGridProperty(_T("PLC Write Head Device"), (_variant_t) 700l, OPT_DESC_PLC_HEAD_DEVICE_WRITE);
	pProp->EnableSpinControl(TRUE, 0, 99999);	
	apGroup_Misc->AddSubItem(pProp);	

	
	m_wndPropList.AddProperty(apGroup_Misc.release());
}

//=============================================================================
// Method		: CPageOpt_PLC::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_PLC::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_PLC(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PLC::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_PLC::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_PLC(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PLC::GetOption
// Access		: protected 
// Returns		: BiwonTech_Option::stOpt_PLC
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_PLC CPageOpt_PLC::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	USES_CONVERSION;

	// PLC 사용여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData	 = rVariant.Detach();
// 	ASSERT (varData.vt == VT_BSTR);	
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UsePLC = (BOOL)nIndex;

	// PLC IP Address ---------------------------
	m_stOption.Address.dwAddress = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	// PLC IP Port
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();	
	ASSERT (varData.vt == VT_I4);	
	m_stOption.Address.dwPort = varData.intVal;	

	// NIC IP Address
	m_stOption.dwNIC_Address = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	//---------------------------------------------------------------
	// 그룹 3 기타 설정
	//---------------------------------------------------------------
	nSubItemIndex	= 0;

	pPropertyGroup	= m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount	= pPropertyGroup->GetSubItemsCount();

	// PLC 모니터링 주기
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();	
	ASSERT (varData.vt == VT_I4);	
	m_stOption.MonitorCycle = varData.intVal;

	// PLC Device Code
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);		
	strValue = OLE2A(varData.bstrVal);
	m_stOption.DeviceCode = strValue;

	// PLC 읽기용 Head Device
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();	
	ASSERT (varData.vt == VT_I4);	
	m_stOption.HeadDevice_Read = varData.intVal;

	// PLC 쓰기용 Head Device
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();
	ASSERT (varData.vt == VT_I4);
	m_stOption.HeadDevice_Write = varData.intVal;
	
	

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_PLC::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOpt_PLC stOption
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
void CPageOpt_PLC::SetOption( stOpt_PLC stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 그룹 1 통신 종류
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	// PLC 사용여부
//	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UsePLC]);

	// PLC IP Address ----------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.Address.dwAddress));
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetOriginalValue(ULONG_VARIANT(m_stOption.Address.dwAddress));

	// PLC IP Port
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.Address.dwPort);

	// NIC IP Address
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.dwNIC_Address));

	//---------------------------------------------------------------
	// 그룹 3 기타 설정
	//---------------------------------------------------------------
	nSubItemIndex	= 0;

	pPropertyGroup	= m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount	= pPropertyGroup->GetSubItemsCount();

	// PLC 모니터링 주기
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.MonitorCycle);

	// PLC Device Code
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.DeviceCode);

	// PLC 읽기용 Head Device
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.HeadDevice_Read);

	// PLC 쓰기용 Head Device
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.HeadDevice_Write);

	

}
