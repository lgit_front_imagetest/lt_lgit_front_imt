﻿//*****************************************************************************
// Filename	: 	ImageProcessing.h
// Created	:	2017/6/16 - 13:20
// Modified	:	2017/6/16 - 13:20
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef ImageProcessing_h__
#define ImageProcessing_h__

#pragma once

#include <windows.h>

class CImageProcessing
{
public:
	CImageProcessing();
	~CImageProcessing();

	static void Contrast(double contrast, unsigned char* lpbyImage, const unsigned int width, const unsigned int height);
	static void Contrast(double contrast, LPRGBQUAD lpbyImage);
	static void Contrast(double contrast, LPRGBTRIPLE lpbyImage);


	static void Saturation(double saturation, unsigned char* lpbyImage, const unsigned int width, const unsigned int height);
	static void Saturation(double saturation, LPRGBQUAD lpbyImage);
	static void Saturation(double saturation, LPRGBTRIPLE lpbyImage);



	void Levels(unsigned int *rgb_buffer, const unsigned int width, const unsigned int height, const float exposure, const float brightness, const float contrast, const float saturation);
	void applyLevels(unsigned int *rgb_buffer, const unsigned int width, const unsigned int height, const float exposure, const float brightness, const float contrast, const float saturation);
};

#endif // ImageProcessing_h__
