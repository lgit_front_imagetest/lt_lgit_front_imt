﻿//*****************************************************************************
// Filename	: 	Def_DAQWrapper.h
// Created	:	2017/3/16 - 17:25
// Modified	:	2017/3/16 - 17:25
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_DAQWrapper_h__
#define Def_DAQWrapper_h__

#pragma once

#include <windows.h>
#include <math.h>

#define		MAX_DAQ_CHANNEL		6				// 최대 6채널 지원
#define		MAX_FRAME_SIZE		5120 * 2880 * 4	// 5K 해상도 56.25 MByte
#define		DAQ_PACKET_SIZE		16384
#define		MAX_I2C_RETRY_CNT	5

// 영상 수신시 처리될 콜백 함수 원형
// 클래스 포인터, 보드 번호(채널), 영상 데이터, 해상도 x, 해상도 y, 영상포맷
typedef void(*Callback_RecvFrame)(PVOID, UINT, LPBYTE, DWORD, DWORD, LONG);

// 영상 출력 상태 변경시 처리될 콜백 함수 원형
// 클래스 포인터, 보드 번호(채널), 영상 상태
typedef void(*Callback_ChgStatus)(PVOID, UINT, UINT);

//-------------------------------------------------------------------
// DAQ 보드 함수에 사용되는 상수값
//-------------------------------------------------------------------
typedef enum enDAQDataMode
{
	DAQ_Data_8bit_Mode,
	DAQ_Data_16bit_Mode,
	DAQ_Data_24bit_Mode,
	DAQ_Data_32bit_Mode,		// Not Used
	DAQ_Data_16bit_YUV_Mode,
	DAQ_Data_Cnt,
};

typedef enum enDAQVideoMode
{
	DAQ_Video_Signal_Mode,		// (Vsync, Hsync, De)
	DAQ_Video_BT656_Mode,
	DAQ_Video_MIPI_Mode,
	DAQ_Video_Cnt,
};

typedef enum enDAQHsyncPolarity
{
	DAQ_HsyncPol_Normal,		// (Vsync, Hsync, De)
	DAQ_HsyncPol_Inverse,
	DAQ_HsyncPol_Cnt,
};

typedef enum enDAQPclkPolarity
{
	DAQ_PclkPol_RisingEdge,		// (Vsync, Hsync, De)
	DAQ_PclkPol_FallingEdge,
	DAQ_PclkPol_Cnt,
};

typedef enum enDAQDvalUse
{
	DAQ_DeUse_HSync_Use,		// (Vsync, Hsync, De)
	DAQ_DeUse_DVAL_Use,
	DAQ_DeUse_Cnt,
};

//-------------------------------------------------------------------
// 추가 열거형
//-------------------------------------------------------------------
typedef enum enDAQBoardNumber
{
	DAQBoardNumber_0,
	DAQBoardNumber_1,
	DAQBoardNumber_2,
	DAQBoardNumber_3,
	DAQBoardNumber_4,
	DAQBoardNumber_5,
	DAQBoardNumber_Cnt
};

typedef enum enDAQMIPILane
{
	DAQMIPILane_1,
	DAQMIPILane_2,
	DAQMIPILane_None,
	DAQMIPILane_4,
	DAQMIPILane_Cnt
};

typedef enum enDAQClockSelect
{
	DAQClockSelect_FixedClock,
	DAQClockSelect_PgmClock,
	DAQClockSelect_Cnt
};

typedef enum enDAQClockUse
{
	DAQClockUse_Off,
	DAQClockUse_On,
	DAQClockUse_Cnt
};

typedef enum enConvFormat
{
	Conv_YCbYCr_BGGR,
	Conv_YCrYCb_RGGB,
	Conv_CrYCbY_GBRG,
	Conv_CbYCrY_GRBG,
	Conv_Max
};

typedef enum enImgSensorType
{
	enImgSensorType_YCBCR,
	enImgSensorType_RGBbayer,
	enImgSensorType_RGBbayer10,
	enImgSensorType_RGBbayer12,
	enImgSensorType_RGB888,
	enImgSensorType_Monochrome_12bit,			// LGIT OMS Entry, OMS Front
	enImgSensorType_Monochrome_CCCC,	// LGIT MRA2, IKC
	enImgSensorType_Cnt
};

typedef enum enCropFrame
{
	Crop_None,
	Crop_OddFrame,
	Crop_EvenFrame,
	Crop_Max,
};

//-------------------------------------------------------------------
// 2Byte 이미지 데이터 포맷
//-------------------------------------------------------------------
#define	 MULTI_BUFFERING_CNT		2

typedef struct _tag_FrameData
{
	// 체크 플래그
	BOOL			bStore;			// 영상 데이터가 버퍼에 저장이 되어있는가 판단 (비어 있는 버퍼와 구별하기 위한 용도)

	BYTE			byBitPerPixels;
	DWORD			dwWidth;		// 영상 가로 크기
	DWORD			dwHeight;		// 영상 세로 크기
	DWORD			dwSize;			// 버퍼 크기 (Byte)

	LPBYTE			pFrameDataz[MULTI_BUFFERING_CNT];	// 최종 영상 버퍼 (더블 버퍼링 / 트리플 버퍼링)
	INT				iReadBufIndex;
	INT				iWriteBufIndex;

	_tag_FrameData()
	{
		bStore			= FALSE;

		dwWidth			= 0;
		dwHeight		= 0;
		dwSize			= 0;

		for (UINT nIdx = 0; nIdx < MULTI_BUFFERING_CNT; nIdx++)
		{
			pFrameDataz[nIdx]	= nullptr;
		}
		iReadBufIndex	= -1;	// 더블 버퍼링 : 0, 1	트리플 버퍼링 : 0, 1, 2
		iWriteBufIndex	= 0;
	};

	~_tag_FrameData()
	{
		ReleaseMem();
	};

	void AssignMem(__in DWORD dwSizeX, __in DWORD dwSizeY, __in DWORD dwMemSize)
	{
		dwWidth = dwSizeX;
		dwHeight = dwSizeY;

		if (dwSize != dwMemSize)
		{
			ReleaseMem();

			dwSize = dwMemSize;
		}

		for (UINT nIdx = 0; nIdx < MULTI_BUFFERING_CNT; nIdx++)
		{
			if ((nullptr == pFrameDataz[nIdx]) && (0 != dwSize))
			{
				pFrameDataz[nIdx] = new BYTE[dwSize];
			}
		}
		
	};

	void ReleaseMem()
	{
		for (UINT nIdx = 0; nIdx < MULTI_BUFFERING_CNT; nIdx++)
		{
			if (nullptr != pFrameDataz[nIdx])
			{
				delete[] (pFrameDataz[nIdx]);

				pFrameDataz[nIdx] = nullptr;
			}
		}
	};

	LPBYTE GetReadFrameData()
	{
		if (0 <= iReadBufIndex)
		{
			return pFrameDataz[iReadBufIndex];
		}
		else
		{
			return nullptr;
		}
	};

	LPBYTE GetWriteFrameData()
	{
		if (0 <= iWriteBufIndex)
		{
			return pFrameDataz[iWriteBufIndex];
		}
		else
		{
			return nullptr;
		}
	};

	void IncreaseBufferIndex()
	{
		if (iReadBufIndex < (MULTI_BUFFERING_CNT - 1))
		{
			++iReadBufIndex;
		}
		else
		{
			iReadBufIndex = 0;
		}

		if (iWriteBufIndex < (MULTI_BUFFERING_CNT - 1))
		{
			++iWriteBufIndex;
		}
		else
		{
			iWriteBufIndex = 0;
		}
	};

	// 버퍼 초기화
	void FlushBuffer()
	{

	};

}ST_FrameData, *PST_FrameData;

//-------------------------------------------------------------------
// 영상 데이터 저장용 구조체
//-------------------------------------------------------------------
typedef struct _tag_VideoRGB
{
	// 체크 플래그
	BOOL			m_bRGB32;		// TRUE: RGBQuad, FALSE: RQGTriple
	BOOL			m_bStore;		// 영상 데이터가 버퍼에 저장이 되어있는가 판단 (비어 있는 버퍼와 구별하기 위한 용도)

	DWORD			m_dwWidth;		// 영상 가로 크기
	DWORD			m_dwHeight;		// 영상 세로 크기
	DWORD			m_dwSize;		// 버퍼 크기 (Byte)

	RGBQUAD**		m_pMatRGBA;		// 최종 영상 버퍼
	RGBTRIPLE**		m_pMatRGB;		// 최종 영상 버퍼

	RGBQUAD**		m_pMatRGBA_DB;	// 더블 버퍼링
	RGBTRIPLE**		m_pMatRGB_DB;	// 더블 버퍼링

	INT				iReadBufIndex;
	INT				iWriteBufIndex;

	_tag_VideoRGB()
	{
		m_bRGB32		= TRUE;
		m_bStore		= FALSE;

		m_dwWidth		= 0;
		m_dwHeight		= 0;
		m_dwSize		= 0;

		m_pMatRGBA		= nullptr;
		m_pMatRGB		= nullptr;
		m_pMatRGBA_DB	= nullptr;
		m_pMatRGB_DB	= nullptr;

		iReadBufIndex	= -1;	// 더블 버퍼링 : 0, 1	트리플 버퍼링 : 0, 1, 2
		iWriteBufIndex	= 0;
	};

	~_tag_VideoRGB()
	{
		ReleaseMem();
	};

	void AssignMem(DWORD dwSizeX, DWORD dwSizeY)
	{
		m_dwWidth	= dwSizeX;
		m_dwHeight	= dwSizeY;

		if (m_bRGB32)
		{
			if (m_dwSize != (dwSizeX * dwSizeY * 4))
			{
				ReleaseMem();

				m_dwSize = dwSizeX * dwSizeY * 4;
			}

			if ((nullptr == m_pMatRGBA) && (0 != m_dwSize))
			{
				m_pMatRGBA = new RGBQUAD*[dwSizeY];
				m_pMatRGBA[0] = new RGBQUAD[dwSizeX * dwSizeY];
				for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
				{
					m_pMatRGBA[dwIdx] = m_pMatRGBA[dwIdx - 1] + dwSizeX;
				}

				m_pMatRGBA_DB = new RGBQUAD*[dwSizeY];
				m_pMatRGBA_DB[0] = new RGBQUAD[dwSizeX * dwSizeY];
				for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
				{
					m_pMatRGBA_DB[dwIdx] = m_pMatRGBA_DB[dwIdx - 1] + dwSizeX;
				}
			}
		}
		else
		{
			if (m_dwSize != (dwSizeX * dwSizeY * 3))
			{
				ReleaseMem();

				m_dwSize = dwSizeX * dwSizeY * 3;
			}

			if ((nullptr == m_pMatRGB) && (0 != m_dwSize))
			{
				m_pMatRGB = new RGBTRIPLE*[dwSizeY];
				m_pMatRGB[0] = new RGBTRIPLE[dwSizeX * dwSizeY];
				for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
				{
					m_pMatRGB[dwIdx] = m_pMatRGB[dwIdx - 1] + dwSizeX;
				}

				m_pMatRGB_DB = new RGBTRIPLE*[dwSizeY];
				m_pMatRGB_DB[0] = new RGBTRIPLE[dwSizeX * dwSizeY];
				for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
				{
					m_pMatRGB_DB[dwIdx] = m_pMatRGB_DB[dwIdx - 1] + dwSizeX;
				}
			}
		}
	};

	void ReleaseMem()
	{
		if (nullptr != m_pMatRGBA)
		{
			delete[] m_pMatRGBA[0];
			delete[] m_pMatRGBA;

			m_pMatRGBA = nullptr;
		}

		if (nullptr != m_pMatRGB)
		{
			delete[] m_pMatRGB[0];
			delete[] m_pMatRGB;

			m_pMatRGB = nullptr;
		}

		if (nullptr != m_pMatRGBA_DB)
		{
			delete[] m_pMatRGBA_DB[0];
			delete[] m_pMatRGBA_DB;

			m_pMatRGBA_DB = nullptr;
		}

		if (nullptr != m_pMatRGB_DB)
		{
			delete[] m_pMatRGB_DB[0];
			delete[] m_pMatRGB_DB;

			m_pMatRGB_DB = nullptr;
		}
	};

	LPBYTE	GetReadRGBData()
	{
		if (m_bRGB32)
		{
			if (0 == iReadBufIndex)
			{
				if (NULL != m_pMatRGBA)
				{
					return (LPBYTE)m_pMatRGBA[0];
				}
				else
				{
					return NULL;
				}
			}
			else if (0 < iReadBufIndex)
			{
				if (NULL != m_pMatRGBA_DB)
				{
					return (LPBYTE)m_pMatRGBA_DB[0];
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			if (0 == iReadBufIndex)
			{
				if (NULL != m_pMatRGB)
				{
					return (LPBYTE)m_pMatRGB[0];
				}
				else
				{
					return NULL;
				}
			}
			else if (0 < iReadBufIndex)
			{
				if (NULL != m_pMatRGB_DB)
				{
					return (LPBYTE)m_pMatRGB_DB[0];
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}
	};
	
	LPBYTE	GetWriteRGBData()
	{
		if (m_bRGB32)
		{
			if (0 == iWriteBufIndex)
			{
				if (NULL != m_pMatRGBA)
				{
					return (LPBYTE)m_pMatRGBA[0];
				}
				else
				{
					return NULL;
				}
			}
			else if (0 < iWriteBufIndex)
			{
				if (NULL != m_pMatRGBA_DB)
				{
					return (LPBYTE)m_pMatRGBA_DB[0];
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			if (0 == iWriteBufIndex)
			{
				if (NULL != m_pMatRGB)
				{
					return (LPBYTE)m_pMatRGB[0];
				}
				else
				{
					return NULL;
				}
			}
			else if (0 < iWriteBufIndex)
			{
				if (NULL != m_pMatRGB_DB)
				{
					return (LPBYTE)m_pMatRGB_DB[0];
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}
	};

	void IncreaseBufferIndex()
	{
		if (iReadBufIndex < (MULTI_BUFFERING_CNT - 1))
		{
			++iReadBufIndex;
		}
		else
		{
			iReadBufIndex = 0;
		}

		if (iWriteBufIndex < (MULTI_BUFFERING_CNT - 1))
		{
			++iWriteBufIndex;
		}
		else
		{
			iWriteBufIndex = 0;
		}
	};

	// 버퍼 초기화
	void FlushBuffer()
	{

	};

}ST_VideoRGB, *PST_VideoRGB;

//-------------------------------------------------------------------
// DAQ 보드 상태 저장용 구조체
//-------------------------------------------------------------------
typedef struct _tag_DAQOption
{
	DWORD				dwClock;
	enDAQDataMode		nDataMode;
	enDAQHsyncPolarity	nHsyncPolarity;
	enDAQPclkPolarity	nPClockPolarity;
	enDAQDvalUse		nDvalUse;
	enDAQVideoMode		nVideoMode;

	enDAQMIPILane		nMIPILane;
	enDAQClockSelect	nClockSelect;
	enDAQClockUse		nClockUse;

	float				fMIPI_VIO;			// MIPI I2C 설정용

	double				dWidthMultiple;		// 영상 너비 보정 계수
	double				dHeightMultiple;	// 영상 높이 보정 계수

	enConvFormat		nConvFormat;		// YUV, Bayer 포맷 추가 옵션
	enImgSensorType		nSensorType;		// 센서 영상 포맷
	DWORD				dwBitWise;			// 영상 픽셀 AND 연산 
	BYTE				byBitPerPixels;		// 픽셀당 비트 사용 수

	DOUBLE				dShiftExp;			// 영상 밝기 조정 옵션

	DOUBLE				dAlpha;
	DOUBLE				dBeta;

	enCropFrame			nCropFrame;			// 수신된 영상 잘라내기 옵션
	UINT				nCropLine_Top;
	UINT				nCropLine_Bottom;

	_tag_DAQOption()
	{
		dwClock				= 10000;
		nDataMode			= enDAQDataMode::DAQ_Data_8bit_Mode;
		nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		nPClockPolarity		= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		nDvalUse			= enDAQDvalUse::DAQ_DeUse_HSync_Use;
		nVideoMode			= enDAQVideoMode::DAQ_Video_Signal_Mode;

		nMIPILane			= enDAQMIPILane::DAQMIPILane_1;
		nClockSelect		= enDAQClockSelect::DAQClockSelect_FixedClock;
		nClockUse			= enDAQClockUse::DAQClockUse_Off;

		fMIPI_VIO			= 3.3f;

		dWidthMultiple		= 1.0;
		dHeightMultiple		= 1.0;

		nSensorType			= enImgSensorType::enImgSensorType_YCBCR;
		nConvFormat			= enConvFormat::Conv_YCbYCr_BGGR;
		dwBitWise			= 0xff;
		byBitPerPixels		= 12;
		
		dShiftExp			= 0.0f;
		dAlpha				= 1.0f;
		dBeta				= 0.0f;

		nCropFrame			= enCropFrame::Crop_None;
		nCropLine_Top		= 0;
		nCropLine_Bottom	= 0;
	};

	void Reset()
	{
		dwClock				= 1000000;
		nDataMode			= enDAQDataMode::DAQ_Data_8bit_Mode;
		nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		nPClockPolarity		= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		nDvalUse			= enDAQDvalUse::DAQ_DeUse_HSync_Use;
		nVideoMode			= enDAQVideoMode::DAQ_Video_Signal_Mode;
		nMIPILane			= enDAQMIPILane::DAQMIPILane_1;
		nClockSelect		= enDAQClockSelect::DAQClockSelect_FixedClock;
		nClockUse			= enDAQClockUse::DAQClockUse_Off;

		fMIPI_VIO			= 3.3f;

		dWidthMultiple		= 1.0;
		dHeightMultiple		= 1.0;

		nSensorType			= enImgSensorType::enImgSensorType_YCBCR;
		nConvFormat			= enConvFormat::Conv_YCbYCr_BGGR;
		dwBitWise			= 0x0fff;
		byBitPerPixels		= 12;
		
		dShiftExp			= 0.0f;
		dAlpha				= 1.0f;
		dBeta				= 0.0f;

		nCropFrame			= Crop_None;
		nCropLine_Top		= 0;
		nCropLine_Bottom	= 0;
	};


	_tag_DAQOption& operator= (_tag_DAQOption& ref)
	{
		dwClock				= ref.dwClock;
		nDataMode			= ref.nDataMode;
		nHsyncPolarity		= ref.nHsyncPolarity;
		nPClockPolarity		= ref.nPClockPolarity;
		nDvalUse			= ref.nDvalUse;
		nVideoMode			= ref.nVideoMode;
		nMIPILane			= ref.nMIPILane;
		nClockSelect		= ref.nClockSelect;
		nClockUse			= ref.nClockUse;

		fMIPI_VIO			= ref.fMIPI_VIO;

		dWidthMultiple		= ref.dWidthMultiple;
		dHeightMultiple		= ref.dHeightMultiple;

		nSensorType			= ref.nSensorType;
		nConvFormat			= ref.nConvFormat;
		dwBitWise			= ref.dwBitWise;
		byBitPerPixels		= ref.byBitPerPixels;
		
		dShiftExp			= ref.dShiftExp;
		dAlpha				= ref.dAlpha;
		dBeta				= ref.dBeta;

		nCropFrame			= ref.nCropFrame;
		nCropLine_Top		= ref.nCropLine_Top;
		nCropLine_Bottom	= ref.nCropLine_Bottom;

		return *this;
	};

	DWORD Get_AdjustXres(__in enImgSensorType SensorType, __in DWORD dwSrcXres)
	{
		DWORD dwAdjXres = 0;

		switch (SensorType)
		{
		case enImgSensorType_YCBCR:
			dwAdjXres = dwSrcXres / 2;
			break;

		case enImgSensorType_RGBbayer10:
			if ((dwSrcXres * 4) % 5 == 0)
			{
				dwAdjXres = dwSrcXres * 4 / 5;
			}
			else
			{
				dwAdjXres = dwSrcXres * 4 / 5;
				dwAdjXres++;
			}
			break;

		case enImgSensorType_RGBbayer12:
		case enImgSensorType_Monochrome_12bit:
			if ((dwSrcXres * 2) % 3 == 0)
			{
				dwAdjXres = dwSrcXres * 2 / 3;
			}
			else
			{
				dwAdjXres = dwSrcXres * 2 / 3;
				dwAdjXres++;
			}
			break;

		case enImgSensorType_RGB888:
		case enImgSensorType_RGBbayer:
		case enImgSensorType_Monochrome_CCCC:		
			dwAdjXres = dwSrcXres;
			break;


		default:
			dwAdjXres = dwSrcXres;
			break;
		}

		return dwAdjXres;
	};

	DWORD Get_MipiLaneXres(__in enDAQMIPILane MipiLane, __in DWORD dwSrcXres)
	{
		DWORD dwAdjXres = 0;

		switch (MipiLane)
		{
		case DAQMIPILane_1:
			dwAdjXres = dwSrcXres;
			break;

		case DAQMIPILane_2:
			dwAdjXres = dwSrcXres * 2;
			break;

		case DAQMIPILane_4:
			dwAdjXres = dwSrcXres * 4;
			break;

		default:
			dwAdjXres = dwSrcXres;
			break;
		}

		return dwAdjXres;
	};

	// Contrast : -50 < 0 < 100
	// Brightness : -150 < 0 < 150
	void SetContrastBrightness(char chContrast = 0, char chBrightness = 0)
	{
		if (chContrast > 0)
		{
			double delta = 127.0 * chContrast / 100;
			dAlpha	= 255.0 / (255.0 - delta * 2);
			dBeta	= dAlpha * (chBrightness - delta);
		}
		else
		{
			double delta = -128.0 * chContrast / 100;
			dAlpha	= (256.0 - delta * 2) / 255.0;
			dBeta	= dAlpha * chBrightness + delta;
		}
	};

	// Exposure : -20 < 0 < 20
	// Gamma : 9.99 < 1 < 0.01
	void SetExposure(DOUBLE	dExposure = 0.0f)
	{
		dShiftExp = dExposure;

		DOUBLE delta = pow(2.0, dShiftExp);
		dAlpha = (0 != delta) ? (1.0f / delta) : 1.0f;
		dBeta = 0.0f;
	};

	void SetCropLineVertical(UINT nLineTopCount, UINT nLineBottomCount)
	{
		nCropLine_Top		= nLineTopCount;
		nCropLine_Bottom	= nLineBottomCount;
	};


}ST_DAQOption, *PST_DAQOption;

typedef struct _tag_DAQStatus
{
	BOOL				bInitBoard;			// 보드 초기화 상태 (Init, Close)
	BOOL				bStartCapture;		// 영상 캡쳐 시작 상태 (Start, Stop)
	BOOL				bSignal;			// 보드 영상 신호 상태	

	DOUBLE				dFPS_Fixed;			// 고정된 최대 프레임 수
	DOUBLE				dDelay_GetFrame;	// 고정 프레임의 프레임 수집 대기 시간 (micro second)
	DOUBLE				dFPS;				// 초당 프레임 수
	DWORD				dwFPS_HW;			// 초당 프레임 수 (H/W)
	DWORD				dwXRes;				// 영상 너비
	DWORD				dwYRes;				// 영상 높이
	DWORD				dwHW_XRes;			// 그래버 보드에서 수신된 영상 너비
	DWORD				dwHW_YRes;			// 그래버 보드에서 수신된 영상 높이

	int					VerFPGA;			// FPGA 버전
	int					VerFirmware;		// Firmware 버전

	ST_VideoRGB			RGBData;			// RGB 영상 데이터

	DWORD				dwFrameCount;		// 수신된 영상 개수 (시작: 0으로 초기화)	
	DWORD				dwFailFrameCount;	// 영상 캡쳐 전체 실패 카운트
	DWORD				dwFailContiCount;	// 영상 캡쳐 연속 실패 카운트

	BOOL				bVideoChangeMode;


	_tag_DAQStatus()
	{
		bInitBoard			= FALSE;
		bStartCapture		= FALSE;
		bSignal				= FALSE;
		bVideoChangeMode = FALSE;
		dFPS_Fixed = 30.0f;
		//dDelay_GetFrame		= (1000.0f / dFPS_Fixed);	//밀리 초		// 1000 / 30.0 = 33.333...
		dDelay_GetFrame		= (1000000.0f / dFPS_Fixed);	//마이크로 초	// 1000 / 30.0 = 33.333...
		dFPS				= 0.0f;
		dwFPS_HW			= 0;
		dwXRes				= 0;
		dwYRes				= 0;
		dwHW_XRes			= 0;
		dwHW_YRes			= 0;

		VerFPGA				= -1;
		VerFirmware			= -1;
		dwFrameCount		= 0;
		dwFailFrameCount	= 0;
		dwFailContiCount	= 0;
	};

	~_tag_DAQStatus()
	{
		
	};

	_tag_DAQStatus& operator= (_tag_DAQStatus& ref)
	{
		bInitBoard			= ref.bInitBoard;
		bStartCapture		= ref.bStartCapture;
		bSignal				= ref.bSignal;
		bVideoChangeMode = ref.bVideoChangeMode;

		dFPS_Fixed			= ref.dFPS_Fixed;
		dDelay_GetFrame		= ref.dDelay_GetFrame;
		dFPS				= ref.dFPS;
		dwFPS_HW			= ref.dwFPS_HW;
		dwXRes				= ref.dwXRes;
		dwYRes				= ref.dwYRes;
		dwHW_XRes			= ref.dwHW_XRes;
		dwHW_YRes			= ref.dwHW_YRes;

		VerFPGA				= ref.VerFPGA;
		VerFirmware			= ref.VerFirmware;

		//RGBData; = ref.RGBData;

		dwFrameCount		= ref.dwFrameCount;
		dwFailFrameCount	= ref.dwFailFrameCount;
		dwFailContiCount	= ref.dwFailContiCount;

		return *this;
	};

	void ResetCapture()
	{ 
		dFPS				= 0.0f;
		dwFPS_HW			= 0;
		dwFrameCount		= 0;
		dwFailFrameCount	= 0;
		dwFailContiCount	= 0;
	};

	LPBYTE GetRGBData()
	{
		return RGBData.GetReadRGBData();
	};

	void SetRGBData(__in DWORD dwSizeX, __in DWORD dwSizeY)
	{
		RGBData.AssignMem(dwSizeX, dwSizeY);
	};

	void SetFixedFrameRate(__in DOUBLE dInFPS)
	{
		if ((0.0 < dInFPS) && (dInFPS <= 120.0))
		{
			dFPS_Fixed = dInFPS;

			//dDelay_GetFrame = (1000.0f / dFPS_Fixed);		// 밀리 초
			dDelay_GetFrame = (1000000.0f / dFPS_Fixed);	// 마이크로 초
		}
	};
	
}ST_DAQStatus, *PST_DAQStatus;



#endif // Def_DAQWrapper_h__
