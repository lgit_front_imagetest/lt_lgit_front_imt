﻿//*****************************************************************************
// Filename	: Grid_Base.cpp
// Created	: 2010/11/25
// Modified	: 2010/11/25 - 17:32
//
// Author	: PiRing
//	
// Purpose	: Ulimate Grid 사용시 기본으로 적용할 코드가 작성되어 있는 기본 클래스
//*****************************************************************************
#include "StdAfx.h"
#include "Grid_Base.h"

//=============================================================================
// 생성자
//=============================================================================
CGrid_Base::CGrid_Base(void)
{
	m_nMaxRows	= 0;
	m_nMaxCols	= 0;

	// 펜 색상 설정. 보더 색상용
	m_pen.CreatePen (PS_SOLID | PS_JOIN_ROUND | PS_ENDCAP_ROUND, 1, RGB(0, 0, 0));

	//setup the fonts
	m_font.CreateFont(10,0,0,0,900,0,0,0,0,0,0,0,0,_T("Arial"));
}

//=============================================================================
// 
//=============================================================================
CGrid_Base::~CGrid_Base(void)
{
	m_pen.DeleteObject	();
	m_font.DeleteObject ();
}

BEGIN_MESSAGE_MAP(CGrid_Base, CUGCtrl)
	ON_WM_SIZE()
END_MESSAGE_MAP()

//=============================================================================
// Method		: CGrid_Base::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/8/5 - 16:11
// Desc.		:
//=============================================================================
void CGrid_Base::OnSize(UINT nType, int cx, int cy)
{
	CUGCtrl::OnSize(nType, cx, cy);

	CalGridOutline();
}

//=============================================================================
// Method		: CGrid_Base::OnSetup
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/5/10 - 15:36
// Desc.		: 그리드 초기 세팅
//=============================================================================
void CGrid_Base::OnSetup()
{
	// 폰트 지정
	SetDefFont(&m_font);

	EnableCellOverLap(FALSE);
	SetDoubleBufferMode(TRUE);

	// 헤더 크기 설정
	SetSH_Width(0);
	SetTH_Height(0);

	//스크롤바 크기 설정
	SetHS_Height(0);
	SetVS_Width(0);

	UseHints (TRUE);

	// 크기변경 잠금	
	SetUserSizingMode(FALSE);

	// 외곽 모양 그리기
	DrawGridOutline();
}

//=============================================================================
// Method		: CGrid_Base::DrawGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/5/10 - 15:44
// Desc.		:
//=============================================================================
void CGrid_Base::DrawGridOutline()
{
	//그리드 행/열 갯수 지정
	SetNumberRows(m_nMaxRows);
	SetNumberCols(m_nMaxCols);

	CalGridOutline ();

	// 셀 정렬 --------------------------------------------
	for (UINT iRow = 0; iRow < m_nMaxRows; iRow++)
		for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
			QuickSetAlignment	(iCol, iRow, UG_ALIGNCENTER | UG_ALIGNVCENTER);

}

//=============================================================================
// Method		: CGrid_Base::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/5/10 - 17:09
// Desc.		:
//=============================================================================
void CGrid_Base::CalGridOutline()
{
	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth	= rect.Width();
	int nHeight	= rect.Height();	

	// 기본 열 추가 ---------------------------------------	
	UINT nUnitWidth		= nWidth / m_nMaxCols;
	UINT nRemindWidth	= nWidth - (nUnitWidth * m_nMaxCols);

	// Width 계산하고 남거나 모자르는 공간 계산하여 헤더 Width추가 처리
	for (UINT iCol = 0; iCol < nRemindWidth; iCol++)
	{
		SetColWidth (iCol, nUnitWidth + 1 );				
	}

	for (UINT iCol = nRemindWidth; iCol < m_nMaxCols; iCol++)
	{
		SetColWidth (iCol, nUnitWidth );				
	}

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight	= nHeight / m_nMaxRows;
	UINT nRemindHeight	= nHeight - (nUnitHeight * m_nMaxRows);

	//SetRowHeight ( 0, nUnitHeight);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight (iRow, nUnitHeight + 1 );				
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight (iRow, nUnitHeight );				
	}
}

//=============================================================================
// Method		: GetDropListString
// Access		: protected  
// Returns		: CString
// Parameter	: __in const TCHAR * * lpszString
// Qualifier	:
// Last Update	: 2016/1/20 - 11:14
// Desc.		:
//=============================================================================
CString CGrid_Base::GetDropListString(__in const TCHAR** lpszString)
{
	CString strBuf = _T("");

	for (int iIndex = 0; NULL != lpszString[iIndex]; iIndex++)
	{
		strBuf += lpszString[iIndex];
		strBuf += _T("\n");
		//strBuf.Append (lpszString[iIndex]);
		//strBuf.Append (_T("\n"));
	}

	return strBuf;
}

//=============================================================================
// Method		: CGrid_Base::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2010/11/25 - 18:21
// Desc.		:
//=============================================================================
void CGrid_Base::OnGetCell( int col,long row,CUGCell *cell )
{
	cell->SetBorderColor (&m_pen);

	long lRow = GetNumberRows();
	long lCol = GetNumberCols();
	
	if  (0 == row)
	{	
		if (col == 0)
			cell->SetBorder (UG_BDR_LMEDIUM | UG_BDR_RTHIN | UG_BDR_TMEDIUM | UG_BDR_BTHIN);
		else if (col == lCol - 1)
			cell->SetBorder (UG_BDR_RMEDIUM | UG_BDR_TMEDIUM | UG_BDR_BTHIN );
		else
			cell->SetBorder (UG_BDR_RTHIN | UG_BDR_TMEDIUM | UG_BDR_BTHIN);
	}
	else if (lRow - 1 == row)
	{
		if (col == 0)
			cell->SetBorder (UG_BDR_LMEDIUM | UG_BDR_RTHIN | UG_BDR_BMEDIUM);
		else if (col == lCol - 1)
			cell->SetBorder (UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
		else
			cell->SetBorder (UG_BDR_RTHIN | UG_BDR_BMEDIUM);		
	}	
	else
	{
		if (col == 0)
			cell->SetBorder (UG_BDR_LMEDIUM | UG_BDR_RTHIN | UG_BDR_BTHIN);
		else if (col == lCol - 1)
			cell->SetBorder (UG_BDR_RMEDIUM | UG_BDR_BTHIN );
		else
			cell->SetBorder (UG_BDR_RTHIN | UG_BDR_BTHIN);
	}
}