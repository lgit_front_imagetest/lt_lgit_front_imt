﻿#pragma once

// CMyEdit
#include "Color_Def.h"

class CMyEdit : public CEdit
{
	DECLARE_DYNAMIC(CMyEdit)

public:
	CMyEdit();
	virtual ~CMyEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocus();
	afx_msg void OnSetFocus(CWnd* pOldWnd);

public:

/*
	MyEdit 속성
*/
	void SetMyTextColor(COLORREF rgb){m_textRGB = rgb;}				// 글자 색상
	void SetMyBackColor(COLORREF rgb);//{m_bkRGB = rgb;}			// 배경 색상

	void FontSizeChange(LONG weight, LONG height);					// 폰트 크기 변경

private:	
	COLORREF m_textRGB;
	COLORREF m_bkRGB;
	CFont	m_font;

public:
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg UINT OnGetDlgCode();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
};


