//*****************************************************************************
// Filename	: 	ScheduleTasks.h
// Created	:	2017/3/9 - 14:41
// Modified	:	2017/3/9 - 14:41
//
// Author	:	PiRing
//	
// Purpose	:	윈도우 스케줄러 생성, 삭제 등의 작업을 하는 클래스
//*****************************************************************************
#ifndef ScheduleTasks_h__
#define ScheduleTasks_h__

#pragma once

class CScheduleTasks
{
public:
	CScheduleTasks();
	~CScheduleTasks();

protected:

public:

	// 기본 Task 명칭
	
	// 현재 스케쥴 검색
	// 스케쥴 생성
	// 스케쥴 삭제


};

#endif // ScheduleTasks_h__

/*
----- 스케줄 생성 -----
SCHTASKS /Create [/S system [/U username [/P [password]]]]
[/RU username [/RP password]] /SC schedule [/MO modifier] [/D day]
[/M months] [/I idletime] /TN taskname /TR taskrun [/ST starttime]
[/RI interval] [ {/ET endtime | /DU duration} [/K] [/XML xmlfile] [/V1]]
[/SD 시작 날짜] [/ED 끝 날짜] [/IT] [/NP][/Z] [/F]

----- 스케줄 삭제 -----
SCHTASKS /Delete [/S system [/U username [/P [password]]]]
/TN taskname [/F]

----- 스케줄 정보 조사 -----
SCHTASKS /Query [/S system [/U username [/P password]]]
[/FO format | /XML [xml_type]] [/NH] [/V] [/TN taskname] [/?]

----- ex -----
gaming" 예약된 작업을 만듭니다. 매일 12:00부터 14:00까지 freecell.exe를 실행하고 종료합니다.
SCHTASKS /Create /SC DAILY /TN gaming /TR c:\freecell /ST 12:00 /ET 14:00 /K

SCHTASKS /Delete /S system /U user /P password /TN "Start Backup" /F
SCHTASKS /Query /S system /U user /P password
*/