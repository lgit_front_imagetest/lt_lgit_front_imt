﻿#include "StdAfx.h"
#include "BSocketClient.h"
#include <strsafe.h>
#include "ErrorCode.h"
//#include "Define_CommonEnums.h"
#include "CommonFunction.h"


using namespace PR_ErrorCode;

// CBSocketClient
const int AF_IPV4   = 0;
const int AF_IPV6   = 1;

//=============================================================================
// 생성자
//=============================================================================
CBSocketClient::CBSocketClient()
{
	m_nMode	= AF_IPV4;
	m_SocketClient.SetInterface(this);

	m_hThreadTryConnect		= NULL;	
	m_hThreadConnect		= NULL;

	m_dwTimeReconnect		= 5000;

	m_dwAckTimeout			= 2000; //400;
	m_bFlag_WaitAck			= TRUE;

	m_hOwnerWnd				= NULL;
	m_nWM_LOG				= NULL;	
	m_nWM_COMM_STATUS		= NULL;
	m_WM_Recv				= NULL;

	m_hACKEvent				= NULL;
	m_hACKEvent				= CreateEvent( NULL, FALSE, FALSE, NULL);
	m_hAbortACKEvent		= NULL;
	m_hAbortACKEvent		= CreateEvent(NULL, FALSE, FALSE, NULL);
	m_bExitProgramFlag		= FALSE;
	m_hExternalExitEvent	= NULL;
	m_hInternalExitEvent	= NULL;
	m_hInternalExitEvent	= CreateEvent( NULL, FALSE, FALSE, NULL);

	m_dwSemaphoreWaitTime	= 10;
	m_hSemaphore			= NULL;	
	//m_hSemaphore			= CreateSemaphore (NULL, 1, 1, _T("BSocket Client"));

	m_nDeviceType			= 0;
	m_bUseConnect			= TRUE;

	m_byLogIndex			= 0;
	memset (m_szLogMsg, 0, sizeof(m_szLogMsg));
	//for (int iIndex = 0; iIndex < MAX_LOG_BUFFER_SOCK_CLI; iIndex++)
	//	m_lpLogMsg[iIndex] = NULL;
}

//=============================================================================
// 소멸자
//=============================================================================
CBSocketClient::~CBSocketClient()
{
	m_bUseConnect			= FALSE;
	DisconnectSever();

	SetEvent(m_hInternalExitEvent);

	if (NULL != m_hThreadTryConnect)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThreadTryConnect, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TRACE (_T("TerminateThread : m_hThreadTryConnect  \n"));
			TerminateThread(m_hThreadTryConnect, dwExitCode);
			WaitForSingleObject(m_hThreadTryConnect, WAIT_ABANDONED);

			TRACE (_T("Client 지속적 연결 쓰레드 종료 : %d\n"), m_nDeviceType);
		}

		CloseHandle(m_hThreadTryConnect);
		m_hThreadTryConnect = NULL;
	}

	if (NULL != m_hThreadConnect)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThreadConnect, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TRACE (_T("TerminateThread : m_hThreadConnect  \n"));
			TerminateThread(m_hThreadConnect, dwExitCode);
			WaitForSingleObject(m_hThreadConnect, WAIT_ABANDONED);

			TRACE (_T("Client 연결 쓰레드 종료 : %d\n"), m_nDeviceType);
		}

		CloseHandle(m_hThreadConnect);
		m_hThreadConnect = NULL;
	}

	if (NULL != m_hACKEvent)
		CloseHandle (m_hACKEvent);

	if (NULL != m_hAbortACKEvent)
		CloseHandle(m_hAbortACKEvent);

	if (NULL != m_hInternalExitEvent)
		CloseHandle (m_hInternalExitEvent);

	if (NULL != m_hSemaphore)
		CloseHandle(m_hSemaphore);

	ReleaseLogBuffer();
}

// CBSocketClient 메시지 처리기입니다.
//#############################################################################
// Category		: 재정의 함수들
// Pupose		: CWnd, ISocketClientHandler 재정의 함수
// Last Update	: 2010/01/19 - 16:15
//#############################################################################
//=============================================================================
// Method		: CBSocketClient::OnThreadBegin
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 통신 접속 쓰레드가 시작되면 이 함수가 호출된다.
//=============================================================================
void CBSocketClient::OnThreadBegin( CSocketHandle* pSH )
{
	ASSERT( pSH == m_SocketClient );
	(pSH);

	::SendNotifyMessage (m_hOwnerWnd, m_nWM_COMM_STATUS, (WPARAM)m_nDeviceType, (LPARAM)COMM_CONNECTED);
}

//=============================================================================
// Method		: CBSocketClient::OnThreadExit
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 통신 쓰레드가 중단되면 이 함수가 호출된다.
//=============================================================================
void CBSocketClient::OnThreadExit( CSocketHandle* pSH )
{
	ASSERT( pSH == m_SocketClient );
	(pSH);

	::SendNotifyMessage (m_hOwnerWnd, m_nWM_COMM_STATUS, (WPARAM)m_nDeviceType, (LPARAM)COMM_DISCONNECT);

	// 사용자에 의한 연결 끊김이 아니면 재 접속 시도를 함.	
	if (!m_bExitProgramFlag)
	{
		ContinuousConnectServer();
	}
}

//=============================================================================
// Method		: CBSocketClient::OnDataReceived
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: const BYTE * pbData
// Parameter	: DWORD dwCount
// Parameter	: const SockAddrIn & addr
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 데이터 수신시 이 함수가 호출된다.
//=============================================================================
void CBSocketClient::OnDataReceived( CSocketHandle* pSH, const BYTE* pbData, DWORD dwCount, const SockAddrIn& addr )
{
	ASSERT( pSH == m_SocketClient );
	(pSH);

#ifdef _DEBUG
	CString strText;
	USES_CONVERSION;

	LPTSTR pszText = strText.GetBuffer(dwCount+1);
	::StringCchCopyN(pszText, dwCount+1, A2CT(reinterpret_cast<LPCSTR>(pbData)), dwCount);
	strText.ReleaseBuffer();
	
//	TRACE(_T("Data Received [%d] : %s(len:%d)\n"), m_nDeviceType, strText, dwCount);
#endif

	PasingRecvAck ((const char*)pbData, dwCount);

	// 수신 이벤트 발생
	/*if (NULL != m_hACKEvent)
		SetEvent(m_hACKEvent);*/
}

//=============================================================================
// Method		: CBSocketClient::OnConnectionDropped
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 서버와 연결이 끊기면 이 함수가 호출된다.
//=============================================================================
void CBSocketClient::OnConnectionDropped( CSocketHandle* pSH )
{
	ASSERT( pSH == m_SocketClient );
	(pSH);

	TRACE ( _T("Connection lost with server. Need restart.\n"));

	// 사용자에 의한 연결 끊김이 아니면 재 접속 시도를 함.
	if (!m_bExitProgramFlag)
	{
		SendLogMsg ( _T("Connection lost with server. Need restart."));
		SendLogMsg ( _T("Disconnection from server."));
		::SendNotifyMessage (m_hOwnerWnd, m_nWM_COMM_STATUS, (WPARAM)m_nDeviceType, (LPARAM)COMM_CONNECT_DROP);

		if (m_SocketClient->IsOpen())
			m_SocketClient.Close();		
	}
}

//=============================================================================
// Method		: CBSocketClient::OnConnectionError
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: DWORD dwError
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 접속 오류 발생하면 이 함수가 호출된다.
//=============================================================================
void CBSocketClient::OnConnectionError( CSocketHandle* pSH, DWORD dwError )
{
	ASSERT( pSH == m_SocketClient );
	(pSH);

	_com_error err(dwError);
	TRACE ( _T("Communication Error:\r\n%s\n"), err.ErrorMessage() );

	// 사용자에 의한 연결 끊김이 아니면 재 접속 시도를 함.
	if (!m_bExitProgramFlag)
	{
		if (NULL != m_hOwnerWnd)
		{
			SendLogMsg ( _T("Communication Error:\r\n%s"), err.ErrorMessage() );	
			::SendNotifyMessage (m_hOwnerWnd, m_nWM_COMM_STATUS, (WPARAM)m_nDeviceType, (LPARAM)COMM_CONNECT_ERROR);
		}

		if (m_SocketClient->IsOpen())
			m_SocketClient.Close();
	}
}

//#############################################################################
// Category		: 동기화 관련 함수들
// Pupose		: 
// Last Update	: 2010/02/10 - 10:57
//#############################################################################
//=============================================================================
// Method		: CBSocketClient::MakeSemaphore
// Access		: virtual protected 
// Returns		: void
// Parameter	: LPCSTR lpName
// Parameter	: DWORD dwWaitDelay
// Qualifier	:
// Last Update	: 2013/6/27 - 17:15
// Desc.		:
//=============================================================================
void CBSocketClient::MakeSemaphore( LPCTSTR lpName, DWORD dwWaitDelay /*= 10*/ )
{
	m_dwSemaphoreWaitTime	= dwWaitDelay;
	
	if (NULL != m_hSemaphore)
		CloseHandle(m_hSemaphore);

	m_hSemaphore = CreateSemaphore (NULL, 1, 1, lpName);
}

void CBSocketClient::ResetSemaphore()
{
	ReleaseSemaphore(m_hSemaphore, 1, NULL);
}

//=============================================================================
// Method		: CBSocketClient::LockSemaphore
// Access		: protected 
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2010/02/10 - 10:57
// Desc.		: 
//=============================================================================
DWORD CBSocketClient::LockSemaphore()
{
	DWORD	dwResult = WaitForSingleObject( m_hSemaphore, m_dwSemaphoreWaitTime);
	if (WAIT_OBJECT_0 != dwResult)
	{
		TRACE (_T("------------** Error : Semaphore is Alive **------------\n"));
		m_dwErrCode	= ERR_SEMAPHORE;		
	}
	else
	{
		//TRACE (_T("------------** Enter Semaphore **------------\n"));
	}

	return dwResult;
}

//=============================================================================
// Method		: CBSocketClient::UnlockSemaphore
// Access		: protected 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/02/10 - 10:57
// Desc.		: 
//=============================================================================
BOOL CBSocketClient::UnlockSemaphore()
{
	BOOL bResult = ReleaseSemaphore (m_hSemaphore, 1, NULL);

	if (!bResult) 
	{ 
		TRACE (_T("------------** Error : ReleaseSemaphore() **------------\n"));
	} 
	//TRACE (_T("------------** ReleaseSemaphore() **------------\n"));

	return bResult;
}

//=============================================================================
// Method		: CBSocketClient::WaitSemaphore
// Access		: protected 
// Returns		: BOOL
// Parameter	: DWORD dwTimeout
// Qualifier	:
// Last Update	: 2010/02/10 - 10:57
// Desc.		: 
//=============================================================================
BOOL CBSocketClient::WaitSemaphore( DWORD dwTimeout )
{
	BOOL bResult = FALSE;
	DWORD	dwResult = WaitForSingleObject( m_hSemaphore, dwTimeout);

	switch (dwResult)
	{
	case WAIT_OBJECT_0:
		//TRACE (_T("------------** WaitSemaphore() : Success **------------\n"));
		bResult = TRUE;
		m_dwErrCode = NO_ERROR;
		break;

	case WAIT_TIMEOUT:
		//TRACE (_T("------------** WaitSemaphore() : Timeout **------------\n"));
		bResult = FALSE;
		m_dwErrCode = ERR_WAIT_TIMEOUT;
		break;

	case WAIT_ABANDONED:
		//TRACE (_T("------------** WaitSemaphore() : Abandoned **------------\n"));
		bResult = FALSE;
		m_dwErrCode = ERR_WAIT_ABANDONED;
		break;
	}	

	return bResult;
}

//#############################################################################
// Category		: 이더넷 통신 함수들
// Pupose		: 연결, 전송, 해제
// Last Update	: 2010/01/19 - 16:15
//#############################################################################
//=============================================================================
// Method		: CBSocketClient::ContinuousConnectServer
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/01/28 - 13:54
// Desc.		: 모니터링 쓰레드 시작
//=============================================================================
void CBSocketClient::ContinuousConnectServer()
{	
	m_bExitProgramFlag	= FALSE;

	m_hThreadTryConnect = HANDLE(_beginthreadex(NULL, 0, ThreadTryConnect, this, 0, NULL));
}

//=============================================================================
// Method		: CBSocketClient::ConnectServer
// Access		: public 
// Returns		: BOOL
// Parameter	: LPCTSTR pszAddr
// Parameter	: LPCTSTR pszPort
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 서버에 접속시도
//=============================================================================
BOOL CBSocketClient::ConnectServer( LPCTSTR pszAddr, LPCTSTR pszPort, LPCTSTR pzNIC_Addr /*= NULL*/ )
{
	if (FALSE == m_bUseConnect)
		return FALSE;

	// 동기화
	/*if (WAIT_OBJECT_0 != LockSemaphore())
		return FALSE;*/

	BOOL bReturn = FALSE;

	__try
	{
		int nFamily = (m_nMode == AF_IPV4) ? AF_INET : AF_INET6;
		if ( !m_SocketClient.StartClient(pzNIC_Addr, pszAddr, pszPort, nFamily, SOCK_STREAM ) )
		{								
			SendErrorMsg(_T("Unable to connect to the server (Address : %s, Port : %s)"), pszAddr, pszPort);

			bReturn = FALSE;
		}
		else
		{
			bReturn = TRUE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketClient::ConnectServer()"));
	}

	// 동기화
	// 세마포어로 동기화 처리
	//UnlockSemaphore ();

	return bReturn;
}

//=============================================================================
// Method		: CBSocketClient::ConnectServer
// Access		: public 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/02/02 - 10:19
// Desc.		: 멤버 변수를 이용하여 서버접속 시도
//=============================================================================
BOOL CBSocketClient::ConnectServer()
{
	if (FALSE == m_bUseConnect)
		return FALSE;

	if (m_strAddr.IsEmpty())
		return FALSE;

	if (m_strPort.IsEmpty())
		return FALSE;

	if (m_SocketClient->IsOpen())
		return FALSE;

	return ConnectServer (m_strAddr, m_strPort, m_strNIC_Addr);	
}

//=============================================================================
// Method		: CBSocketClient::ConnectServerThread
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/20 - 21:14
// Desc.		:
//=============================================================================
BOOL CBSocketClient::ConnectServerThread()
{
	m_hThreadConnect = HANDLE(_beginthreadex(NULL, 0, ThreadConnect, this, 0, NULL));

	return WaitConnection(2000);
}

//=============================================================================
// Method		: CBSocketClient::DisconnectSever
// Access		: public 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 통신 연결해제
//=============================================================================
BOOL CBSocketClient::DisconnectSever()
{
	m_bExitProgramFlag	= TRUE;

	if (m_SocketClient->IsOpen())
		m_SocketClient.Close();

	// 통신 종료 이벤트 발생
	if (NULL != m_hInternalExitEvent)
		SetEvent (m_hInternalExitEvent);

	return TRUE;
}

//=============================================================================
// Method		: CBSocketClient::ThreadConnect
// Access		: protected static 
// Returns		: UINT WINAPI
// Parameter	: LPVOID lParam
// Qualifier	:
// Last Update	: 2010/12/20 - 21:10
// Desc.		:
//=============================================================================
UINT WINAPI CBSocketClient::ThreadConnect( LPVOID lParam )
{
	ASSERT (NULL != lParam);

	CBSocketClient* pThis;
	pThis = (CBSocketClient*)lParam;

	if (pThis->ConnectServer ())
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CBSocketClient::ThreadSockManager
// Access		: protected static 
// Returns		: UINT WINAPI
// Parameter	: LPVOID lParam
// Qualifier	:
// Last Update	: 2010/01/19 - 16:14
// Desc.		: 소켓 연결이 끊어져 있으면 접속을 계속 시도한다.
//=============================================================================
UINT WINAPI CBSocketClient::ThreadTryConnect( LPVOID lParam )
{	
	ASSERT (NULL != lParam);

	DWORD	dwTimeConnect	= 0;
	DWORD	dwTimeReconnect = 0;

	CBSocketClient* pThis;
	pThis = (CBSocketClient*)lParam;

	//TRACE (_T("CBSocketClient::ThreadTryConnect Start : %d\n"), pThis->m_nDeviceType);

	if (FALSE == pThis->m_bUseConnect)
		return FALSE;

	Sleep(1000);
	if (pThis->ConnectServer ())
	{
		//TRACE (_T("CBSocketClient::ThreadTryConnect Exit : %d\n"), pThis->m_nDeviceType);
		return TRUE;
	}

	HANDLE hEvent[2] = {NULL, NULL};
	hEvent[0]	= pThis->m_hInternalExitEvent;

	if (NULL != pThis->m_hExternalExitEvent)
	{
		hEvent[1]	= pThis->m_hExternalExitEvent;
	}

	BOOL bContinue = TRUE;
	DWORD dwEvent = 0;

	__try
	{
		while (bContinue)
		{
			// 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != hEvent[1])
				dwEvent = WaitForMultipleObjects(2, hEvent, FALSE, pThis->m_dwTimeReconnect);
			else
				dwEvent = WaitForSingleObject (pThis->m_hInternalExitEvent, pThis->m_dwTimeReconnect);

			switch (dwEvent)
			{
			case WAIT_OBJECT_0 :	// Exit Program
				TRACE (_T(" -- 프로그램 종료 m_hInternalExitEvent 이벤트 감지 \n"));
				bContinue = FALSE;
				break;

			case WAIT_OBJECT_0 + 1 :// Exit Program
				TRACE (_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
				bContinue = FALSE;
				break;		

			case WAIT_TIMEOUT:
				if (false == pThis->m_SocketClient->IsOpen())
				{
					if (pThis->ConnectServer ())
						bContinue = FALSE;
				}
				break;
			}		
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CBSocketClient::ThreadTryConnect()\n"));
	}

	TRACE (_T("쓰레드 종료 : CBSocketClient::ThreadTryConnect Loop Exit : %d\n"), pThis->m_nDeviceType);
	return TRUE;
}

//=============================================================================
// Method		: CBSocketClient::WaitConnection
// Access		: public 
// Returns		: BOOL
// Parameter	: DWORD dwTimeOut
// Qualifier	:
// Last Update	: 2010/10/28 - 13:16
// Desc.		:
//=============================================================================
BOOL CBSocketClient::WaitConnection( DWORD dwTimeOut /*= INFINITE*/ )
{
	DWORD	dwTimeStart	= 0;
	DWORD	dwTimeCheck = 0;

	dwTimeStart = GetTickCount();

	BOOL bContinue = TRUE;

	__try
	{
		while (bContinue)
		{
			DWORD dwEvent = WaitForSingleObject (m_hThreadTryConnect, 10);
			switch (dwEvent)
			{
			case WAIT_OBJECT_0 :
				bContinue = FALSE;
				return TRUE;
				break;		

			default:
				if (m_SocketClient->IsOpen())
				{
					bContinue = FALSE;
					return TRUE;
					break;
				}
				else
				{
					if (INFINITE != dwTimeOut)
					{
						dwTimeCheck = GetTickCount ();

						if (dwTimeOut < (dwTimeCheck - dwTimeStart))
							bContinue = FALSE;					
					}

					MSG msg;
					while (PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE))         
					{            
						TranslateMessage(&msg);            
						DispatchMessage(&msg);         
					}
				}
				break;
			}		
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CBSocketClient::WaitConnection()\n"));
	}

	return FALSE;
}

//=============================================================================
// Method		: CBSocketClient::SendString
// Access		: public 
// Returns		: BOOL
// Parameter	: LPCTSTR lpszString
// Qualifier	:
// Last Update	: 2010/01/29 - 14:31
// Desc.		: 문자열 전송
//=============================================================================
BOOL CBSocketClient::SendString( LPCTSTR lpszString )
{
	BOOL bReturn = TRUE;

	__try
	{
		if ( m_SocketClient.IsOpen() )
		{
			if ( NULL == lpszString ) 
			{
				SendLogMsg( _T("Please enter the message to send.") );
				m_dwErrCode = ERROR_INVALID_PARAMETER;
				bReturn = FALSE;
			}
			else
			{
				USES_CONVERSION;
				DWORD_PTR dwResult = m_SocketClient.Write((const LPBYTE)(T2CA(lpszString)), (DWORD)_tcslen(lpszString), NULL);

				if (0 < dwResult)
				{
					m_dwErrCode = NO_ERROR;
					bReturn = TRUE;
				}
				else
				{
					m_dwErrCode = ERR_TCPIP_TRANSFER;
					bReturn = FALSE;
				}
			}
		}
		else
		{
			SendErrorMsg(_T("Socket is not connected"));
			m_dwErrCode = ERR_COMM_NOTCONNECTED;
			bReturn = FALSE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketClient::SendString"));
	}

	return bReturn;
}

//=============================================================================
// Method		: CBSocketClient::SendData
// Access		: protected 
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwCount
// Qualifier	:
// Last Update	: 2010/01/19 - 17:11
// Desc.		: 데이터 전송
//=============================================================================
BOOL CBSocketClient::SendData( const char* lpBuffer, DWORD dwCount )
{
	BOOL bReturn = TRUE;

	__try
	{
		if ( m_SocketClient.IsOpen() )
		{
			//TRACE (_T("Data Send : %s \n"), lpBuffer);
			DWORD dwResult = m_SocketClient.Write((const LPBYTE)lpBuffer, dwCount, NULL);

			if (0 < dwResult)
			{
				m_dwErrCode = NO_ERROR;
				bReturn = TRUE;
			}
			else
			{
				m_dwErrCode = ERR_TCPIP_TRANSFER;
				bReturn = FALSE;
			}
		}
		else
		{
			SendErrorMsg(_T("Socket is not connected"));
			m_dwErrCode = ERR_COMM_NOTCONNECTED;
			bReturn = FALSE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketClient::SendData"));
	}

	return bReturn;
}

//#############################################################################
// Category		: 여러가지 설정 함수들
// Pupose		: 오너 윈도우와 메세지 처리하기 위한 함수들
// Last Update	: 2010/01/20 - 9:55
//#############################################################################
//=============================================================================
// Method		: CBSocketClient::SetOwnerHWND
// Access		: public 
// Returns		: BOOL
// Parameter	: HWND hOwnerWnd
// Qualifier	:
// Last Update	: 2010/01/20 - 10:05
// Desc.		: 오너 윈도우 핸들 설정
//=============================================================================
BOOL CBSocketClient::SetOwnerHWND( HWND hOwnerWnd )
{
	ASSERT(NULL != hOwnerWnd);

	m_hOwnerWnd = hOwnerWnd;

	return TRUE;
}

//=============================================================================
// Method		: CBSocketClient::SetWmLog
// Access		: public 
// Returns		: void
// Parameter	: UINT nWindowsMessage
// Qualifier	:
// Last Update	: 2010/01/20 - 10:05
// Desc.		: 로그용 윈도 메세지 설정
//=============================================================================
void CBSocketClient::SetWmLog( UINT nWindowsMessage )
{
	m_nWM_LOG = nWindowsMessage;
}

//=============================================================================
// Method		: CBSocketClient::SetWmRecv
// Access		: public 
// Returns		: void
// Parameter	: UINT nWindowMessage
// Qualifier	:
// Last Update	: 2010/12/19 - 16:47
// Desc.		:
//=============================================================================
void CBSocketClient::SetWmRecv( UINT nWindowMessage )
{
	m_WM_Recv = nWindowMessage;
}

//=============================================================================
// Method		: CBSocketClient::SendLogMsg
// Access		: protected 
// Returns		: void _cdecl
// Parameter	: LPCTSTR lpszFormat
// Parameter	: ...
// Qualifier	:
// Last Update	: 2010/01/20 - 10:38
// Desc.		: 로그 메세지을 윈도 메세지로 전송
//=============================================================================
void _cdecl CBSocketClient::SendLogMsg( LPCTSTR lpszFormat, ... )
{
	ASSERT(NULL != lpszFormat);
	ASSERT(NULL != m_hOwnerWnd);
	ASSERT(NULL != m_nWM_LOG);

	__try
	{
		UINT_PTR nLogSize	= _tcslen(lpszFormat) + 255;
		LPTSTR lpszLog	= GetLogBuffer(nLogSize);
		size_t cb		= 0;

		HRESULT hR = 0;
		va_list args;
		va_start(args, lpszFormat);
		hR = ::StringCchVPrintfEx(lpszLog, nLogSize, NULL, &cb, 0, lpszFormat, args);
		va_end(args);
		
		if (S_OK == hR)
			::SendNotifyMessage (m_hOwnerWnd, m_nWM_LOG, (WPARAM)lpszLog, (LPARAM)MAKELONG((WORD)m_nDeviceType, (WORD)FALSE));
		else
			SendErrorMsg(_T("*** CBSocketClient::SendLogMsg() -> StringCchVPrintfEx Error (%d)"), hR);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketClient::SendLogMsg"));
	}
}

//=============================================================================
// Method		: CBSocketClient::SendErrorMsg
// Access		: protected 
// Returns		: void _cdecl
// Parameter	: LPCTSTR lpszFormat
// Parameter	: ...
// Qualifier	:
// Last Update	: 2010/01/20 - 10:38
// Desc.		: 에러 메세지을 윈도 메세지로 전송
//=============================================================================
void _cdecl CBSocketClient::SendErrorMsg( LPCTSTR lpszFormat, ... )
{
	ASSERT(NULL != lpszFormat);
	ASSERT(NULL != m_hOwnerWnd);

	__try
	{
		UINT_PTR nLogSize = _tcslen(lpszFormat) + 255;
		LPTSTR lpszLog	= GetLogBuffer(nLogSize);
		size_t cb		= 0;

		HRESULT hR = 0;
		va_list args;
		va_start(args, lpszFormat);
		hR = ::StringCchVPrintfEx(lpszLog, nLogSize, NULL, &cb, 0, lpszFormat, args);
		va_end(args);

		if (S_OK == hR)
			::SendNotifyMessage (m_hOwnerWnd, m_nWM_LOG, (WPARAM)lpszLog, (LPARAM)MAKELONG((WORD)m_nDeviceType, (WORD)TRUE));
		else
			TRACE (_T("*** CBSocketClient::SendErrorMsg() -> StringCchVPrintfEx Error (%d)"), hR);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE (_T("*** Exception Error : CBSocketClient::SendErrorMsg()"));
	}
}

//=============================================================================
// Method		: _tinet_addr
// Access		: protected  
// Returns		: DWORD
// Parameter	: __in const TCHAR * cp
// Qualifier	:
// Last Update	: 2016/9/28 - 11:38
// Desc.		:
//=============================================================================
DWORD CBSocketClient::_tinet_addr(__in const TCHAR *cp)
{
#ifdef UNICODE
	char IP[16];
	int Ret = 0;
	Ret = WideCharToMultiByte(CP_ACP, 0, cp, (int)_tcslen(cp), IP, 15, NULL, NULL);
	IP[Ret] = 0;
	return inet_addr(IP);
#else
	return inet_addr(cp);
#endif
}

//=============================================================================
// Method		: CBSocketClient::GetLogBuffer
// Access		: protected 
// Returns		: LPTSTR
// Parameter	: UINT_PTR dwBufferSize
// Qualifier	:
// Last Update	: 2011/1/17 - 16:27
// Desc.		:
//=============================================================================
LPTSTR CBSocketClient::GetLogBuffer(UINT_PTR nBufferSize)
{
	LPTSTR pReturn = NULL;

	if (m_byLogIndex < (MAX_LOG_BUFFER_SOCK_CLI - 1))
		++m_byLogIndex;
	else
		m_byLogIndex = 0;

	//ZeroMemory (m_szLogMsg[m_byLogIndex], nBufferSize);
	pReturn = m_szLogMsg[m_byLogIndex];

	//if (NULL != m_lpLogMsg[m_byLogIndex])
	//{
	//	delete [] m_lpLogMsg[m_byLogIndex];
	//	m_lpLogMsg[m_byLogIndex] = NULL;
	//}

	//m_lpLogMsg[m_byLogIndex] = new TCHAR[nBufferSize];
	//ZeroMemory (m_lpLogMsg[m_byLogIndex], nBufferSize);
	//pReturn = m_lpLogMsg[m_byLogIndex];
	
	return pReturn;
}

//=============================================================================
// Method		: CBSocketClient::ReleaseBuffer
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2011/1/17 - 16:13
// Desc.		:
//=============================================================================
void CBSocketClient::ReleaseLogBuffer()
{
	//for (int iIndex = 0; iIndex < MAX_LOG_BUFFER_SOCK_CLI; iIndex++)
	//{
	//	if (NULL != m_lpLogMsg[iIndex])
	//		delete [] m_lpLogMsg[iIndex];
	//}
}

//=============================================================================
// Method		: CBSocketClient::SetExitEvent
// Access		: public 
// Returns		: void
// Parameter	: HANDLE hExitEvent
// Qualifier	:
// Last Update	: 2010/02/01 - 14:09
// Desc.		: 
//=============================================================================
void CBSocketClient::SetExitEvent( HANDLE hExitEvent )
{
	m_hExternalExitEvent = hExitEvent;
}

//=============================================================================
// Method		: CBSocketClient::SetReconnectCycle
// Access		: public 
// Returns		: void
// Parameter	: DWORD dwMiliseconds
// Qualifier	:
// Last Update	: 2010/02/08 - 14:15
// Desc.		: 
//=============================================================================
void CBSocketClient::SetReconnectCycle( DWORD dwMiliseconds )
{
	ASSERT (500 <= dwMiliseconds && dwMiliseconds <= 10000);

	m_dwTimeReconnect = dwMiliseconds;
}

//=============================================================================
// Method		: CBSocketClient::SetAckTimeout
// Access		: public 
// Returns		: void
// Parameter	: DWORD dwMiliseconds
// Qualifier	:
// Last Update	: 2010/01/28 - 13:46
// Desc.		: 
//=============================================================================
void CBSocketClient::SetAckTimeout( DWORD dwMiliseconds )
{
	ASSERT (10 <= dwMiliseconds && dwMiliseconds <= 5000);

	m_dwAckTimeout = dwMiliseconds;
}

//=============================================================================
// Method		: CBSocketClient::SetAddress
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR pszAddr
// Parameter	: LPCTSTR pszPort
// Qualifier	:
// Last Update	: 2010/02/01 - 17:53
// Desc.		: 서버 IP, 포트를 설정함
//=============================================================================
void CBSocketClient::SetAddress( LPCTSTR pszAddr, LPCTSTR pszPort, LPCTSTR pzNIC_Addr /*= NULL*/)
{
	m_strAddr = pszAddr;
	m_strPort = pszPort;
	m_strNIC_Addr = pzNIC_Addr;
}

void CBSocketClient::SetAddress( DWORD dwIPAddr, UINT nPort, DWORD dwNIC_IPAddr /*= 0*/ )
{
	in_addr address;
	address.s_addr = dwIPAddr;
	//m_strAddr = inet_ntoa(address);
	m_strAddr.Format(_T("%d.%d.%d.%d"), (BYTE)FIRST_IPADDRESS(dwIPAddr), (BYTE)SECOND_IPADDRESS(dwIPAddr), (BYTE)THIRD_IPADDRESS(dwIPAddr), (BYTE)FOURTH_IPADDRESS(dwIPAddr));
	m_strPort.Format(_T("%d"), nPort);

	if (0 != dwNIC_IPAddr)
	{
		address.s_addr = dwNIC_IPAddr;
		//m_strAddr = inet_ntoa(address);
		m_strNIC_Addr.Format(_T("%d.%d.%d.%d"), (BYTE)FIRST_IPADDRESS(dwNIC_IPAddr), (BYTE)SECOND_IPADDRESS(dwNIC_IPAddr), (BYTE)THIRD_IPADDRESS(dwNIC_IPAddr), (BYTE)FOURTH_IPADDRESS(dwNIC_IPAddr));
	}
}

//=============================================================================
// Method		: CBSocketClient::GetAddress
// Access		: protected 
// Returns		: void
// Parameter	: const SockAddrIn & addrIn
// Parameter	: CString & rString
// Qualifier	: const
// Last Update	: 2010/01/20 - 10:28
// Desc.		: 
//=============================================================================
void CBSocketClient::GetAddress( const SockAddrIn& addrIn, CString& rString ) const
{
	TCHAR szIPAddr[MAX_PATH] = { 0 };
	CSocketHandle::FormatIP(szIPAddr, MAX_PATH, addrIn);
	rString.Format(_T("%s : %d"), szIPAddr, static_cast<int>(static_cast<UINT>(ntohs(addrIn.GetPort()))) );
}


//#############################################################################
// Category		: READ/WRITE 처리
// Pupose		: Read / Write 통신 처리 함수들
// Last Update	: 2010/02/08 - 16:26
//#############################################################################
//=============================================================================
// Method		: CBSocketClient::PasingRecvAck
// Access		: protected 
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwReaded
// Qualifier	:
// Last Update	: 2010/01/27 - 10:53
// Desc.		: 수신된 응답 처리 (정상 / 오류)
//=============================================================================
BOOL CBSocketClient::PasingRecvAck( const char* lpBuffer, DWORD dwReaded )
{
	// STX, ETX 체크
	// Command 체크

	return TRUE;
}

//=============================================================================
// Method		: CBSocketClient::WaitACK
// Access		: protected 
// Returns		: BOOL
// Parameter	: DWORD dwWaitTime
// Parameter	: LPDWORD dwOutBufSize
// Qualifier	:
// Last Update	: 2010/02/01 - 13:28
// Desc.		: TCP/IP 포트로 응답이 들어오길 기다림
//=============================================================================
BOOL CBSocketClient::WaitACK( DWORD dwWaitTime /*= 1000*/, LPDWORD  dwOutBufSize /*= NULL*/ )
{
	BOOL	bRet		= FALSE;
	DWORD	dwBufSize	= 0;
	BOOL	fDone		= TRUE;
	DWORD	dwStartTick = GetTickCount();
	DWORD	dwChkTick	= 0;
	DWORD	dwResult	= 0;
	DWORD	dwCurrentTick = 0;

	HANDLE     hArray[3];
	hArray[0] = m_hACKEvent;	// Ack 이벤트가
	hArray[1] = m_hAbortACKEvent;
	hArray[2] = m_hInternalExitEvent;	// 쓰레드 종료 이벤트

	m_bFlag_WaitAck = TRUE;

	__try
	{
		while (fDone)
		{
			dwCurrentTick = GetTickCount();
			if (dwStartTick <= dwCurrentTick)
			{
				dwChkTick = dwWaitTime - (dwCurrentTick - dwStartTick);
			}
			else
			{
				dwChkTick = dwWaitTime - (0xFFFFFFFF - dwStartTick + dwCurrentTick);
			}

			DWORD dwResult = ::MsgWaitForMultipleObjects(3, hArray, FALSE, dwChkTick, QS_ALLINPUT);
			switch (dwResult)
			{
			case WAIT_OBJECT_0:		// 포트에 데이터 수신됨
				ResetEvent (m_hACKEvent);
				//TRACE (_T("WaitACK is Passed!! \n"));
				m_dwErrCode = NO_ERROR;
				bRet		= TRUE;
				fDone		= FALSE;				
				break;

			case WAIT_OBJECT_0 + 1:	// Wait Ack 취소 이벤트 처리
				ResetEvent(m_hAbortACKEvent);
				bRet = FALSE;
				fDone = FALSE;
				break;

			case WAIT_OBJECT_0 + 2:	// 프로그램 종료 이벤트 처리
				fDone = FALSE;
				break;

			case WAIT_TIMEOUT:
				TRACE (_T("WaitACK is TimeOut\n"));			
				m_dwErrCode = ERR_TCPIP_ACK_TIMEOUT;
				bRet		= FALSE;
				fDone		= FALSE;
				break;

			case WAIT_OBJECT_0 + 3:
				{
					MSG msg;
					while (::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))         
					{
						if (msg.message == WM_QUIT) 
							exit(0);

						::TranslateMessage(&msg);            
						::DispatchMessage(&msg);         
					}  
				}
				break;
			}

			if (FALSE == m_SocketClient.IsOpen())
			{
				TRACE (_T("WaitACK is Disconnected\n"));
				m_dwErrCode = ERR_COMM_DISCONNECT;
				bRet		= FALSE;
				fDone		= FALSE;
				break;
			}

			if (FALSE == m_bFlag_WaitAck)
			{
				bRet = FALSE;
				break;
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketClient::WaitACK()"));
	}

	return bRet;
}

//=============================================================================
// Method		: StopWaitACK
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/18 - 17:45
// Desc.		:
//=============================================================================
void CBSocketClient::StopWaitACK()
{
	m_bFlag_WaitAck = FALSE;
	if (NULL != m_hAbortACKEvent)
		SetEvent(m_hAbortACKEvent);
}

//=============================================================================
// Method		: SetExitProgramFlag
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/5 - 14:48
// Desc.		:
//=============================================================================
void CBSocketClient::SetExitProgramFlag()
{
	m_bExitProgramFlag = TRUE;

	// 통신 종료 이벤트 발생
	if (NULL != m_hInternalExitEvent)
		SetEvent(m_hInternalExitEvent);
}
