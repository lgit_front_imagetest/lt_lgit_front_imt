﻿//*****************************************************************************
// Filename	: 	ClientInfo.cpp
// Created	:	2014/7/28 - 18:37
// Modified	:	2014/7/28 - 18:37
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "ClientInfo.h"


CClientInfo::CClientInfo(void)
{
	m_nConnectedClientCount	= 0;
	m_nDefinedClientCount	= 0;
}


CClientInfo::~CClientInfo(void)
{
	m_arClients.RemoveAll();
	m_arClients.FreeExtra();
}

//=============================================================================
// Method		: AddDefineClient
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwAddress
// Parameter	: UINT nClientType
// Qualifier	:
// Last Update	: 2014/7/28 - 20:47
// Desc.		:
//=============================================================================
BOOL CClientInfo::AddDefineClient( DWORD dwAddress, UINT nClientType )
{
 	CStringA strAddr;
 	strAddr.Format("%d.%d.%d.%d",	FOURTH_IPADDRESS(dwAddress), 
 									THIRD_IPADDRESS(dwAddress), 
 									SECOND_IPADDRESS(dwAddress), 
 									FIRST_IPADDRESS(dwAddress));
 
 	DWORD	dwConvAddress = ntohl(inet_addr(strAddr));// String to DWORD

	// 이미 설정되어 있는 클라이언트는 리스트에 삽입하지 않는다.
	ST_ClientUnit stTemp;
	
	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);
		
		if (stTemp.Address == dwConvAddress)
			return FALSE;

		if (stTemp.ClientType == nClientType)
			return FALSE;
	}

	ST_ClientUnit stClient;

	stClient.Address	= dwConvAddress;
	stClient.ClientType = nClientType;

	m_arClients.Add(stClient);

	++m_nDefinedClientCount;

	return TRUE;
}

//=============================================================================
// Method		: AddConnectedClient
// Access		: public  
// Returns		: BOOL
// Parameter	: SOCKET newSocket
// Parameter	: DWORD dwAddress
// Qualifier	:
// Last Update	: 2014/7/28 - 20:49
// Desc.		:
//=============================================================================
BOOL CClientInfo::AddConnectedClient( SOCKET newSocket, DWORD dwAddress )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Address == dwAddress)
		{
			stTemp.Sock = newSocket;
			m_arClients.SetAt(iIdx, stTemp);

			++m_nConnectedClientCount;

			return TRUE;
		}
	}

	// 접속이 허가되지 않은 클라이언트
	return FALSE;
}

//=============================================================================
// Method		: RemoveConnectedClient
// Access		: public  
// Returns		: BOOL
// Parameter	: SOCKET newSocket
// Parameter	: DWORD dwAddress
// Qualifier	:
// Last Update	: 2014/7/28 - 20:51
// Desc.		:
//=============================================================================
BOOL CClientInfo::RemoveConnectedClient( SOCKET newSocket, DWORD dwAddress )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if ((stTemp.Address == dwAddress) && (stTemp.Sock == newSocket))
		{
			stTemp.Sock = NULL;
			m_arClients.SetAt(iIdx, stTemp);

			--m_nConnectedClientCount;

			return TRUE;
		}
	}

	// 클라이언트를 찾지 못함
	return FALSE;
}

//=============================================================================
// Method		: RemoveAllClient
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2014/7/28 - 22:00
// Desc.		:
//=============================================================================
void CClientInfo::RemoveAllClient()
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (NULL != stTemp.Sock)
		{
			stTemp.Sock = NULL;
			m_arClients.SetAt(iIdx, stTemp);
		}
	}

	m_nConnectedClientCount = 0;
}

//=============================================================================
// Method		: GetClientIndex_ByAddr
// Access		: public  
// Returns		: int
// Parameter	: DWORD dwAddr
// Qualifier	:
// Last Update	: 2014/7/28 - 22:00
// Desc.		:
//=============================================================================
int CClientInfo::GetClientIndex_ByAddr( DWORD dwAddr )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Address == dwAddr)
		{
			return iIdx;
		}
	}

	return -1;
}

int CClientInfo::GetClientIndex_BySocket( SOCKET Socket )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Sock == Socket)
		{
			return iIdx;
		}
	}

	return -1;
}

//=============================================================================
// Method		: GetClientType_ByAddr
// Access		: public  
// Returns		: int
// Parameter	: DWORD dwAddr
// Qualifier	:
// Last Update	: 2014/7/28 - 22:01
// Desc.		:
//=============================================================================
int CClientInfo::GetClientType_ByAddr( DWORD dwAddr )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Address == dwAddr)
		{
			return stTemp.ClientType;
		}
	}

	return -1;
}

int CClientInfo::GetClientType_BySocket( SOCKET Socket )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Sock == Socket)
		{
			return stTemp.ClientType;
		}
	}

	return -1;
}

//=============================================================================
// Method		: GetClientSocket_ByAddr
// Access		: public  
// Returns		: SOCKET
// Parameter	: DWORD dwAddr
// Qualifier	:
// Last Update	: 2014/7/28 - 22:01
// Desc.		:
//=============================================================================
SOCKET CClientInfo::GetClientSocket_ByAddr( DWORD dwAddr )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Address == dwAddr)
		{
			return stTemp.Sock;
		}
	}

	return NULL;
}

SOCKET CClientInfo::GetClientSocket_ByType( UINT nClientType )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.ClientType == nClientType)
		{
			return stTemp.Sock;
		}
	}

	return NULL;
}

//=============================================================================
// Method		: GetClientInfo_ByAddr
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwAddr
// Parameter	: __out ST_ClientUnit & stClient
// Qualifier	:
// Last Update	: 2014/7/28 - 22:01
// Desc.		:
//=============================================================================
BOOL CClientInfo::GetClientInfo_ByAddr( DWORD dwAddr, __out ST_ClientUnit& stClient )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Address == dwAddr)
		{
			stClient = stTemp;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CClientInfo::GetClientInfo_BySocket( SOCKET Socket, __out ST_ClientUnit& stClient )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.Sock == Socket)
		{
			stClient = stTemp;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CClientInfo::GetClientInfo_ByType( UINT nClientType, __out ST_ClientUnit& stClient )
{
	ST_ClientUnit stTemp;

	for (int iIdx = 0; iIdx <= m_arClients.GetUpperBound(); iIdx++)
	{
		stTemp = m_arClients.GetAt(iIdx);

		if (stTemp.ClientType == nClientType)
		{
			stClient = stTemp;

			return TRUE;
		}
	}

	return FALSE;
}
