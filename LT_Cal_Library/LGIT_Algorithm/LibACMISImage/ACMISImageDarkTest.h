#pragma once

#include "export.h"
#include "ACMISImageDef.h"

// DO NOT INCLUDE BLEMISH HEADER FILE


class ACMISIMAGE_API CACMISImageDefectPixel : public CACMISImageCommon<TDefectPixel>
{
	// below functions do not have any meanings in this algorithm.
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDefectPixel();
	~CACMISImageDefectPixel();
};

class ACMISIMAGE_API CACMISImageDefectCluster : public CACMISImageCommon<TDefectCluster>
{
	// below functions do not have any meanings in this algorithm.
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TDefectResult* GetDefectResult(EImageRegion pos, int nIndex) const;
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDefectCluster();
	~CACMISImageDefectCluster();
};

class ACMISIMAGE_API CACMISImageDarkShading : public CACMISImageCommon<TDarkShading>
{
	// below functions do not have any meanings in this algorithm.
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TDefectResult* GetDefectResult(EImageRegion pos, int nIndex) const;
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDarkShading();
	~CACMISImageDarkShading();
};

class ACMISIMAGE_API CACMISImageDarkBLC : public CACMISImageCommon<TDarkBLC>
{
	// below functions do not have any meanings in this algorithm.
	const TDefectResult* GetMaxDefectResult(EImageRegion pos = EIMAGEREGION_CENTER) const;
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TDefectResult* GetDefectResult(EImageRegion pos, int nIndex) const;
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDarkBLC();
	~CACMISImageDarkBLC();
};

class ACMISIMAGE_API CACMISImageDarkLineNoise : public CACMISImageCommon<TDarkLineNoise>
{
	// below functions do not have any meanings in this algorithm.
	const TDefectResult* GetMaxDefectResult(EImageRegion pos = EIMAGEREGION_CENTER) const;
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TDefectResult* GetDefectResult(EImageRegion pos, int nIndex) const;
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDarkLineNoise();
	~CACMISImageDarkLineNoise();
};

class ACMISIMAGE_API CACMISImageDefectPixelCalibration : public CACMISImageCommon<TDefectPixelCalibration> //, short>
{
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDefectPixelCalibration();
	~CACMISImageDefectPixelCalibration();
};

class ACMISIMAGE_API CACMISImageFDWhitePixel : public CACMISImageCommon<TFDWhitePixel>//, short>
{
	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageFDWhitePixel();
	~CACMISImageFDWhitePixel();
};

class ACMISIMAGE_API CACMISImageBadPixelTable : public CACMISImageCommon<TBadPixelTable> //, short>
{
	// below functions do not have any meanings in this algorithm.
//	int GetDefectBlobCount() const;
	const RECT& GetDefectBlobRect(int nIndex) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageBadPixelTable();
	~CACMISImageBadPixelTable();
};

class ACMISIMAGE_API CACMISImageBrightModeFixedCommon : public CACMISImageCommon<TDefectPixel>
{
	//const TDefectResult* GetMaxDefectResult(EImageRegion pos = EIMAGEREGION_CENTER) const;
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageBrightModeFixedCommon();
	~CACMISImageBrightModeFixedCommon();
};

class ACMISIMAGE_API CACMISImageDarkModeFixedCommon : public CACMISImageCommon<TDefectPixel>
{
	//const TDefectResult* GetMaxDefectResult(EImageRegion pos = EIMAGEREGION_CENTER) const;
	int GetDefectCount(EImageRegion pos = EIMAGEREGION_CENTER) const;
	const TBlobResult* GetBlobDefectResult(int nIndex) const;

public:
	CACMISImageDarkModeFixedCommon();
	~CACMISImageDarkModeFixedCommon();
};

class ACMISIMAGE_API CACMISImageVeryDefectPixel : public CACMISImageCommon<TVeryDefectPixel>
{
	// below functions do not have any meanings in this algorithm.
public:
	CACMISImageVeryDefectPixel();
	~CACMISImageVeryDefectPixel();

	int GetVeryDefectCount() const;
	const TDefectResult* GetVeryDefectResult(int nIndex) const;
	int GetNormalDefectCount() const;
	const TDefectResult* GetNormalDefectResult(int nIndex) const;
};

class ACMISIMAGE_API CACMISImageDetectAllDefectPixel : public CACMISImageCommon<TAllDefectPixel>
{
	// below functions do not have any meanings in this algorithm.

public:
	CACMISImageDetectAllDefectPixel();
	~CACMISImageDetectAllDefectPixel();

	int GetBrightDefectCount() const;
	const TDefectResult* GetBrightDefectResult(int nIndex) const;
	const TDefectResult* GetMaxBrightDefectResult() const;

	int GetDarkDefectCount() const;
	const TDefectResult* GetDarkDefectResult(int nIndex) const;
	const TDefectResult* GetMaxDarkDefectResult() const;
};