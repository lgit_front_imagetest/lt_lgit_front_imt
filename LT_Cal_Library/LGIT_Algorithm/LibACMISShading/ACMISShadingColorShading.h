#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"

// DO NOT INCLUDE HEADER FILE!
class CColorShading;

class ACMISSHADING_API CACMISShadingColorShading
{
public:
	CACMISShadingColorShading(void);
	~CACMISShadingColorShading(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TColorShadingSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TColorShadingSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TColorShadingSpec& _Spec);

	const TColorShadingResult* GetInspectionResult(void);

	const TColorRatio* GetColorRatio(int nIndex);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	bool InSpec(const TColorShadingSpec& _Spec);

	const RECT* GetInspectionROI(EPos ePos);

private:
	CColorShading* m_pMethod;
};
