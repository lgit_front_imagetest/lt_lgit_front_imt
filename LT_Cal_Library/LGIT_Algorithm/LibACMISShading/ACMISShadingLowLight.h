#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
#include <iostream>

// DO NOT INCLUDE HEADER FILE!
class CLowLight;

class ACMISSHADING_API CACMISShadingLowLight
{
public:
	CACMISShadingLowLight(void);
	~CACMISShadingLowLight(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TLowLightSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TLowLightSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TLowLightSpec& _Spec);

	const TLowLightResult* GetInspectionResult(void);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	const char* GetVersion(void);

	bool InSpec(const TLowLightSpec& _Spec);

private:
	CLowLight* m_pMethod;
};