#pragma once
#include<vector>

typedef enum _EShadingDevPos
{
	SHADINGDEV_CENTER = 0,
	SHADINGDEV_EDGE,
	SHADINGDEV_CORNER
} EShadingDevPos;

// color uniformity spec.
typedef struct _TColorUniformitySpec
{
	int nGridSizeX;
	int nGridSizeY;
	double dCenterMin;
	double dCenterMax;
	double dEdgeMin;
	double dEdgeMax;
	double dCornerMin;
	double dCornerMax;
} TColorUniformitySpec;

// Golden shading deviation spec.
typedef struct _TGoldenShadingDevSpec
{
	int nGridSizeX;
	int nGridSizeY;
	double dMinDevCenter;
	double dMaxDevCenter;
	double dMinDevEdge;
	double dMaxDevEdge;
	double dMinDevCorner;
	double dMaxDevCorner;
	char* strGoldenLSCOffFilePath;
	char* strGoldenLSCOnFilePath;
	char* strGoldenGainFilePath;
} TGoldenShadingDevSpec;

// color uniformity or deviation with golden
// [colorUniformity] calculates normalization with center level
// ex., dev_rg(i) = r(i) / g(i), g(i) = (gr(i) + gb(i)) /2
// [goldenShadingDev] calculates shading deviation with golden shading
// ex., dev_rg(i) = (dev_rg(i) - dev_rg(i)_golden) / dev_rg(i)_golden
typedef struct _TShadingDev
{
	double dev_rg;
	double dev_bg;
	double dev_grgb;
	bool bRstFlag;	// 0:false 1:pass
} TShadingDev;

// average level of grid
typedef struct _TGridData
{
	double r_avg;
	double gr_avg;
	double gb_avg;
	double b_avg;
} TGridData;

//vignetting
typedef struct _TVignettingSpec
{
	double dYmeanSpec;
	double dYmeanROISizeRatio;
	double dDStripeSpec;
	double dDStripeStrideRatio;
	int nMode;
} TVignettingSpec;