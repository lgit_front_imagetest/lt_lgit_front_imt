#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
//#include "LibACMISSoftISP\Include\SoftISP.h"

// DO NOT INCLUDE HEADER FILE!
class CRelativeIllumination;

class ACMISSHADING_API CACMISShadingRelativeIllumination
{
public:
	CACMISShadingRelativeIllumination(void);
	~CACMISShadingRelativeIllumination(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TRelativeIlluminationSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode,  ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TRelativeIlluminationSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TRelativeIlluminationSpec& _Spec);

	const TRelativeIlluminationResult* GetInspectionResult(void);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	const char* GetVersion(void);

	bool InSpec(const TRelativeIlluminationSpec& _Spec);

	const RECT* GetInspectionROI(EPos ePos);

private:
	CRelativeIllumination* m_pMethod;
};
