﻿#ifndef Def_Mes_EachTest_h__
#define Def_Mes_EachTest_h__

#include <afxwin.h>


#pragma pack(push,1)



static LPCTSTR g_szMESHeader_Current[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Current_LED(Vcsel)"),
	NULL,
};

static LPCTSTR g_szMESHeader_CurrentEntry[] =
{
	_T("Current_CH1"),
	_T("Current_CH2"),
	_T("Current_CH3"),
	_T("Current_CH4"),
	_T("Current_CH5"),
	NULL,
};

static LPCTSTR g_szMESHeader_CurrentFront[] =
{
	_T("Current_CH1"),
	NULL,
};

static LPCTSTR g_szMESHeader_OC[] =
{
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Offset_X"),
	_T("Offset_Y"),
	NULL,
};
static LPCTSTR g_szMESHeader_SFR[] =
{
	_T("SFR_0"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	NULL,
};

static LPCTSTR g_szMESHeader_Rotate[] =
{
	_T("Rotation"),
	NULL,
};

static LPCTSTR g_szMESHeader_Defect_White[] =
{
	_T("Defect_White_ Count"),
	NULL,
};

static LPCTSTR g_szMESHeader_Defect_Black[] =
{
	_T("Defect_Black_ Count"),
	NULL,
};

static LPCTSTR g_szMESHeader_LCB[] =
{
	_T("LCB_ Count"),
	NULL,
};


static LPCTSTR g_szMESHeader_BlackSpot[] =
{
	_T("BlackSpot_ Count"),
	NULL,
};

static LPCTSTR g_szMESHeader_Distortion[] =
{
	_T("Distortion"),
	//_T("Reserved"),
	//_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_Fov[] =
{
	_T("FOV_Diagonal"),
	_T("Fov_Horizontal"),
	_T("Fov_Vertical"),
	//_T("Reserved"),
	//_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_Stain[] =
{
	_T("Stain_#1 Ymean"),
	_T("Stain_#1 Count"),
	_T("Stain_#2 Ymean"),
	_T("Stain_#2 Count"),
	_T("Stain_#3 Ymean"),
	_T("Stain_#3 Count"),
	_T("Stain_#4 Ymean"),
	_T("Stain_#4 Count"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_Defectpixel[] =
{
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_Dynamic_BW[] =
{
	_T("Dynamic Range"),
	_T("SNR_BW"),
	NULL,
};

static LPCTSTR g_szMESHeader_Dynamic[] =
{
	_T("D/R_1 - Brightness(W)"),
	_T("D/R_1 - Brightness(G)"),
	_T("D/R_1 - Brightness(B)"),
	_T("D/R_1 - STD(W)"),
	_T("D/R_1 - STD(G"),
	_T("D/R_1 - STD(B)"),
	_T("D/R_1 - SNR_BW"),
	_T("D/R_1 - DynamicRange"),
	_T("D/R_2 - Brightness(W)"),
	_T("D/R_2 - Brightness(G)"),
	_T("D/R_2 - Brightness(B)"),
	_T("D/R_2 - STD(W)"),
	_T("D/R_2 - STD(G)"),
	_T("D/R_2 - STD(B)"),
	_T("D/R_2 - SNR_BW"),
	_T("D/R_2 - DynamicRange"),
	_T("D/R_3 - Brightness(W)"),
	_T("D/R_3 - Brightness(G)"),
	_T("D/R_3 - Brightness(B)"),
	_T("D/R_3 - STD(W)"),
	_T("D/R_3 - STD(G)"),
	_T("D/R_3 - STD(B)"),
	_T("D/R_3 - SNR_BW"),
	_T("D/R_3 - DynamicRange"),
	_T("D/R_4 - Brightness(W)"),
	_T("D/R_4 - Brightness(G)"),
	_T("D/R_4 - Brightness(B)"),
	_T("D/R_4 - STD(W)"),
	_T("D/R_4 - STD(G)"),
	_T("D/R_4 - STD(B"),
	_T("D/R_4 - SNR_BW"),
	_T("D/R_4 - DynamicRange"),
	_T("D/R_5 - Brightness(W)"),
	_T("D/R_5 - Brightness(G)"),
	_T("D/R_5 - Brightness(B)"),
	_T("D/R_5 - STD(W)"),
	_T("D/R_5 - STD(G)"),
	_T("D/R_5 - STD(B)"),
	_T("D/R_5 - SNR_BW"),
	_T("D/R_5 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};
static LPCTSTR g_szMESHeader_Shading_RI[] =
{
	_T("Shading_Center - Brightness"),
	_T("Shading_Center - ratio"),
	_T("Shading_RightTop - Brightness"),
	_T("Shading_RightTop - ratio"),
	_T("Shading_RightBottom - Brightness"),
	_T("Shading_RightBottom - ratio"),
	_T("Shading_LeftTop - Brightness"),
	_T("Shading_LeftTop - ratio"),
	_T("Shading_LeftBottom - Brightness"),
	_T("Shading_LeftBottom - ratio"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

// 
// static CString g_szMESHeader_IR_Shading[] =
// {
// 	_T("Horizon"),
// 	_T("Vertical"),
// 	_T("DiagonalA"),
// 	_T("DiagonalB"),
// };
// 
// static CString g_szMESHeader_IR_Shading_ROI[] =
// {
// 	_T("Shading_0"),
// 	_T("Shading_1"),
// 	_T("Shading_2"),
// 	_T("Shading_3"),
// 	_T("Shading_4"),
// 	_T("Shading_5"),
// 	_T("Shading_6"),
// 	_T("Shading_7"),
// 	_T("Shading_8"),
// 	_T("Shading_9"),
// 	_T("Shading_10"),
// 	_T("Shading_11"),
// 	_T("Shading_12"),
// 	_T("Shading_13"),
// 	_T("Shading_14"),
// 	_T("Shading_15"),
// 	_T("Shading_16"),
// 	_T("Shading_17"),
// 	_T("Shading_18"),
// };

static LPCTSTR g_szMESHeader_HotPixel[] =
{
	_T("HotPixel"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_FPN[] =
{
	_T("FPN_X"),
	_T("FPN_Y"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_3DDepth[] =
{
	_T("Brightness"),
	_T("Deviation"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};

static LPCTSTR g_szMESHeader_EEPROM[] =
{
	_T("OC_X"),
	_T("OC_Y"),
	_T("CHECK_1"),
	_T("CHECK_2"),
	_T("Reserved"),
	_T("Reserved"),
	NULL,
};


static LPCTSTR g_szMESHeader_SNR_Light[] =
{
	_T("SNRLIGHT_1"),
	_T("SNRLIGHT_2"),
	_T("SNRLIGHT_3"),
	_T("SNRLIGHT_4"),
	_T("SNRLIGHT_5"),
	_T("SNRLIGHT_6"),
	_T("SNRLIGHT_7"),
	_T("SNRLIGHT_8"),
	_T("SNRLIGHT_9"),
	_T("SNRLIGHT_10"),
	_T("SNRLIGHT_11"),
	_T("SNRLIGHT_12"),
	_T("SNRLIGHT_13"),
	_T("SNRLIGHT_14"),
	_T("SNRLIGHT_15"),
	_T("SNRLIGHT_16"),
	_T("SNRLIGHT_17"),
	_T("SNRLIGHT_18"),
	_T("SNRLIGHT_19"),
	_T("SNRLIGHT_20"),
	_T("SNRLIGHT_21"),
	_T("SNRLIGHT_22"),
	_T("SNRLIGHT_23"),
	_T("SNRLIGHT_24"),
	_T("SNRLIGHT_25"),
	_T("SNRLIGHT_26"),
	_T("SNRLIGHT_27"),
	_T("SNRLIGHT_28"),
	_T("SNRLIGHT_29"),
	_T("SNRLIGHT_30"),
	_T("SNRLIGHT_31"),
	_T("SNRLIGHT_32"),
	_T("SNRLIGHT_33"),
	_T("SNRLIGHT_34"),
	_T("SNRLIGHT_35"),
	_T("SNRLIGHT_36"),
	_T("SNRLIGHT_37"),
	_T("SNRLIGHT_38"),
	_T("SNRLIGHT_39"),
	_T("SNRLIGHT_40"),
	_T("SNRLIGHT_41"),
	_T("SNRLIGHT_42"),
	_T("SNRLIGHT_43"),
	_T("SNRLIGHT_44"),
	_T("SNRLIGHT_45"),
	_T("SNRLIGHT_46"),
	_T("SNRLIGHT_47"),
	_T("SNRLIGHT_48"),
	_T("SNRLIGHT_49"),
	_T("SNRLIGHT_50"),
	_T("SNRLIGHT_51"),
	_T("SNRLIGHT_52"),
	_T("SNRLIGHT_53"),
	_T("SNRLIGHT_54"),
	_T("SNRLIGHT_55"),
	_T("SNRLIGHT_56"),
	NULL,
};
static LPCTSTR g_szMESHeader_Shading_SNR[] =
{
	_T("Shading_1"),
	_T("Shading_2"),
	_T("Shading_3"),
	_T("Shading_4"),
	_T("Shading_5"),
	_T("Shading_6"),
	_T("Shading_7"),
	_T("Shading_8"),
	_T("Shading_9"),
	_T("Shading_10"),
	_T("Shading_11"),
	_T("Shading_12"),
	_T("Shading_13"),
	_T("Shading_14"),
	_T("Shading_15"),
	_T("Shading_16"),
	_T("Shading_17"),
	_T("Shading_18"),
	_T("Shading_19"),
	_T("Shading_20"),
	_T("Shading_21"),
	_T("Shading_22"),
	_T("Shading_23"),
	_T("Shading_24"),
	_T("Shading_25"),
	_T("Shading_26"),
	_T("Shading_27"),
	_T("Shading_28"),
	_T("Shading_29"),
	_T("Shading_30"),
	_T("Shading_31"),
	_T("Shading_32"),
	_T("Shading_33"),
	_T("Shading_34"),
	_T("Shading_35"),
	_T("Shading_36"),
	_T("Shading_37"),
	_T("Shading_38"),
	_T("Shading_39"),
	_T("Shading_40"),
	_T("Shading_41"),
	_T("Shading_42"),
	_T("Shading_43"),
	_T("Shading_44"),
	_T("Shading_45"),
	_T("Shading_46"),
	_T("Shading_47"),
	_T("Shading_48"),
	_T("Shading_49"),
	_T("Shading_50"),
	_T("Shading_51"),
	_T("Shading_52"),
	_T("Shading_53"),
	_T("Shading_54"),
	_T("Shading_55"),
	_T("Shading_56"),
	_T("Shading_57"),
	_T("Shading_58"),
	_T("Shading_59"),
	_T("Shading_60"),
	_T("Shading_61"),
	_T("Shading_62"),
	_T("Shading_63"),
	_T("Shading_64"),
	_T("Shading_65"),
	_T("Shading_66"),
	_T("Shading_67"),
	_T("Shading_68"),
	_T("Shading_69"),
	_T("Shading_70"),
	_T("Shading_71"),
	_T("Shading_72"),
	_T("Shading_73"),
	NULL,
};
static LPCTSTR g_szMESHeader_RI[] =
{
	_T("Corner"),
	_T("Min"),
	_T("RIcorner"),
	_T("RImin"),
	_T("RIcenter"),
	_T("RI_UL"),
	_T("RI_UR"),
	_T("RI_LL"),
	_T("RI_LR"),
	NULL,
};
#pragma pack(pop)

#endif // Def_FOV_h__