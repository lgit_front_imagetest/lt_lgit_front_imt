// Wnd_TestResult_ImgT_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_ImgT_MainView.h"


// CWnd_TestResult_ImgT_MainView
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_ImgT_MainView, CWnd)

CWnd_TestResult_ImgT_MainView::CWnd_TestResult_ImgT_MainView()
{
	for (UINT nItem = 0; nItem < TI_ImgT_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	m_InspectionType = Sys_Image_Test;//Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_ImgT_MainView::~CWnd_TestResult_ImgT_MainView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_ImgT_MainView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_ImgT_MainView 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_ImgT_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_ImgT_Main_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_ImgT_Main_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	UINT nWndID = IDC_LIST_ITEM;


	m_Wnd_Rst_SFR.SetOwner(GetOwner());
	m_Wnd_Rst_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Ymean.SetOwner(GetOwner());
	m_Wnd_Rst_Ymean.Create(NULL, _T("Ymean"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_BlackSpot.SetOwner(GetOwner());
	m_Wnd_Rst_BlackSpot.Create(NULL, _T("BlackSpot"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Defect_Black.SetOwner(GetOwner());
	m_Wnd_Rst_Defect_Black.Create(NULL, _T("Defect_Black"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Defect_White.SetOwner(GetOwner());
	m_Wnd_Rst_Defect_White.Create(NULL, _T("Defect_White"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_LCB.SetOwner(GetOwner());
	m_Wnd_Rst_LCB.Create(NULL, _T("LCB"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_Rst_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Rotate.SetOwner(GetOwner());
	m_Wnd_Rst_Rotate.Create(NULL, _T("Rotate"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Current.SetOwner(GetOwner());
	m_Wnd_Rst_Current.Create(NULL, _T("Current"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_FOV.SetOwner(GetOwner());
	m_Wnd_Rst_FOV.Create(NULL, _T("FOV"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Distortion.SetOwner(GetOwner());
	m_Wnd_Rst_Distortion.Create(NULL, _T("Distortion"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_DynamicBW.SetOwner(GetOwner());
	m_Wnd_Rst_DynamicBW.Create(NULL, _T("DynamicBW"), dwStyle, rectDummy, this, nWndID++);

	m_wnd_Rst_Shading.SetOwner(GetOwner());
	m_wnd_Rst_Shading.Create(NULL, _T("Shading"), dwStyle, rectDummy, this, nWndID++);

	//m_Wnd_Rst_ActiveAlign.SetOwner(GetOwner());
	//m_Wnd_Rst_ActiveAlign.Create(NULL, _T("ActiveAlign"), dwStyle, rectDummy, this, nWndID++);
	//
	//m_Wnd_Rst_Torque.SetOwner(GetOwner());
	//m_Wnd_Rst_Torque.Create(NULL, _T("Torque"), dwStyle, rectDummy, this, nWndID++);

	m_cb_TestItem.AddString(_T("SFR"));
	m_cb_TestItem.AddString(_T("Current"));

	m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_Black.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_White.ShowWindow(SW_HIDE);
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_OpticalCenter.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Rotate.ShowWindow(SW_HIDE);
	m_Wnd_Rst_FOV.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Distortion.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DynamicBW.ShowWindow(SW_HIDE);
	m_wnd_Rst_Shading.ShowWindow(SW_HIDE);
// 	m_Wnd_Rst_ActiveAlign.ShowWindow(SW_HIDE);
// 	m_Wnd_Rst_Torque.ShowWindow(SW_HIDE);;

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;
	int iHeightTemp = iHeight / 9;

	m_st_Item[STI_TR_ImgT_Main_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;

	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;

	m_Wnd_Rst_Current.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_SFR.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_BlackSpot.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Ymean.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Defect_Black.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Defect_White.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_LCB.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Rotate.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_FOV.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Distortion.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_DynamicBW.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_Rst_Shading.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
// 	m_Wnd_Rst_ActiveAlign.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
// 	m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);

	switch (m_iSelectTest[m_cb_TestItem.GetCurSel()])
	{

	case TI_ImgT_Fn_ECurrent:
		m_Wnd_Rst_Current.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_OpticalCenter:
		m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_SFR:
		m_Wnd_Rst_SFR.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_Rotation:
		m_Wnd_Rst_Rotate.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_BlackSpot:
		m_Wnd_Rst_BlackSpot.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_Ymean:
		m_Wnd_Rst_Ymean.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_Defect_Black:
		m_Wnd_Rst_Defect_Black.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_Defect_White:
		m_Wnd_Rst_Defect_White.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_LCB:
		m_Wnd_Rst_LCB.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_FOV:
		m_Wnd_Rst_FOV.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_Distortion:
		m_Wnd_Rst_Distortion.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_ImgT_Fn_DynamicBW:
		m_Wnd_Rst_DynamicBW.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;
	case TI_ImgT_Fn_Shading:
		m_wnd_Rst_Shading.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;
// 	case TI_ImgT_Re_TorqueCheck:
// 		m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
// 		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_ImgT_MainView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}


//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::AllDataReset()
{

	m_Wnd_Rst_Current.Result_Reset();
	m_Wnd_Rst_SFR.Result_Reset();
	m_Wnd_Rst_BlackSpot.Result_Reset();
	m_Wnd_Rst_Ymean.Result_Reset();
	m_Wnd_Rst_Defect_Black.Result_Reset();
	m_Wnd_Rst_Defect_White.Result_Reset();
	m_Wnd_Rst_LCB.Result_Reset();
	m_Wnd_Rst_OpticalCenter.Result_Reset();
	m_Wnd_Rst_Rotate.Result_Reset();
	m_Wnd_Rst_FOV.Result_Reset();
	m_Wnd_Rst_Distortion.Result_Reset();
	m_Wnd_Rst_DynamicBW.Result_Reset();
	m_wnd_Rst_Shading.Result_Reset();
	//m_Wnd_Rst_ActiveAlign.Result_Reset();
	//m_Wnd_Rst_Torque.DataReset();

}
//=============================================================================
// Method		: SetUIData_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 8:52
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetUIData_Reset(int nIdx)
{

	switch (nIdx)
	{
// 	case TI_ImgT_Fn_PreFocus:
// 		m_Wnd_Rst_OpticalCenter.Result_Reset();
// 		break;
	case TI_ImgT_Fn_ECurrent:
	{
		m_Wnd_Rst_Current.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_SFR:
	{
		m_Wnd_Rst_SFR.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_BlackSpot:
	{
		m_Wnd_Rst_BlackSpot.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_Ymean:
	{
		m_Wnd_Rst_Ymean.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_Defect_Black:
	{
		m_Wnd_Rst_Defect_Black.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_Defect_White:
	{
		m_Wnd_Rst_Defect_White.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_LCB:
	{
		m_Wnd_Rst_LCB.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		m_Wnd_Rst_OpticalCenter.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		m_Wnd_Rst_Rotate.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
		m_Wnd_Rst_FOV.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
		m_Wnd_Rst_Distortion.Result_Reset();
	}
		break;

	case TI_ImgT_Fn_DynamicBW:
	{
		m_Wnd_Rst_DynamicBW.Result_Reset();
	}
		break;
	case TI_ImgT_Fn_Shading:
	{
		 m_wnd_Rst_Shading.Result_Reset();
	}
		break;
// 	case TI_ImgT_Fn_ActiveAlign:
// 	{
// 								  m_Wnd_Rst_ActiveAlign.Result_Reset();
// 	}
// 		break;
// 	case TI_ImgT_Re_TorqueCheck:
// 	{							//!SH _180915: 검토 필요
// 								  m_Wnd_Rst_Torque.DataReset();
// 	}
// 		break;
// 	case TI_ImgT_Motion_LockingScrew:
// 	{							//!SH _180915: 검토 필요
// 									   m_Wnd_Rst_Torque.DataReset();
// 	}
// 		break;
// 	case TI_ImgT_Motion_ReleaseScrew:
// 	{							//!SH _180915: 검토 필요
// 									   m_Wnd_Rst_Torque.DataReset();
// 	}
// 		break;
	default:
		break;
	}

	SelectItem(nIdx);

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: __in int nIdx
// Parameter	: __in LPVOID pParam
// Parameter	: __in enFocus_AAView enView
// Qualifier	:
// Last Update	: 2018/3/9 - 10:01
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetUIData(__in int nIdx, __in LPVOID pParam)//, __in enImageMode enView)
{
	if (pParam == NULL)
		return;

	//for (int iIndx = 0; iIndx < m_cb_TestItem.GetCount(); iIndx++)
	//{
	//	if (nIdx == m_iSelectTest[iIndx])
	//	{
	//		SelectNum(iIndx);
	//		break;
	//	}
	//}

	switch (nIdx)
	{
	case TI_ImgT_Fn_ECurrent:
	{
							   //!SH _180911: Current 관련 수정 필요
							   m_Wnd_Rst_Current.Result_Display(0, 0, ((ST_IQ_Result *)pParam)->stCurrent.bResult[0], ((ST_IQ_Result *)pParam)->stCurrent.dbValue[0]);


	}
		break;

	case TI_ImgT_Fn_SFR:
	{

						  for (int nNum = 0; nNum < enSFR_Result_Max; nNum++)
						  {
							  m_Wnd_Rst_SFR.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stSFR.bResult[nNum], ((ST_IQ_Result *)pParam)->stSFR.dbValue[nNum]);
						  }

	}
		break;

	case TI_ImgT_Fn_BlackSpot:
	{
								for (int nNum = 0; nNum < enBlackSpot_Result_Max; nNum++)
								{
									m_Wnd_Rst_BlackSpot.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stBlackSpot.bBlackSpotResult, ((ST_IQ_Result *)pParam)->stBlackSpot.nDefectCount);
								}
	}
		break;

	case TI_ImgT_Fn_Ymean:
	{
							for (int nNum = 0; nNum < enYmean_Result_Max; nNum++)
							{
								m_Wnd_Rst_Ymean.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stYmean.bYmeanResult, ((ST_IQ_Result *)pParam)->stYmean.nDefectCount);
							}

	}
		break;


	case TI_ImgT_Fn_Defect_Black:
	{
								   for (int nNum = 0; nNum < enDefect_Black_Result_Max; nNum++)
								   {
									   m_Wnd_Rst_Defect_Black.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stDefect_Black.bDefect_BlackResult, ((ST_IQ_Result *)pParam)->stDefect_Black.nDefect_BlackCount);
								   }
	}
		break;

	case TI_ImgT_Fn_Defect_White:
	{
								   for (int nNum = 0; nNum < enDefect_White_Result_Max; nNum++)
								   {
									   m_Wnd_Rst_Defect_White.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stDefect_White.bDefect_WhiteResult, ((ST_IQ_Result *)pParam)->stDefect_White.nDefect_WhiteCount);
								   }

	}
		break;

	case TI_ImgT_Fn_LCB:
	{
						  for (int nNum = 0; nNum < enLCB_Result_Max; nNum++)
						  {
							  m_Wnd_Rst_LCB.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stDefect_White.bDefect_WhiteResult, ((ST_IQ_Result *)pParam)->stDefect_White.nDefect_WhiteCount);
						  }
	}
		break;

// 	case TI_ImgT_Fn_PreFocus:
// 	{
// 								m_Wnd_Rst_OpticalCenter.Result_Display(0, 0, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultX, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultPosX);
// 								m_Wnd_Rst_OpticalCenter.Result_Display(0, 1, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultY, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultPosY);
// 								m_Wnd_Rst_OpticalCenter.Result_Display(0, 2, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultX, ((ST_IQ_Result *)pParam)->stOpticalCenter.iStandPosDevX);
// 								m_Wnd_Rst_OpticalCenter.Result_Display(0, 3, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultY, ((ST_IQ_Result *)pParam)->stOpticalCenter.iStandPosDevY);
// 
// 	}
// 		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
									 m_Wnd_Rst_OpticalCenter.Result_Display(0, 0, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultX, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultPosX);
									 m_Wnd_Rst_OpticalCenter.Result_Display(0, 1, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultY, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultPosY);
									 m_Wnd_Rst_OpticalCenter.Result_Display(0, 2, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultX, ((ST_IQ_Result *)pParam)->stOpticalCenter.iStandPosDevX);
									 m_Wnd_Rst_OpticalCenter.Result_Display(0, 3, ((ST_IQ_Result *)pParam)->stOpticalCenter.nResultY, ((ST_IQ_Result *)pParam)->stOpticalCenter.iStandPosDevY);

	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
							   for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
							   {
								   m_Wnd_Rst_Rotate.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stRotate.bRotation, ((ST_IQ_Result *)pParam)->stRotate.dbRotation);
							   }

	}
		break;

	case TI_ImgT_Fn_FOV:
	{
								for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
								{
									m_Wnd_Rst_FOV.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stFOV.bDFOV, ((ST_IQ_Result *)pParam)->stFOV.dbDFOV);	//^^SH2 1107 다시볼것! 2개추가
								}

	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
								for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
								{
									m_Wnd_Rst_Distortion.Result_Display(0, nNum, ((ST_IQ_Result *)pParam)->stDistortion.bDistortion, ((ST_IQ_Result *)pParam)->stDistortion.dbDistortion);
								}

	}
		break;

	case TI_ImgT_Fn_DynamicBW:
	{

									//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
									//{
									m_Wnd_Rst_DynamicBW.Result_Display(0, 0, ((ST_IQ_Result *)pParam)->stDynamicBW.bDynamic, ((ST_IQ_Result *)pParam)->stDynamicBW.dbDynamic);
									m_Wnd_Rst_DynamicBW.Result_Display(0, 1, ((ST_IQ_Result *)pParam)->stDynamicBW.bSNR_BW, ((ST_IQ_Result *)pParam)->stDynamicBW.dbSNR_BW);
									//}

	}
		break;
	case TI_ImgT_Fn_Shading:
	{

							   for (int nNum = 0; nNum < enShadingEachResultMax; nNum++)
							   {
								   //if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
								   //{
								   m_wnd_Rst_Shading.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stShading.bHorizonResult[nNum], ((ST_IR_Result *)pParam)->stShading.dbHorizonValue[nNum]);
								   //}
							   }
							   int ndata = 0;
							   int nVerticalnum = enShadingEachResultMax * 2;
							   for (int nNum = enShadingEachResultMax; nNum < nVerticalnum; nNum++)
							   {

								   m_wnd_Rst_Shading.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stShading.bVerticalResult[ndata], ((ST_IR_Result *)pParam)->stShading.dbVerticalValue[ndata]);
								   ndata++;

							   }
							   ndata = 0;
							   int nDiagonalAnum = enShadingEachResultMax * 3;
							   for (int nNum = nVerticalnum; nNum < nDiagonalAnum; nNum++)
							   {
								   m_wnd_Rst_Shading.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stShading.bDiaAResult[ndata], ((ST_IR_Result *)pParam)->stShading.dbDiaAValue[ndata]);
								   ndata++;
							   }
							   ndata = 0;
							   for (int nNum = nDiagonalAnum; nNum < enShadingData_MAX; nNum++)
							   {
								   m_wnd_Rst_Shading.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stShading.bDiaBResult[ndata], ((ST_IR_Result *)pParam)->stShading.dbDiaBValue[ndata]);
								   ndata++;
							   }

	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetClearTab()
{
	for (UINT nItem = 0; nItem < TI_ImgT_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	if (m_cb_TestItem.GetCount() > 0)
	{
		m_cb_TestItem.ResetContent();
	}
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int & iItemCnt
// Qualifier	:
// Last Update	: 2018/3/9 - 9:03
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetAddTab(int nIdx, int &iItemCnt)
{
	// 첫 아이템 등록시 콤보 박스 초기화
	if (iItemCnt == 0)
	{
		m_cb_TestItem.ResetContent();
	}

	// 기존에 항목이 있으면 추가 하지 말고 지나가라.
	for (int iItem = 0; iItem < iItemCnt; iItem++)
	{
		if (nIdx == m_iSelectTest[iItem])
			return;
	}

	switch (nIdx)
	{
// 	case TI_ImgT_Fn_PreFocus:
// 		m_iSelectTest[iItemCnt++] = nIdx;
// 		m_cb_TestItem.AddString(_T("PreFocus"));
// 		break;
	case TI_ImgT_Fn_ECurrent:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Current"));
		break;
	case TI_ImgT_Fn_OpticalCenter:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("OpticalCenter"));
		break;
	case TI_ImgT_Fn_Rotation:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Rotate"));
		break;
	case TI_ImgT_Fn_SFR:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("SFR"));
		break;
	case TI_ImgT_Fn_Ymean:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Ymean"));
		break;
	case TI_ImgT_Fn_BlackSpot:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("BlackSpot"));
		break;
	case TI_ImgT_Fn_LCB:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("LCB"));
		break;
	case TI_ImgT_Fn_Defect_Black:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Defect_Black"));
		break;
	case TI_ImgT_Fn_Defect_White:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Defect_White"));
		break;
	case TI_ImgT_Fn_FOV:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("FOV"));
		break;
	case TI_ImgT_Fn_Distortion:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Distortion"));
		break;
	case TI_ImgT_Fn_DynamicBW:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("DynamicBW"));
		break;
	case TI_ImgT_Fn_Shading:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Shading"));
		break;
// 	case TI_ImgT_Fn_ActiveAlign:
// 		m_iSelectTest[iItemCnt++] = nIdx;
// 		m_cb_TestItem.AddString(_T("ActiveAlign"));
// 		break;
// 	case TI_ImgT_Re_TorqueCheck:
// 		m_iSelectTest[iItemCnt++] = nIdx;
// 		m_cb_TestItem.AddString(_T("Torque"));
// 		break;
// 	case TI_ImgT_Motion_ReleaseScrew:
// 		m_iSelectTest[iItemCnt++] = nIdx;
// 		m_cb_TestItem.AddString(_T("Torque"));
// 		break;
// 	case TI_ImgT_Motion_LockingScrew:
// 		m_iSelectTest[iItemCnt++] = nIdx;
// 		m_cb_TestItem.AddString(_T("Torque"));
// 		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnLbnSelChangeTest()
{

	m_Wnd_Rst_Current.ShowWindow(SW_HIDE);
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_Black.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_White.ShowWindow(SW_HIDE);
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_OpticalCenter.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Rotate.ShowWindow(SW_HIDE);
	m_Wnd_Rst_FOV.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Distortion.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DynamicBW.ShowWindow(SW_HIDE);
	m_wnd_Rst_Shading.ShowWindow(SW_HIDE);
// 	m_Wnd_Rst_ActiveAlign.ShowWindow(SW_HIDE);
// 	m_Wnd_Rst_Torque.ShowWindow(SW_HIDE);

	int iIdex = m_cb_TestItem.GetCurSel();

	switch (m_iSelectTest[iIdex])
	{
// 	case TI_ImgT_Fn_PreFocus:
// 		m_Wnd_Rst_OpticalCenter.ShowWindow(SW_SHOW);
// 		break;
	case TI_ImgT_Fn_ECurrent:
	{
							   m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_SFR:
	{
						  m_Wnd_Rst_SFR.ShowWindow(SW_SHOW);

	}
		break;

	case TI_ImgT_Fn_BlackSpot:
	{
								m_Wnd_Rst_BlackSpot.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Ymean:
	{
							m_Wnd_Rst_Ymean.ShowWindow(SW_SHOW);
	}
		break;


	case TI_ImgT_Fn_Defect_Black:
	{
								   m_Wnd_Rst_Defect_Black.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Defect_White:
	{
								   m_Wnd_Rst_Defect_White.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_LCB:
	{
						  m_Wnd_Rst_LCB.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
									m_Wnd_Rst_OpticalCenter.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
							   m_Wnd_Rst_Rotate.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
								m_Wnd_Rst_FOV.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
								m_Wnd_Rst_Distortion.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_DynamicBW:
	{
							  m_Wnd_Rst_DynamicBW.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_Shading:
	{
								 m_wnd_Rst_Shading.ShowWindow(SW_SHOW);
	}
		break;

	default:
		break;
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
}

//=============================================================================
// Method		: SelectItem
// Access		: public  
// Returns		: void
// Parameter	: UINT nTestItem
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SelectItem(UINT nTestItem)
{
	int iSelect = 0;

	for (int nItem = 0; nItem < m_cb_TestItem.GetCount(); nItem++)
	{
		if (m_iSelectTest[nItem] == nTestItem)
		{
			iSelect = nItem;
			break;
		}
	}

	m_cb_TestItem.SetCurSel(iSelect);
	OnLbnSelChangeTest();
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10 
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}