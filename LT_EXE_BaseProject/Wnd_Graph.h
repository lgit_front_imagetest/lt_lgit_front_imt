#ifndef Wnd_Graph_h__
#define Wnd_Graph_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
// #include "Def_T_Graph.h"
// #include "Def_TestItem_VI.h"
#include "OScopeCtrl.h"

//-----------------------------------------------------------------------------
// CWnd_Graph
//-----------------------------------------------------------------------------
class CWnd_Graph : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Graph)

public:
	CWnd_Graph();
	virtual ~CWnd_Graph();

	enum enGraph_R_Static
	{
		STI_GP_R_TITLE = 0,
		STI_GP_R_CURRENT,
		STI_GP_R_MAX_SFR,
		STI_GP_R_SPEC_MIN,
		STI_GP_R_SPEC_MAX,
		STI_GP_R_RESULT,
		STI_GP_R_MAX,
	};

	enum enGraph_R_Button
	{
		BTN_GP_R_MAX = 1,
	};

	enum enGraph_R_Comobox
	{
		CMB_GP_R_MAX = 1,
	};

	enum enGraph_R_Edit
	{
		EDT_GP_R_MAX = 1,
	};		  

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl(UINT nID);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	COScopeCtrl*		m_pGraph	=	NULL;
	CFont				m_font;

	CVGStatic			m_st_Item[STI_GP_R_MAX];
	CButton				m_bn_Item[BTN_GP_R_MAX];
	CComboBox			m_cb_Item[CMB_GP_R_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_GP_R_MAX];

//	ST_Graph_Opt*		m_pstConfigInfo = NULL;

public:

// 	void	SetPtr_RecipeInfo(__in ST_Graph_Opt* pstRecipeInfo)
// 	{
// 		if (pstRecipeInfo == NULL)
// 			return;
// 
// 		m_pstConfigInfo = pstRecipeInfo;
// 	};

	void	SetUpdateData	(__in double dbCurrentSFR, __in double dbMaxSFR);
	void	SetUpdateSpec	();
	void	SetResetData	();
};

#endif // Wnd_Graph_h__
