﻿//*****************************************************************************
// Filename	: 	Overlay_Proc.cpp
// Created	:	2017/3/23 - 08:14
// Modified	:	2017/3/23 - 08:14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Overlay_Proc.h"
#include "CommonFunction.h"

COverlay_Proc::COverlay_Proc()
{
}


COverlay_Proc::~COverlay_Proc()
{
}

//=============================================================================
// Method		: Overlay_SFR
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_SFR stOption
// Parameter	: __in ST_Result_SFR stResult
// Qualifier	:
// Last Update	: 2018/8/7 - 1:11
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SFR(__inout IplImage* lpImage, __in ST_UI_SFR stOption, __in ST_Result_SFR_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;
	
	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		if (TRUE == stOption.stInput[nIdx].bEnable)
		{
			int iLineSize		= 2;
			int iMarkSize		= 10;
			double dbFontSize	= 1.0;

			if (TRUE == m_bTest)
			{
				rtRect = stResult.rtROI[nIdx];
			}
			else
			{
				rtRect = stOption.stInput[nIdx].rtROI;
			}
	
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
	
			rtRectText.SetRectEmpty();

			switch (stOption.stInput[nIdx].nOverlayDir)
			{
			case 0:	// OverlayDir_Left
				rtRectText.left = rtRect.left - 70;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 1: // OverlayDir_Right
				rtRectText.left = rtRect.right + 10;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 2: // OverlayDir_Top
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.top - 40;
				break;
			case 3: // OverlayDir_Bottom
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.bottom + 40;
				break;
			default:
				break;
			}
	
			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stResult.dbValue[nIdx]);

			iLineSize = 4;

			if (TRUE == stResult.bResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}

void COverlay_Proc::Overlay_SFR(__inout IplImage* lpImage, __in ST_UI_SFR stOption, __in ST_Result_SFR_FOC stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		if (TRUE == stOption.stInput[nIdx].bEnable)
		{
			int iLineSize = 1;
			int iMarkSize = 5;
			double dbFontSize = 0.5;

			if (TRUE == m_bTest)
			{
				rtRect = stResult.rtROI[nIdx];
			}
			else
			{
				rtRect = stOption.stInput[nIdx].rtROI;
			}

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stInput[nIdx].nOverlayDir)
			{
			case 0:	// OverlayDir_Left
				rtRectText.left = rtRect.left - 70;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 1: // OverlayDir_Right
				rtRectText.left = rtRect.right + 10;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 2: // OverlayDir_Top
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.top - 40;
				break;
			case 3: // OverlayDir_Bottom
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.bottom + 40;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stResult.dbValue[nIdx]);

			iLineSize = 2;
			if (TRUE == stResult.bResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}
//=============================================================================
// Method		: Overlay_Chart
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Chart stOption
// Parameter	: __in ST_Result_Chart stResult
// Qualifier	:
// Last Update	: 2018/8/23 - 16:11
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Chart(__inout IplImage* lpImage, __in ST_UI_Chart stOption, __in ST_Result_Chart stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 2.0;

	if (TRUE == m_bTest)
	{
		rtRect = stResult.rtROI;
	}
	else
	{
		rtRect = stOption.rtROI;

		stResult.bDetect = FALSE;
	}

	if (TRUE == stResult.bDetect)
	{
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
	}

	iLineSize = 3;
	iMarkSize = 30;

	rtRectText.left = lpImage->width / 3;
	rtRectText.top  = lpImage->height - 50;

	if (TRUE == m_bTest)
	{
		if (TRUE == stResult.bResult)
		{
			szText = _T("Chart Matching OK");
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
		}
		else
		{
			
			szText = _T("Chart Matching NG");
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			
			
		}
	}
}

void COverlay_Proc::Overlay_PreFocusing(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data_Foc stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect = stOption.rtROI;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Detech the Optical Center!!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 370;
	rtRect.top = 100;

	if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}

	rtRect = stOption.rtROI;
	iLineSize = 3;
	dbFontSize = 1.3;

	ptCenter.x = stData.nPixX;
	ptCenter.y = stData.nPixY;

	if (0 >= ptCenter.x || 0 >= ptCenter.y)
		return;

	rtRect.left = ptCenter.x - iMarkSize;
	rtRect.top = ptCenter.y;
	rtRect.right = ptCenter.x + iMarkSize;
	rtRect.bottom = ptCenter.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	rtRect.left = ptCenter.x;
	rtRect.top = ptCenter.y - iMarkSize;
	rtRect.right = ptCenter.x;
	rtRect.bottom = ptCenter.y + iMarkSize;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	szText.Format(_T("X : %d Y : %d"), stData.iStandPosDevX, stData.iStandPosDevY);

	rtRect.left = stOption.rtROI.left;
	rtRect.top = stOption.rtROI.bottom + 60;

	iLineSize = 3;
	dbFontSize = 1.3;

	if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Current(__inout IplImage* lpImage, __in ST_UI_Current stOption, __in ST_Result_Current_FOC stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	//!SH _180809: 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	szText.Format(_T("CH1 : %.2f"), stResult.dbValue[Current_CH1]);

	iLineSize = 4;

	if (TRUE == stResult.GetFinalResult())
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);		//1107 SH2 : rect -> rtRectText 
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Current(__inout IplImage* lpImage, __in ST_UI_Current stOption, __in ST_Result_Current_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	//!SH _180809: 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	szText.Format(_T("CH1 : %.2f"), stResult.dbValue[Current_CH1]);

	iLineSize = 4;

	if (TRUE == stResult.GetFinalResult())
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
//=============================================================================
// Method		: Overlay_DynamicBW
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_DynamicBW stOption
// Parameter	: __in ST_Result_DynamicBW stResult
// Qualifier	:
// Last Update	: 2018/8/8 - 16:07
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_DynamicBW(__inout IplImage* lpImage, __in ST_UI_DynamicBW stOption, __in ST_Result_DynamicBW_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CPoint	ptCenter;
	CRect	rtRectText;
	CRect	rtRectResultText_DR;
	CRect	rtRectResultText_SNR;
	CString szText_DR;
	CString szText_SNR;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_DnyBW_Max; nIdx++)
	{
		if (TRUE == stOption.stInput[nIdx].bEnable)
		{
			

			if (TRUE == m_bTest)
			{
				ptCenter = stResult.ptROI[nIdx];
			}
			else
			{
				ptCenter = stOption.stInput[nIdx].ptROI;
			}

			rtRect.left = ptCenter.x - stOption.iMaxROIWidth / 2;
			rtRect.right = ptCenter.x + stOption.iMaxROIWidth / 2;
			rtRect.top = ptCenter.y - stOption.iMaxROIHeight / 2;
			rtRect.bottom = ptCenter.y + stOption.iMaxROIHeight / 2;

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stInput[nIdx].nOverlayDir)
			{
			case 0:	// OverlayDir_Left
				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2 - 70;
				rtRectText.top = ptCenter.y;
				break;
			case 1: // OverlayDir_Right

				rtRectText.left = ptCenter.x + stOption.iMaxROIWidth / 2 + 10;
				rtRectText.top = ptCenter.y;
				break;
			case 2: // OverlayDir_Top

				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
				rtRectText.top = ptCenter.y - stOption.iMaxROIHeight / 2 - 40;
				break;
			case 3: // OverlayDir_Bottom

				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
				rtRectText.top = ptCenter.y + stOption.iMaxROIHeight / 2 + 40;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("AVG : %.2f"), stResult.dbAverageValue[nIdx]);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("VAR : %.2f"), stResult.dbVarianceValue[nIdx]);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);
// 
			iLineSize = 4;

			rtRectResultText_DR.left = lpImage->width / 2 - 50;
			rtRectResultText_DR.top = lpImage->height * 2 / 3 - 100;

			rtRectResultText_SNR.left = lpImage->width / 2 - 50;
			rtRectResultText_SNR.top = lpImage->height * 2 / 3 - 100;


			szText_DR.Format(_T("DR : %2.3f"), stResult.dbDynamic);
			szText_SNR.Format(_T("SNR : %2.3f"), stResult.dbSNR_BW);
		

			if (TRUE == stResult.bDynamic && TRUE == stResult.bSNR_BW)
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_DR);
				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_SNR);


			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_DR);
				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_SNR);

			}
		}
	}
}

void COverlay_Proc::Overlay_Defect_Black(__inout IplImage* lpImage, __in ST_UI_Defect_Black		stOption, __in ST_Result_Defect_Black_FOC stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	//CPoint	ptPoint;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_BlackCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_BlackCount; nIdx++)
		{
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_BlackCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_BlackResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_Defect_Black(__inout IplImage* lpImage, __in ST_UI_Defect_Black		stOption, __in ST_Result_Defect_Black_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	//CPoint	ptPoint;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_BlackCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_BlackCount; nIdx++)
		{
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_BlackCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_BlackResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Defect_White(__inout IplImage* lpImage, __in ST_UI_Defect_White	stOption, __in ST_Result_Defect_White_FOC stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_WhiteCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_WhiteCount; nIdx++)
		{
			//!SH _180908: Point 버전 필요
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_WhiteCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_WhiteResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_Defect_White(__inout IplImage* lpImage, __in ST_UI_Defect_White	stOption, __in ST_Result_Defect_White_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_WhiteCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_WhiteCount; nIdx++)
		{
			//!SH _180908: Point 버전 필요
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_WhiteCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_WhiteResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
//=============================================================================
// Method		: Overlay_Shading
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Shading stOption
// Qualifier	:
// Last Update	: 2018/8/4 - 17:50
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Shading(__inout IplImage* lpImage, __in ST_UI_Shading stOption, __in ST_Result_Shading_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect		rtRect;
	CPoint		ptCenter;
	CPoint		ptText;
	CString		szText;
	COLORREF	clrLineColor;
	double		dbValue = 0.0;

	for (UINT nTemp = 0; nTemp < 4; nTemp++)
	{
		for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
		{
			int iLineSize = 2;
			int iMarkSize = 10;

			switch (nTemp)
			{
			case 0:	// 
				if (TRUE == m_bTest)
				{
					ptCenter = stResult.ptHorROI[iROI];
				}
				else
				{
					ptCenter = stOption.stInputHor[iROI].ptROI;
				}
				clrLineColor = RGB(255, 228, 0);
				dbValue = stResult.dbHorizonValue[iROI];
				break;
			case 1: // OverlayDir_Right
				if (TRUE == m_bTest)
				{
					ptCenter = stResult.ptVerROI[iROI];
				}
				else
				{
					ptCenter = stOption.stInputVer[iROI].ptROI;
				}
				clrLineColor = RGB(255, 150, 0);
				dbValue = stResult.dbVerticalValue[iROI];
				break;
			case 2: // OverlayDir_Top
				if (TRUE == m_bTest)
				{
					ptCenter = stResult.ptDiaAROI[iROI];
				}
				else
				{
					ptCenter = stOption.stInputDirA[iROI].ptROI;
				}
				clrLineColor = RGB(255, 100, 0);
				dbValue = stResult.dbDiaAValue[iROI];
				break;
			case 3: // OverlayDir_Bottom
				if (TRUE == m_bTest)
				{
					ptCenter = stResult.ptDiaBROI[iROI];
				}
				else
				{
					ptCenter = stOption.stInputDirB[iROI].ptROI;
				}
				clrLineColor = RGB(255, 50, 0);
				dbValue = stResult.dbDiaBValue[iROI];
				break;
			default:
				break;
			}

			if (TRUE == m_bTest)
			{
				if (TRUE == stResult.bHorizonResult[iROI])
				{
					clrLineColor = RGB(0, 84, 255);
				}
				else
				{
					clrLineColor = RGB(255, 0, 0);

				}

			}

			//rtRect.SetRectEmpty();

			rtRect.left		= ptCenter.x - stOption.iMaxROIWidth / 2;
			rtRect.right	= ptCenter.x + stOption.iMaxROIWidth / 2;
			rtRect.top		= ptCenter.y - stOption.iMaxROIHeight / 2;
			rtRect.bottom	= ptCenter.y + stOption.iMaxROIHeight / 2;

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, clrLineColor, iLineSize);

			double dbFontSize = 0.7;

			rtRect.left		+= 3;
			rtRect.top		-= 10;

			szText.Format(_T("[%d](%.3f)"), iROI, dbValue);
			Overlay_Process(lpImage, OvrMode_TXT, rtRect, clrLineColor, iLineSize, dbFontSize, szText);

			//if (TRUE == stResult.bHorizonResult[iROI]) //나중에 주석 풀어야 됨
			//{
			//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			//	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);

			//}
			//else
			//{
			//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			//	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);

			//}

			//	if (TRUE == stResult.bVerticalResult[iROI])
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);

			//	}
			//	else
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);

			//	}

			//	if (TRUE == stResult.bDiaAResult[iROI])
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);

			//	}
			//	else
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);

			//	}

			//	if (TRUE == stResult.bDiaBResult[iROI])
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);

			//	}
			//	else
			//	{
			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);

			//	}
		}

//		DoEvents(10);
	}



}

//=============================================================================
// Method		: Overlay_Process
// Access		: protected  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in enOverlayMode enMode
// Parameter	: __in CRect rtROI
// Parameter	: __in COLORREF clrLineColor
// Parameter	: __in int iLineSize
// Parameter	: __in double dbFontSize
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2018/3/2 - 10:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor /*= RGB(255, 200, 0)*/, __in int iLineSize /*= 1*/, __in double dbFontSize /*= 1.0*/, __in CString szText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = rtROI.left;
	nStartPt.y = rtROI.top;

	CvPoint nEndPt;
	nEndPt.x = rtROI.right;
	nEndPt.y = rtROI.bottom;

	CvPoint nCenterPt;
	nCenterPt.x = nStartPt.x + ((nEndPt.x - nStartPt.x) / 2);
	nCenterPt.y = nStartPt.y + ((nEndPt.y - nStartPt.y) / 2);

	CvSize Ellipse_Axis;
	Ellipse_Axis.width  = (nEndPt.x - nStartPt.x)/2;
	Ellipse_Axis.height = (nEndPt.y - nStartPt.y)/2;

	switch (enMode)
	{
	case OvrMode_LINE:
		cvLine(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_RECTANGLE:
		cvRectangle(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_CIRCLE:
		//cvCircle(lpImage, nStartPt, abs(nEndPt.x - nStartPt.x) / 2, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		cvEllipse(lpImage, nCenterPt, Ellipse_Axis, 0, 0, 360, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_TXT:
	{
		CvFont* cvFont = new CvFont;
		cvInitFont(cvFont, CV_FONT_VECTOR0, dbFontSize, dbFontSize, 3, iLineSize);
		cvPutText(lpImage, CT2A(szText), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor))); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete cvFont; // 해제
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Ymean(__inout IplImage* lpImage, __in ST_UI_Ymean		stOption, __in ST_Result_Ymean_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bYmeanResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Ymean(__inout IplImage* lpImage, __in ST_UI_Ymean		stOption, __in ST_Result_Ymean_Foc stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bYmeanResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_BlackSpot(__inout IplImage* lpImage, __in ST_UI_BlackSpot stOption, __in ST_Result_BlackSpot_Foc stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bBlackSpotResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_BlackSpot(__inout IplImage* lpImage, __in ST_UI_BlackSpot stOption, __in ST_Result_BlackSpot_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bBlackSpotResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_LCB(__inout IplImage* lpImage, __in ST_UI_LCB		stOption, __in ST_Result_LCB_Foc stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bLCBResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_LCB(__inout IplImage* lpImage, __in ST_UI_LCB		stOption, __in ST_Result_LCB_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bLCBResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_RI
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_RI(__inout IplImage* lpImage, __in ST_UI_Rllumination		stOption, __in ST_Result_Rllumination stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;
	CString szTextResult;
	CRect	rtRectTextResult;
	int		iLineSize = 2;
	double	dbFontSize = 1.0;


	for (int nIdx = 0; nIdx <ROI_Rllumination_Max; nIdx++)
	{
		rtRectText.SetRectEmpty();
		rtRect = stResult.ptRIROI[nIdx];
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		switch (nIdx)
		{
		case 0:	
		case 1: 
		case 2: 
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 3: // OverlayDir_Bottom
		case 4:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 40;
			break;
		default:
			break;
		}

		szText.Format(_T("%.3f"), stResult.dRIValue[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectTextResult.SetRectEmpty();
	rtRectTextResult.left = 300;
	rtRectTextResult.top = 360;
	
	szTextResult.Format(_T("RI Corner : %.3f , RI Min : %.3f"), stResult.dRICorner, stResult.dRIMin);

	iLineSize = 4;

	if (TRUE == stResult.dRIResult[0] && TRUE == stResult.dRIResult[1])
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectTextResult, RGB(0, 84, 255), iLineSize, dbFontSize, szTextResult);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectTextResult, RGB(255, 0, 0), iLineSize, dbFontSize, szTextResult);
	}
}

void COverlay_Proc::Overlay_OpticalCenter(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data_Foc stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	CRect	rtRect;

	rtRect = stOption.rtROI;

	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	CPoint	ptCenter;

	ptCenter.x = stData.nPixX;
	ptCenter.y = stData.nPixY;

	if (0 >= ptCenter.x || 0 >= ptCenter.y)
		return;

	rtRect.left = ptCenter.x - iMarkSize;
	rtRect.top = ptCenter.y;
	rtRect.right = ptCenter.x + iMarkSize;
	rtRect.bottom = ptCenter.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	rtRect.left = ptCenter.x;
	rtRect.top = ptCenter.y - iMarkSize;
	rtRect.right = ptCenter.x;
	rtRect.bottom = ptCenter.y + iMarkSize;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	CString szText;
	szText.Format(_T("X : %d Y : %d"), stData.iStandPosDevX, stData.iStandPosDevY);

	rtRect.left = stOption.rtROI.left;
	rtRect.top = stOption.rtROI.bottom + 60;

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}
void COverlay_Proc::Overlay_OpticalCenter(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data_IQ stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	CRect	rtRect;

	rtRect = stOption.rtROI;

	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	CPoint	ptCenter;

	ptCenter.x = stData.nPixX;
	ptCenter.y = stData.nPixY;

	if (0 >= ptCenter.x || 0 >= ptCenter.y)
		return;

	rtRect.left = ptCenter.x - iMarkSize;
	rtRect.top = ptCenter.y;
	rtRect.right = ptCenter.x + iMarkSize;
	rtRect.bottom = ptCenter.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	rtRect.left = ptCenter.x;
	rtRect.top = ptCenter.y - iMarkSize;
	rtRect.right = ptCenter.x;
	rtRect.bottom = ptCenter.y + iMarkSize;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	CString szText;
	szText.Format(_T("X : %d Y : %d"), stData.iStandPosDevX, stData.iStandPosDevY);

	rtRect.left = stOption.rtROI.left;
	rtRect.top = stOption.rtROI.bottom + 60;

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Rotation(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Rotation_Foc stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_Rotation_Max; nIdx++)
	{

		if (TRUE == m_bTest)
		{
			rtRect = stResult.ptROI[nIdx];
		}
		else
		{
			rtRect = stOption.stInput[nIdx].rtROI;
		}

		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		rtRectText.SetRectEmpty();


		switch (nIdx)
		{
		case 0:
		case 1:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 2:
		case 3:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 20;
			break;
		default:
			break;
		}

		szText.Format(_T("%s"), g_szROI_Rotate[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	iLineSize = 4;

	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("%.2f"), stResult.dbRotation);

	if (TRUE == stResult.bRotation)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Rotation(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Rotation_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_Rotation_Max; nIdx++)
	{

		if (TRUE == m_bTest)
		{
			rtRect = stResult.ptROI[nIdx];
		}
		else
		{
			rtRect = stOption.stInput[nIdx].rtROI;
		}

		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		rtRectText.SetRectEmpty();


		switch (nIdx)
		{
		case 0:
		case 1:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 2:
		case 3:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 20;
			break;
		default:
			break;
		}

		szText.Format(_T("%s"), g_szROI_Rotate[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	iLineSize = 4;

	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("%.2f"), stResult.dbRotation);

	if (TRUE == stResult.bRotation)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_FOV(__inout IplImage* lpImage, __in ST_UI_FOV stOption, __in ST_Result_FOV_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_Rotation_Max; nIdx++)
	{

		if (TRUE == m_bTest)
		{
			rtRect = stResult.ptROI[nIdx];
		}
		else
		{
			rtRect = stOption.stInput[nIdx].rtROI;
		}

		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		rtRectText.SetRectEmpty();


		switch (nIdx)
		{
		case 0:
		case 1:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 2:
		case 3:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 20;
			break;
		default:
			break;
		}

		szText.Format(_T("%s"), g_szROI_Rotate[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	iLineSize = 4;

	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("%.2f"), stResult.bDFOV);

	if (TRUE == stResult.bDFOV)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
	szText.Format(_T("%.2f"), stResult.bHFOV);

	if (TRUE == stResult.bHFOV)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
	szText.Format(_T("%.2f"), stResult.bHFOV);

	if (TRUE == stResult.bHFOV)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Distortion(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Distortion_IQ stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_Rotation_Max; nIdx++)
	{

		if (TRUE == m_bTest)
		{
			rtRect = stResult.ptROI[nIdx];
		}
		else
		{
			rtRect = stOption.stInput[nIdx].rtROI;
		}

		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		rtRectText.SetRectEmpty();


		switch (nIdx)
		{
		case 0:
		case 1:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 2:
		case 3:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 20;
			break;
		default:
			break;
		}

		szText.Format(_T("%s"), g_szROI_Rotate[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	iLineSize = 4;

	rtRectText.left = 300;
	rtRectText.top = 360;

	szText.Format(_T("%.2f"), stResult.dbDistortion);

	if (TRUE == stResult.bDistortion)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

// 
// void COverlay_Proc::Overlay_DynamicBW(__inout IplImage* lpImage, __in ST_UI_DynamicBW stOption, __in ST_Result_DynamicBW stResult)
// {
// 	if (NULL == lpImage)
// 		return;
// 
// 	CRect	rtRect;
// 	CPoint	ptCenter;
// 	CRect	rtRectText;
// 	CRect	rtRectResultText_DR;
// 	CRect	rtRectResultText_SNR;
// 	CString szText_DR;
// 	CString szText_SNR;
// 	CString szText;
// 
// 	int iLineSize = 2;
// 	int iMarkSize = 10;
// 	double dbFontSize = 1.0;
// 
// 	for (UINT nIdx = 0; nIdx < ROI_DnyBW_Max; nIdx++)
// 	{
// 		if (TRUE == stOption.stInput[nIdx].bEnable)
// 		{
// 
// 
// 			if (TRUE == m_bTest)
// 			{
// 				ptCenter = stResult.ptROI[nIdx];
// 			}
// 			else
// 			{
// 				ptCenter = stOption.stInput[nIdx].ptROI;
// 			}
// 
// 			rtRect.left = ptCenter.x - stOption.iMaxROIWidth / 2;
// 			rtRect.right = ptCenter.x + stOption.iMaxROIWidth / 2;
// 			rtRect.top = ptCenter.y - stOption.iMaxROIHeight / 2;
// 			rtRect.bottom = ptCenter.y + stOption.iMaxROIHeight / 2;
// 
// 			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
// 
// 			rtRectText.SetRectEmpty();
// 
// 			switch (stOption.stInput[nIdx].nOverlayDir)
// 			{
// 			case 0:	// OverlayDir_Left
// 				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2 - 70;
// 				rtRectText.top = ptCenter.y;
// 				break;
// 			case 1: // OverlayDir_Right
// 
// 				rtRectText.left = ptCenter.x + stOption.iMaxROIWidth / 2 + 10;
// 				rtRectText.top = ptCenter.y;
// 				break;
// 			case 2: // OverlayDir_Top
// 
// 				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
// 				rtRectText.top = ptCenter.y - stOption.iMaxROIHeight / 2 - 40;
// 				break;
// 			case 3: // OverlayDir_Bottom
// 
// 				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
// 				rtRectText.top = ptCenter.y + stOption.iMaxROIHeight / 2 + 40;
// 				break;
// 			default:
// 				break;
// 			}
// 
// 			szText.Format(_T("%d"), nIdx);
// 			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);
// 
// 			rtRectText.top += 30;
// 			szText.Format(_T("AVG : %.2f"), stResult.dbAverageValue[nIdx]);
// 			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);
// 
// 			rtRectText.top += 30;
// 			szText.Format(_T("VAR : %.2f"), stResult.dbVarianceValue[nIdx]);
// 			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);
// 			// 
// 			iLineSize = 4;
// 
// 			rtRectResultText_DR.left = lpImage->width / 2 - 50;
// 			rtRectResultText_DR.top = lpImage->height * 2 / 3 - 100;
// 
// 			rtRectResultText_SNR.left = lpImage->width / 2 - 50;
// 			rtRectResultText_SNR.top = lpImage->height * 2 / 3 - 100;
// 
// 
// 			szText_DR.Format(_T("DR : %2.3f"), stResult.dbDynamic);
// 			szText_SNR.Format(_T("SNR : %2.3f"), stResult.dbSNR_BW);
// 
// 
// 			if (TRUE == stResult.bDynamic && TRUE == stResult.bSNR_BW)
// 			{
// 				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_DR);
// 				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
// 				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_SNR);
// 
// 
// 			}
// 			else
// 			{
// 				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_DR);
// 				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
// 				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_SNR);
// 
// 			}
// 		}
// 	}
// }
// 
// void COverlay_Proc::Overlay_Shading(__inout IplImage* lpImage, __in ST_UI_Shading stOption, __in ST_Result_Shading stResult)
// {
// 	if (NULL == lpImage)
// 		return;
// 
// 	CRect		rtRect;
// 	CPoint		ptCenter;
// 	CPoint		ptText;
// 	CString		szText;
// 	COLORREF	clrLineColor;
// 	double		dbValue = 0.0;
// 
// 	for (UINT nTemp = 0; nTemp < 4; nTemp++)
// 	{
// 		for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
// 		{
// 			int iLineSize = 2;
// 			int iMarkSize = 10;
// 
// 			switch (nTemp)
// 			{
// 			case 0:	// 
// 				if (TRUE == m_bTest)
// 				{
// 					ptCenter = stResult.ptHorROI[iROI];
// 				}
// 				else
// 				{
// 					ptCenter = stOption.stInputHor[iROI].ptROI;
// 				}
// 				clrLineColor = RGB(255, 228, 0);
// 				dbValue = stResult.dbHorizonValue[iROI];
// 				break;
// 			case 1: // OverlayDir_Right
// 				if (TRUE == m_bTest)
// 				{
// 					ptCenter = stResult.ptVerROI[iROI];
// 				}
// 				else
// 				{
// 					ptCenter = stOption.stInputVer[iROI].ptROI;
// 				}
// 				clrLineColor = RGB(255, 150, 0);
// 				dbValue = stResult.dbVerticalValue[iROI];
// 				break;
// 			case 2: // OverlayDir_Top
// 				if (TRUE == m_bTest)
// 				{
// 					ptCenter = stResult.ptDiaAROI[iROI];
// 				}
// 				else
// 				{
// 					ptCenter = stOption.stInputDirA[iROI].ptROI;
// 				}
// 				clrLineColor = RGB(255, 100, 0);
// 				dbValue = stResult.dbDiaAValue[iROI];
// 				break;
// 			case 3: // OverlayDir_Bottom
// 				if (TRUE == m_bTest)
// 				{
// 					ptCenter = stResult.ptDiaBROI[iROI];
// 				}
// 				else
// 				{
// 					ptCenter = stOption.stInputDirB[iROI].ptROI;
// 				}
// 				clrLineColor = RGB(255, 50, 0);
// 				dbValue = stResult.dbDiaBValue[iROI];
// 				break;
// 			default:
// 				break;
// 			}
// 
// 			if (TRUE == m_bTest)
// 			{
// 				if (TRUE == stResult.bHorizonResult[iROI])
// 				{
// 					clrLineColor = RGB(0, 84, 255);
// 				}
// 				else
// 				{
// 					clrLineColor = RGB(255, 0, 0);
// 
// 				}
// 
// 			}
// 
// 			//rtRect.SetRectEmpty();
// 
// 			rtRect.left = ptCenter.x - stOption.iMaxROIWidth / 2;
// 			rtRect.right = ptCenter.x + stOption.iMaxROIWidth / 2;
// 			rtRect.top = ptCenter.y - stOption.iMaxROIHeight / 2;
// 			rtRect.bottom = ptCenter.y + stOption.iMaxROIHeight / 2;
// 
// 			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, clrLineColor, iLineSize);
// 
// 			double dbFontSize = 0.7;
// 
// 			rtRect.left += 3;
// 			rtRect.top -= 10;
// 
// 			szText.Format(_T("[%d](%.3f)"), iROI, dbValue);
// 			Overlay_Process(lpImage, OvrMode_TXT, rtRect, clrLineColor, iLineSize, dbFontSize, szText);
// 
// 			//if (TRUE == stResult.bHorizonResult[iROI]) //나중에 주석 풀어야 됨
// 			//{
// 			//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
// 			//	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
// 
// 			//}
// 			//else
// 			//{
// 			//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
// 			//	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);
// 
// 			//}
// 
// 			//	if (TRUE == stResult.bVerticalResult[iROI])
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
// 
// 			//	}
// 			//	else
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);
// 
// 			//	}
// 
// 			//	if (TRUE == stResult.bDiaAResult[iROI])
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
// 
// 			//	}
// 			//	else
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);
// 
// 			//	}
// 
// 			//	if (TRUE == stResult.bDiaBResult[iROI])
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
// 
// 			//	}
// 			//	else
// 			//	{
// 			//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
// 			//		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);
// 
// 			//	}
// 		}
// 
// 		//		DoEvents(10);
// 	}
// }
void COverlay_Proc::Overlay_LockingScrew(__inout IplImage* lpImage, __in ST_UI_Torque stOption, __in ST_Result_Torque_Foc stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Locking The Screw!!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 310;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}


void COverlay_Proc::Overlay_ReleaseScrew(__inout IplImage* lpImage, __in ST_UI_Torque stOption, __in ST_Result_Torque_Foc stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Release The Screw!!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 310;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}
