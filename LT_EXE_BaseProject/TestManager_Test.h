﻿//*****************************************************************************
// Filename	: 	TestManager_Test.h
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Test_h__
#define TestManager_Test_h__

#pragma once

#include "Def_ResultCode_Cm.h"
#include "TI_CircleDetect.h"

#include "Def_UI_DynamicBW.h"
#include "Def_UI_Shading.h"
#include "Def_UI_Rotation.h"
#include "Def_UI_FOV.h"
#include "Def_T_IR.h"
#include "Def_T_FOC.h"
#include "Def_T_IQ.h"

#include "LGIT_Algorithm.h"

class CTestManager_Test
{
public:
	CTestManager_Test();
	virtual ~CTestManager_Test();

	void		Func_CameraModel(__in UINT nCameraType)
	{
		m_nCameraModel = nCameraType;
	}

	LRESULT		CenterPointFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data_Foc &stTestData, __in int iCenterX = 0, __in int iCenterY = 0, __in UINT nParaID = 0);
	LRESULT		CenterPointFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data_IQ &stTestData, __in int iCenterX = 0, __in int iCenterY = 0, __in UINT nParaID = 0);

	LRESULT		LGIT_Func_Chart(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Chart stOption, __out ST_Result_Chart& stResult);
	LRESULT		LGIT_Func_SFR(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption, __out ST_Result_SFR_IQ& stResult);
	LRESULT		LGIT_Func_SFR(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption, __out ST_Result_SFR_FOC& stResult);
	LRESULT		LGIT_Func_Ymean(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption, __out ST_Result_Ymean_IQ& stResult);
	LRESULT		LGIT_Func_Ymean(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption, __out ST_Result_Ymean_Foc& stResult);
	LRESULT		LGIT_Func_Rotation(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation_IQ& stResult);
	LRESULT		LGIT_Func_Distortion(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Distortion_IQ& stResult);
	LRESULT		LGIT_Func_FOV(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_FOV stOption, __out ST_Result_FOV_IQ& stResult);
	//1106 SH2 수정함 Rotation_IQ 확인필요
	LRESULT		LGIT_Func_Rotation(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation_Foc& stResult);
	LRESULT		LGIT_Func_LCB(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB_IQ& stResult);
	LRESULT		LGIT_Func_LCB(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB_Foc& stResult);
	LRESULT		LGIT_Func_BlackSpot(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot_IQ& stResult);
	LRESULT		LGIT_Func_BlackSpot(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot_Foc& stResult);
	LRESULT		LGIT_Func_DefectBlack(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black_IQ& stResult);
	LRESULT		LGIT_Func_DefectBlack(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black_FOC& stResult);
	LRESULT		LGIT_Func_DefectWhite(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White_IQ& stResult);
	LRESULT		LGIT_Func_DefectWhite(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White_FOC& stResult);
	LRESULT		LGIT_Func_DynamicBW(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption, __out ST_Result_DynamicBW_IQ& stResult);
	LRESULT		LGIT_Func_DynamicBW(__in LPBYTE pImage8bit, __in LPBYTE pImageWhite, __in LPBYTE pImageGray, __in LPBYTE  pImageBlack, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption, __out ST_Result_DynamicBW_IQ& stResult);
	LRESULT		LGIT_Func_Shading(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption, __out ST_Result_Shading_IQ& stResult);
	LRESULT		LGIT_Func_Rllumination(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rllumination stOption, __out ST_Result_Rllumination& stResult);
	bool		Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpec, __in double dbValue);
	bool		Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue);

protected:

	CTI_CircleDetect	m_LT_CircleDetect;
	CLG_LGIT_Algorithm	m_LG_Algorithm;

	void		CamType_CenterPoint(__in int iImgW, __in int iImgH, __out int &nPosX, __out  int &nPosY);

	// 보정 ROI 값 산풀
	LRESULT		LURI_Func_DetectROI_Ver1(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult);
	LRESULT		LURI_Func_DetectROI_White(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult, __in UINT nMarkColor);
	//LRESULT		LURI_Func_DetectROI_Ver2		(__in IplImage* pFrameImage, __in CPoint ptCenter, __in CRect rtOption, __out CRect &rtResult); // SFR 전용
	void		OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR_IQ &stTestData);
	void		OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR_FOC &stTestData);


	double		GetDistance(__in int x1, __in int y1, __in int x2, __in int y2);
	UINT m_nCameraModel;

	// 외부에서 접근 할 필요가 없는 거는 protected 로 선언해서 사용하세요.
	//!SH _180808: 셋팅 Array 부분 포인터로 넘기기에 필요한 배열들 셋팅함.
	void		Set_Shading_ROI_Array(__in ST_UI_Shading & stOption, __out double * pdHorThres, __out double * pdVerThres, __out double * pdDiaThres, __out POINT * ppHorizonPoint, __out POINT * ppVerticalPoint, __out POINT * ppDiaAPoint, __out POINT * ppDiaBPoint);
	void		Set_Dinamic_ROI_Array(__in ST_UI_DynamicBW & stOption, __out POINT * ppDinamicBWPoint);
	void		Set_Rotation_ROI_Array(__in ST_Result_Rotation_Foc & stOption, __out RECT * ppDinamicBWPoint);
	//void		Set_Rotation_ROI_Array			(__in ST_Result_Rotation_IQ & stOption, __out RECT * ppDinamicBWPoint);
};

#endif // TestManager_Test_h__
