﻿//*****************************************************************************
// Filename	: 	Def_Motion.h
// Created	:	2016/10/03 - 16:40
// Modified	:	2016/10/03 - 16:40
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Motion_h__
#define Def_Motion_h__

#include <afxwin.h>

#define IR_MotorX_Crash 168000
#define IR_Chart_Crash	50000

//=============================================================================
// Motor Axis Name
//=============================================================================
// 2D CAL 검사 -------------------------------------
typedef enum enAxis_2D_CAL 
{
	AX_2D_StageX = 0,
	AX_2D_StageY,
	AX_2D_StageZ,
	AX_2D_StageR,
	AX_2D_ChartR,
	AX_2D_Shutter,
	AX_2D_MAX
};

typedef enum enTeach_2D_CAL
{
	TC_2D_Load_X = 0,
	TC_2D_Load_Y,
	TC_2D_Load_Z,
	TC_2D_Load_R,
	TC_2D_ShutterOpen,
	TC_2D_ShutterClose,
	TC_2D_Center_X,
	TC_2D_Center_Y,
	TC_2D_Center_R,
	TC_2D_Distance,	
	TC_2D_MAX,
};

static LPCTSTR g_szTeachItem_2D_CAL[] =
{
	_T("Load X "),
	_T("Load Y "),
	_T("Load Z "),
	_T("Load R "),
	_T("Load Shutter Open "),
	_T("Load Shutter Close "),
	_T("Center X "),
	_T("Center Y "),
	_T("Center R "),
	_T("Distance "),
	NULL
};

// Image 검사 -------------------------------------
typedef enum enAxis_Image
{
	AX_Image_StageX = 0,
	AX_Image_StageY,
	AX_Image_StageZ,
	AX_Image_StageR,
	AX_Image_Shutter,
	AX_Image_MAX
};

typedef enum enTeach_Image
{
	TC_Imgt_Load_X = 0,						// Loding X (Puls)				
	TC_Imgt_Load_Y,							// Loding Y (Puls)				
	TC_Imgt_Load_Z,							// Loding Z (Puls)				
	TC_Imgt_Load_R,							// Loding R (Puls)				
	TC_Imgt_Test_R,							// Test R (Puls)				
	//TC_Imgt_Test_MRA2_R,					// Test R (Puls)				
	TC_Imgt_Test_Front_Z,					// Test Front Z (Puls)			
	//TC_Imgt_Test_MRA2_Z,					// Test MRA2 (Puls)			
	//TC_Imgt_Test_IKC_Z,						// Test IKC Z (Puls)			
	TC_Imgt_Chart_Front_X,					// Chart Front  X (Puls)		
	//TC_Imgt_Chart_MRA2L_X,					// Chart MRA2_L X (Puls)		
	//TC_Imgt_Chart_MRA2R_X,					// Chart MRA2_R X (Puls)		
	//TC_Imgt_Chart_IKC_X,					// Chart IKC X (Puls)			
	TC_Imgt_Par_Front_X,					// Particle Front  X (Puls)	
	//TC_Imgt_Par_MRA2L_X,					// Particle MRA2_L X (Puls)	
	//TC_Imgt_Par_MRA2R_X,					// Particle MRA2_R X (Puls)	
	//TC_Imgt_Par_IKC_X,						// Particle IKC X (Puls)		
	TC_Imgt_Par_Y,							// Particle Axis Y (Puls)		
	TC_Imgt_Distance,						// Distance					
	TC_Imgt_Crash_X,						// Crash_StartX					
	TC_Imgt_Shutter,						// Crash_StartX					
	//TC_Imgt_Load_R_Entry,
	//TC_Imgt_Test_X_Entry,
	//TC_Imgt_Test_Z_Entry,
	//TC_Imgt_Test_R_Entry,
	//TC_Imgt_Par_X_Entry,
	//TC_Imgt_Par_Y_Entry,
	TC_Imgt_MAX,
};

static LPCTSTR g_szTeachItem_Image[] =
{
	_T("Loding X (Puls)		"),			// Loding X (Puls)				
	_T("Loding Y (Puls)		"),			// Loding Y (Puls)				
	_T("Loding Z (Puls)		"),			// Loding Z (Puls)				
	_T("Loding R (Puls)		"),			// Loding R (Puls)				
	_T("TEST R (Puls)		"),			// Test R (Puls)				
	//_T("TEST R (Puls)		"),			// Test R (Puls)				
	_T("TEST Z (Puls)		"),			// Test Front Z (Puls)			
	//_T("TEST Z (Puls)		"),			// Test MRA2 (Puls)			
	//_T("TEST Z (Puls)		"),			// Test IKC Z (Puls)			
	_T("TEST X (Puls)		"),			// Chart Front  X (Puls)		
	//_T("TEST X [L] (Puls)	"),			// Chart MRA2_L X (Puls)		
	//_T("TEST X [R] (Puls)	"),			// Chart MRA2_R X (Puls)		
	//_T("TEST X (Puls)		"),			// Chart IKC X (Puls)			
	_T("PAR X (Puls)		"),			// Particle Front  X (Puls)	
	//_T("PAR X [L] (Puls)	"),			// Particle MRA2_L X (Puls)	
	//_T("PAR X [R] (Puls)	"),			// Particle MRA2_R X (Puls)	
	//_T("PAR X (Puls)		"),			// Particle IKC X (Puls)		
	_T("PAR Y (Puls)		"),			// Particle Axis Y (Puls)		
	_T("Distance			"),			// Distance					
	_T("Crash X (Puls)		"),			// Crash_X	
	_T("Shutter (Puls)		"),			// Crash_X	
	//_T("Loding R (Puls)		"),			// TC_Imgt_Load_R_Entry		
	//_T("TEST X (Puls)		"),			// TC_Imgt_Test_X_Entry,		
	//_T("TEST Z (Puls)		"),			// TC_Imgt_Test_Z_Entry,		
	//_T("TEST R (Puls)		"),			// TC_Imgt_Test_R_Entry,		
	//_T("PAR X (Puls)		"),			// TC_Imgt_Par_X_Entry,		
	//_T("PAR Y (Puls)		"),			// TC_Imgt_Par_X_Entry,		
	NULL
};

// Focus 검사 -------------------------------------
typedef enum enAxis_Focus
{
	AX_Focus_StageX = 0,
	AX_Focus_StageY,
	AX_Focus_StageR,
	AX_Focus_MAX
};

typedef enum enTeach_Focus
{
	TC_Focus_CenterX = 0,
	TC_Focus_CenterY,
	TC_Focus_CenterR,
	TC_Focus_MAX,
};

static LPCTSTR g_szTeachItem_Focus[] =
{
	_T("Center X (Puls)"),
	_T("Center Y (Puls)"),
	_T("Center R (Puls)"),
	NULL
};

#if (SET_INSPECTOR == SYS_2D_CAL)
	#define		TC_ALL_MAX		TC_2D_MAX
#elif (SET_INSPECTOR == SYS_FOCUSING)
	#define		TC_ALL_MAX		TC_Focus_MAX
#elif (SET_INSPECTOR == SYS_IMAGE_TEST)
	#define 	TC_ALL_MAX		TC_Imgt_MAX
#else
	#define		TC_ALL_MAX		TC_2D_MAX
#endif

//------------------------------------------------------
// TEACH 위치 정보
//------------------------------------------------------
typedef struct _tag_TeachInfo
{
	double dbTeachData[TC_ALL_MAX];

	_tag_TeachInfo()
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = 0.0;
	}

	_tag_TeachInfo& operator= (_tag_TeachInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = ref.dbTeachData[nIdx];

		return *this;
	};
}ST_TeachInfo, *LPST_TeachInfo;


#endif // Def_Motion_h__
