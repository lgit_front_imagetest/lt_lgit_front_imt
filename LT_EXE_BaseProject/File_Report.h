﻿//*****************************************************************************
// Filename	: 	File_Report.h
// Created	:	2016/8/5 - 15:50
// Modified	:	2016/8/5 - 15:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Report_h__
#define File_Report_h__

#pragma once

#include "Def_DataStruct.h"

//=============================================================================
// CFile_Report
//=============================================================================

class CFile_Report
{
public:
	CFile_Report();
	~CFile_Report();


protected:
	// 최종 파일 저장
	BOOL	SaveFinalizeResult					(__in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	// 장비별 Log 데이터 생성
	BOOL	Make2DCalHeadAndData				(__in CString szBarcode, __in const ST_2DCal_Result* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	Make2DCalHeadAndData				(__in const ST_CamInfo* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	
public:
	// 2D CAL Log 저장 --------------------------------------------------------
	BOOL	SaveFinalizeResult2DCal				(__in LPCTSTR szPath, __in UINT nParaIdx, __in CString szBarcode, __in const ST_2DCal_Result* pstResult);
	BOOL	SaveFinalizeResult2DCal				(__in LPCTSTR szPath, __in UINT nParaIdx, __in const ST_CamInfo* pstResult);

};

#endif // File_Report_h__

