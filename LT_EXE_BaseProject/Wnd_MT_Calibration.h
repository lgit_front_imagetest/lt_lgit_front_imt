#pragma once

// CWnd_MT_Calibration
#ifndef Wnd_MT_Calibration_h__
#define Wnd_MT_Calibration_h__

#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "Def_MTCtrl_Item.h"

class CWnd_MT_Calibration : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MT_Calibration)

public:
	CWnd_MT_Calibration();
	virtual ~CWnd_MT_Calibration();

	enum
	{
		Title_MTCtrl_Cal_Name,
		Title_MTCtrl_Cal_Send,
		Title_MTCtrl_Cal_Recv,
		Title_MTCtrl_Cal_Value,
		Title_MTCtrl_Cal_Func,
		Title_MTCtrl_Cal_MaxNum,
	};

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	void			OnRangeCmds				(__in UINT nID);
	void			OnRangePort				(__in UINT nID);

	void			GetTextValue			(__in UINT nTestItem, __out CString &szValue);
	void			SetTextSendProtocol		(__in UINT nTestItem, __in CString szValue);
	void			SetTextRecvProtocol		(__in UINT nTestItem, __in CString szValue);
	void			ResetInfo				();
	void			SetButtonEnable			(__in BOOL bEnable);

	CVGStatic		m_st_Name;
	CVGStatic		m_stTitle[Title_MTCtrl_Cal_MaxNum];

	CMFCButton		m_bn_Item[Para_MaxEnum];

	CVGStatic		m_stTestName[MTCtrl_Cal_MaxNum];
	CVGStatic		m_stSendProtol[MTCtrl_Cal_MaxNum];
	CVGStatic		m_stRecvProtol[MTCtrl_Cal_MaxNum];
	CMFCButton		m_btTestFunc[MTCtrl_Cal_MaxNum];
	CMFCMaskedEdit	m_ed_Index[MTCtrl_Cal_MaxNum];

	CFont			m_font;
	UINT			m_nPort;

protected:
	DECLARE_MESSAGE_MAP()

};

#endif // Wnd_MT_Calibration_h__