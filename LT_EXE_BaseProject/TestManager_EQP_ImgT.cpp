﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_ImgT.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_ImgT.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"
#include "CRC16.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP_ImgT::CTestManager_EQP_ImgT()
{
	OnInitialize();

}

CTestManager_EQP_ImgT::~CTestManager_EQP_ImgT()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_ImgT >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_ImgT >>> \n"));	
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_LoadUnload(bLoad);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);

	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	//lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_Test_All()
{
	CTestManager_EQP::StopProcess_Test_All();
	m_bFlag_UserStop = TRUE;
}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);
}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;

	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);

	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);
	TRACE(_T("=-=-= [%d] Start Test operation =-=-=\n"), nParaIdx + 1);
	OnLog(_T("=-=-= [%d] Start Test operation =-=-="), nParaIdx + 1);

	LRESULT lReturn = RC_OK;

	__try
	{
		// 초기화
		//	OnInitial_Test_All(nParaIdx);

		lReturn = OnStart_Test_All(nParaIdx);
	}
	__finally
	{
		//for (int t = 0; t < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; t++)
		{
			// 작업 종료 처리
			if (RC_OK == lReturn)
			{
				OnFinally_Test_All(TRUE, nParaIdx);
				TRACE(_T("=-=-= [%d] Complete the entire test =-=-=\n"), nParaIdx + 1);
				OnLog(_T("=-=-= [%d] Complete the entire test =-=-="), nParaIdx + 1);
			}
			else
			{
				m_stInspInfo.CamInfo[nParaIdx].ResultCode = lReturn;

				OnFinally_Test_All(FALSE, nParaIdx);
				TRACE(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-=\n"), nParaIdx + 1, lReturn);
				OnLog(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-="), nParaIdx + 1), lReturn;

				// 
				// 				// * 검사 결과 판정
				// 				OnSet_TestResult_Unit(TR_Check, nParaIdx);
				// 				OnUpdate_TestReport(nParaIdx);


			}
		}

		if (RC_OK != lReturn)
		{
			// * Unload Product
			lReturn = StartOperation_LoadUnload(Product_Unload);
			if (RC_OK != lReturn)
			{
				// 에러 상황
			}
		}
	}
}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);
}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)
	{
		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::OnInitial_Test_All(nParaIdx);

	m_stInspInfo.nTestPara = nParaIdx;
	//m_bFlag_CameraChange = FALSE;

	OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	//OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	OnSet_TestProgress_Unit(TP_Testing, nParaIdx);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(nParaIdx);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();

}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);
}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	m_bFlag_UserStop = FALSE;

	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}

	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		m_stInspInfo.nTestPara = nCamPara;

		OnSetCamerParaSelect(nCamPara);
		m_Test_ResultDataView.TestResultView_AllReset_ImgT(nCamPara);

		if (m_InspectionType == Sys_Image_Test && nCamPara == Para_Right)
		{
			OnSet_InputTime__Unit(nCamPara);
			OnInitial_Test_All(nCamPara);
	//		OnChangeTheCamera(TRUE, nCamPara); //화면 전환 및 카운트 시작
#ifndef MOTION_NOT_USE			// 1109 SH2 임의모션삭제
			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);
			m_MotionSequence.OnActionLampControl(DO_Fo_09_StartLempR, IO_SignalT_ToggleStart);
#endif
			while (TRUE)
			{
				if (TRUE == m_bFlag_UserStop)
				{
					break;
				}

// 				// 변경 완료 변수
// 				if (TRUE == m_bFlag_CameraChange)
// 				{
// 					OnChangeTheCameraStop();
// 					break;
// 				}
// 
// 				//카운트가 끝난 경우
// 				if (FALSE == OnFoc_ChangeCamFlag() && FALSE == m_bFlag_CameraChange)
// 				{
// 					// 강제 종료 루틴 수행,,
// 					OnChangeTheCameraStop();
// 					break;
// 				}

				Sleep(100);
			}
#ifndef MOTION_NOT_USE			// 1109 SH2 임의모션삭제
			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
			m_MotionSequence.OnActionLampControl(DO_Fo_09_StartLempR, IO_SignalT_ToggleStop);
#endif
//			OnChangeTheCamera(FALSE, nCamPara); //화면 전환 및 카운트 시작
		}

		if (FALSE == m_bFlag_CameraChange && Para_Right == nCamPara)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		}

		if (TRUE == m_bFlag_UserStop)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		}

		for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
		{
			m_stInspInfo.CamInfo[nCamPara].stImageQ.Reset();

			// 검사기에 맞추어 검사 시작
			lReturn = StartTest_Equipment(nCamPara);

			// * 검사 결과 판정
			nResult = Judgment_Inspection(nCamPara);

			if (TRUE == m_bFlag_UserStop)
			{
				nResult = TR_Stop;
				break;
			}

			if ((TR_Pass == nResult))
			{
				break;
			}
		}

		if (TRUE == m_bFlag_UserStop)
		{
			nResult = TR_Stop;
			break;
		}

		OnSet_TestResult_Unit(nResult, nCamPara);

		// * 검사 결과 판정 정보 세팅 및 UI표시
		OnUpdate_TestReport(nCamPara);

		if (TR_Check == lReturn)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		}

		if (TR_Pass != nResult)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		}
	}

	Judgment_Inspection_All();

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		// * MES 검사 결과 보고
		if (MES_Online == m_stInspInfo.MESOnlineMode)
		{
			lReturn = MES_SendInspectionData(nCamPara);

			if (RC_OK != lReturn)
			{
				// 에러 상황
			}
		}
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);
}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);
}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::Load_Product()
{
	LRESULT lReturn = RC_OK;

	//20180326 원복
	//lReturn = CTestManager_EQP::Load_Product();

#ifndef USE_TEST_MODE
	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)
	lReturn = OnDIn_CheckSafetyIO();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Safety Error [%s]"), g_szResultCode[lReturn]);
		return lReturn;
	}

	// * JIG Cover 체크 ()
	lReturn = OnDIn_CheckJIGCoverStatus();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : JIG Cover Opened"));
		return lReturn;
	}

	// * 모터 제어	
	lReturn = OnMotion_LoadProduct();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Error Loading Product"));
		return lReturn;
	}

	// * POGO PIN 컨택 확인,  POGO PIN 사용 횟수 증가
	Increase_ConsumCount();
#endif

	// * 통신 초기화, 전원 On, 광원 On

	// * 최종 정상이 아니면 CHECK 판정 후 배출한다.
	if (RC_OK == lReturn)
	{
		// * 검사 시작 (쓰레드)
		lReturn = StartOperation_Inspection(0);
		if (RC_OK != lReturn) // 에러 상황
		{
			OnAddAlarm_F(_T("===== Can't Start Inspection ====="));
			return lReturn;
		}
	}
	else	// 에러 상황
	{
		// * 검사 판정 NG or CHECK
		m_stInspInfo.CamInfo[Para_Left].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Right].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Left].ResultCode = lReturn;
		m_stInspInfo.CamInfo[Para_Right].ResultCode = lReturn;

		// * 검사 결과 판정 정보 세팅 및 UI표시
		OnSet_TestResult_Unit(TR_Check, Para_Left);
		OnSet_TestResult_Unit(TR_Check, Para_Right);
		OnUpdate_TestReport(Para_Left);
		OnUpdate_TestReport(Para_Right);
		Judgment_Inspection_All();

		AutomaticProcess_LoadUnload(Product_Unload);
		return lReturn;//RC_LoadingFailed
	}


	// * 검사 시작 (쓰레드)
	//	lReturn = StartOperation_Inspection(0);

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_ImageTest(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: StartTest_ImageTest
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 16:18
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_ImageTest(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;

	m_stInspInfo.CamInfo[nParaIdx].szBarcode = m_stInspInfo.szBarcodeBuf;
	m_bFlag_UserStop = FALSE;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// * 기구 이동		
		if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
		{
#ifndef MOTION_NOT_USE
			lReturn = m_MotionSequence.OnActionTesting_ImgT_Y(pStepInfo->StepList[nStepIdx].nMoveY, m_stOption.Inspector.bUseBlindShutter);
#endif
		}

		// * 검사 시작
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
			{
					//Front의 경우 바로 언로딩하게 한다. 1106
					lReturn = StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaIdx);
				
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// * 검사 중단 여부 판단
		if (RC_OK != lReturn)
		{
			// 에러 상황
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_ImgT_Initialize_Test:
				nStepIdx = (INT)iStepCnt - 1;
				bError = TRUE;
				break;
			default:
				break;
			}
		}

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		if (TRUE == m_bFlag_UserStop)
		{
			m_stInspInfo.CamInfo[nParaIdx].nJudgment = TR_Stop;
			bError = TRUE;
			break;
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		

			_TI_Cm_Finalize_Error(Para_Left);
		
		lReturn = TR_Check;

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, Para_Left);
	}

	return lReturn;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
// 	if (TI_2D_Detect_CornerExt == nTestItemID)
// 	{
// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
// 	}
}

//=============================================================================
// Method		: _TI_ImgT_MoveStage_Chart
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:34
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Chart(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ChartTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Chart].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Chart [Para -> %s]"), g_szParaName[nParaIdx]);

	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_MoveStage_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ParticleTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Particle].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Particle Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_ParticleTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_ParticleTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_ImgT_Par(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnMotion_ChartTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_ChartTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_ImgT_Chart(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: StartTest_ImageTest_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 11:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_ImageTest_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// Overlay Draw
	OnSetOverlayInfo_ImgT(nTestItemID, nParaIdx);

	TestResultView_Reset(nTestItemID, nParaIdx);

	switch (nTestItemID)
	{
	case TI_ImgT_CaptureImage:
		lReturn = _TI_Cm_CaptureImage(nStepIdx, nParaIdx, TRUE);
		break;
	case TI_ImgT_Motion_Load:
		lReturn = _TI_ImgT_MoveStage_Load(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Motion_Chart:
		lReturn = _TI_ImgT_MoveStage_Chart(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Motion_Particle:
		lReturn = _TI_ImgT_MoveStage_Particle(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Initialize_Test:
		lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Finalize_Test:
		lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_ECurrent :
		lReturn = _TI_ImgT_Pro_Current(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_OpticalCenter:
		lReturn = _TI_ImgT_Pro_OpticalCenter(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_SFR:
		lReturn = _TI_ImgT_Pro_SFR(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Rotation:
		lReturn = _TI_ImgT_Pro_Rotation(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_BlackSpot:
		lReturn = _TI_ImgT_Pro_BlackSpot(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_LCB:
		lReturn = _TI_ImgT_Pro_LCB(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Ymean:
		lReturn = _TI_ImgT_Pro_Ymean(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Defect_Black:
		lReturn = _TI_ImgT_Pro_DefectBlack(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Defect_White:
		lReturn = _TI_ImgT_Pro_DefectWhite(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_FOV:
		lReturn = _TI_ImgT_Pro_FOV(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Distortion:
		lReturn = _TI_ImgT_Pro_Distortion(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_DynamicBW:
		lReturn = _TI_ImgT_Pro_DynamicBW(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Shading:
		lReturn = _TI_ImgT_Pro_Shading(nStepIdx, nParaIdx);
		break;

//	case TI_ImgT_Re_TorqueCheck:

	case TI_ImgT_Re_ECurrent:
	case TI_ImgT_Re_OpticalCenterX:
	case TI_ImgT_Re_OpticalCenterY:
	case TI_ImgT_Re_SFR:
	case TI_ImgT_Re_Rotation:
	case TI_ImgT_Re_BlackSpot:
	case TI_ImgT_Re_LCB:
	case TI_ImgT_Re_Ymean:
	case TI_ImgT_Re_Defect_Black:
	case TI_ImgT_Re_Defect_White:
	//case TI_ImgT_Re_FOV:
	case TI_ImgT_Re_FOV_Dia:
	case TI_ImgT_Re_FOV_Hor:
	case TI_ImgT_Re_FOV_Ver:
	case TI_ImgT_Re_Distortion:
	case TI_ImgT_Re_DynamicRange:
	case TI_ImgT_Re_Shading:

		lReturn = _TI_ImgT_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;

	default:
		break;
	}

	switch (nTestItemID)
	{
	case TI_ImgT_Fn_ECurrent:
	case TI_ImgT_Fn_OpticalCenter:
	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_Rotation:
	case TI_ImgT_Fn_Ymean:
	case TI_ImgT_Fn_LCB:
	case TI_ImgT_Fn_BlackSpot:
	case TI_ImgT_Fn_Defect_Black:
	case TI_ImgT_Fn_Defect_White:
	case TI_ImgT_Fn_FOV:
// 	case TI_ImgT_Re_FOV_Dia:
// 	case TI_ImgT_Re_FOV_Hor:
// 	case TI_ImgT_Re_FOV_Ver:
	case TI_ImgT_Fn_Distortion:
	case TI_ImgT_Fn_DynamicBW:
	case TI_ImgT_Fn_Shading:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ);
		break;

	default:
		break;
	}

	if (m_stOption.Inspector.bSaveImage_RGB)
	{
		switch (nTestItemID)
		{
		case TI_ImgT_Fn_OpticalCenter:
		case TI_ImgT_Fn_SFR:
		case TI_ImgT_Fn_Rotation:
		case TI_ImgT_Fn_Ymean:
		case TI_ImgT_Fn_LCB:
		case TI_ImgT_Fn_BlackSpot:
		case TI_ImgT_Fn_Defect_Black:
		case TI_ImgT_Fn_Defect_White:
		case TI_ImgT_Fn_FOV:
		case TI_ImgT_Fn_Distortion:
		case TI_ImgT_Fn_DynamicBW:
		case TI_ImgT_Fn_Shading:
			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
			break;
		}
	}
	switch (nTestItemID)
	{
	case TI_ImgT_Fn_ECurrent:
	case TI_ImgT_Fn_OpticalCenter:
	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_Rotation:
	case TI_ImgT_Fn_Ymean:
	case TI_ImgT_Fn_LCB:
	case TI_ImgT_Fn_BlackSpot:
	case TI_ImgT_Fn_Defect_Black:
	case TI_ImgT_Fn_Defect_White:
	case TI_ImgT_Fn_FOV:
	case TI_ImgT_Fn_Distortion:
	case TI_ImgT_Fn_DynamicBW:
	case TI_ImgT_Fn_Shading:
		EachTest_ResultDataSave(nTestItemID, nParaIdx);
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
	LRESULT CTestManager_EQP_ImgT::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
	{
		LRESULT lReturn = RC_OK;

		lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, Para_Left);

		COleVariant varResult;
		varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Initialize_Test].vt);
		varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
		m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

		if (RC_OK == lReturn)
		{
			// * 검사 항목별 결과
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
		}
		else
		{
			// * 에러 상황
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		}
		return lReturn;
	}
//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, Para_Left);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Finalize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:49
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_Finalize_Error(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize_Error(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurVoltage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraRegisterSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nRegisterType
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraAlphaSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nAlpha
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraPowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in enPowerOnOff nOnOff
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Get_ImageBuffer_CaptureImage
// Access		: protected  
// Returns		: BOOL
// Parameter	: enImageMode eImageMode
// Parameter	: __in UINT nParaIdx
// Parameter	: __out UINT & nImgW
// Parameter	: __out UINT & nImgH
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::_TI_Get_ImageBuffer_CaptureImage(enImageMode eImageMode, __in UINT nParaIdx, __out UINT &nImgW, __out UINT &nImgH, __out CString &strTestFileName, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	int iWidth  = 0;
	int iHeight = 0;

	// 이미지 캡쳐 (Request Capture)
	if (ImageMode_LiveCam == eImageMode)
	{
		UINT nPara = Para_Left;

		if (Sys_Image_Test == m_InspectionType)
			nPara = Para_Left;
		else
			nPara = nParaIdx;

		if (!m_Device.DAQ_LVDS.GetSignalStatus(nPara))
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		DWORD dwRawSize = 0;
		LPBYTE lpbyRawImage = m_Device.DAQ_LVDS.GetSourceFrameData(nPara, dwRawSize);
		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nPara);
		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nPara);
		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nPara);

		iWidth = pstVideo->m_dwWidth;
		iHeight = pstVideo->m_dwHeight;
		m_stRawBuf[nParaIdx].AssignMem(dwRawSize);
		// 이미지 버퍼에 복사
		memcpy(m_stRawBuf[nParaIdx].lpbyImage_Raw, lpbyRawImage, dwRawSize);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, iWidth * iHeight * sizeof(WORD));
		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, iWidth * iHeight * 3);
	}
	else
	{
		if (m_stImageMode.szImagePath.IsEmpty())
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}

		IplImage *LoadImage = cvLoadImage((CStringA)m_stImageMode.szImagePath, -1);

		iWidth = LoadImage->width;
		iHeight = LoadImage->height;

		if (iWidth < 1 || iHeight < 1)
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		m_stImageBuf[nParaIdx].AssignMem(iWidth, iHeight);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, LoadImage->imageData, LoadImage->width*LoadImage->height * 2);

		IplImage *LoadImage_8bit = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);

		cv::Mat Bit16Mat(LoadImage->height, LoadImage->width, CV_16UC1, LoadImage->imageData);
		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.2, 0);

		cv::Mat rgb8BitMat(LoadImage_8bit->height, LoadImage_8bit->width, CV_8UC3, LoadImage_8bit->imageData);
		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);

		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, LoadImage_8bit->imageData, LoadImage->width*LoadImage->height * 3);

		cvReleaseImage(&LoadImage);
		cvReleaseImage(&LoadImage_8bit);
	}

	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		if (szFileName)
		{
			CString strDate;
			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

			szImgFileName = (szFileName + strDate);
		}
		else
		{
			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		}

		CString strPath;
		//-바코드
		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s/%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode);
		}
		else{
			strPath.Format(_T("%s/No Barcode"), m_stInspInfo.Path.szImage);
		}
		MakeDirectory(strPath);

		if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
			szPath.Format(_T("%s/Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		else
			szPath.Format(_T("%s/Capatue_%s"), strPath, szImgFileName);


		strTestFileName = szPath;
		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			CString szFullPath;
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szImgFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}

		szPath += _T(".png");
		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			OnSaveImage_Gray16(szPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);
		}
	}

	nImgW = iWidth;
	nImgH = iHeight;

	m_szImageFileName = strTestFileName;

	return TRUE;
}

////=============================================================================
//// Method		: _TI_Get_ImageBuffer_CaptureImage
//// Access		: protected  
//// Returns		: BOOL
//// Parameter	: enImageMode eImageMode
//// Parameter	: __in UINT nParaIdx
//// Parameter	: __out int & iImgW
//// Parameter	: __out int & iImgH
//// Parameter	: __out CString & strTestFileName
//// Parameter	: __in BOOL bSaveImage
//// Parameter	: __in LPCTSTR szFileName
//// Qualifier	:
//// Last Update	: 2018/10/10 - 17:45
//// Desc.		:
////=============================================================================
//BOOL CTestManager_EQP_ImgT::_TI_Get_ImageBuffer_CaptureImage(enImageMode eImageMode, __in UINT nParaIdx, __out int &iImgW, __out int &iImgH, __out CString &strTestFileName, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
//{
//	int iWidth = 0;
//	int iHeight = 0;
//
//	// 이미지 캡쳐 (Request Capture)
//	if (ImageMode_LiveCam == eImageMode)
//	{
//		if (!m_Device.DAQ_LVDS.GetSignalStatus(nParaIdx))
//		{
//			iImgW = 0;
//			iImgH = 0;
//			return FALSE;
//		}
//
//		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
//
//		iWidth = pstVideo->m_dwWidth;
//		iHeight = pstVideo->m_dwHeight;
//
//		// raw 데이터를 가져왔다.
//		DWORD dwRawSize = 0;
//		LPBYTE lpbyRawImage = m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx, dwRawSize);
//		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);
//		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);
//
//		m_stRawBuf[nParaIdx].AssignMem(dwRawSize);
//
//		// 이미지 버퍼에 복사
//		memcpy(m_stRawBuf[nParaIdx].lpbyImage_Raw, lpbyRawImage, dwRawSize);
//		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, iWidth * iHeight * 3);
//		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, iWidth * iHeight * sizeof(WORD));
//	}
//	else
//	{
//		if (m_stImageMode.szImagePath.IsEmpty())
//		{
//			iImgW = 0;
//			iImgH = 0;
//			return FALSE;
//		}
//
//		IplImage *LoadImage = cvLoadImage((CStringA)m_stImageMode.szImagePath, -1);
//
//		iWidth = LoadImage->width;
//		iHeight = LoadImage->height;
//
//		if (iWidth < 1 || iHeight < 1)
//		{
//			iImgW = 0;
//			iImgH = 0;
//			return FALSE;
//		}
//		m_stImageBuf[nParaIdx].AssignMem(iWidth, iHeight);
//		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, LoadImage->imageData, LoadImage->width*LoadImage->height * 2);
//
//		IplImage *LoadImage_8bit = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
//
//
//		cv::Mat Bit16Mat(LoadImage->height, LoadImage->width, CV_16UC1, LoadImage->imageData);
//		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.2, 0);
//
//		cv::Mat rgb8BitMat(LoadImage_8bit->height, LoadImage_8bit->width, CV_8UC3, LoadImage_8bit->imageData);
//		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
//
//
//		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, LoadImage_8bit->imageData, LoadImage->width*LoadImage->height * 3);
//
//		cvReleaseImage(&LoadImage);
//		cvReleaseImage(&LoadImage_8bit);
//	}
//
//	// 이미지 저장
//	if (bSaveImage)
//	{
//		CString szPath;
//		CString szImgFileName;
//
//		SYSTEMTIME tmLocal;
//		GetLocalTime(&tmLocal);
//		if (szFileName)
//		{
//			CString strDate;
//			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
//
//			szImgFileName = (szFileName + strDate);
//		}
//		else
//		{
//			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
//
//		}
//
//
//		CString strPath;
//		//-바코드
//		if (FALSE == m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
//		{
//			strPath.Format(_T("%s/%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[0].szBarcode);
//		}
//		else{
//			strPath.Format(_T("%s/No Barcode"), m_stInspInfo.Path.szImage);
//		}
//		MakeDirectory(strPath);
//
//		if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
//			szPath.Format(_T("%s/Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
//		else
//			szPath.Format(_T("%s/Capatue_%s"), strPath, szImgFileName);
//
//		strTestFileName = szPath;
//
//		CString szTemp;
//
//		if (m_stOption.Inspector.bSaveImage_RAW)
//		{
//			szTemp = szPath + _T(".raw");
//			OnSaveImage_Raw(szTemp.GetBuffer(0), m_stRawBuf[nParaIdx].lpbyImage_Raw, m_stRawBuf[nParaIdx].dwSize);
//		}
//
//		if (m_stOption.Inspector.bSaveImage_Gray12)
//		{
//			szTemp = szPath + _T("_12bit.png");
//			OnSaveImage_Gray16(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);
//
//			szTemp = szPath + _T("_8bit.png");
//			OnSaveImage_Gray8(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);
//		}
//
//		if (m_stOption.Inspector.bSaveImage_RGB)
//		{
//			szTemp = szPath + _T(".bmp");
//			OnSaveImage_RGB(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, iWidth, iHeight);
//		}
//	}
//
//	iImgW = iWidth;
//	iImgH = iHeight;
//
//	m_szImageFileName = strTestFileName;
//	return TRUE;
//
//}


//=============================================================================
// Method		: _TI_ImgT_Pro_StageLoad
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 11:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Load(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
#ifndef MOTION_NOT_USE			// 1109 SH2 임의모션삭제
	lReturn = m_MotionSequence.OnActionLoad();
#endif
	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Load].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}
// 
// //=============================================================================
// // Method		: _TI_Foc_Pro_StageUnLoad
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/3/5 - 11:06
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_Mot_StageUnLoad(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	lReturn = m_MotionSequence.OnActionUnLoad_Foc();
// 
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_StageUnLoad].vt);
// 
// 	if (RC_OK == lReturn)
// 	{
// 		// * 검사 항목별 결과
// 		varResult.SetString(_T("OK"), VT_BSTR);
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
// 	}
// 	else
// 	{
// 		// * 에러 상황
// 		varResult.SetString(_T("NG"), VT_BSTR);
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		OnAddAlarm_F(_T("Motion : Move to Stage UnLoad [Para -> %s]"), g_szParaName[nParaIdx]);
// 	}
// 
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: _TI_Foc_Mot_LockingCheck
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/3/7 - 16:44
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_Mot_LockingCheck(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	if (FALSE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL))
// 	{
// 		lReturn = RC_Rution_FixLocking_Err;
// 	}
// 
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_LockingCheck].vt);
// 
// 	if (RC_OK == lReturn)
// 	{
// 		// * 검사 항목별 결과
// 		varResult.SetString(_T("OK"), VT_BSTR);
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
// 	}
// 	else
// 	{
// 		// * 에러 상황
// 		varResult.SetString(_T("NG"), VT_BSTR);
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 
// 		OnAddAlarm_F(_T("Motion : Check Fix Senser Locking [Para -> %s]"), g_szParaName[nParaIdx]);
// 		Sleep(2000);
// 	}
// 
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
// 
// 	return lReturn;
// }


//=============================================================================
// Method		: _TI_ImgT_Pro_OpticalCenter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("OpticalCenter"))){
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenter.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		return lReturn;
	}

	// 광축
	if (Para_Left == nParaIdx)
	{
		lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stOpticalCenter, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenter);
	}
	else
	{
		lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stOpticalCenter, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenter, m_stInspInfo.CamInfo[Para_Left].stImageQ.stOpticalCenter.iStandPosDevX, m_stInspInfo.CamInfo[Para_Left].stImageQ.stOpticalCenter.iStandPosDevY, nParaIdx);
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_OpticalCenter].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Current(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	float fCurrent[5] = { 0.0, };

	lReturn = OnCameraBrd_Read_Current(nParaIdx, fCurrent);

	for (UINT nCH = 0; nCH < Current_Max_Front; nCH++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.dbValue[nCH] = fCurrent[nCH] * m_stInspInfo.RecipeInfo.stImageQ.stCurrent.dbOffset[nCH];

		// 판정
		BOOL	bMinUse = m_stInspInfo.RecipeInfo.stImageQ.stCurrent.stSpecMin[nCH].bEnable;
		BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stImageQ.stCurrent.stSpecMax[nCH].bEnable;
		double  dbMinDev = m_stInspInfo.RecipeInfo.stImageQ.stCurrent.stSpecMin[nCH].dbValue;
		double  dbMaxDev = m_stInspInfo.RecipeInfo.stImageQ.stCurrent.stSpecMax[nCH].dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.nEachResult[nCH] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.dbValue[nCH]);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.bResult[nCH] &= m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.nEachResult[nCH];
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_ECurrent].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_ImgT_Pro_SFR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_SFR(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// Capture Cnt = 1 이상이면 작업시작
	//-----------------------------------------
	UINT iImageW = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Width;
	UINT iImageH = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Height;

	double dAverageSFR[ROI_SFR_Max] = { 0 };
	UINT nTestCount = m_stInspInfo.RecipeInfo.stImageQ.stSFR.nAverageCount;

	if (nTestCount == 0)
	{
		nTestCount = 1;
	}

	for (UINT nTestNum = 0; nTestNum < nTestCount; nTestNum++)
	{
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("SFR")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
			return lReturn;
		}


		lReturn = m_tm_Test.LGIT_Func_SFR(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stImageQ.stSFR, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR);

		for (int nROINum = 0; nROINum < ROI_SFR_Max; nROINum++)
		{
			dAverageSFR[nROINum] += m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR.dbValue[nROINum];
		}
	}


	for (int nROINum = 0; nROINum < ROI_SFR_Max; nROINum++)
	{
		BOOL	bMinUse = m_stInspInfo.RecipeInfo.stImageQ.stSFR.stInput[nROINum].stSpecMin.bEnable;
		BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stImageQ.stSFR.stInput[nROINum].stSpecMax.bEnable;
		double dbMinDev = m_stInspInfo.RecipeInfo.stImageQ.stSFR.stInput[nROINum].stSpecMin.dbValue;
		double dbMaxDev = m_stInspInfo.RecipeInfo.stImageQ.stSFR.stInput[nROINum].stSpecMax.dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR.dbValue[nROINum] = dAverageSFR[nROINum] / nTestCount;
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR.bResult[nROINum] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR.dbValue[nROINum]);

		if (m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.bResult[nROINum] == FALSE)
		{
			lReturn = RC_Err_TestFail;
		}
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_SFR].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}
//=============================================================================
// Method		: _TI_ImgT_Pro_Rotation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Rotation(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Rotation")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}
		lReturn = m_tm_Test.LGIT_Func_Rotation(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stRotation, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotate);

	//// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Rotation].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Distortion(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Distortion")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	//	!1106 SH2 수정필요
	lReturn = m_tm_Test.LGIT_Func_Distortion(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stDistortion, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortion);

	//// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Distortion].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_FOV(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("FOV")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	//	!1106 SH2 수정필요
	lReturn = m_tm_Test.LGIT_Func_FOV(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stFOV, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFOV);

	//// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_FOV].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_DynamicBW
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/11 - 15:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_DynamicBW(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;


	LPBYTE DynamicImageBuffer[3];
	CString strCaptureCnt;
	for (UINT nMode = 0; nMode <= Light_I_SNR_B_ImgT; nMode++)
	{
		UINT nImgW = 0, nImgH = 0;

		if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode].wStep)){
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			//m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
			lReturn = RC_Light_Brd_Err_Ack;
			return lReturn;
		}

		Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

		strCaptureCnt.Format(_T("DynamicRange_%d"), nMode + 1);
		// 이미지 버퍼에 복사
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, strCaptureCnt))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			//m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
			lReturn = RC_NoImage;
			return lReturn;
		}

		DynamicImageBuffer[nMode] = new BYTE[nImgW * nImgH * 2];

		memcpy(DynamicImageBuffer[nMode], m_stRawBuf[nParaIdx].lpbyImage_Raw, nImgW * nImgH * 2);
	}

	//!SH _181107: 이미지 세장 캡쳐 필요, 광원 값도 줄일 필요 


	lReturn = m_tm_Test.LGIT_Func_DynamicBW(m_stImageBuf[nParaIdx].lpbyImage_8bit, DynamicImageBuffer[Light_I_SNR_W_ImgT], DynamicImageBuffer[Light_I_SNR_G_ImgT], DynamicImageBuffer[Light_I_SNR_B_ImgT], nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stDynamicBW, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicBW);


	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_DynamicBW].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	for (UINT nMode = 0; nMode <= Light_I_SNR_B_ImgT; nMode++)
	{
		if (nullptr != DynamicImageBuffer[nMode])
		{
			delete[](DynamicImageBuffer[nMode]);

			DynamicImageBuffer[nMode] = nullptr;
		}
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Shading
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Shading(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT iImageW = 0;
	UINT iImageH = 0;

	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Shading")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_Shading(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stImageQ.stShading, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShading);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Shading].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	// 	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	// 	UINT nImgW = 0, nImgH = 0;
	// 
	// 	OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep);
	// 
	// 	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
	// 
	// 	// 이미지 버퍼에 복사
	// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, _T("Stain")))
	// 	{
	// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	// 		lReturn = RC_NoImage;
	// 		return lReturn;
	// 	}
	// 
	// 	lReturn = m_tm_Test.ParticleFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stParticleOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stParticleData);
	// 
	// 	COleVariant varResult;
	// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_Stain].vt);
	// 	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	// 	if (RC_OK == lReturn)
	// 	{
	// 		// * 검사 항목별 결과
	// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	// 	}
	// 	else
	// 	{
	// 		// * 에러 상황
	// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	// 	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Ymean(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean_ImgT].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean_ImgT].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_Light_Brd_Err_Ack;
		//return lReturn;
	}

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Ymean")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}
		lReturn = m_tm_Test.LGIT_Func_Ymean(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stYmean, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stYmean);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Ymean].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_BlackSpot(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_BlackSpot_ImgT].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_BlackSpot_ImgT].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_Light_Brd_Err_Ack;
		//return lReturn;
	}

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("BlackSpot")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}
		lReturn = m_tm_Test.LGIT_Func_BlackSpot(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stBlackSpot, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stBlackSpot);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_BlackSpot].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_LCB(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_LCB_ImgT].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_LCB_ImgT].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_Light_Brd_Err_Ack;
		//return lReturn;
	}

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("LCB")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_LCB(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stLCB, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stLCB);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_LCB].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_DefectBlack(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Defect_B_ImgT].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Defect_B_ImgT].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_Light_Brd_Err_Ack;
		//return lReturn;
	}

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Defect_Black")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}
		lReturn = m_tm_Test.LGIT_Func_DefectBlack(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stDefect_Black, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefect_Black);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Defect_Black].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_DefectWhite(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nImageW = 0;
	UINT nImageH = 0;


	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Defect_W_ImgT].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Defect_W_ImgT].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_Light_Brd_Err_Ack;
		//return lReturn;
	}


	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szFileName, TRUE, _T("Defect_White")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}
	//	!1106 SH2 수정필요
//	lReturn = m_tm_Test.LGIT_Func_DefectWhite(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stImageQ.stDefect_White, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefect_White);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Defect_White].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_DefectPixel
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_DefectPixel(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	// 
	// 	stDPList stDefectList;
	// 	CString strCaptureCnt;
	// 
	// 	for (UINT nMode = 0; nMode < UI_Defect_MAX; nMode++)
	// 	{
	// 		UINT nImgW = 0, nImgH = 0;
	// 
	// 		OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].wStep);
	// 
	// 		Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
	// 		strCaptureCnt.Format(_T("DefectPixel_%d"), nMode + 1);
	// 
	// 		// 이미지 버퍼에 복사
	// 		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, strCaptureCnt))
	// 		{
	// 			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	// 			lReturn = RC_NoImage;
	// 			return lReturn;
	// 		}
	// 
	// 		lReturn = m_tm_Test.DefectPixelFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, nMode, stDefectList, m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData);
	// 		if (nMode < (UI_Defect_MAX-1))
	// 		{
	// 			if (m_stOption.Inspector.bSaveImage_RGB)
	// 			{
	// 				m_bPicCaptureMode = TRUE;
	// 				for (int t = 0; t < 100; t++)
	// 				{
	// 					Sleep(50);
	// 					if (m_bPicCaptureMode == FALSE)
	// 					{
	// 						break;
	// 					}
	// 				}
	// 			}
	// 			
	// 		}
	// 		if (RC_OK != lReturn)
	// 			return lReturn;
	// 	}
	// 
	// 	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	// 	{
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx] = 0;
	// 	}
	// 
	// 	UINT nCnt[Spec_Defect_Max] = { 0, };
	// 
	// 	for (INT nIdx = 0; nIdx < stDefectList.DefectPixe_Cnt; nIdx++)
	// 	{
	// 		// 유형별 카운팅
	// 		UINT nItem = stDefectList.DP_List[nIdx].nType;
	// 
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailType[nItem][nCnt[nItem]] = stDefectList.DP_List[nIdx].nType;
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nItem]++;
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].left = stDefectList.DP_List[nIdx].x;
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].top = stDefectList.DP_List[nIdx].y;
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].right = stDefectList.DP_List[nIdx].x + stDefectList.DP_List[nIdx].W;
	// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].bottom = stDefectList.DP_List[nIdx].y + stDefectList.DP_List[nIdx].H;
	// 
	// 		nCnt[nItem]++; //HTH 추가 0305
	// 	}
	// 
	// 	// 양불 판정
	// 	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	// 	{
	// 		if (m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt.stSpec_Min[nIdx].iValue <= (int)m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx] && m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt.stSpec_Max[nIdx].iValue >= (int)m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx])
	// 		{
	// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nIdx] = TR_Pass;
	// 		}
	// 		else
	// 		{
	// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nIdx] = TR_Fail;
	// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nResult = TR_Fail;
	// 		}
	// 	}
	// 	if (m_stOption.Inspector.bSaveImage_RGB)
	// 	{
	// 
	// 		m_bPicCaptureMode = TRUE;
	// 		for (int t = 0; t < 100; t++)
	// 		{
	// 			Sleep(50);
	// 			if (m_bPicCaptureMode == FALSE)
	// 			{
	// 				break;
	// 			}
	// 		}
	// 	}
	// 
	// 	COleVariant varResult;
	// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_DefectPixel].vt);
	// 	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList.GetAt(nTestItem).nResultCount; nIdx++)
	{
		switch (nTestItem)
		{
// 		case TI_Foc_Re_TorqueCheck:
// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.dbValue[nIdx];
// 			VarArray.Add(varResult);
// 			break;
		case TI_ImgT_Re_ECurrent:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stCurrent.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_OpticalCenterX:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevX;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_OpticalCenterY:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevY;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_SFR:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFR.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Rotation:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotate.dbRotation;
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_Ymean:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stYmean.nDefectCount;
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_BlackSpot:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stBlackSpot.nDefectCount;
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_LCB:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stLCB.nDefectCount;
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_Defect_Black:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefect_Black.nDefect_BlackCount;
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_Defect_White:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefect_White.nDefect_WhiteCount;
			VarArray.Add(varResult);
			break;
// 		case TI_ImgT_Re_FOV:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV;									//^^ SH2 1107 FOV 나머지 2개 추가!
// 			VarArray.Add(varResult);
// 			break;
		case TI_ImgT_Re_FOV_Dia:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV;									
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_FOV_Hor:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFOV.dbHFOV;									
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_FOV_Ver:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFOV.dbVFOV;									
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Distortion:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortion.dbDistortion;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_DynamicRange:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicBW.dbDynamic;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Shading:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShading.dbHorizonValue[nIdx];
			VarArray.Add(varResult);
			break;
			//case TI_Foc_Re_X1Tilt_SFR:
			//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFR.dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit];
			//	VarArray.Add(varResult);
			//	break;

			//case TI_Foc_Re_X2Tilt_SFR:
			//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFR.dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit];
			//	VarArray.Add(varResult);
			//	break;

			//case TI_Foc_Re_Y1Tilt_SFR:
			//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFR.dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit];
			//	VarArray.Add(varResult);
			//	break;

			//case TI_Foc_Re_Y2Tilt_SFR:
			//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFR.dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit];
			//	VarArray.Add(varResult);
			//	break;
		default:
			break;
		}
	}

	// 측정값 입력 및 판정
	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}
// 
// //=============================================================================
// // Method		: _TI_Foc_AA_OpticalCenter
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/4/23 - 15:33
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_AA_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 	UINT nImgW = 0, nImgH = 0;
// 
// 	// 이미지 버퍼에 복사
//  	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szFileName, FALSE, _T("AA_OpticalCenter")))
//  	{
//  		CString szText = _T("AA Optical Center Image Empty Err");
//  		OnAddAlarm(szText);
//  		m_Log_ErrLog.LogWrite(szText);
//  
//  		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
//  		return RC_NoImage;
//  	}
//  
//  	// 광축 측정
//  	lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter);
//  
//  	if (RC_OK != lReturn)
//  	{
//  		CString szText = _T("AA Optical Center No Detect Err");
//  		OnAddAlarm(szText);
//  		m_Log_ErrLog.LogWrite(szText);
//  
//  		return lReturn;
//  	}
//  
//  	// 광축 조정
//  	int iTargetX = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevX - m_stInspInfo.RecipeInfo.stFocus.stActiveAlign.iTargetX[nParaIdx];
//  	int iTargetY = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevY - m_stInspInfo.RecipeInfo.stFocus.stActiveAlign.iTargetY[nParaIdx];
//  
//  	lReturn = m_MotionSequence.OnActionTesting_OC_Adj(iTargetX, iTargetY, (double)m_stInspInfo.RecipeInfo.fPixelSize);
//  
//  	if (RC_OK != lReturn)
//  	{
//  		CString szText = _T("AA Optical Center Move Err");
//  		OnAddAlarm(szText);
//  		m_Log_ErrLog.LogWrite(szText);
//  
//  		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
//  		return RC_NoImage;
//  	}
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: _TI_Foc_AA_Rotate
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/4/23 - 15:32
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_AA_Rotate(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 	UINT nImgW = 0, nImgH = 0;
// 
// 	// 이미지 버퍼에 복사
// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szFileName, FALSE, _T("AA_Rotate")))
// 	{
// 		CString szText = _T("AA Rotate Image Empty Err");
// 		OnAddAlarm(szText);
// 		m_Log_ErrLog.LogWrite(szText);
// 
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_NoImage;
// 	}
// 
// 	// 로테이트 측정
// 	lReturn = m_tm_Test.LGIT_Func_Rotation(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stRotation, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotate);
// 
// 	double dbTargetR = m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotate.dbRotation - m_stInspInfo.RecipeInfo.stFocus.stActiveAlign.dbTargetR;
// 
// 	if (RC_OK != lReturn)
// 	{
// 		CString szText = _T("AA Rotate No Detect Err");
// 		OnAddAlarm(szText);
// 		m_Log_ErrLog.LogWrite(szText);
// 
// 		return lReturn;
// 	}
// 
// 	// 로테이트 조정
// 	lReturn = m_MotionSequence.OnActionTesting_Rotate_Adj(dbTargetR, (double)m_stInspInfo.RecipeInfo.fPixelSize);
// 
// 	if (RC_OK != lReturn)
// 	{
// 		CString szText = _T("AA Rotate Move Err");
// 		OnAddAlarm(szText);
// 		m_Log_ErrLog.LogWrite(szText);
// 
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_NoImage;
// 	}
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: _TI_Foc_AA_ScrewLocking
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/3/9 - 15:58
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_AA_ScrewLocking(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
// 
// 	// Loop 중인 Item 선정
// 	SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);
// 
// 	m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);
// 
// 	while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
// 	{
// 		// 스크류 토크 값 확인 필요  & 카운팅 
// 		DoEvents(50);
// 	}
// 
// 	m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: _TI_Foc_AA_Testing
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nStepIdx
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/3/9 - 16:00
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_AA_Data(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	UINT nImageW = 0;
// 	UINT nImageH = 0;
// 
// 	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
// 
// 	// 이미지 버퍼에 복사
// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImageW, nImageH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szFileName, FALSE, _T("AA Data")))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_NoImage;
// 	}
// 
// 	// 광축
// 	m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImageW, nImageH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter);
// 
// 	// 로테이트
// 	m_tm_Test.LGIT_Func_Rotation(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stFocus.stRotation, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotate);
// 
// 	// AA 결과 판정
// 	BOOL bMinUseX = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Min[Spec_OC_X].bEnable;
// 	BOOL bMaxUseX = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Max[Spec_OC_X].bEnable;
// 	
// 	int iMinDevX  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Min[Spec_OC_X].iValue;
// 	int iMaxDevX  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Max[Spec_OC_X].iValue;
// 
// 	BOOL bMinUseY = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Min[Spec_OC_Y].bEnable;
// 	BOOL bMaxUseY = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Max[Spec_OC_Y].bEnable;
// 
// 	int iMinDevY  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Min[Spec_OC_Y].iValue;
// 	int iMaxDevY  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenter.stSpec_Max[Spec_OC_Y].iValue;
// 
// 
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.iOC_X = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevX;
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.iOC_Y = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevY;
// 
// 	// 광축 판정
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultX = m_tm_Test.Get_MeasurmentData(bMinUseX, bMaxUseX, iMinDevX, iMaxDevX, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.iOC_X);
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultY = m_tm_Test.Get_MeasurmentData(bMinUseY, bMaxUseY, iMinDevY, iMaxDevY, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.iOC_Y);
// 
// 	// 로테이트
// 	BOOL bMinUseR = m_stInspInfo.RecipeInfo.stFocus.stRotation.stSpecMin[Spec_Rotation].bEnable;
// 	BOOL bMaxUseR = m_stInspInfo.RecipeInfo.stFocus.stRotation.stSpecMax[Spec_Rotation].bEnable;
// 
// 	double dbMinDevR = m_stInspInfo.RecipeInfo.stFocus.stRotation.stSpecMin[Spec_Rotation].dbValue;
// 	double dbMaxDevR = m_stInspInfo.RecipeInfo.stFocus.stRotation.stSpecMax[Spec_Rotation].dbValue;
// 
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.dbRoatae = m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotate.dbRotation;
// 
// 	// 로테이트 판정
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultR = m_tm_Test.Get_MeasurmentData(bMinUseR, bMaxUseR, dbMinDevR, dbMaxDevR, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.dbRoatae);
// 
// 	// 최종 판정
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResult = m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultX & m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultY & m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResultR;
// 
// 	if (TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenter.nDetectResult || TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotate.bRotation)
// 	{
// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign.nResult = TR_Fail;
// 	}
// 
// 	TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlign, Foc_AA);
// 	
// 	// SFR
// 	m_tm_Test.LGIT_Func_SFR(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, nImageW, nImageH, m_stInspInfo.RecipeInfo.stFocus.stSFR, m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFR);
// 
// 	TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque, Foc_AA_Torque);
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: _TI_Foc_Pro_Torque
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nParaIdx
// // Qualifier	:
// // Last Update	: 2018/7/5 - 10:00
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::_TI_Foc_Pro_Torque(__in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.nResult = TR_Pass;
// 
// 	for (UINT nIdx = 0; nIdx <= Spec_Tor_B; nIdx++) //!SH _180915: 주의
// 	{
// 		BOOL bMinUseX = m_stInspInfo.RecipeInfo.stFocus.stTorque.stSpec_Min[nIdx].bEnable;
// 		BOOL bMaxUseX = m_stInspInfo.RecipeInfo.stFocus.stTorque.stSpec_Max[nIdx].bEnable;
// 
// 		double dbMinDevX = m_stInspInfo.RecipeInfo.stFocus.stTorque.stSpec_Min[nIdx].dbValue;
// 		double dbMaxDevX = m_stInspInfo.RecipeInfo.stFocus.stTorque.stSpec_Max[nIdx].dbValue;
// 
// 		m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.nEachResult[nIdx] = m_tm_Test.Get_MeasurmentData(bMinUseX, bMaxUseX, dbMinDevX, dbMaxDevX, m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.dbValue[nIdx]);
// 
// 		if (TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.nEachResult[nIdx])
// 		{
// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.nResult = TR_Fail;
// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.enStatus[nIdx] = Torque_Init;
// 		}
// 		else
// 		{
// 			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorque.enStatus[nIdx] = Torque_Pass;
// 		}
// 	}
// 
// 	return lReturn;
// }

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_ImgT(nParaIdx);

		// MES 로그 남기기
		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		// MES 로그 남기기
		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);
		
		// MES
		m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &tmLocal);
		
		// 로그
		m_FileTestLog.SaveLOGFile(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &tmLocal);
	}

	Sleep(1000);

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_ImgT
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_MakeInspecData_ImgT(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;
	int iCamCount	 = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	// * 설정된 스텝 진행
	m_stMesData.Reset(nParaIdx);
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		switch (pStepInfo->StepList[nStepIdx].nTestItem)
		{
		case TI_ImgT_Fn_ECurrent:
			m_TestMgr_TestMes.Current_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		case TI_ImgT_Fn_OpticalCenter:
			m_TestMgr_TestMes.OpticalCenter_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		case TI_ImgT_Fn_SFR:
			m_TestMgr_TestMes.SFR_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		case TI_ImgT_Fn_Rotation:
			m_TestMgr_TestMes.Rotate_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_Ymean:
			m_TestMgr_TestMes.Ymean_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_BlackSpot:
			m_TestMgr_TestMes.BlackSpot_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_LCB:
			m_TestMgr_TestMes.LCB_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_Defect_Black:
			m_TestMgr_TestMes.Defect_Black_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_Defect_White:
			m_TestMgr_TestMes.Defect_White_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_FOV:
			m_TestMgr_TestMes.FOV_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;

		case TI_ImgT_Fn_Distortion:
			m_TestMgr_TestMes.Distortion_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		case TI_ImgT_Fn_DynamicBW:
			m_TestMgr_TestMes.DynamicBW_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		case TI_ImgT_Fn_Shading:
			m_TestMgr_TestMes.Shading_DataSave_ImgT(nParaIdx, &m_stMesData);
			break;
		default:
			break;
		}
	}

	m_FileMES.Reset();
	m_FileTestLog.Reset();
	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

#ifdef USE_BARCODE_SCANNER
	m_FileMES.Set_EquipmnetID(m_stInspInfo.CamInfo[0].szBarcode);
#else
	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
#endif
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	m_FileTestLog.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileTestLog.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileTestLog.Set_Barcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);


	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정
	UINT		nHeaderCnt = 0;

	for (int t = 0; t < MesDataIdx_ImgT_MAX; t++)
	{
		if (!m_stMesData.szMesTestData[nParaIdx][t].IsEmpty())
		{
			CString strData;
			CString szBlockData;
			CString szValueData;
			CString szResultData;

			strData = m_stMesData.szMesTestData[nParaIdx][t];
			//- ,를 찾고
			int nNum = 0;
			int nStartNum = 0;
			int nDataNum = 0;

			int nDataCnt = 0;
			while (true)
			{
				nNum = strData.Find(',', nStartNum);
				if (nNum != -1)
				{
					szBlockData = strData.Mid(nStartNum, nNum);


					nDataNum = szBlockData.Find(':', 0);
					szValueData = szBlockData.Mid(0, nDataNum);
					szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());


					m_FileMES.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], szValueData, _ttoi(szResultData));
					m_FileTestLog.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], szValueData, _ttoi(szResultData));
					nHeaderCnt++;

					nStartNum = nNum + 1;
					nDataCnt++;
				}
				else
				{
					break;
				}
			}

			szBlockData = strData.Mid(nStartNum, strData.GetLength());
			nDataNum = szBlockData.Find(':', 0);
			szValueData = szBlockData.Mid(0, nDataNum);
			szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());
			m_FileMES.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], szValueData, _ttoi(szResultData));
			m_FileTestLog.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], szValueData, _ttoi(szResultData));
			nHeaderCnt++;
			nDataCnt++;

			for (UINT k = nDataCnt; k < m_stMesData.nMesDataNum[t]; k++)
			{
				m_FileMES.Add_ItemData(m_stMesData.szDataName[t], _T(""), 1);
				m_FileTestLog.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], _T(""), 1);
				nHeaderCnt++;
			}
		}
		else
		{
			CString strHeader;
			for (UINT k = 0; k < m_stMesData.nMesDataNum[t]; k++)
			{

				m_FileMES.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], _T(""), 1);

				strHeader.Format(_T("%s"), g_szMESHeader_ImgT[nHeaderCnt]);
				if (strHeader == _T("Reserved"))
				{
					m_FileTestLog.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], _T(""), 1);
				}
				else{
					m_FileTestLog.Add_ItemData(g_szMESHeader_ImgT[nHeaderCnt], _T("X"), 1);
				}
				nHeaderCnt++;
			}
		}
	}
	
	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);
}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx); //!SH _181112 : IMGT 는 안쓰는듯
	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);


	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToUnloadPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_SteCal_Test
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_SteCal_Test(__in UINT nParaIdx /*= 0*/)
{
	return RC_OK;
}

//=============================================================================
// Method		: OnResetInfo_Measurment
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/29 - 19:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnResetInfo_Measurment(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnResetInfo_Measurment(nParaIdx);
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
//	CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);

	OnDIn_DetectSignal_ImgT(byBitOffset, bOnOff);
}

 //=============================================================================
// Method		: OnDIn_DetectSignal_ImgT
 // Access		: virtual protected  
 // Returns		: void
 // Parameter	: __in BYTE byBitOffset
 // Parameter	: __in BOOL bOnOff
 // Qualifier	:
// Last Update	: 2018/3/5 - 12:53
 // Desc.		:
 //=============================================================================
void CTestManager_EQP_ImgT::OnDIn_DetectSignal_ImgT(__in BYTE byBitOffset, __in BOOL bOnOff)
 {
	enDI_ImageTest nDI_Signal = (enDI_ImageTest)byBitOffset;
	BOOL bStatus = TRUE;

	switch (nDI_Signal)
	{
	case DI_ImgT_00_MainPower:
		if (FALSE == bOnOff)
		{
			OnDIn_MainPower();
		}
		break;

	case DI_ImgT_01_EMO:
		if (FALSE == bOnOff)
		{
			OnDIn_EMO();
		}
		break;

	case DI_ImgT_02_CoverSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_ImgT_03_AreaSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_ImgT_04_DoorSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_DoorSensor();
		}
		break;

	case DI_ImgT_05_Start:
		if (bOnOff)
		{
			StartOperation_LoadUnload(Product_Load);
		}
		break;
	default:
		break;
	}
 }

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/16 - 14:20
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();

// 	if (TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
// 	{
// 		if (IsTesting())
// 		{
// 			//m_bFlag_ReadyTest = FALSE;
// 
// 			// 모터 정지??
// 			OnMotion_EStop();
// 
// 			OnLog_Err(_T("I/O : Detected Area Sesnor !!"));
// 			OnAddAlarm_F(_T("I/O : Detected Area Sesnor  !!"));
// 
// 			// 검사 중지??
// 			StopProcess_Test_All();
// 		}
// 	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	 lReturn = CTestManager_EQP::OnDIn_DoorSensor();

// 	if (m_stOption.Inspector.bUseDoorOpen_Err)
// 	{
// 		if (IsTesting())
// 		{
// 			//m_bFlag_ReadyTest = FALSE;
// 
// 			// 모터 정지??
// 			OnMotion_EStop();
// 
// 			OnLog_Err(_T("I/O : Detected Area Sesnor !!"));
// 			OnAddAlarm_F(_T("I/O : Detected Area Sesnor  !!"));
// 
// 			// 검사 중지??
// 			StopProcess_Test_All();
// 		}
// 	}



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();
	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();

		switch (m_stInspInfo.RecipeInfo.ModelType)
		{
		case Model_OMS_Front:
			return RC_OK;
			break;
		default:
			break;
		}

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		// * 검사 진행 중 커버 열리면 오류 처리
		if (IsTesting())
		{
			if (!m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
			{
				lReturn = RC_DIO_Err_JIGCorverCheck;

				OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
				OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

				StopProcess_Test_All();

				AfxMessageBox(_T("I/O : Opened JIG Cover!!"), MB_SYSTEMMODAL);
			}
		}
	}
	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Start
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:14
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Stop
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:14
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	//	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_00_MainPower])
	{
		lReturn = RC_DIO_Err_MainPower;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_01_EMO])
	{
		lReturn = RC_DIO_Err_EMO;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_03_AreaSensor] && TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
	{
		lReturn = RC_DIO_Err_AreaSensor;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_04_DoorSensor] && TRUE == m_stOption.Inspector.bUseDoorOpen_Err)
	{
		lReturn = RC_DIO_Err_DoorSensor;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		if (!m_stInspInfo.byDIO_DI[DI_ImgT_02_CoverSensor])
		{
			lReturn = RC_DIO_Err_JIGCorverCheck;
		}
	}
	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_FluorescentLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StartLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StopLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_18_TowerLampRed, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_19_TowerLampYellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_20_TowerLampGreen, enSignalType);
		break;
	case Lamp_All:
		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_18_TowerLampRed + nIdx, enSignalType);

			if (RC_OK != lReturn)
				break;
		}
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_Buzzer
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Front:
		lReturn = OnDAQ_EEPROM_OMS_Front_ImgT(nIdxBrd);
		break;
 	default:
		break;
 	}

	return lReturn;
}
// 
// //=============================================================================
// // Method		: OnDAQ_EEPROM_OMS_Entry_ImgT
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nIdxBrd
// // Qualifier	:
// // Last Update	: 2018/2/13 - 17:17
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_OMS_Entry_ImgT(__in UINT nIdxBrd /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 
// 	return lReturn;
//}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Front_ImgT
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_OMS_Front_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}
// 
// //=============================================================================
// // Method		: OnDAQ_EEPROM_MRA2_FImgT
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nIdxBrd
// // Qualifier	:
// // Last Update	: 2018/3/17 - 10:23
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_MRA2_ImgT(__in UINT nIdxBrd /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 
// 	return lReturn;
// }
// 
// //=============================================================================
// // Method		: OnDAQ_EEPROM_IKC_ImgT
// // Access		: virtual protected  
// // Returns		: LRESULT
// // Parameter	: __in UINT nIdxBrd
// // Qualifier	:
// // Last Update	: 2018/3/17 - 10:23
// // Desc.		:
// //=============================================================================
// LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_IKC_ImgT(__in UINT nIdxBrd /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 
// 	return lReturn;
// }

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);

	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

	StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);

	return lReturn;

// 	LRESULT lReturn = RC_OK;
// 
// 	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
// 	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
// 
// 	// * 기구 이동		
// 	if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
// 	{
// #ifndef MOTION_NOT_USE
// 		lReturn = m_MotionSequence.OnActionTesting_ImgT_Y(pStepInfo->StepList[nStepIdx].nMoveY, m_stOption.Inspector.bUseBlindShutter);
// #endif
// 	}
// 
// 	// * 검사 시작
// 	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
// 	{
// 		if (OpMode_DryRun != m_stInspInfo.OperateMode)
// 		{
// 			StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
// 		}
// 	}
// 
// 	return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();
	
	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();

	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;

	//fileReport.SaveFinalizeResult2DCal(nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_Result);
}

//=============================================================================
// Method		: OnJugdement_And_Report_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_ImgT::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

//=============================================================================
// Method		: DeleteTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::DeleteTimer_UpdateUI()
{
// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();
// 
// #ifndef NO_CHECK_ELAP_TIME
// 	// 현재 시간 체크
// 	m_dwTimeCheck = timeGetTime();
// 
// 	// 개별 채널 검사 시간 체크
// 	for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
// 	{
// 		// 검사 중이면
// 		if (TP_Testing == m_stInspInfo.GetTestProgress(nUnitIdx))
// 		{
// 			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Test(m_dwTimeCheck);
// 
// 			OnUpdate_ElapTime_TestUnit(nUnitIdx);
// 		}
// 	}
// 
// 	if ((TP_Loading <= m_stInspInfo.GetTestStatus()) && (m_stInspInfo.GetTestStatus() <= TP_Unloading))
// 	{
// 		for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
// 		{
// 			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Cycle(m_dwTimeCheck);
// 		}
// 
// 		OnUpdate_ElapTime_Cycle();
// 	}
//
// 
// #endif

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_ImgT::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnSaveWorklist()
{
// 	m_stInspInfo.WorklistInfo.Reset();
// 
// 	ST_CamInfo* pstCam = NULL;
// 
// 	CString szText;
// 	CString szTime;
// 	SYSTEMTIME lcTime;
// 
// 	GetLocalTime(&lcTime);
// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
// 
// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
// 	{
// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
// 			continue;
// 
// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
// 
// 		m_stInspInfo.WorklistInfo.Time = szTime;
// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
// 		m_stInspInfo.WorklistInfo.SWVersion;
// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
// 
// 		szText.Format(_T("%d"), nChIdx + 1);
// 		m_stInspInfo.WorklistInfo.Socket = szText;
// 
// 
// 		m_stInspInfo.WorklistInfo.MakeItemz();
// 
// 		// 파일 저장
// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
// 
// 		// UI 표시
// 		OnInsertWorklist();
// 
// 	}// End of for
 }
// 
// //=============================================================================
// // Method		: SetLoopItem_Info
// // Access		: virtual protected  
// // Returns		: void
// // Parameter	: __in UINT nTestItemID
// // Parameter	: __in enTestLoop enUse
// // Qualifier	:
// // Last Update	: 2018/3/17 - 10:23
// // Desc.		:
// //=============================================================================
// void CTestManager_EQP_ImgT::SetLoopItem_Info(__in UINT nTestItemID, __in enTestLoop enUse)
// {
// 	switch (enUse)
// 	{
// 	case TestLoop_NotUse:
// 		m_bFlag_LoopItem[nTestItemID] = FALSE;
// 		break;
// 	case TestLoop_Use:
// 		m_bFlag_LoopItem[nTestItemID] = TRUE;
// 		break;
// 	default:
// 		break;
// 	}
// }
// 
// //=============================================================================
// // Method		: GetLoopItem_Info
// // Access		: virtual protected  
// // Returns		: BOOL
// // Parameter	: __in UINT nTestItemID
// // Qualifier	:
// // Last Update	: 2018/3/17 - 10:23
// // Desc.		:
// //=============================================================================
// BOOL CTestManager_EQP_ImgT::GetLoopItem_Info(__in UINT nTestItemID)
// {
// 	return m_bFlag_LoopItem[nTestItemID];
// }

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitialize()
{
	CTestManager_EQP::OnInitialize();
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);
}

//=============================================================================
// Method		: TestResultView
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Parameter	: __in LPVOID pParam
// Parameter	:
// Qualifier	:
// Last Update	: 2018/3/9 - 10:04
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::TestResultView(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam)//, __in enFocus_AAView enItem /*= Foc_AA_MaxNum*/)
{
	m_Test_ResultDataView.TestResultView_ImgT(nTestID, nPara, pParam);
}

//=============================================================================
// Method		: TestResultView_Reset
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Qualifier	:
// Last Update	: 2018/3/9 - 9:56
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::TestResultView_Reset(__in UINT nTestID, __in UINT nPara)
{
	m_Test_ResultDataView.TestResultView_Reset_ImgT(nTestID, nPara);

}
void CTestManager_EQP_ImgT::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.ResetCamInfo(nParaIdx);
}

//=============================================================================
// Method		: EachTest_ResultDataSave
// Access		: public  
// Returns		: void
// Parameter	: UINT nTestID
// Parameter	: UINT nPara
// Qualifier	:
// Last Update	: 2018/4/23 - 13:18
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::EachTest_ResultDataSave(UINT nTestID, UINT nPara)
{
	ST_MES_TestItemLog m_MesData;
	CString str;
	SYSTEMTIME systime;

	GetLocalTime(&systime);

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&systime);

	//- Model
	m_MesData.Model = m_stInspInfo.szRecipeName;

	//- SW Ver
	m_MesData.SWVersion = m_Worklist.GetSWVersion(g_szProgramName[m_InspectionType]);

	if (m_stInspInfo.CamInfo[nPara].szBarcode.IsEmpty())
	{
		m_MesData.Barcode = _T("No Barcode");
	}
	else
	{
		m_MesData.Barcode = m_stInspInfo.CamInfo[nPara].szBarcode;
	}

	//- Channel
	str.Format(_T("%d"), nPara);
	m_MesData.Socket = str;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strResult;
	UINT nResult = 0;

	CString strTestName;
	switch (nTestID)
	{
	case TI_ImgT_Fn_ECurrent:
		m_TestMgr_TestMes.Current_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Current");
		break;
	case TI_ImgT_Fn_OpticalCenter:
		m_TestMgr_TestMes.OpticalCenter_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("OpticalCenter");
		break;
	case TI_ImgT_Fn_SFR:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("SFR");
		break;
	case TI_ImgT_Fn_Rotation:
		m_TestMgr_TestMes.Rotate_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Rotation");
		break;

	case TI_ImgT_Fn_Ymean:
		m_TestMgr_TestMes.Ymean_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Ymean");
		break;

	case TI_ImgT_Fn_BlackSpot:
		m_TestMgr_TestMes.BlackSpot_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("BlackSpot");
		break;

	case TI_ImgT_Fn_LCB:
		m_TestMgr_TestMes.LCB_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("LCB");
		break;

	case TI_ImgT_Fn_Defect_Black:
		m_TestMgr_TestMes.Defect_Black_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Defect_Black");
		break;

	case TI_ImgT_Fn_Defect_White:
		m_TestMgr_TestMes.Defect_White_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Defect_White");
		break;
	
	case TI_ImgT_Fn_FOV:
		m_TestMgr_TestMes.FOV_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("FOV");
		break;

	case TI_ImgT_Fn_Distortion:
		m_TestMgr_TestMes.Distortion_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Distortion");
		break;

	case TI_ImgT_Fn_DynamicBW:
		m_TestMgr_TestMes.DynamicBW_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("DynamicBW");
		break;
	case TI_ImgT_Fn_Shading:
		m_TestMgr_TestMes.Shading_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Shading");
		break;
	default:
		break;
	}

	m_MesData.Result.Format(_T("%d"), nResult);

	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &systime, &m_MesData, strTestName);
}
