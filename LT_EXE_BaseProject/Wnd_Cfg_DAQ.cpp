﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_DAQ.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_DAQ.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_DAQ.h"
#include "resource.h"
#include "Def_WindowMessage.h"

static LPCTSTR	g_szDAQ_Static[] =
{
	_T("Board Number"),
	_T("Data Type"),
	_T("Signal Type"),
	_T("MIPI Lane"),
	_T("Sensor Type"),
	_T("Color Array"),
	_T("Width Multiple"),
	_T("Hsync Polarity"),
	_T("PClK Polarity"),
	_T("DVal Use"),
	_T("Clock Select"),
	_T("Clock Set (Hz)"),
	_T("Clock Use"),
	//_T("I2C Enable"),
	_T("Height Multiple"),
	NULL
};


#define IDC_BN_I2C_LOAD		2001
#define IDC_BN_I2C_CLEAR	2002
#define IDC_BN_I2C_LOAD2		2003
#define IDC_BN_I2C_CLEAR2	2004
typedef enum Model_ID
{
	IDC_ED_Cam_Clock_Hz = 1001,
	IDC_ED_Cam_Width_Multiple,
	IDC_CB_Cam_BoardNum,
	IDC_CB_Cam_Data_Type,
	IDC_CB_Cam_Signal_Type,
	IDC_CB_Cam_MIPI_Lane,
	IDC_CB_Cam_Sensor_Type,
	IDC_CB_Cam_Color_Array,
	IDC_CB_Cam_Hsync_Polarity,
	IDC_CB_Cam_Pclk_Polarity,
	IDC_CB_Cam_DVal_Use,
	IDC_CB_Cam_Clock_Select,
	IDC_CB_Cam_Clock_Use,
	IDC_CB_Cam_I2c_Enable,
};

static LPCTSTR lpszBoard[] = 
{
	_T("0"),
	_T("1"),
	_T("2"),
	_T("3"),
	_T("4"),
	_T("5"),
};

static LPCTSTR lpszDataType[] =
{
	_T("8 bit"),
	_T("16 bit"),
	_T("24 bit"),
	_T("32 bit"),
	_T("16 bit YUV"),
};

static LPCTSTR lpszSignalType[] =
{
	_T("Vsync, Hsync, De"),
	_T("BT656"),
	_T("MIPI"),
};

static LPCTSTR lpszMIPILane[] =
{
	_T("1 Lane"),
	_T("2 Lane"),
	_T("Empty"),
	_T("4 Lane"),
};

// Grid_ModelInfo.cpp에도 있음
static LPCTSTR lpszSensorType[] =
{
	_T("YCBCR"),			// enImgSensorType_YCBCR,
	_T("BAYER"),			// enImgSensorType_RGBbayer
	_T("BAYER 10"),			// enImgSensorType_RGBbayer10,
	_T("BAYER 12"),			// enImgSensorType_RGBbayer12,
	_T("RGB888"),			// enImgSensorType_RGB888,
	_T("Monochrome 12bit"),	// enImgSensorType_Monochrome_12bit,
	_T("Monochrome - CCCC"),// enImgSensorType_Monochrome_CCCC
};

static LPCTSTR lpszColorArr[] =
{
	_T("YCbYCr / BGGR"),
	_T("YCrYCb / RGGB"),
	_T("CrYCbY / GBRG"),
	_T("CbYCrY / GRBG"),
};

static LPCTSTR lpszHsyncPol[] =
{
	_T("Normal"),
	_T("Inverse"),
};

static LPCTSTR lpszPclkPol[] =
{
	_T("Rising Edge"),
	_T("Falling Edge"),
};

static LPCTSTR lpszDValUse[] =
{
	_T("Hsync Use"),
	_T("DVAL Use"),
};

static LPCTSTR lpszClockSelect[] =
{
	_T("Fixed Clock"),
	_T("Program Clock"),
};

static LPCTSTR lpszClockOff[] =
{
	_T("Off"),
	_T("On"),
};

// CWnd_Cfg_DAQ
IMPLEMENT_DYNAMIC(CWnd_Cfg_DAQ, CWnd_BaseView)

CWnd_Cfg_DAQ::CWnd_Cfg_DAQ()
{
	VERIFY(m_font_Data.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_DAQ::~CWnd_Cfg_DAQ()
{
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_DAQ, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_I2C_LOAD, OnBnClickedBnI2CLoad)
	ON_BN_CLICKED(IDC_BN_I2C_CLEAR, OnBnClickedBnI2CClear)

	ON_BN_CLICKED(IDC_BN_I2C_LOAD2, OnBnClickedBnI2CLoad2)
	ON_BN_CLICKED(IDC_BN_I2C_CLEAR2, OnBnClickedBnI2CClear2)

END_MESSAGE_MAP()


// CWnd_Cfg_DAQ message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_DAQ::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_DAQ_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Item[nIdx].SetFont_Gdip(_T("Arial"), 14.0F);
		m_st_Item[nIdx].Create(g_szDAQ_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < CBO_DAQ_MAXNUM; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_Cam_BoardNum + nIdex);
		m_cb_Item[nIdex].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; nIdx < EDT_DAQ_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_Cam_Clock_Hz + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
		m_ed_Item[nIdx].SetValidChars(_T("0123456789."));
	}

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	case Sys_2D_Cal:
	case Sys_Image_Test:
	{
					   m_st_I2C_File[FILESET_DAQ_I2CFile].SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
					   m_st_I2C_File[FILESET_DAQ_I2CFile].SetColorStyle(CVGStatic::ColorStyle_Black);
					   m_st_I2C_File[FILESET_DAQ_I2CFile].SetFont_Gdip(_T("Arial"), 14.0F);
					   m_st_I2C_File[FILESET_DAQ_I2CFile].Create(_T("I2C File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

					   m_st_I2C_File_Data[FILESET_DAQ_I2CFile].SetFont_Gdip(_T("Arial"), 14.0F);
					   m_st_I2C_File_Data[FILESET_DAQ_I2CFile].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

					   m_bn_I2C_File_Load[FILESET_DAQ_I2CFile].Create(_T("Load I2C File"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_I2C_LOAD);
					   m_bn_I2C_File_Clear[FILESET_DAQ_I2CFile].Create(_T("Clear I2C File"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_I2C_CLEAR);
	}
		break;

	default:
		break;
	}
	
	InitUI();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iCnt = 0;

	int iMagrin			= 10;
	//int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlWidth	= 0;
	int iCtrlHeight = 38;
	int iCapWidth	= 160;
	int iHalfWidth	= (iWidth - iCateSpacing - iCateSpacing - iMagrin) / 2;
	int iLeftSub  	= 0;
	int iTempWidth  = 0;
	//int iTempTop	= 0;

	iTop = iMagrin;
	iCtrlWidth = iHalfWidth - iCapWidth;

	for (UINT nIdx = 0; nIdx < STI_DAQ_MAXNUM; nIdx++)
	{
		if (nIdx == 7)
		{
			iTop = iMagrin;
			iLeft = iLeftSub + iCtrlWidth + iMagrin;
		}

		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		
		iLeftSub = iLeft + iCapWidth + iCateSpacing;

		switch (nIdx)
		{
		case STI_DAQ_Cam_Clock_Hz:
			m_ed_Item[EDT_DAQ_ClockHz].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;

		case STI_DAQ_Cam_Width_Multiple:
			m_ed_Item[EDT_DAQ_WidthMultiple].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;

		case STI_DAQ_Cam_Height_Multiple:
			m_ed_Item[EDT_DAQ_HeightMultiple].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;

		default:
			m_cb_Item[iCnt].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			break;
		}

		iCnt++;
		iTop += iCtrlHeight + iCateSpacing;
	}

	switch (m_InspectionType)
	{
		case Sys_Focusing:
		case Sys_2D_Cal:
		case Sys_Image_Test:
		{
						   iLeft = iMagrin;
						   iLeftSub = iLeft + iCapWidth + iCateSpacing;
						   iTop += iCtrlHeight + iCateSpacing;
						   iTempWidth = iWidth - iCapWidth - iCateSpacing;
						   m_st_I2C_File[FILESET_DAQ_I2CFile].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
						   m_st_I2C_File_Data[FILESET_DAQ_I2CFile].MoveWindow(iLeftSub, iTop, iTempWidth, iCtrlHeight);

						   iLeftSub = iMagrin + iCapWidth + iCateSpacing;
						   iTop += iCtrlHeight + iCateSpacing;
						   m_bn_I2C_File_Load[FILESET_DAQ_I2CFile].MoveWindow(iLeftSub, iTop, iCapWidth, iCtrlHeight);
						   iLeftSub = cx - iMagrin - iCapWidth;
						   m_bn_I2C_File_Clear[FILESET_DAQ_I2CFile].MoveWindow(iLeftSub, iTop, iCapWidth, iCtrlHeight);
	
		}
		break;

		default:
			break;
	}


}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DAQ::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DAQ::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnBnClickedBnI2CLoad
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/10 - 20:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::OnBnClickedBnI2CLoad()
{
	CString DstFile = _T("");

	CFile file;
	CStdioFile studioFile;
	CFileDialog dlg(TRUE, _T("*.set"), _T("*.*"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT, _T("Files (*.set)|*.set;|All Files (*.*)|*.*|"));

	if (dlg.DoModal() == IDOK)
	{
		m_szI2CFilePath[FILESET_DAQ_I2CFile] = dlg.GetPathName();//	load 할 파일 이름이 있는 경로	

		if (!studioFile.Open(m_szI2CFilePath[FILESET_DAQ_I2CFile], CFile::modeRead))
		{
			AfxMessageBox(_T("I2C File Open Fail!"));
		}
		else
		{
			m_st_I2C_File_Data[FILESET_DAQ_I2CFile].SetText(m_szI2CFilePath[FILESET_DAQ_I2CFile]);
		}
	}
}

//=============================================================================
// Method		: OnBnClickedBnI2CClear
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/10 - 20:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::OnBnClickedBnI2CClear()
{
	m_szI2CFilePath[FILESET_DAQ_I2CFile].Empty();
	m_st_I2C_File_Data[FILESET_DAQ_I2CFile].SetText(_T(""));
}

void CWnd_Cfg_DAQ::OnBnClickedBnI2CLoad2()
{
	CString DstFile = _T("");

	CFile file;
	CStdioFile studioFile;
	CFileDialog dlg(TRUE, _T("*.set"), _T("*.*"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT, _T("Files (*.set)|*.set;|All Files (*.*)|*.*|"));

	if (dlg.DoModal() == IDOK)
	{
		m_szI2CFilePath[FILESET_DAQ_I2CFile_2] = dlg.GetPathName();//	load 할 파일 이름이 있는 경로	

		if (!studioFile.Open(m_szI2CFilePath[FILESET_DAQ_I2CFile_2], CFile::modeRead))
		{
			AfxMessageBox(_T("I2C File Open Fail!"));
		}
		else
		{
			m_st_I2C_File_Data[FILESET_DAQ_I2CFile_2].SetText(m_szI2CFilePath[FILESET_DAQ_I2CFile_2]);
		}
	}
}

//=============================================================================
// Method		: OnBnClickedBnI2CClear
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/10 - 20:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::OnBnClickedBnI2CClear2()
{
	m_szI2CFilePath[FILESET_DAQ_I2CFile_2].Empty();
	m_st_I2C_File_Data[FILESET_DAQ_I2CFile_2].SetText(_T(""));
}
//=============================================================================
// Method		: InitUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::InitUI()
{
	m_ed_Item[EDT_DAQ_ClockHz].SetWindowTextW(_T("0"));
	m_ed_Item[EDT_DAQ_WidthMultiple].SetWindowTextW(_T("1.0"));
	m_ed_Item[EDT_DAQ_HeightMultiple].SetWindowTextW(_T("1.0"));

	for (int i = 0; i < DAQBoardNumber_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_BoardNumber].AddString(lpszBoard[i]);
	}
	m_cb_Item[CBO_DAQ_BoardNumber].SetCurSel(0);

	for (int i = 0; i < DAQ_Data_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_DataType].AddString(lpszDataType[i]);
	}
	m_cb_Item[CBO_DAQ_BoardNumber].SetCurSel(DAQ_Data_16bit_Mode);

	for (int i = 0; i < DAQ_Video_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_SignalType].AddString(lpszSignalType[i]);
	}
	m_cb_Item[CBO_DAQ_SignalType].SetCurSel(DAQ_Video_Signal_Mode);

	for (int i = 0; i < DAQ_HsyncPol_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_HsyncPolarity].AddString(lpszHsyncPol[i]);
	}
	m_cb_Item[CBO_DAQ_HsyncPolarity].SetCurSel(DAQ_HsyncPol_Inverse);

	for (int i = 0; i < DAQ_PclkPol_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_PClkPolarity].AddString(lpszPclkPol[i]);
	}
	m_cb_Item[CBO_DAQ_PClkPolarity].SetCurSel(DAQ_PclkPol_RisingEdge);

	for (int i = 0; i < DAQ_DeUse_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_DValUse].AddString(lpszDValUse[i]);
	}
	m_cb_Item[CBO_DAQ_DValUse].SetCurSel(DAQ_DeUse_HSync_Use);

	for (int i = 0; i < DAQMIPILane_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_MIPILane].AddString(lpszMIPILane[i]);
	}
	m_cb_Item[CBO_DAQ_MIPILane].SetCurSel(DAQMIPILane_1);

	for (int i = 0; i < enImgSensorType_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_SensorType].AddString(lpszSensorType[i]);
	}
	m_cb_Item[CBO_DAQ_SensorType].SetCurSel(enImgSensorType_RGBbayer);

	for (int i = 0; i < DAQClockSelect_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_ClockSelect].AddString(lpszClockSelect[i]);
	}
	m_cb_Item[CBO_DAQ_ClockSelect].SetCurSel(DAQClockSelect_FixedClock);

	for (int i = 0; i < DAQClockUse_Cnt; i++)
	{
		m_cb_Item[CBO_DAQ_ClockUse].AddString(lpszClockOff[i]);
	}
	m_cb_Item[CBO_DAQ_ClockUse].SetCurSel(DAQClockUse_Off);

	for (int i = 0; i < Conv_Max; i++)
	{
		m_cb_Item[CBO_DAQ_ColorArray].AddString(lpszColorArr[i]);
	}
	m_cb_Item[CBO_DAQ_ColorArray].SetCurSel(Conv_YCbYCr_BGGR);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_LVDSInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::GetUIData(__out ST_LVDSInfo& stLVDSInfo)
{
	CString strData;
	UINT nData;
	
	nData = m_cb_Item[CBO_DAQ_BoardNumber].GetCurSel();
	stLVDSInfo.nBoardNo[0] = nData;

	nData = m_cb_Item[CBO_DAQ_SensorType].GetCurSel();
	stLVDSInfo.stLVDSOption.nSensorType = (enImgSensorType)nData;

	nData = m_cb_Item[CBO_DAQ_ColorArray].GetCurSel();
	stLVDSInfo.stLVDSOption.nConvFormat = (enConvFormat)nData;

	nData = m_cb_Item[CBO_DAQ_ClockSelect].GetCurSel();
	stLVDSInfo.stLVDSOption.nClockSelect = (enDAQClockSelect)nData;

	nData = m_cb_Item[CBO_DAQ_ClockUse].GetCurSel();
	stLVDSInfo.stLVDSOption.nClockUse = (enDAQClockUse)nData;

	nData = m_cb_Item[CBO_DAQ_DataType].GetCurSel();
	stLVDSInfo.stLVDSOption.nDataMode = (enDAQDataMode)nData;

	nData = m_cb_Item[CBO_DAQ_DValUse].GetCurSel();
	stLVDSInfo.stLVDSOption.nDvalUse = (enDAQDvalUse)nData;

	nData = m_cb_Item[CBO_DAQ_HsyncPolarity].GetCurSel();
	stLVDSInfo.stLVDSOption.nHsyncPolarity = (enDAQHsyncPolarity)nData;

	nData = m_cb_Item[CBO_DAQ_MIPILane].GetCurSel();
	stLVDSInfo.stLVDSOption.nMIPILane = (enDAQMIPILane)nData;

	nData = m_cb_Item[CBO_DAQ_PClkPolarity].GetCurSel();
	stLVDSInfo.stLVDSOption.nPClockPolarity = (enDAQPclkPolarity)nData;

	nData = m_cb_Item[CBO_DAQ_SignalType].GetCurSel();
	stLVDSInfo.stLVDSOption.nVideoMode = (enDAQVideoMode)nData;

	m_ed_Item[EDT_DAQ_ClockHz].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dwClock = (DWORD)_ttol(strData);

	m_ed_Item[EDT_DAQ_WidthMultiple].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dWidthMultiple = _ttof(strData);

	m_ed_Item[EDT_DAQ_HeightMultiple].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dHeightMultiple = _ttof(strData);

	// m_szI2CFilePath

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	case Sys_2D_Cal:
	case Sys_Image_Test:
	{

					   stLVDSInfo.strI2CFileName = m_szI2CFilePath[FILESET_DAQ_I2CFile];
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LVDSInfo * pModelInfo
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::SetUIData(__in const ST_LVDSInfo* pstLVDSInfo)
{
	if (pstLVDSInfo == NULL)
		return;

	//ui
	CString strData;
	UINT nData;

	nData = pstLVDSInfo->nBoardNo[0];
	m_cb_Item[CBO_DAQ_BoardNumber].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nSensorType;
	m_cb_Item[CBO_DAQ_SensorType].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nConvFormat;
	m_cb_Item[CBO_DAQ_ColorArray].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nClockSelect;
	m_cb_Item[CBO_DAQ_ClockSelect].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nClockUse;
	m_cb_Item[CBO_DAQ_ClockUse].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nDataMode;
	m_cb_Item[CBO_DAQ_DataType].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nDvalUse;
	m_cb_Item[CBO_DAQ_DValUse].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nHsyncPolarity;
	m_cb_Item[CBO_DAQ_HsyncPolarity].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nMIPILane;
	m_cb_Item[CBO_DAQ_MIPILane].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nPClockPolarity;
	m_cb_Item[CBO_DAQ_PClkPolarity].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nVideoMode;
	m_cb_Item[CBO_DAQ_SignalType].SetCurSel(nData);

	strData.Format(_T("%d"), pstLVDSInfo->stLVDSOption.dwClock);
	m_ed_Item[EDT_DAQ_ClockHz].SetWindowTextW(strData);

	strData.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dWidthMultiple);
	m_ed_Item[EDT_DAQ_WidthMultiple].SetWindowTextW(strData);

	strData.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dHeightMultiple);
	m_ed_Item[EDT_DAQ_HeightMultiple].SetWindowTextW(strData);

	// m_szI2CFilePath

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	case Sys_2D_Cal:
	case Sys_Image_Test:
	{

					   m_szI2CFilePath[FILESET_DAQ_I2CFile] = pstLVDSInfo->strI2CFileName;
					   m_st_I2C_File_Data[FILESET_DAQ_I2CFile].SetText(m_szI2CFilePath[FILESET_DAQ_I2CFile]);
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Enable_Setting
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bEnable
// Qualifier	:
// Last Update	: 2018/2/8 - 17:09
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::Enable_Setting(__in BOOL bEnable)
{
	for (UINT nIdx = 0; nIdx < CBO_DAQ_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].EnableWindow(bEnable);
	}

	for (UINT nIdx = 0; nIdx < EDT_DAQ_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].EnableWindow(bEnable);
	}
}

//=============================================================================
// Method		: SetUIData_ByModel
// Access		: public  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Parameter	: __in const ST_LVDSInfo * pstLVDSInfo
// Qualifier	:
// Last Update	: 2018/2/9 - 9:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::SetUIData_ByModel(__in enModelType nModelType, __in ST_LVDSInfo* pstLVDSInfo)
{
	switch (nModelType)
	{
	case Model_OMS_Front:
	{
		// OMS Front 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
		pstLVDSInfo->stLVDSOption.dWidthMultiple	= 1.0;
		pstLVDSInfo->stLVDSOption.dHeightMultiple	= 0.5;
		pstLVDSInfo->stLVDSOption.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		pstLVDSInfo->stLVDSOption.nHsyncPolarity	= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		pstLVDSInfo->stLVDSOption.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		pstLVDSInfo->stLVDSOption.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
		pstLVDSInfo->stLVDSOption.nVideoMode		= enDAQVideoMode::DAQ_Video_MIPI_Mode;
		pstLVDSInfo->stLVDSOption.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
		pstLVDSInfo->stLVDSOption.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_12bit;
		pstLVDSInfo->stLVDSOption.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		pstLVDSInfo->stLVDSOption.nClockSelect		= DAQClockSelect_FixedClock;
		pstLVDSInfo->stLVDSOption.nClockUse			= DAQClockUse_Off;
		pstLVDSInfo->stLVDSOption.dwClock			= 1000000;
		pstLVDSInfo->stLVDSOption.byBitPerPixels	= 12;
		pstLVDSInfo->stLVDSOption.nCropFrame		= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
		pstLVDSInfo->stLVDSOption.dShiftExp			= 0.0f;
		pstLVDSInfo->stLVDSOption.SetExposure(0.0f);
	}
		break;

	default:
		break;
	}

	SetUIData(pstLVDSInfo);
}

//=============================================================================
// Method		: SetUIData_ByModel
// Access		: public  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Qualifier	:
// Last Update	: 2018/9/28 - 11:10
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::SetUIData_ByModel(__in enModelType nModelType)
{
	ST_LVDSInfo stLVDSInfo;

	// m_szI2CFilePath

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	case Sys_2D_Cal:
	case Sys_Image_Test:
	{

					  stLVDSInfo.strI2CFileName = m_szI2CFilePath[FILESET_DAQ_I2CFile];
	}
		break;
	default:
		break;
	}

	SetUIData_ByModel(nModelType, &stLVDSInfo);
}

