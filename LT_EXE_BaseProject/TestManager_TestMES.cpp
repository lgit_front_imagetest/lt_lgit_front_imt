﻿#include <afxwin.h>
#include "stdafx.h"
#include "TestManager_TestMES.h"

CTestManager_TestMES::CTestManager_TestMES()
{
}

CTestManager_TestMES::~CTestManager_TestMES()
{
}

//=============================================================================
// Method		: Current_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:13
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Current_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stCurrent.dbValue[Current_CH1], m_pInspInfo->CamInfo[nParaIdx].stFocus.stCurrent.nEachResult[Current_CH1]);
	strFullData += strData;


	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_ECurrent] = strFullData;
}

//=============================================================================
// Method		: Current_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:13
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Current_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stCurrent.dbValue[Current_CH1], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stCurrent.nEachResult[Current_CH1]);
	strFullData += strData;


	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_ECurrent] = strFullData;
}

//=============================================================================
// Method		: OpticalCenter_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::OpticalCenter_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultPosX, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultPosY, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultY);
	strFullData += strData;

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevX, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevY, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultY);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_OpticalCenter] = strFullData;
}

//=============================================================================
// Method		: OpticalCenter_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::OpticalCenter_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultPosX, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultPosY, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultY);
	strFullData += strData;

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevX, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevY, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultY);
 	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_OpticalCenter] = strFullData;
}
//=============================================================================
// Method		: SFR_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SFR_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;
	CString strData;
	CString strFullData;

	strFullData.Empty();
	 
 	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
 	{
		if (m_pInspInfo->RecipeInfo.stFocus.stSFR.stInput[nROI].bEnable)
 		{
 			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFR.dbValue[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFR.bResult[nROI]);
 		}
 		else
 		{
 			strData.Format(_T("X:1"));
 		}
 
 		if (nROI < (ROI_SFR_Max - 1))
 		{
 			strData += _T(",");
 		}
 
 		strFullData += strData;
 	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_SFR] = strFullData;
}
//=============================================================================
// Method		: SFR_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SFR_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (m_pInspInfo->RecipeInfo.stImageQ.stSFR.stInput[nROI].bEnable)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFR.dbValue[nROI], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFR.bResult[nROI]);
		}
		else
		{
			strData.Format(_T("X:1"));
		}

		if (nROI < (ROI_SFR_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_SFR] = strFullData;
}
//=============================================================================
// Method		: Rotate_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Rotate_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.dbRotation, m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.bRotation);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Rotation] = strFullData;
}
//=============================================================================
// Method		: Rotate_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Rotate_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotate.dbRotation, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotate.bRotation);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Rotation] = strFullData;
}

//=============================================================================
// Method		: Stain_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Stain_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

// 	for (UINT nROI = 0; nROI < ROI_Particle_Max; nROI++)
// 	{
// 		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nFailTypeCount[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nResult);
// 		strData += _T(",");
// 		strFullData += strData;
// 
// 		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.dFailTypeConcentration[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nResult);
// 		
// 		if (nROI < (ROI_Particle_Max - 1))
// 		{
// 			strData += _T(",");
// 		}
// 
// 		strFullData += strData;
// 	}

	//pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Stain] = strFullData;
}

//=============================================================================
// Method		: Stain_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Stain_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();
}
//=============================================================================
// Method		: Defectpixel_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Defectpixel_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

// 	for (UINT nItem = 0; nItem < Spec_Defect_Max; nItem++)
// 	{
// 		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nItem]);
// 
// 		if (nItem < (Spec_Defect_Max-1))
// 		{
// 			strData += _T(",");
// 		}
// 
// 		strFullData += strData;
// 	}

	//pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_DefectPixel] = strFullData;
}
//=============================================================================
// Method		: Defectpixel_DataSave_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_ImgT * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Defectpixel_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

}


void CTestManager_TestMES::FOV_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bDFOV);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbHFOV, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bHFOV);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbVFOV, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bVFOV);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_FOV] = strFullData;			//^^ SH2 1107 Rotation -> FOV 로 교체해야함
}
void CTestManager_TestMES::Distortion_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortion.dbDistortion, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortion.bDistortion);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Distortion] = strFullData;
}

void CTestManager_TestMES::DynamicBW_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbDynamic, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bDynamic);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbSNR_BW, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bSNR_BW);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_DynamicBW] = strFullData;

}
void CTestManager_TestMES::Shading_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{

// 	CString strData;
// 	CString strFullData;
// 
// 	strFullData.Empty();
// 
// 	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbDynamic, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bDynamic);
// 	strData += _T(",");
// 	strFullData += strData;
// 
// 	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbSNR_BW, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bSNR_BW);
// 	strFullData += strData;
// 
// 	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Rotation] = strFullData;

}
//=============================================================================
// Method		: Torque_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/5/9 - 17:52
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Torque_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

 	for (UINT nItem = 0; nItem < Spec_Tor_Max; nItem++)
 	{
		if (nItem <= Spec_Tor_B)
 		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorque.dbValue[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorque.nEachResult[nItem]);
 		}
 		else
 		{
			strData.Format(_T("%.f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorque.dbValue[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorque.nEachResult[nItem]);
 		}
 
 		if (nItem < (Spec_Tor_Max - 1))
 		{
 			strData += _T(",");
 		}
 
 		strFullData += strData;
 	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Torque] = strFullData;
}


//=============================================================================
// Method		: Current_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:01
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{

	//-광축 X, Y
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
		nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stCurrent.bResult[0];

		for (int t = 0; t < MESDataNum_Foc_ECurrent; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_CurrentFront[t]);

		}

		for (int t = 0; t <MESDataNum_Foc_ECurrent; t++)
		{
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stCurrent.dbValue[t]);
			pAddData->Add(strData);
		}
	}
		break;
		case Sys_Image_Test:
	{
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stCurrent.bResult[0];

		for (int t = 0; t < MESDataNum_ImgT_ECurrent; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_CurrentFront[t]);

		}

		for (int t = 0; t <MESDataNum_ImgT_ECurrent; t++)
		{
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stCurrent.dbValue[t]);
			pAddData->Add(strData);
		}
	}
		break;
	default:
		break;
	}


}

//=============================================================================
// Method		: OpticalCenter_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:01
// Desc.		:
//=============================================================================
void CTestManager_TestMES::OpticalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	//-광축 X, Y
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
		nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResult;
		for (int t = 0; t < MESDataNum_Foc_OpticalCenter; t++)
		{
			 pAddHeaderz->Add(g_szMESHeader_OC[t]);
		}

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultPosX);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.nResultPosY);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevX);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenter.iStandPosDevY);
		pAddData->Add(strData);
	}
		break;
		case Sys_Image_Test:
	{
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResult;
		for (int t = 0; t < MESDataNum_ImgT_OpticalCenter; t++)
		{
			 pAddHeaderz->Add(g_szMESHeader_OC[t]);
		}

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultPosX);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.nResultPosY);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevX);
		pAddData->Add(strData);

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenter.iStandPosDevY);
		pAddData->Add(strData);
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SFR_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:01
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
		 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFR.GetFinalResult();

		 for (int t = 0; t < MESDataNum_Foc_SFR; t++)
		 {
			 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
		 }

		 for (int t = 0; t < ROI_SFR_Max; t++)
		 {
			 if (m_pInspInfo->RecipeInfo.stFocus.stSFR.stInput[t].bEnable)
			 {
				 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFR.dbValue[t]);
			 }
			 else
			 {
				 strData.Format(_T("X"));
			 }

			 pAddData->Add(strData);
		 }
	}
		break;
		case Sys_Image_Test:
	{
		 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFR.GetFinalResult();

		 for (int t = 0; t < MESDataNum_ImgT_SFR; t++)
		 {
			 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
		 }

		 for (int t = 0; t < ROI_SFR_Max; t++)
		 {
			 if (m_pInspInfo->RecipeInfo.stImageQ.stSFR.stInput[t].bEnable)
			 {
				 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFR.dbValue[t]);
			 }
			 else
			 {
				 strData.Format(_T("X"));
			 }

			 pAddData->Add(strData);
		 }
	}
		break;
	
	default:
		break;
	}
}

//=============================================================================
// Method		: Rotate_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:02
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Rotate_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
 	switch (nSysType)
 	{
 	case Sys_2D_Cal:
 		break;
 	case Sys_Focusing:
 	{
 		 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.bRotation;
 		 for (int t = 0; t < MESDataNum_Foc_Rotation; t++)
 		 {
 			 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
 		 }
 
		 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.dbRotation);
 		 pAddData->Add(strData);
 						
 	}
 		break;
	case Sys_Image_Test:
 	{
 		 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotate.bRotation;
 		 for (int t = 0; t < MESDataNum_ImgT_Rotation; t++)
 		 {
 			 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
 		 }
 
		 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotate.dbRotation);
 		 pAddData->Add(strData);
 						
 	}
 		break;
 	default:
 		break;
 	}

}

//=============================================================================
// Method		: Stain_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:04
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Stain_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.bYmeanResult;

	pAddHeaderz->Add(g_szMESHeader_Stain[1]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount);

	pAddData->Add(strData);
}

//=============================================================================
// Method		: Shading_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.GetFinalResult();

	for (int t = 0; t < ROI_Shading_Max; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dbHorizonValue[t]);
		pAddData->Add(strData);
	}

	for (int t = 0; t < ROI_Shading_Max; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dbVerticalValue[t]);
		pAddData->Add(strData);
	}

	for (int t = 0; t < ROI_Shading_Max; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dbDiaAValue[t]);
		pAddData->Add(strData);
	}

	for (int t = 0; t < ROI_Shading_Max; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dbDiaBValue[t]);
		pAddData->Add(strData);
	}
}

//=============================================================================
// Method		: RI_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/9/28 - 11:06
// Desc.		:
//=============================================================================
void CTestManager_TestMES::RI_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
}

void CTestManager_TestMES::Ymean_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stYmean.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stFocus.stYmean.bYmeanResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Ymean] = strFullData;

}
void CTestManager_TestMES::Ymean_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stYmean.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stYmean.bYmeanResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Ymean] = strFullData;

}


void CTestManager_TestMES::BlackSpot_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stBlackSpot.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stFocus.stBlackSpot.bBlackSpotResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_BlackSpot] = strFullData;

}

void CTestManager_TestMES::BlackSpot_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stBlackSpot.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stBlackSpot.bBlackSpotResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_BlackSpot] = strFullData;

}

void CTestManager_TestMES::LCB_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stLCB.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stFocus.stLCB.bLCBResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_LCB] = strFullData;

}

void CTestManager_TestMES::LCB_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stLCB.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stLCB.bLCBResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_LCB] = strFullData;

}

void CTestManager_TestMES::Defect_Black_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_Black.nDefect_BlackCount, m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_Black.bDefect_BlackResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Defect_Black] = strFullData;

}

void CTestManager_TestMES::Defect_Black_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_Black.nDefect_BlackCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_Black.bDefect_BlackResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Defect_Black] = strFullData;

}
void CTestManager_TestMES::Defect_White_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_White.nDefect_WhiteCount, m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_White.bDefect_WhiteResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Defect_White] = strFullData;

}
void CTestManager_TestMES::Defect_White_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_White.nDefect_WhiteCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_White.bDefect_WhiteResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_ImgT_Defect_White] = strFullData;

}
void CTestManager_TestMES::Ymean_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stYmean.bYmeanResult;

						 pAddHeaderz->Add(g_szMESHeader_Stain[1]); //Ymean Count만

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stYmean.nDefectCount);

						 pAddData->Add(strData);

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stYmean.bYmeanResult;

						   pAddHeaderz->Add(g_szMESHeader_Stain[1]); //Ymean Count만

						   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stYmean.nDefectCount);

						   pAddData->Add(strData);

	}
		break;
	default:
		break;
	}
	
}

void CTestManager_TestMES::BlackSpot_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stBlackSpot.bBlackSpotResult;
						 for (int t = 0; t < MESDataNum_Foc_BlackSpot; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_BlackSpot[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stBlackSpot.nDefectCount);
						 pAddData->Add(strData);

	}
		break;
	case Sys_Image_Test:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stBlackSpot.bBlackSpotResult;
						 for (int t = 0; t < MESDataNum_ImgT_BlackSpot; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_BlackSpot[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stBlackSpot.nDefectCount);
						 pAddData->Add(strData);

	}
		break;
	default:
		break;
	}
}

void CTestManager_TestMES::LCB_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stLCB.bLCBResult;
						 for (int t = 0; t < MESDataNum_Foc_LCB; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_LCB[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stLCB.nDefectCount);
						 pAddData->Add(strData);

	}
		break;
	case Sys_Image_Test:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stLCB.bLCBResult;
						 for (int t = 0; t < MESDataNum_ImgT_LCB; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_LCB[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stLCB.nDefectCount);
						 pAddData->Add(strData);

	}
		break;
	default:
		break;
	}
}

void CTestManager_TestMES::Defect_Black_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_Black.bDefect_BlackResult;
						 for (int t = 0; t < MESDataNum_Foc_Defect_Black; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Defect_Black[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_Black.nDefect_BlackCount);
						 pAddData->Add(strData);

	}
		break;
	case Sys_Image_Test:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_Black.bDefect_BlackResult;
						 for (int t = 0; t < MESDataNum_ImgT_Defect_Black; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Defect_Black[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_Black.nDefect_BlackCount);
						 pAddData->Add(strData);

	}
		break;
	default:
		break;
	}
}

void CTestManager_TestMES::Defect_White_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{

	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_White.bDefect_WhiteResult;
						 for (int t = 0; t < MESDataNum_Foc_Defect_White; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Defect_White[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefect_White.nDefect_WhiteCount);
						 pAddData->Add(strData);

	}
		break;
	case Sys_Image_Test:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_White.bDefect_WhiteResult;
						 for (int t = 0; t < MESDataNum_ImgT_Defect_White; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Defect_White[t]);
						 }

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefect_White.nDefect_WhiteCount);
						 pAddData->Add(strData);

	}
		break;
	default:
		break;
	}

}

void CTestManager_TestMES::FOV_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_Focusing:
// 	{
// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.bRotation;
// 						 for (int t = 0; t < MESDataNum_Foc_Rotation; t++)
// 						 {
// 							 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
// 						 }
// 
// 						 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.dbRotation);
// 						 pAddData->Add(strData);
// 
// 	}
// 		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bDFOV;
						   for (int t = 0; t < MESDataNum_ImgT_FOV; t++)								//^^SH2 1107 Rotation ->FOV 교체해야함
						   {
							   pAddHeaderz->Add(g_szMESHeader_Fov[t]);
						   }
						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV);
						   pAddData->Add(strData);
 						    strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbHFOV);
 						   pAddData->Add(strData);
						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbVFOV);
 						   pAddData->Add(strData);
// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bDFOV;
// 						   for (int t = 0; t < MESDataNum_ImgT_Rotation; t++)								//^^SH2 1107 Rotation ->FOV 교체해야함
// 						   {
// 							   pAddHeaderz->Add(g_szMESHeader_Fov[t]);
// 						   }
// 						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV);
// 						   pAddData->Add(strData);
// 						  
// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.bVFOV;
// 						   for (int t = 0; t < MESDataNum_ImgT_Rotation; t++)								//^^SH2 1107 Rotation ->FOV 교체해야함
// 						   {
// 							   pAddHeaderz->Add(g_szMESHeader_Fov[t]);
// 						   }
// 						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFOV.dbDFOV);
// 						   pAddData->Add(strData);

	}
		break;
	default:
		break;
	}

}

void CTestManager_TestMES::Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_Focusing:
// 	{
// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.bRotation;
// 						 for (int t = 0; t < MESDataNum_Foc_Rotation; t++)
// 						 {
// 							 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
// 						 }
// 
// 						 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.dbRotation);
// 						 pAddData->Add(strData);
// 
// 	}
// 		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortion.bDistortion;
						   for (int t = 0; t < MESDataNum_ImgT_Distortion; t++)												//^^SH2 1107 Rotation ->Distortion 교체해야함
						   {
							   pAddHeaderz->Add(g_szMESHeader_Distortion[t]);
						   }

						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortion.dbDistortion);
						   pAddData->Add(strData);

	}
		break;
	default:
		break;
	}

}

void CTestManager_TestMES::DynamicBW_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
		// 	case Sys_2D_Cal:
		// 		break;
		// 	case Sys_Focusing:
		// 	{
		// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.bRotation;
		// 						 for (int t = 0; t < MESDataNum_Foc_Rotation; t++)
		// 						 {
		// 							 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
		// 						 }
		// 
		// 						 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotate.dbRotation);
		// 						 pAddData->Add(strData);
		// 
		// 	}
		// 		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicBW.bDynamic;
						   for (int t = 0; t < MESDataNum_ImgT_DynamicBW; t++)										//^^SH2 1108 Rotation -> DynamicBW 교체해야함
						   {
							   pAddHeaderz->Add(g_szMESHeader_Dynamic_BW[t]);
						   }


						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicBW.dbDynamic);
						   pAddData->Add(strData);


						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicBW.dbSNR_BW);
						   pAddData->Add(strData);

	}
		break;
	default:
		break;
	}

}
