﻿// Wnd_IOTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_OriginOp.h"

// CWnd_OriginOp

IMPLEMENT_DYNAMIC(CWnd_OriginOp, CWnd)

CWnd_OriginOp::CWnd_OriginOp()
{
	m_pstDevice				= NULL;
	m_nAxis					= 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_OriginOp::~CWnd_OriginOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_OriginOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BN_ETC_START, OnBnClickedBnEtcStart	)
	ON_BN_CLICKED(IDC_BN_ALL_START, OnBnClickedBnAllStart	)
	ON_BN_CLICKED(IDC_BN_STOP,		OnBnClickedBnStop		)
	ON_BN_CLICKED(IDC_OP_BN_SAVE,	OnBnClickedBnSave		)
	ON_BN_CLICKED(IDC_BN_SETTING,	OnBnClickedBnSetting	)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_OriginOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR ORIGIN SETTING"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = ST_OR_DIR; nIdx < ST_OR_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szOriginOpName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ED_OR_MAXNUM; nIdx++)
	{
		DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;

		m_ed_Item[nIdx].Create(dwEtStyle, CRect(0, 0, 0, 0), this, IDC_ET_VEL1_DATA + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdx = 0; nIdx < BN_OR_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szOriginOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SETTING + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < CB_OR_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_DIR_DATA + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; NULL != g_szOriginComboDir[nIdx]; nIdx++)
	{
		m_cb_Item[CB_OR_DIR].AddString(g_szOriginComboDir[nIdx]);
	}

	for (UINT nIdx = 0; NULL != g_szOriginComboSignal[nIdx]; nIdx++)
	{
		m_cb_Item[CB_OR_SIGNAL].AddString(g_szOriginComboSignal[nIdx]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin  = 5;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth	 = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iSpacing * 5) / 6;
	int iBnWidth = (iWidth - iSpacing * 2) / 3;
	int iEditHeight	= 23;
	int iTitleH = 35;

	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);
	
	iTop += iTitleH + iSpacing;

	for (UINT nIdx = 0; nIdx < ST_OR_SIG; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
		
		if (nIdx == CB_OR_DIR)
			m_cb_Item[CB_OR_DIR].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight + 2);
		else
			m_ed_Item[nIdx - 1].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ST_OR_SIG; nIdx < ST_OR_OFFSET; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		if (nIdx == ST_OR_SIG)
			m_cb_Item[CB_OR_SIGNAL].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight + 2);
		else
			m_ed_Item[nIdx - 2].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ST_OR_OFFSET; nIdx < ST_OR_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_ed_Item[nIdx - 2].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < BN_OR_STOP; nIdx++)
	{
		m_bn_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	m_bn_Item[BN_OR_STOP].MoveWindow(iLeft, iTop, iBnWidth, iEditHeight);
}

//=============================================================================
// Method		: OnBnClickedBnStart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnBnClickedBnEtcStart()
{
	if (m_pstDevice == NULL)
		return;

	m_pstDevice->SetMotorOrigin(m_nAxis);
}

//=============================================================================
// Method		: OnBnClickedBnAllStart
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/3 - 20:15
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnBnClickedBnAllStart()
{
	if (m_pstDevice == NULL)
		return;

	GetOwner()->SendNotifyMessage(WM_MOTOR_ORIGIN, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedBnStop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnBnClickedBnStop()
{
	if (m_pstDevice == NULL)
		return;

	m_pstDevice->SetAllOriginStop();
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 셋팅
	m_pstDevice->SetMotionSetting(m_nAxis);

	// 수집된 데이터 저장
	m_pstDevice->SaveMotionInfo();
}

//=============================================================================
// Method		: OnBnClickedBnSetting
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/3 - 20:13
// Desc.		:
//=============================================================================
void CWnd_OriginOp::OnBnClickedBnSetting()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 셋팅
	m_pstDevice->SetMotionSetting(m_nAxis);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_OriginOp::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_OriginOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSelectAxis
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 11:38
// Desc.		:
//=============================================================================
void CWnd_OriginOp::SetSelectAxis(UINT nAxis)
{
	m_nAxis = nAxis;
	SetUpdateData();
}

//=============================================================================
// Method		: SetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:29
// Desc.		:
//=============================================================================
void CWnd_OriginOp::SetUpdateData()
{
	if (m_pstDevice == NULL)
		return;

	if (m_pstDevice->m_AllMotorData.pMotionParam == NULL)
		return;

	CString strData;

	ST_MotionParam stMotorParam = m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis];

	// 원점 방향
	m_cb_Item[CB_OR_DIR].SetCurSel(stMotorParam.iAxisHomeDir);

	// 원점 센서 
	switch (stMotorParam.iAxisHomeSig)
	{
	case 0:
		m_cb_Item[CB_OR_SIGNAL].SetCurSel(0);
		break;
	case 1:
		m_cb_Item[CB_OR_SIGNAL].SetCurSel(1);
		break;
	case 4:
		m_cb_Item[CB_OR_SIGNAL].SetCurSel(2);
		break;
	case 5:
		m_cb_Item[CB_OR_SIGNAL].SetCurSel(3);
		break;
	default:
		break;
	}

	// 원점 옵셋
	strData.Format(_T("%.f"), stMotorParam.dbOriginOffset);
	m_ed_Item[ED_OR_OFFSET].SetWindowText(strData);

	// 1차 속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginVelFirst);
	m_ed_Item[ED_OR_VEL1].SetWindowText(strData);

	// 2차 속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginVelSecond);
	m_ed_Item[ED_OR_VEL2].SetWindowText(strData);

	// 3차 속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginVelThird);
	m_ed_Item[ED_OR_VEL3].SetWindowText(strData);

	// 4차 속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginVelLast);
	m_ed_Item[ED_OR_VEL4].SetWindowText(strData);

	// 1차 가속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginAccFirst);
	m_ed_Item[ED_OR_ACC1].SetWindowText(strData);

	// 2차 가속도
	strData.Format(_T("%.f"), stMotorParam.dbOriginAccSecond);
	m_ed_Item[ED_OR_ACC2].SetWindowText(strData);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:41
// Desc.		:
//=============================================================================
void CWnd_OriginOp::GetUpdateData()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	// 원점 방향
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeDir = m_cb_Item[CB_OR_DIR].GetCurSel();

	// 원점 센서 
	switch (m_cb_Item[CB_OR_SIGNAL].GetCurSel())
	{
	case 0:
		m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSig = 0;
		break;
	case 1:
		m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSig = 1;
		break;
	case 2:
		m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSig = 4;
		break;
	case 3:
		m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSig = 5;
		break;
	default:
		break;
	}

	// 원점 옵셋
	m_ed_Item[ED_OR_OFFSET].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginOffset = _ttof(strData);

	// 1차 속도
	m_ed_Item[ED_OR_VEL1].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginVelFirst = _ttof(strData);

	// 2차 속도
	m_ed_Item[ED_OR_VEL2].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginVelSecond = _ttof(strData);

	// 3차 속도
	m_ed_Item[ED_OR_VEL3].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginVelThird = _ttof(strData);

	// 4차 속도
	m_ed_Item[ED_OR_VEL4].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginVelLast = _ttof(strData);

	// 1차 가속도
	m_ed_Item[ED_OR_ACC1].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginAccFirst = _ttof(strData);

	// 2차 가속도
	m_ed_Item[ED_OR_ACC2].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbOriginAccSecond = _ttof(strData);
}
