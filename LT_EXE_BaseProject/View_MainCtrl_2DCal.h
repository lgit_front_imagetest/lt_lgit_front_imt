﻿//*****************************************************************************
// Filename	: View_MainCtrl_2DCal.h
// Created	: 2010/11/26
// Modified	: 2016/06/07
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef View_MainCtrl_2DCal_h__
#define View_MainCtrl_2DCal_h__

#pragma once


#include "View_MainCtrl.h"
#include "TestManager_EQP_2DCal.h"


//=============================================================================
// CView_MainCtrl_2DCal 창
//=============================================================================
class CView_MainCtrl_2DCal : public CView_MainCtrl, public CTestManager_EQP_2DCal
{
// 생성입니다.
public:
	CView_MainCtrl_2DCal();
	virtual ~CView_MainCtrl_2DCal();

protected:

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	// 로그 메세지
	afx_msg	LRESULT	OnLogMsg				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnLogMsg_PLC			(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnTestStart				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestStop				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestInit				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnTestCompleted			(WPARAM wParam, LPARAM lParam);
	
	// MES 통신
	afx_msg LRESULT	OnCommStatus_MES		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRecvMES				(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnSwitchPermissionMode	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnSwitchMESOnlineMode	(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeRecipe			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnDeviceCtrl			(WPARAM wParam, LPARAM lParam);	
	
	// 통신 데이터
	afx_msg LRESULT	OnRecvBarcode			(WPARAM wParam, LPARAM lParam);

	// 영상처리
	afx_msg LRESULT	OnCameraChgStatus		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraRecvVideo		(WPARAM wParam, LPARAM lParam);

	// Digital I/O
	afx_msg LRESULT	OnRecvDIOMon			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRecvDIOFirstRead		(WPARAM wParam, LPARAM lParam);

	// 컨트롤 보드 통신 처리용	
	afx_msg LRESULT	OnRecvMainBrd			(WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT	OnMotorOrigin			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMotor			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMaintenance		(WPARAM wParam, LPARAM lParam);

	// 메뉴얼 동작 테스트
	afx_msg LRESULT OnManualSequence		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualTestItem		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCanComm			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCanCommPg2		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCanCommPg3		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCanCommPg4		(WPARAM wParam, LPARAM lParam);

	// 소모품 카운트
	afx_msg LRESULT OnConsumableReset		(WPARAM wParam, LPARAM lParam);
	
	DECLARE_MESSAGE_MAP()
	

	//-----------------------------------------------------
	// 차일드 윈도우 구분용
	//-----------------------------------------------------	

	//-----------------------------------------------------
	// 로그/파일 처리
	//-----------------------------------------------------
	virtual void	OnInitLogFolder					();
		
	//-----------------------------------------------------
	// 초기 설정 관련
	//-----------------------------------------------------
	// 생성자에서 초기화 할 세팅
	virtual void	InitConstructionSetting			();
	// Window 생성 후 세팅
	virtual void	InitUISetting					();
	// 주변장치들 기본 설정
	virtual void	InitDeviceSetting				();
	
	//-----------------------------------------------------
	// 통신 연결 상태
	//-----------------------------------------------------
	// MES 통신 상태
	virtual void	OnSetStatus_MES					(__in UINT nCommStatus);
	virtual void	OnSetStatus_MES_Online			(__in UINT nOnlineMode);
	
	// 바코드 리더기 통신 연결 상태
// 	virtual void	OnSetStatus_HandyBCR			(__in UINT nConnect);
// 	virtual void	OnSetStatus_FixedBCR			(__in UINT nConnect);
	// 제어 보드 연결 상태
	virtual void	OnSetStatus_Camera_Brd			(__in UINT nConnect, __in UINT nIdxBrd = 0);
	// 광원 보드 통신 연결상태
	virtual void	OnSetStatus_LightBrd			(__in UINT nConnect, __in UINT nIdxBrd = 0);
	// 파워 서플라이 연결상태
	virtual void	OnSetStatus_LightPSU			(__in UINT nConnect, __in UINT nIdxBrd = 0);
	// Digital I/O, Motion 통신 상태
	virtual void	OnSetStatus_Motion				(__in UINT nCommStatus);
	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in UINT nConnect, __in UINT nIdxBrd = 0);
	// 프레임 그래버 보드 통신 연결 상태
	virtual void	OnSetStatus_GrabBoard			(__in UINT nConnect, __in UINT nIdxBrd = 0);
	virtual void	OnSetStatus_VideoSignal			(__in UINT bSignalStatus, __in UINT nIdxBrd = 0);

	// 바코드 입력
	virtual void	OnSet_BarcodeWithDialog			(__in LPCTSTR szBarcode);
	virtual void	OnAddAlarmInfo					(__in enResultCode nResultCode);
	virtual void	OnAddAlarm						(__in LPCTSTR szAlarm);
	virtual void	OnAddAlarm_F					(__in LPCTSTR szAlarm, ...);
	virtual void	OnResetAlarm					();
	virtual void	OnSet_TestTime					(__in UINT nParaIdx = 0);
	virtual void	OnSet_CycleTime					();

	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	virtual void	OnUpdate_EquipmentInfo			();
	virtual void	OnUpdate_ElapTime_TestUnit		(__in UINT nParaIdx = 0);
	virtual void	OnUpdate_ElapTime_Cycle			();
	virtual void	OnUpdate_TestReport				(__in UINT nParaIdx = 0);
	
	virtual void	OnSet_TestProgress				(__in enTestProcess nProcess);
	virtual void	OnSet_TestProgress_Unit			(__in enTestProcess nProcess, __in UINT nParaIdx = 0);
	virtual void	OnSet_TestProgressStep			(__in UINT nTotalStep, __in UINT nProgStep);
	virtual void	OnSet_TestResult_Unit			(__in enTestResult nResult, __in UINT nParaIdx = 0);
	virtual void	OnSet_ResultCode_Unit			(__in LRESULT nResultCode, __in UINT nParaIdx = 0);

	virtual void	OnSet_TestStepSelect			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual void	OnSet_TestStepResult			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	
	virtual void	OnSet_InputTime					();
	virtual void	OnSet_BeginTestTime				(__in UINT nParaIdx = 0);
	virtual void	OnSet_OutputTime				();

	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode, __in UINT nRetryCnt = 0);
	//virtual void	OnSet_VCSEL_Status				(__in BOOL bOn, __in UINT nParaIdx = 0);
	
	// PLC 신호 감지 및 신호별 기능 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);

	//-----------------------------------------------------
	// 영상 작업 처리
	//-----------------------------------------------------
	virtual void	DisplayVideo					(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight);
	virtual void	DisplayVideo_LastImage			(__in UINT nChIdx);
	virtual void	DisplayVideo_NoSignal			(__in UINT nChIdx);
	virtual void	DisplayVideo_Overlay			(__in UINT nChIdx, __in enOverlayItem enItem, __inout IplImage *TestImage);

	// 캡쳐된 이미지를 History 탭윈도우에 영상 표시
	virtual void	OnImage_AddHistory				(__in UINT nChIdx, __in LPCTSTR szTitle);
	virtual void	OnImage_SetHistory				(__in UINT nChIdx, __in UINT nHistoryIndex, __in LPCTSTR szTitle);

	// 팝업된 윈도우 숨김
	virtual void	OnHidePopupUI					();
	
	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------

	// 카메라 데이터 초기화
	virtual void	OnReset_CamInfo					(__in UINT nParaIdx = 0);
	virtual void	OnReset_CamInfo_All				();
	// 제품 로딩시 데이터 초기화
	virtual void	OnResetInfo_Loading				();
	// 현재 검사 중인 데이터 초기화
	virtual void	OnResetInfo_StartTest			(__in UINT nParaIdx = 0);
	// 제품 배출시 데이터 초기화
	virtual void	OnResetInfo_Unloading			();
	// 제품 재검사를 위해서 측정 데이터 및 UI 초기화
	virtual void	OnResetInfo_Measurment			(__in UINT nParaIdx = 0);
	
	// Worklist 처리
	virtual void	OnInsertWorklist				();
	virtual void	OnSaveWorklist					();
	virtual void	OnLoadWorklist					();

	// 수율
	virtual void	OnUpdateYield					(__in UINT nParaIdx = 0);
	virtual void	OnLoadYield						();

	// 소모품 (pogo count)
	virtual void	OnSetStatus_ConsumInfo			();
	virtual void	OnSetStatus_ConsumInfo			(__in UINT nItemIdx);	
	
	// 수율, Cycle Time 초기화
	virtual void	OnResetYieldCycleTime			();

	//-------------------------------------------------------------------------
	// 모델 파일 불러오기 및 세팅
	virtual inline BOOL		LoadRecipeInfo			(__in LPCTSTR szRecipe, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadRecipeInfo		();

	// 모터 파일 불러오기 및 세팅
	virtual inline BOOL		LoadMotorInfo			(__in LPCTSTR szMotir, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadMotorInfo		();

	// 유지 관리 파일 불러오기 및 세팅
	virtual inline BOOL		LoadMaintenanceInfo		(__in LPCTSTR szMaintenance, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadMaintenanceInfo	();
	
	// 수동으로 보드 제어
	virtual void			Manual_DeviceControl	(__in UINT nChIdx, __in UINT nBnIdx);	

//=============================================================================
public: 
//=============================================================================
	
	// 검사기 종류 설정
	virtual void	SetSystemType				(__in enInsptrSysType nSysType);

	// 로그 메세지 처리용 함수
	virtual void	AddLog						(__in LPCTSTR lpszLog, __in BOOL bError = FALSE, __in UINT nLogType = LOGTYPE_NORMAL, __in BOOL bOnlyLogType = FALSE);

	// 차일드 윈도우 전환 시 사용
	virtual UINT	SwitchWindow				(__in UINT nIndex);
	// 장치 통신 상태 표시 윈도우 포인터 설정
	virtual void	SetCommPanePtr				(__in CWnd* pwndCommPane);
	
	// 옵션이 변경 되었을 경우 다시 UI나 데이터를 세팅하기 위한 함수
	virtual void	ReloadOption				();
	
	// 프로그램 로딩 끝난 후 자동 처리를 위한 함수
	virtual void	InitStartProgress			();	
	virtual BOOL	InitStartDeviceProgress		();	

	// 프로그램 종료시 처리해야 할 기능들을 처리하는 함수
	virtual void	FinalExitProgress			();
	
	// 수동 바코드 입력
	virtual void	ManualBarcode				();

	// 모터 원점
	virtual BOOL	MotorOrigin					();

	// 제어 권한 설정
	virtual void	SetPermissionMode			(__in enPermissionMode nAcessMode);	
	// MES Online Mode
	virtual void	SetMESOnlineMode			(__in enMES_Online nOnlineMode);
	virtual void	ChangeMESOnlineMode			(__in enMES_Online nOnlineMode);
	// 설비 구동 모드
	virtual void	SetOperateMode				(__in enOperateMode nOperMode);

	virtual void	OnManual_OneItemTest		(__in UINT nStepIdx, __in UINT nParaIdx);

	// 설비 초기화 (원점 수행) : 설비 오류 시 초기화 하기 위해 사용
	virtual void	EquipmentInit				(__in UINT nCondition = 0);

	//--------------------- TEST --------------------------
	virtual void	Test_Process				(__in UINT nTestNo);
	//--------------------- TEST --------------------------
	
	// KHO 스레오에서 만 빌드 되어 추가. 추후 수정 필요....
	virtual void	OnEEPROM_Ctrl				(__in UINT nCondition){};
};

#endif // View_MainCtrl_2DCal_h__


