﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_ROI_Image.cpp
// Created	:	2018/2/8 - 16:36
// Modified	:	2018/2/8 - 16:36
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_Cfg_ROI_Image.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_ROI_Image.h"
#include "resource.h"
#include "Def_WindowMessage.h"
#include "File_RomBinary.h"

#define IDC_ED_MODELCODE			1001

 
#define		TABSTYLE_COUNT			1

 static UINT g_TabOrder[TABSTYLE_COUNT] =
 {
 	1001
 };


// CWnd_Cfg_ROI_Image
IMPLEMENT_DYNAMIC(CWnd_Cfg_ROI_Image, CWnd_BaseView)

CWnd_Cfg_ROI_Image::CWnd_Cfg_ROI_Image()
{
	VERIFY(m_font_Default.CreateFont(
		32,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		32,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_Cfg_ROI_Image::~CWnd_Cfg_ROI_Image()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_ROI_Image, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_Cfg_ROI_Image message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_ROI_Image::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 10;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlHeight		= 40;
	int iCapWidth		= iWidth / 5;
	int iDataWidth		= iCapWidth * 3;
	int iHalfWidth		= (iWidth - iCateSpacing) * 2 / 3;
	int iLeftSub		= 0;
	int iTempWidth		= 0;

	
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ROI_Image::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ROI_Image::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_CHAR:
		if (VK_TAB == pMsg->wParam)
		{
			UINT nID = GetFocus()->GetDlgCtrlID();
 			for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
 			{
 				if (nID == g_TabOrder[iCnt])
 				{
 					if ((TABSTYLE_COUNT - 1) == iCnt)
 					{
 						nID = g_TabOrder[0];
 					}
 					else
 					{
 						nID = g_TabOrder[iCnt + 1];
 					}
 
 					break;
 				}
 			}

			GetDlgItem(nID)->SetFocus();
		}
		break;

	default:
		break;
	}

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::GetUIData(__out ST_RecipeInfo& stRecipeInfo)
{
	CString strValue;	

	

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::SetUIData(__in const ST_RecipeInfo* pRecipeInfo)
{
	CString strValue;
	

}

//=============================================================================
// Method		: SetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo * pRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::SetRecipeInfo(__in const ST_RecipeInfo* pRecipeInfo)
{
	SetUIData(pRecipeInfo);
}

//=============================================================================
// Method		: GetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::GetRecipeInfo(__out ST_RecipeInfo& stRecipeInfo)
{
	GetUIData(stRecipeInfo);
	
}

//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/8 - 17:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_ROI_Image::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{

}

void CWnd_Cfg_ROI_Image::NoSignal_Ch(__in INT iChIdx)
{

}
