// CWnd_MT_Particle
#ifndef Wnd_MT_Particle_h__
#define Wnd_MT_Particle_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "Def_MTCtrl_Item.h"

class CWnd_MT_Particle : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MT_Particle)

public:
	CWnd_MT_Particle();
	virtual ~CWnd_MT_Particle();

	enum
	{
		Title_CANCommPg4_Name,
		Title_CANCommPg4_Send,
		Title_CANCommPg4_Recv,
		Title_CANCommPg4_Value,
		Title_CANCommPg4_Func,
		Title_CANCommPg4_MaxNum,
	};

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	void			OnRangeCmds				(__in UINT nID);
	void			OnRangePort				(__in UINT nID);

	void			GetTextValue			(__in UINT nTestItem, __out CString &szValue);
	void			SetTextSendProtocol		(__in UINT nTestItem, __in CString szValue);
	void			SetTextRecvProtocol		(__in UINT nTestItem, __in CString szValue);
	void			ResetInfo				();
	void			SetButtonEnable			(__in BOOL bEnable);

	CVGStatic		m_st_Name;
	CVGStatic		m_stTitle[Title_CANCommPg4_MaxNum];

	CMFCButton		m_bn_Item[Para_MaxEnum];

	CVGStatic		m_stTestName[MTCtrl_Particle_MaxNum];
	CVGStatic		m_stSendProtol[MTCtrl_Particle_MaxNum];
	CVGStatic		m_stRecvProtol[MTCtrl_Particle_MaxNum];
	CMFCButton		m_btTestFunc[MTCtrl_Particle_MaxNum];
	CMFCMaskedEdit	m_ed_Index[MTCtrl_Cal_MaxNum];

	CFont			m_font;
	UINT			m_nPort;

protected:
	DECLARE_MESSAGE_MAP()
};

#endif // Wnd_MT_Particle_h__
