#ifndef Wnd_TestResult_ImgT_h__
#define Wnd_TestResult_ImgT_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct_Cm.h"
#include "Def_DataStruct.h"

#include "Wnd_Rst_Ymean.h"
#include "Wnd_Rst_BlackSpot.h"
#include "Wnd_Rst_LCB.h"
#include "Wnd_Rst_Current.h"
#include "Wnd_Rst_Defect_Black.h"
#include "Wnd_Rst_Defect_White.h"
#include "Wnd_Rst_Rotate.h"
#include "Wnd_Rst_SFR.h"
#include "Wnd_Rst_OpticalCenter.h"
#include "Wnd_Rst_FOV.h"
#include "Wnd_Rst_Dynamic.h"
#include "Wnd_Rst_Shading.h"
// #include "Wnd_Rst_ActiveAlign.h"
// #include "Wnd_Rst_TorqueData.h"

enum enTestResult_ImgT_Static
{
	STI_TR_ImgT_Title,
	STI_TR_ImgT_MAX,
};

static LPCTSTR	g_szTestResult_ImgT_Static[] =
{
	_T("TEST RESULT"),
	NULL
};
enum enTestResult_ImgT_Item
{

	TR_ImgT_Current,
	TR_ImgT_OpticalCenter,
	TR_ImgT_Rotation,
	TR_ImgT_SFR,
	TR_ImgT_Ymean,
	TR_ImgT_BlackSpot,
	TR_ImgT_LCB,
	TR_ImgT_Defect_Black,
	TR_ImgT_Defect_White,
	TR_ImgT_FOV,
	TR_ImgT_Distortion,
	TR_ImgT_DynamicBW,
	TR_ImgT_Shading,
// 	TR_ImgT_ActiveAlign,
// 	TR_ImgT_Torque,
	TR_ImgT_MAX,

};

class CWnd_TestResult_ImgT : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_ImgT)

public:
	CWnd_TestResult_ImgT();
	virtual ~CWnd_TestResult_ImgT();
	CMFCTabCtrl		m_tc_Option;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl(UINT nID);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	enInsptrSysType				m_InspectionType;

	CFont						m_font;

	CVGStatic					m_st_default;
	CVGStatic					m_st_Item[STI_TR_ImgT_MAX];

	int m_iSelectTest[TR_ImgT_MAX];

	CWnd_Rst_SFR				m_wnd_SFRRst;
	CWnd_Rst_Current			m_wnd_CurrentRst;
	CWnd_Rst_OpticalCenter		m_wnd_OpticalCenterRst;
	CWnd_Rst_Rotate				m_wnd_RotateRst;
	CWnd_Rst_Defect_Black		m_wnd_Defect_BlackRst;
	CWnd_Rst_Defect_White		m_wnd_Defect_WhiteRst;
	CWnd_Rst_Ymean				m_wnd_YmeanRst;
	CWnd_Rst_BlackSpot			m_wnd_BlackSpotRst;
	CWnd_Rst_LCB				m_wnd_LCBRst;
	CWnd_Rst_Rotate				m_wnd_DistortionRst;
	CWnd_Rst_Dynamic			m_wnd_DynamicBWRst;
	CWnd_Rst_Shading			m_wnd_ShadingRst;
	//CWnd_Rst_ActiveAlign		m_wnd_ActiveAlignRst;
	//CWnd_Rst_Torque				m_wnd_TorqueRst;
	CWnd_Rst_FOV				m_wnd_FOVRst;

	// 	CWnd_ActiveAlignData		m_Wnd_ActiveAlignData;
	// 	CWnd_CurrentData			m_Wnd_CurrentData;
	// 	CWnd_DefectPixelData		m_Wnd_DefectPixelData;
	// 	CWnd_OpticalData			m_Wnd_OpticalData;
	// 	CWnd_RotateData				m_Wnd_RotateData;
	// 	CWnd_SFRData				m_Wnd_SFRData;
	// 	CWnd_ParticleData			m_Wnd_ParticleData;
	// 	CWnd_TorqueData				m_Wnd_TorqueData;

	void	SetShowWindowResult(int iItem);
	void	MoveWindow_Result(int x, int y, int nWidth, int nHeight);

public:
	CComboBox		m_cb_TestItem;

	// 검사기 종류 설정
	void	SetSystemType(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void SetTestItem(__in int iTestItem);
	void SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem);
	void SetUpdateClear();

	void AllDataReset();
	void SetUIData_Reset(int nIdx);
	void SetUIData(int nIdx, LPVOID pParam);
	void SetClearTab();
	void SetAddTab(int nIdx, int iItemCnt);
	afx_msg void OnLbnSelChangeTest();
	void SelectNum(UINT nSelect);
};
#endif // Wnd_TestResult_ImgT_h__
