// Wnd_TestResult_ImgT.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_ImgT.h"

// CWnd_TestResult_ImgT
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM = 1001,
	IDC_CMB_ITEM = 2001,
	IDC_EDT_ITEM = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_ImgT, CWnd)

CWnd_TestResult_ImgT::CWnd_TestResult_ImgT()
{
	m_InspectionType = Sys_Image_Test;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_ImgT::~CWnd_TestResult_ImgT()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_ImgT, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_ImgT 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_ImgT_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_ImgT_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	UINT	nWndID = IDC_LIST_ITEM;
	//m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, nWndID++, CMFCTabCtrl::LOCATION_BOTTOM);

	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);


	m_wnd_CurrentRst.SetOwner(GetOwner());
	m_wnd_CurrentRst.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);


	m_wnd_OpticalCenterRst.SetOwner(GetOwner());
	m_wnd_OpticalCenterRst.Create(NULL, _T("OpticalCenter"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_RotateRst.SetOwner(GetOwner());
	m_wnd_RotateRst.Create(NULL, _T("Rotate"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_SFRRst.SetOwner(GetOwner());
	m_wnd_SFRRst.Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_YmeanRst.SetOwner(GetOwner());
	m_wnd_YmeanRst.Create(NULL, _T("Ymean"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_BlackSpotRst.SetOwner(GetOwner());
	m_wnd_BlackSpotRst.Create(NULL, _T("BlackSpot"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_LCBRst.SetOwner(GetOwner());
	m_wnd_LCBRst.Create(NULL, _T("LCB"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_Defect_BlackRst.SetOwner(GetOwner());
	m_wnd_Defect_BlackRst.Create(NULL, _T("Defect_Black"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_Defect_WhiteRst.SetOwner(GetOwner());
	m_wnd_Defect_WhiteRst.Create(NULL, _T("Defect_White"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_FOVRst.SetOwner(GetOwner());
	m_wnd_FOVRst.Create(NULL, _T("FOV"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_DistortionRst.SetOwner(GetOwner());
	m_wnd_DistortionRst.Create(NULL, _T("Distortion"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_DynamicBWRst.SetOwner(GetOwner());
	m_wnd_DynamicBWRst.Create(NULL, _T("DynamicBW"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_ShadingRst.SetOwner(GetOwner());
	m_wnd_ShadingRst.Create(NULL, _T("Shading"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);


// 	m_wnd_ActiveAlignRst.SetOwner(GetOwner());
// 	m_wnd_ActiveAlignRst.Create(NULL, _T("ActiveAlign"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
// 
// 	m_wnd_TorqueRst.SetOwner(GetOwner());
// 	m_wnd_TorqueRst.Create(NULL, _T("Torque"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_cb_TestItem.AddString(_T("Current"));
	m_cb_TestItem.AddString(_T("OpticalCenter"));
	m_cb_TestItem.AddString(_T("Rotate"));
	m_cb_TestItem.AddString(_T("SFR"));
	m_cb_TestItem.AddString(_T("Ymean"));
	m_cb_TestItem.AddString(_T("BlackSpot"));
	m_cb_TestItem.AddString(_T("LCB"));
	m_cb_TestItem.AddString(_T("Defect_Black"));
	m_cb_TestItem.AddString(_T("Defect_White"));
	m_cb_TestItem.AddString(_T("FOV"));
	m_cb_TestItem.AddString(_T("Distortion"));
	m_cb_TestItem.AddString(_T("DynamicBW"));
	m_cb_TestItem.AddString(_T("Shading"));
//	m_cb_TestItem.AddString(_T("ActiveAlign"));
//	m_cb_TestItem.AddString(_T("Torque"));

	m_iSelectTest[TR_ImgT_Current]			= TI_ImgT_Fn_ECurrent;			//들어온 순서대로 ID Input;							
	m_iSelectTest[TR_ImgT_OpticalCenter]	= TI_ImgT_Fn_OpticalCenter;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Rotation]			= TI_ImgT_Fn_Rotation;			//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_SFR]				= TI_ImgT_Fn_SFR;				//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Ymean]			= TI_ImgT_Fn_Ymean;				//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_BlackSpot]		= TI_ImgT_Fn_BlackSpot;			//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_LCB]				= TI_ImgT_Fn_LCB;				//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Defect_Black]		= TI_ImgT_Fn_Defect_Black;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Defect_White]		= TI_ImgT_Fn_Defect_White;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_FOV]				= TI_ImgT_Fn_FOV;				//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Distortion]		= TI_ImgT_Fn_Distortion;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_DynamicBW]		= TI_ImgT_Fn_DynamicBW;			//들어온 순서대로 ID Input;
	m_iSelectTest[TR_ImgT_Shading]			= TI_ImgT_Fn_Shading;			//들어온 순서대로 ID Input;
	//m_iSelectTest[TR_ImgT_ActiveAlign]		= TI_ImgT_Fn_ActiveAlign;	//들어온 순서대로 ID Input;
	//m_iSelectTest[TR_ImgT_Torque]			= TI_ImgT_Re_TorqueCheck;		//들어온 순서대로 ID Input;

	m_wnd_CurrentRst.ShowWindow(SW_HIDE);
	m_wnd_OpticalCenterRst.ShowWindow(SW_SHOW);
	m_wnd_RotateRst.ShowWindow(SW_HIDE);
	m_wnd_SFRRst.ShowWindow(SW_HIDE);
	m_wnd_YmeanRst.ShowWindow(SW_HIDE);
	m_wnd_BlackSpotRst.ShowWindow(SW_HIDE);
	m_wnd_LCBRst.ShowWindow(SW_HIDE);
	m_wnd_Defect_BlackRst.ShowWindow(SW_HIDE);
	m_wnd_Defect_WhiteRst.ShowWindow(SW_HIDE);
	m_wnd_FOVRst.ShowWindow(SW_HIDE);
	m_wnd_DistortionRst.ShowWindow(SW_HIDE);
	m_wnd_DynamicBWRst.ShowWindow(SW_HIDE);
	m_wnd_ShadingRst.ShowWindow(SW_HIDE);
// 	m_wnd_ActiveAlignRst.ShowWindow(SW_HIDE);
// 	m_wnd_TorqueRst.ShowWindow(SW_HIDE);

	SelectNum(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;

	m_st_Item[STI_TR_ImgT_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iTop += iStHeight + 2;
	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iTop += iStHeight + 2;

	m_wnd_CurrentRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_OpticalCenterRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_RotateRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_SFRRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_YmeanRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_BlackSpotRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_LCBRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_Defect_BlackRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_Defect_WhiteRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_FOVRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DistortionRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DynamicBWRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_ShadingRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_wnd_ActiveAlignRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_wnd_TorqueRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_ImgT::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetShowWindowResult(int iItem)
{
	// 	switch (m_InspectionType)
	// 	{
	// 	case Sys_Focusing:
	// 		break;
	// 	case Sys_2D_Cal:
	// 		break;
	// 	case Sys_Image_Test:
	// 		break;
	// 	case Sys_Stereo_Cal:
	// 		break;
	// 	case Sys_3D_Cal:
	// 		break;
	// 	default:
	// 		break;
	// 	}
}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;

	// 	switch (m_InspectionType)
	// 	{
	// 	case Sys_Focusing:
	// 		break;
	// 	case Sys_2D_Cal:
	// 		break;
	// 	case Sys_Image_Test:
	// 		break;
	// 	case Sys_Stereo_Cal:
	// 		break;
	// 	case Sys_3D_Cal:
	// 		break;
	// 	default:
	// 		break;
	// 	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetUpdateClear()
{
}
//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:27
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::AllDataReset()
{

	m_wnd_CurrentRst.Result_Reset();
	m_wnd_OpticalCenterRst.Result_Reset();
	m_wnd_RotateRst.Result_Reset();
	m_wnd_SFRRst.Result_Reset();
	m_wnd_YmeanRst.Result_Reset();
	m_wnd_BlackSpotRst.Result_Reset();
	m_wnd_LCBRst.Result_Reset();
	m_wnd_Defect_BlackRst.Result_Reset();
	m_wnd_Defect_WhiteRst.Result_Reset();
	m_wnd_FOVRst.Result_Reset();
	m_wnd_DistortionRst.Result_Reset();
	m_wnd_DynamicBWRst.Result_Reset();
	m_wnd_ShadingRst.Result_Reset();
// 	m_wnd_ActiveAlignRst.Result_Reset();
// 	m_wnd_TorqueRst.DataReset();
}
//=============================================================================
// Method		: SetUIData_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 10:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetUIData_Reset(int nIdx)
{
	switch (nIdx)
	{
	case TI_ImgT_Fn_ECurrent:
		m_wnd_CurrentRst.Result_Reset();
		break;

	case TI_ImgT_Fn_OpticalCenter:
		m_wnd_OpticalCenterRst.Result_Reset();
		break;

	case TI_ImgT_Fn_Rotation:
		m_wnd_RotateRst.Result_Reset();
		break;

	case TI_ImgT_Fn_SFR:
		m_wnd_SFRRst.Result_Reset();
		break;

	case TI_ImgT_Fn_Ymean:
		m_wnd_YmeanRst.Result_Reset();
		break;

	case TI_ImgT_Fn_BlackSpot:
		m_wnd_BlackSpotRst.Result_Reset();
		break;

	case TI_ImgT_Fn_LCB:
		m_wnd_LCBRst.Result_Reset();
		break;

	case TI_ImgT_Fn_Defect_Black:
		m_wnd_Defect_BlackRst.Result_Reset();
		break;

	case TI_ImgT_Fn_Defect_White:
		m_wnd_Defect_WhiteRst.Result_Reset();
		break;

	case TI_ImgT_Fn_FOV:
		m_wnd_FOVRst.Result_Reset();
		break;
	case TI_ImgT_Fn_Distortion:
		m_wnd_DistortionRst.Result_Reset();
		break;
	case TI_ImgT_Fn_DynamicBW:
		m_wnd_DynamicBWRst.Result_Reset();
		break;
	case TI_ImgT_Fn_Shading:
		m_wnd_ShadingRst.Result_Reset();
		break;
// 	case TI_ImgT_Re_TorqueCheck:
// 		m_wnd_TorqueRst.DataReset();
// 		break;
// 
// 	case TI_ImgT_Fn_ActiveAlign:
// 		m_wnd_ActiveAlignRst.Result_Reset();
// 		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: LPVOID pParam
// Qualifier	:
// Last Update	: 2018/3/9 - 10:13
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetUIData(int nIdx, LPVOID pParam)
{
	if (pParam == NULL)
		return;

	for (int iItem = 0; iItem < m_cb_TestItem.GetCount(); iItem++)
	{
		if (m_iSelectTest[iItem] == nIdx)
		{
			SelectNum(iItem);
			break;
		}
	}

	switch (nIdx)
	{
	case TI_ImgT_Fn_ECurrent:
	{
		 ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		 m_wnd_CurrentRst.Result_Display(0, 0, pData->stCurrent.bResult[0], pData->stCurrent.dbValue[0]);
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;

		m_wnd_OpticalCenterRst.Result_Display(0, 0, pData->stOpticalCenter.nResultX, pData->stOpticalCenter.nResultPosX);
		m_wnd_OpticalCenterRst.Result_Display(0, 1, pData->stOpticalCenter.nResultY, pData->stOpticalCenter.nResultPosY);
		m_wnd_OpticalCenterRst.Result_Display(0, 2, pData->stOpticalCenter.nResultX, pData->stOpticalCenter.iStandPosDevX);
		m_wnd_OpticalCenterRst.Result_Display(0, 3, pData->stOpticalCenter.nResultY, pData->stOpticalCenter.iStandPosDevY);
	}
		break;

	case TI_ImgT_Fn_SFR:
	{
		 ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		 for (int nNum = 0; nNum < enSFR_Result_Max; nNum++)
		 {
			  m_wnd_SFRRst.Result_Display(0, nNum, pData->stSFR.bResult[nNum], pData->stSFR.dbValue[nNum]);
		 }
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		  ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		  for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
		   {
			   m_wnd_RotateRst.Result_Display(0, nNum, pData->stRotate.bRotation, pData->stRotate.dbRotation);
		   }
	}
		break;

	case TI_ImgT_Fn_Ymean:
	{
			ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
			for (int nNum = 0; nNum < enYmean_Result_Max; nNum++)
			{
				m_wnd_YmeanRst.Result_Display(0, nNum, pData->stYmean.bYmeanResult, pData->stYmean.nDefectCount);
			}
	}
		break;

	case TI_ImgT_Fn_BlackSpot:
	{
		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
			for (int nNum = 0; nNum < enBlackSpot_Result_Max; nNum++)
			{
				m_wnd_BlackSpotRst.Result_Display(0, nNum, pData->stBlackSpot.bBlackSpotResult, pData->stBlackSpot.nDefectCount);
			}
	}
		break;

	case TI_ImgT_Fn_LCB:
	{
		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		for (int nNum = 0; nNum < enLCB_Result_Max; nNum++)
		{
			m_wnd_LCBRst.Result_Display(0, nNum, pData->stLCB.bLCBResult, pData->stLCB.nDefectCount);
		}
	}
		break;

	case TI_ImgT_Fn_Defect_Black:
	{
		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		for (int nNum = 0; nNum < enDefect_Black_Result_Max; nNum++)
		{
			m_wnd_Defect_BlackRst.Result_Display(0, nNum, pData->stDefect_Black.bDefect_BlackResult, pData->stDefect_Black.nDefect_BlackCount);
		}
	}
		break;

	case TI_ImgT_Fn_Defect_White:
	{
		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		for (int nNum = 0; nNum < enDefect_White_Result_Max; nNum++)
		{
			m_wnd_Defect_WhiteRst.Result_Display(0, nNum, pData->stDefect_White.bDefect_WhiteResult, pData->stDefect_White.nDefect_WhiteCount);
		}
	}
		break;

		case TI_ImgT_Fn_FOV:
	{
		  ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		  //for (int nNum = 0; nNum < enFOV_Result_Max; nNum++)
		 //  {
			   m_wnd_FOVRst.Result_Display(0, 0, pData->stFOV.bDFOV, pData->stFOV.dbDFOV);			//^^ SH2 1107 stFOV.dbFOV 값이 맞는지 확인필요
 			   m_wnd_FOVRst.Result_Display(0, 1, pData->stFOV.bHFOV, pData->stFOV.dbHFOV);
 			   m_wnd_FOVRst.Result_Display(0, 2, pData->stFOV.bVFOV, pData->stFOV.dbVFOV);
		 //  }
	}
		break;

		case TI_ImgT_Fn_Distortion:
	{
		  ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		  for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
		   {
			   m_wnd_DistortionRst.Result_Display(0, nNum, pData->stDistortion.bDistortion, pData->stDistortion.dbDistortion);
		   }
	}
		break;
	case TI_ImgT_Fn_DynamicBW:
	{

		ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		m_wnd_DynamicBWRst.Result_Display(0, 0, pData->stDynamicBW.bDynamic, pData->stDynamicBW.dbDynamic);
		m_wnd_DynamicBWRst.Result_Display(0, 1, pData->stDynamicBW.bSNR_BW, pData->stDynamicBW.dbSNR_BW);

	}
			break;
	case TI_ImgT_Fn_Shading:
	{
		   ST_IQ_Result *pData = (ST_IQ_Result *)pParam;
		   for (int nNum = 0; nNum < enShadingEachResultMax; nNum++)
		   {
			   m_wnd_ShadingRst.Result_Display(0, nNum, pData->stShading.bHorizonResult[nNum], pData->stShading.dbHorizonValue[nNum]);
			   m_wnd_ShadingRst.Result_Display(0, enShadingEachResultMax * 1 + nNum, pData->stShading.bVerticalResult[nNum], pData->stShading.dbVerticalValue[nNum]);
			   m_wnd_ShadingRst.Result_Display(0, enShadingEachResultMax * 2 + nNum, pData->stShading.bDiaAResult[nNum], pData->stShading.dbDiaAValue[nNum]);
			   m_wnd_ShadingRst.Result_Display(0, enShadingEachResultMax * 3 + nNum, pData->stShading.bDiaBResult[nNum], pData->stShading.dbDiaBValue[nNum]);
		   }
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 10:14
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SetClearTab()
{
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 10:14
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::OnLbnSelChangeTest()
{
	m_wnd_CurrentRst.ShowWindow(SW_HIDE);
	m_wnd_OpticalCenterRst.ShowWindow(SW_HIDE);
	m_wnd_SFRRst.ShowWindow(SW_HIDE);
	m_wnd_RotateRst.ShowWindow(SW_HIDE);
	m_wnd_YmeanRst.ShowWindow(SW_HIDE);
	m_wnd_BlackSpotRst.ShowWindow(SW_HIDE);
	m_wnd_LCBRst.ShowWindow(SW_HIDE);
	m_wnd_Defect_BlackRst.ShowWindow(SW_HIDE);
	m_wnd_Defect_WhiteRst.ShowWindow(SW_HIDE);
	m_wnd_FOVRst.ShowWindow(SW_HIDE);
	m_wnd_DistortionRst.ShowWindow(SW_HIDE);
	m_wnd_DynamicBWRst.ShowWindow(SW_HIDE);
	m_wnd_ShadingRst.ShowWindow(SW_HIDE);
// 	m_wnd_ActiveAlignRst.ShowWindow(SW_HIDE);
// 	m_wnd_TorqueRst.ShowWindow(SW_HIDE);

	int nIndex = m_cb_TestItem.GetCurSel();

	switch (m_iSelectTest[nIndex])
	{
	case TI_ImgT_Fn_ECurrent:
	{
		m_wnd_CurrentRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		m_wnd_OpticalCenterRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_SFR:
	{
		m_wnd_SFRRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		m_wnd_RotateRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Ymean:
	{
		m_wnd_YmeanRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_BlackSpot:
	{
		m_wnd_BlackSpotRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_LCB:
	{
		m_wnd_LCBRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Defect_Black:
	{
		m_wnd_Defect_BlackRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Defect_White:
	{
		m_wnd_Defect_WhiteRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
		m_wnd_FOVRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_Distortion:
	{
		m_wnd_DistortionRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_DynamicBW:
	{
		m_wnd_DynamicBWRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_ImgT_Fn_Shading:
	{
		 m_wnd_ShadingRst.ShowWindow(SW_SHOW);
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/3/9 - 10:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}