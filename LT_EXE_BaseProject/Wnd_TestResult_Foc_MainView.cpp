﻿// Wnd_TestResult_Foc_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_Foc_MainView.h"


// CWnd_TestResult_Foc_MainView
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_Foc_MainView, CWnd)

CWnd_TestResult_Foc_MainView::CWnd_TestResult_Foc_MainView()
{
	for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_Foc_MainView::~CWnd_TestResult_Foc_MainView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_Foc_MainView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_Foc_MainView 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_Foc_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_Foc_Main_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_Foc_Main_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	UINT nWndID = IDC_LIST_ITEM;


	m_Wnd_Rst_SFR.SetOwner(GetOwner());
	m_Wnd_Rst_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Ymean.SetOwner(GetOwner());
	m_Wnd_Rst_Ymean.Create(NULL, _T("Ymean"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_BlackSpot.SetOwner(GetOwner());
	m_Wnd_Rst_BlackSpot.Create(NULL, _T("BlackSpot"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Defect_Black.SetOwner(GetOwner());
	m_Wnd_Rst_Defect_Black.Create(NULL, _T("Defect_Black"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Defect_White.SetOwner(GetOwner());
	m_Wnd_Rst_Defect_White.Create(NULL, _T("Defect_White"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_LCB.SetOwner(GetOwner());
	m_Wnd_Rst_LCB.Create(NULL, _T("LCB"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_Rst_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Rotate.SetOwner(GetOwner());
	m_Wnd_Rst_Rotate.Create(NULL, _T("Rotate"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Current.SetOwner(GetOwner());
	m_Wnd_Rst_Current.Create(NULL, _T("Current"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_ActiveAlign.SetOwner(GetOwner());
	m_Wnd_Rst_ActiveAlign.Create(NULL, _T("ActiveAlign"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Torque.SetOwner(GetOwner());
	m_Wnd_Rst_Torque.Create(NULL, _T("Torque"), dwStyle, rectDummy, this, nWndID++);

	m_cb_TestItem.AddString(_T("SFR"));
	m_cb_TestItem.AddString(_T("Current"));

	m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_Black.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_White.ShowWindow(SW_HIDE);
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_OpticalCenter.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Rotate.ShowWindow(SW_HIDE);
	m_Wnd_Rst_ActiveAlign.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Torque.ShowWindow(SW_HIDE);;

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;
	int iHeightTemp	= iHeight / 9;

	m_st_Item[STI_TR_Foc_Main_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;
	
	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;

	m_Wnd_Rst_Current.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_SFR.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_BlackSpot.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Ymean.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Defect_Black.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Defect_White.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_LCB.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Rotate.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_ActiveAlign.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);

 	switch (m_iSelectTest[m_cb_TestItem.GetCurSel()])
 	{
 	case TI_Foc_Fn_PreFocus:
		m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Fn_ActiveAlign:
 		m_Wnd_Rst_ActiveAlign.MoveWindow(iLeft, iTop, iWidth, iHeightTemp * 3);
 		iTop += iHeightTemp * 3;
 
 		m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Fn_ECurrent:
 		m_Wnd_Rst_Current.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Fn_OpticalCenter:
		m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Fn_SFR:
 		m_Wnd_Rst_SFR.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Fn_Rotation:
		m_Wnd_Rst_Rotate.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
	case TI_Foc_Fn_BlackSpot:
		m_Wnd_Rst_BlackSpot.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_Ymean:
		m_Wnd_Rst_Ymean.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_Defect_Black:
		m_Wnd_Rst_Defect_Black.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_Defect_White:
		m_Wnd_Rst_Defect_White.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_LCB:
		m_Wnd_Rst_LCB.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

 	case TI_Foc_Re_TorqueCheck:
 		m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Motion_ReleaseScrew:
		m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	case TI_Foc_Motion_LockingScrew:
		m_Wnd_Rst_Torque.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
 		break;
 
 	default:
 		break;
 	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_Foc_MainView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}


//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::AllDataReset()
{

	m_Wnd_Rst_Current.Result_Reset();
	m_Wnd_Rst_SFR.Result_Reset();
	m_Wnd_Rst_BlackSpot.Result_Reset();
	m_Wnd_Rst_Ymean.Result_Reset();
	m_Wnd_Rst_Defect_Black.Result_Reset();
	m_Wnd_Rst_Defect_White.Result_Reset();
	m_Wnd_Rst_LCB.Result_Reset();
	m_Wnd_Rst_OpticalCenter.Result_Reset();
	m_Wnd_Rst_Rotate.Result_Reset();
	m_Wnd_Rst_ActiveAlign.Result_Reset();
	m_Wnd_Rst_Torque.DataReset();

}
//=============================================================================
// Method		: SetUIData_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 8:52
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUIData_Reset(int nIdx)
{

	switch (nIdx)
	{
	case TI_Foc_Fn_PreFocus:
		m_Wnd_Rst_OpticalCenter.Result_Reset();
		break;
	case TI_Foc_Fn_ECurrent:
	{
							   m_Wnd_Rst_Current.Result_Reset();
	}
		break;

	case TI_Foc_Fn_SFR:
	{
									m_Wnd_Rst_SFR.Result_Reset();
	}
		break;

	case TI_Foc_Fn_BlackSpot:
	{
								m_Wnd_Rst_BlackSpot.Result_Reset();
	}
		break;

	case TI_Foc_Fn_Ymean:
	{
							 m_Wnd_Rst_Ymean.Result_Reset();
	}
		break;


	case TI_Foc_Fn_Defect_Black:
	{
								  m_Wnd_Rst_Defect_Black.Result_Reset();
	}
		break;

	case TI_Foc_Fn_Defect_White:
	{
								  m_Wnd_Rst_Defect_White.Result_Reset();
	}
		break;

	case TI_Foc_Fn_LCB:
	{
								  m_Wnd_Rst_LCB.Result_Reset();
	}
		break;

	case TI_Foc_Fn_OpticalCenter:
	{
								  m_Wnd_Rst_OpticalCenter.Result_Reset();
	}
		break;

	case TI_Foc_Fn_Rotation:
	{
								  m_Wnd_Rst_Rotate.Result_Reset();
	}
		break;
	case TI_Foc_Fn_ActiveAlign:
	{
							   m_Wnd_Rst_ActiveAlign.Result_Reset();
	}
		break;
	case TI_Foc_Re_TorqueCheck:
	{							//!SH _180915: 검토 필요
								  m_Wnd_Rst_Torque.DataReset();
	}
		break;
	case TI_Foc_Motion_LockingScrew:
	{							//!SH _180915: 검토 필요
									   m_Wnd_Rst_Torque.DataReset();
	}
		break;
	case TI_Foc_Motion_ReleaseScrew:
	{							//!SH _180915: 검토 필요
									   m_Wnd_Rst_Torque.DataReset();
	}
		break;
	default:
		break;
	}

	SelectItem(nIdx);

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: __in int nIdx
// Parameter	: __in LPVOID pParam
// Parameter	: __in enFocus_AAView enView
// Qualifier	:
// Last Update	: 2018/3/9 - 10:01
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUIData(__in int nIdx, __in LPVOID pParam, __in enFocus_AAView enView)
{
	if (pParam == NULL)
		return;

	//for (int iIndx = 0; iIndx < m_cb_TestItem.GetCount(); iIndx++)
	//{
	//	if (nIdx == m_iSelectTest[iIndx])
	//	{
	//		SelectNum(iIndx);
	//		break;
	//	}
	//}

	switch (nIdx)
	{
	case TI_Foc_Fn_ECurrent:
	{
							   //!SH _180911: Current 관련 수정 필요
							  m_Wnd_Rst_Current.Result_Display(0, 0, ((ST_Foc_Result *)pParam)->stCurrent.bResult[0], ((ST_Foc_Result *)pParam)->stCurrent.dbValue[0]);

						
	}
		break;

	case TI_Foc_Fn_SFR:
	{

						  for (int nNum = 0; nNum < enSFR_Result_Max; nNum++)
						  {
							  m_Wnd_Rst_SFR.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stSFR.bResult[nNum], ((ST_Foc_Result *)pParam)->stSFR.dbValue[nNum]);
						  }

	}
		break;

	case TI_Foc_Fn_BlackSpot:
	{
								for (int nNum = 0; nNum < enBlackSpot_Result_Max; nNum++)
								{
									m_Wnd_Rst_BlackSpot.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stBlackSpot.bBlackSpotResult, ((ST_Foc_Result *)pParam)->stBlackSpot.nDefectCount);
								}
	}
		break;

	case TI_Foc_Fn_Ymean:
	{
							for (int nNum = 0; nNum < enYmean_Result_Max; nNum++)
							{
								m_Wnd_Rst_Ymean.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stYmean.bYmeanResult, ((ST_Foc_Result *)pParam)->stYmean.nDefectCount);
							}

	}
		break;
	

	case TI_Foc_Fn_Defect_Black:
	{
								   for (int nNum = 0; nNum < enDefect_Black_Result_Max; nNum++)
								   {
									   m_Wnd_Rst_Defect_Black.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stDefect_Black.bDefect_BlackResult, ((ST_Foc_Result *)pParam)->stDefect_Black.nDefect_BlackCount);
								   }
	}
		break;

	case TI_Foc_Fn_Defect_White:
	{
								   for (int nNum = 0; nNum < enDefect_White_Result_Max; nNum++)
								   {
									   m_Wnd_Rst_Defect_White.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stDefect_White.bDefect_WhiteResult, ((ST_Foc_Result *)pParam)->stDefect_White.nDefect_WhiteCount);
								   }

	}
		break;

	case TI_Foc_Fn_LCB:
	{
						  for (int nNum = 0; nNum < enLCB_Result_Max; nNum++)
						  {
							  m_Wnd_Rst_LCB.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stDefect_White.bDefect_WhiteResult, ((ST_Foc_Result *)pParam)->stDefect_White.nDefect_WhiteCount);
						  }
	}
		break;

	case TI_Foc_Fn_PreFocus:
	{
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 0, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultX, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultPosX);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 1, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultY, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultPosY);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 2, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultX, ((ST_Foc_Result *)pParam)->stOpticalCenter.iStandPosDevX);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 3, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultY, ((ST_Foc_Result *)pParam)->stOpticalCenter.iStandPosDevY);

	}
		break;

	case TI_Foc_Fn_OpticalCenter:
	{
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 0, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultX, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultPosX);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 1, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultY, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultPosY);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 2, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultX, ((ST_Foc_Result *)pParam)->stOpticalCenter.iStandPosDevX);
								m_Wnd_Rst_OpticalCenter.Result_Display(0, 3, ((ST_Foc_Result *)pParam)->stOpticalCenter.nResultY, ((ST_Foc_Result *)pParam)->stOpticalCenter.iStandPosDevY);

	}
		break;

	case TI_Foc_Fn_Rotation:
	{
							   for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
							   {
								   m_Wnd_Rst_Rotate.Result_Display(0, nNum, ((ST_Foc_Result *)pParam)->stRotate.bRotation, ((ST_Foc_Result *)pParam)->stRotate.dbRotation);
							   }

	}
		break;
	case TI_Foc_Fn_ActiveAlign:
	{

								  m_Wnd_Rst_ActiveAlign.Result_Display_OC(0, 0, ((ST_Foc_Result *)pParam)->stActiveAlign.nResultX, ((ST_Foc_Result *)pParam)->stActiveAlign.iOC_X);
								  m_Wnd_Rst_ActiveAlign.Result_Display_OC(0, 1, ((ST_Foc_Result *)pParam)->stActiveAlign.nResultY, ((ST_Foc_Result *)pParam)->stActiveAlign.iOC_Y);
								  m_Wnd_Rst_ActiveAlign.Result_Display_Rotate(0, 2, ((ST_Foc_Result *)pParam)->stActiveAlign.nResult, ((ST_Foc_Result *)pParam)->stActiveAlign.dbRoatae);
								  ST_Foc_Result *pData = (ST_Foc_Result *)pParam;
								  for (UINT nCH = 0; nCH < enTorData_MAX; nCH++)
								  {
									  m_Wnd_Rst_Torque.Result_Display(pData->stTorque.enStatus[nCH], nCH, pData->stTorque.dbValue[nCH], pData->stTorque.dbValue[Spec_Cnt_A + nCH]);
								  }
	}
		break;
	case TI_Foc_Re_TorqueCheck:
	{
								  ST_Foc_Result *pData = (ST_Foc_Result *)pParam;

								  for (UINT nCH = 0; nCH < enTorData_MAX; nCH++)
								  {
									  m_Wnd_Rst_Torque.Result_Display(pData->stTorque.enStatus[nCH], nCH, pData->stTorque.dbValue[nCH], pData->stTorque.dbValue[Spec_Cnt_A + nCH]);

								  }
	}
		break;

	case TI_Foc_Motion_ReleaseScrew:
	{
									   ST_Foc_Result *pData = (ST_Foc_Result *)pParam;

									   for (UINT nCH = 0; nCH < enTorData_MAX; nCH++)
									   {
										   m_Wnd_Rst_Torque.Result_Display(pData->stTorque.enStatus[nCH], nCH, pData->stTorque.dbValue[nCH], pData->stTorque.dbValue[Spec_Cnt_A + nCH]);

									   }
	}
		break;

	case TI_Foc_Motion_LockingScrew:
	{

									   ST_Foc_Result *pData = (ST_Foc_Result *)pParam;

									   for (UINT nCH = 0; nCH < enTorData_MAX; nCH++)
									   {
										   m_Wnd_Rst_Torque.Result_Display(pData->stTorque.enStatus[nCH], nCH, pData->stTorque.dbValue[nCH], pData->stTorque.dbValue[Spec_Cnt_A + nCH]);

									   }
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetClearTab()
{
	for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	if (m_cb_TestItem.GetCount() > 0)
	{
		m_cb_TestItem.ResetContent();
	}
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int & iItemCnt
// Qualifier	:
// Last Update	: 2018/3/9 - 9:03
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetAddTab(int nIdx, int &iItemCnt)
{
	// 첫 아이템 등록시 콤보 박스 초기화
	if (iItemCnt == 0)
	{
		m_cb_TestItem.ResetContent();
	}

	// 기존에 항목이 있으면 추가 하지 말고 지나가라.
	for (int iItem = 0; iItem < iItemCnt; iItem++)
	{
		if (nIdx == m_iSelectTest[iItem])
			return;
	}

	switch (nIdx)
  	{
	case TI_Foc_Fn_PreFocus:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("PreFocus"));
		break;
	case TI_Foc_Fn_ECurrent:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Current"));
		break;
	case TI_Foc_Fn_OpticalCenter:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("OpticalCenter"));
		break;
	case TI_Foc_Fn_Rotation:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Rotate"));
		break;
	case TI_Foc_Fn_SFR:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("SFR"));
		break;
	case TI_Foc_Fn_Ymean:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Ymean"));
		break;
	case TI_Foc_Fn_BlackSpot:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("BlackSpot"));
		break;
	case TI_Foc_Fn_LCB:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("LCB"));
		break;
	case TI_Foc_Fn_Defect_Black:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Defect_Black"));
		break;
	case TI_Foc_Fn_Defect_White:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Defect_White"));
		break;
	case TI_Foc_Fn_ActiveAlign:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("ActiveAlign"));
		break;
	case TI_Foc_Re_TorqueCheck:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;
	case TI_Foc_Motion_ReleaseScrew:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;
	case TI_Foc_Motion_LockingScrew:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;
  	default:
  		break;
  	}
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnLbnSelChangeTest()
{

	m_Wnd_Rst_Current.ShowWindow(SW_HIDE);
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_Black.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Defect_White.ShowWindow(SW_HIDE);
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_OpticalCenter.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Rotate.ShowWindow(SW_HIDE);
	m_Wnd_Rst_ActiveAlign.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Torque.ShowWindow(SW_HIDE);

	int iIdex = m_cb_TestItem.GetCurSel();

	switch (m_iSelectTest[iIdex])
	{
	case TI_Foc_Fn_PreFocus:
		m_Wnd_Rst_OpticalCenter.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_ECurrent:
	{
							  m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_SFR:
	{
									m_Wnd_Rst_SFR.ShowWindow(SW_SHOW);

	}
		break;

	case TI_Foc_Fn_BlackSpot:
	{
								m_Wnd_Rst_BlackSpot.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_Ymean:
	{
							 m_Wnd_Rst_Ymean.ShowWindow(SW_SHOW);
	}
		break;

	
	case TI_Foc_Fn_Defect_Black:
	{
								  m_Wnd_Rst_Defect_Black.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_Defect_White:
	{
								  m_Wnd_Rst_Defect_White.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_LCB:
	{
								  m_Wnd_Rst_LCB.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_OpticalCenter:
	{
								  m_Wnd_Rst_OpticalCenter.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_Rotation:
	{
								  m_Wnd_Rst_Rotate.ShowWindow(SW_SHOW);
	}
		break;

	case TI_Foc_Fn_ActiveAlign:
	{
							   m_Wnd_Rst_ActiveAlign.ShowWindow(SW_SHOW);
							   m_Wnd_Rst_Torque.ShowWindow(SW_SHOW);
	}
		break;
	case TI_Foc_Re_TorqueCheck:
								m_Wnd_Rst_Torque.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Motion_ReleaseScrew:
								m_Wnd_Rst_Torque.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Motion_LockingScrew:
								m_Wnd_Rst_Torque.ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
}

//=============================================================================
// Method		: SelectItem
// Access		: public  
// Returns		: void
// Parameter	: UINT nTestItem
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SelectItem(UINT nTestItem)
{
	int iSelect = 0;

	for (int nItem = 0; nItem < m_cb_TestItem.GetCount(); nItem++)
	{
		if (m_iSelectTest[nItem] == nTestItem)
		{
			iSelect = nItem;
			break;
		}
	}

	m_cb_TestItem.SetCurSel(iSelect);
	OnLbnSelChangeTest();
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10 
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}