﻿//*****************************************************************************
// Filename	: 	Wnd_MasterView.cpp
// Created	:	2016/10/29 - 16:48
// Modified	:	2016/10/29 - 16:48
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_MasterView.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_MasterView.h"


// CWnd_MasterView

IMPLEMENT_DYNAMIC(CWnd_MasterView, CWnd)

CWnd_MasterView::CWnd_MasterView()
{
	VERIFY(m_font_Default.CreateFont(
		32,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		12,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("굴림체")));			// lpszFacename

}

CWnd_MasterView::~CWnd_MasterView()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_MasterView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_MasterView message handlers

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/10/29 - 16:50
// Desc.		:
//=============================================================================
int CWnd_MasterView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Title.SetBorderTickness(3);
	m_st_Title.SetBackColor_COLORREF(RGB(5, 0, 160));
	m_st_Title.SetFont_Gdip(L"Arial", 32.0F);
	m_st_Title.Create(_T("Master Mode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_stCount.SetFont_Gdip(L"Arial", 100.0F);
	m_stCount.Create(_T("00"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 	m_lc_Error.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ERROR);
// 
// 	m_st_DescCap.Create(_T("Description"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 	m_ed_Desc.Create(dwStyle | WS_BORDER, rectDummy, this, IDC_ED_DESC);
// 
// 	m_bn_Delete.Create(_T("Delete Alarm Message"), dwStyle, rectDummy, this, IDC_BN_DELETE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/10/29 - 16:50
// Desc.		:
//=============================================================================
void CWnd_MasterView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = 0;
	int iTop = 0;
	int iWidth = cx - 0 - 0;
	int iHeight = cy - 0 - 0;
	int iCtrlWidth = 300;
	int iCtrlHeight = 50;
// 	int iSubTop = iTop + iCtrlHeight + iSpacing;
// 	int iSubLeft = iLeft;

	int iTempWidth = (iWidth - iCateSpacing) * 2 / 3;
	int iTempHeight = 60;
	m_st_Title.MoveWindow(iLeft, iTop, iWidth, iTempHeight);

	iTop += iTempHeight + iCateSpacing;
	int nH = iHeight - iTop;
	m_stCount.MoveWindow(iLeft, iTop, iWidth, nH);


// 	iTempHeight = iHeight - iTempHeight - iCateSpacing - iCateSpacing - iCtrlHeight;
// 	m_lc_Error.MoveWindow(iLeft, iTop, iTempWidth, iTempHeight);
// 
// 	iLeft += iTempWidth + iCateSpacing;
// 	iTempWidth = iWidth - iCateSpacing - iTempWidth;
// 	m_ed_Desc.MoveWindow(iLeft, iTop, iTempWidth, iTempHeight);
// 
// 	iLeft = 0;
// 	iTop += iTempHeight + iCateSpacing;
// 	m_bn_Delete.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/10/29 - 16:50
// Desc.		:
//=============================================================================
BOOL CWnd_MasterView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

void CWnd_MasterView::SetExitEvent(HANDLE hExitEvent)
{
	m_hExternalExitEvent = hExitEvent;

}

UINT WINAPI CWnd_MasterView::Thread_CounterMon(__in LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CWnd_MasterView* pThis = (CWnd_MasterView*)lParam;

	DWORD dwEvent = 0;
	pThis->m_nStartTime = GetTickCount();
	__try
	{
		pThis->m_bFlag_Counter = TRUE;
		while (pThis->m_bFlag_Counter)
		{
			// 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != pThis->m_hExternalExitEvent)
			{
				dwEvent = WaitForSingleObject(pThis->m_hExternalExitEvent, pThis->m_dwCounterMonCycle);

				switch (dwEvent)
				{
				case WAIT_OBJECT_0:	// Exit Program
					TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
					pThis->m_bFlag_Counter = FALSE;
					break;

				case WAIT_TIMEOUT:
					if (pThis->OnCounterCheck()){
						Sleep(pThis->m_dwCounterMonCycle);
					}
					else{
						break;
					}
					break;
				}
			}
			else
			{
				if (pThis->OnCounterCheck()){
					Sleep(pThis->m_dwCounterMonCycle);
				}
				else{
					break;
				}
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_MasterView::Thread_CounterMon()\n"));
	}

	TRACE(_T("쓰레드 종료 : CWnd_MasterView::Thread_CounterMon Loop Exit\n"));
	return TRUE;
}

BOOL CWnd_MasterView::OnCounterCheck()
{
	
	CString strData;

	UINT nEndTime = GetTickCount();
	UINT nGapTime = nEndTime - m_nStartTime;
	UINT nGap = nGapTime / 1000;
	UINT nTime = nCountTime - nGap;
	strData.Format(_T("%03d s"), nTime);

	m_stCount.SetText(strData);
	if (nGap >= nCountTime)
	{
		return FALSE;
	}

	if (nTime <= 20)
	{
		if (nTime > 10)
		{
			if (nTime % 2 == 0)
			{
				m_stCount.SetColorStyle(CVGStatic::ColorStyle_Yellow);
			}
			else{
				m_stCount.SetColorStyle(CVGStatic::ColorStyle_White);
			}
		}
		else{
			if (nTime % 2 == 0)
			{
				m_stCount.SetColorStyle(CVGStatic::ColorStyle_Red);
			}
			else{
				m_stCount.SetColorStyle(CVGStatic::ColorStyle_White);
			}
		}
	
	}


	return TRUE;
}

BOOL CWnd_MasterView::Start_CounterCheck_Mon()
{
	if (NULL != m_hThr_Counter)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Counter, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return FALSE;
		}
	}

	if (NULL != m_hThr_Counter)
	{
		CloseHandle(m_hThr_Counter);
		m_hThr_Counter = NULL;
	}

	
	m_hThr_Counter = HANDLE(_beginthreadex(NULL, 0, Thread_CounterMon, this, 0, NULL));

	return TRUE;
}
BOOL CWnd_MasterView::Stop_CounterCheck_Mon()
{
	m_hThr_Counter = FALSE;

	if (NULL != m_hThr_Counter)
	{
		WaitForSingleObject(m_hThr_Counter, m_dwCounterMonCycle);

		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Counter, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_Counter, dwExitCode);
			WaitForSingleObject(m_hThr_Counter, WAIT_ABANDONED);
			CloseHandle(m_hThr_Counter);
			m_hThr_Counter = NULL;
		}
	}

	return TRUE;
}