﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_2DCal.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_2DCal.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"
#include "CRC16.h"
#include "Def_EEPROM_Cm.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#if (SET_INSPECTOR == SYS_2D_CAL)
#ifdef _WIN64	// 64bit
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment (lib,"../x64/Unicode Debug/LT_Cal_Library_d.lib")
		#else
			#pragma comment (lib,"../x64/Debug/LT_Cal_Library_d.lib")
		#endif
	#else
		#ifdef _UNICODE
			#pragma comment (lib,"../x64/Unicode Release/LT_Cal_Library.lib")
		#else
			#pragma comment (lib,"../x64/Release/LT_Cal_Library.lib")
		#endif
	#endif
#endif
#endif

CTestManager_EQP_2DCal::CTestManager_EQP_2DCal()
{
	OnInitialize();
}

CTestManager_EQP_2DCal::~CTestManager_EQP_2DCal()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_2DCal >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_2DCal >>> \n"));	
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_2DCal::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();

	

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_LoadUnload(bLoad);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::StopProcess_Test_All()
{
	CTestManager_EQP::StopProcess_Test_All();

}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);

}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_2DCal::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;
	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);



	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);

	
}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);

	
}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)
	{
		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_All(nParaIdx);

 
}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);

	
}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnStart_Test_All(nParaIdx);
	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		// * 검사 시작 위치로 제품 이동
		if (g_InspectorTable[m_InspectionType].UseMotion)
		{
			lReturn = OnMotion_MoveToTestPos(nCamPara);
			if (RC_OK != lReturn)
			{
				// 에러 상황

				// 알람
				OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nCamPara]);
				//return lReturn;
			}
		}

		OnSetCamerParaSelect(nCamPara);

		for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
		{
			// 검사기에 맞추어 검사 시작
			lReturn = StartTest_Equipment(nCamPara);

			// * 검사 결과 판정
			nResult = Judgment_Inspection(nCamPara);

			if ((TR_Pass == nResult) || (TR_Check == nResult))
			{
				break;
			}
			else
			{
				// 측정 데이터, UI 초기화
			}
		}

		// * 검사 결과 판정 정보 세팅 및 UI표시
		OnSet_TestResult_Unit(nResult, nParaIdx);
		OnUpdate_TestReport(nParaIdx);
	}

	Judgment_Inspection_All();

	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}
	

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);


	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::Load_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Load_Product();

	

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();



	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 19:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_2D_CAL();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_2D_CAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/26 - 14:44
// Desc.		:
// [검사동작 시퀀스]
//	1. PC -> CAN 프로토콜 전송[2D Cal 모드 진입]
//	2. Y 축 움직여 거리별로 Instric Parameter 추출
//	   500mm -> 450 -> 400 -> 350 -> 300 -> 250(차트 +18도) -> 250(차트 0도) -> 250(차트 -18도)
//	   -> 180(+15도) -> 180(0도) -> 180(-15도) -> 180(+30도) -> 180(-30) -> 홈 복귀
//	3. 거리별로 추출된 Instric Parameter 정보 DLL(LGE측제공) 에 넣어 광축 값 추출
//	4. 양/불 판정
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartTest_2D_CAL(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 전진 7단계
	// Rotate 4단계
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;
	UINT nCornerStep = 0;

	//BOOL bInitialize = FALSE;	//m_stInspInfo.CamInfo[nParaIdx].bInitialize
	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		lReturn = RC_OK;

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// * 팔레트 전진/후진
		if ((TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Rot) || (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY) || (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveX))
		{
			lReturn = OnMotion_2DCAL_Test(pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot, nParaIdx);
			if (lReturn != RC_OK)
			{
				// ERROR
				TRACE("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)\r\n", pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
				OnLog_Err(_T("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)"), pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
			
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, TR_Check);
			}
		}

		// * 검사 항목 처리
		if (lReturn == RC_OK)
		{
			// * 검사 시작
			if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
			{
				if (OpMode_DryRun != m_stInspInfo.OperateMode)
				{
					if (TI_2D_Detect_CornerExt == pStepInfo->StepList[nStepIdx].nTestItem)
					{
						// ** 쓰레드로 함수 구동
						lReturn = StartTest_2D_CAL_TestItem(TI_2D_Detect_CornerExt, nStepIdx, nCornerStep++, nParaIdx);
					}
					else
					{
						lReturn = StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, 0, nParaIdx);
					}
				}
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		// 검사 중단 판별
		if (RC_OK != lReturn)
		{
			if ((TI_2D_Initialize_Test == pStepInfo->StepList[nStepIdx].nTestItem) || (TI_2D_Detect_CornerExt == pStepInfo->StepList[nStepIdx].nTestItem))
			{
				bError = TRUE;
				break;
			}
			else if (TI_2D_Exec_Intrinsic == pStepInfo->StepList[nStepIdx].nTestItem)
			{
				// EEPROM 기록??
				bError = TRUE;
				break;
			}
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		_TI_Cm_Finalize(0, nParaIdx);

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, nParaIdx);
	}

	return lReturn;
}

//=============================================================================
// Method		: StartTest_2D_CAL_WithThread
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/12 - 13:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartTest_2D_CAL_WithThread(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt	= m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime	= 0;
	UINT nCornerStep	= 0;
	BOOL bError			= FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		lReturn = RC_OK;

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// ** 검사 아이템 쓰레드가 구동 중이면 대기한다.
		Wait_FinishThead_TestItem();

		if ((TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Rot) || (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY) || (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveX))
		{
			lReturn = OnMotion_2DCAL_Test(pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot, nParaIdx);
			if (lReturn != RC_OK)
			{
				// ERROR
				TRACE("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)\r\n", pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
				OnLog_Err(_T("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)"), pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
			}
		}

		// * 검사 시작
		if ((TRUE == pStepInfo->StepList[nStepIdx].bTest) && (RC_OK == lReturn))
		{
			if (OpMode_DryRun != m_stInspInfo.OperateMode)
			{
				if (pStepInfo->StepList[nStepIdx].nTestItem == TI_2D_Detect_CornerExt)
				{
					// ** 쓰레드로 함수 구동
					lReturn = StartThread_TestItem(nParaIdx, TI_2D_Detect_CornerExt, nStepIdx, nCornerStep++);

					// ** Image Capture 끝났다는 이벤트 발생때까지 대기
					if (RC_OK == lReturn)
					{
						lReturn = Wait_Event_ReqCapture();
					}
				}
				else
				{
					lReturn = StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, 0, nParaIdx);
				}
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		// 검사 중단 판별
		if (RC_OK != lReturn)
		{
			if ((TI_2D_Initialize_Test == pStepInfo->StepList[nStepIdx].nTestItem) || (TI_2D_Detect_CornerExt == pStepInfo->StepList[nStepIdx].nTestItem))
			{
				bError = TRUE;
				break;
			}
			else if (TI_2D_Exec_Intrinsic == pStepInfo->StepList[nStepIdx].nTestItem)
			{
				// EEPROM 기록??
			}
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		_TI_Cm_Finalize(0, nParaIdx);

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, nParaIdx);
	}

	return lReturn;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
// 	if (TI_2D_Detect_CornerExt == nTestItemID)
// 	{
// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
// 	}
}

//=============================================================================
// Method		: StartTest_2D_CAL_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 16:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::StartTest_2D_CAL_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (nTestItemID)
	{
	case TI_2D_Initialize_Test:
		lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		break;

	case TI_2D_Finalize_Test:
		lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		break;

	case TI_2D_Set_CalibCornerParam:
		lReturn = _TI_2DCAL_SetParameter(nStepIdx, nParaIdx);
		break;

	case TI_2D_Detect_CornerExt:
		lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
		break;

	case TI_2D_Exec_Intrinsic:
		lReturn = _TI_2DCAL_ExecIntrinsic(nStepIdx, nParaIdx);
		break;

	case TI_2D_Re_KK0:
	case TI_2D_Re_KK1:
	case TI_2D_Re_KK2:
	case TI_2D_Re_KK3:
	case TI_2D_Re_Kc4:
	case TI_2D_Re_R2Max:
	case TI_2D_Re_RepError:
	case TI_2D_Re_EvalError:
	case TI_2D_Re_OrgOffset:
	case TI_2D_Re_DetectionFailureCnt:
	case TI_2D_Re_InvalidPointCnt:
	case TI_2D_Re_ValidPointCnt:
		// 	case TI_2D_Re_FocalLengthU:
		// 	case TI_2D_Re_FocalLengthV:
		// 	case TI_2D_Re_PrincipalPointU:
		// 	case TI_2D_Re_PrincipalPointV:
		// 	case TI_2D_Re_IntrinsicFov:
		// 	case TI_2D_Re_ReprojectionError:
		lReturn = _TI_2DCAL_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;

	case TI_2D_Write_EEPROM:
		lReturn = _TI_2DCAL_WriteEEPROM(nStepIdx, nParaIdx);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_2D_Initialize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_SetParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/10 - 13:03
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_SetParameter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// CAN Init가 되어 있어야 함 (Loading 루틴에서 처리)

	// 파라미터 전송	
	for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
	{
		lReturn = OnLib_2DCAL_SetParameter();

		if (RC_OK == lReturn)
			break;
	}

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/1 - 20:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_CaptureImage(__in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 영상 신호 체크
	if (OnDAQ_CheckVideoSignal(2000, nParaIdx))
	{
		// 이미지 캡쳐 (Request Capture)
		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);
		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);

		// 이미지 버퍼에 복사
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * sizeof(WORD));
		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * 3);
		}

		// 이미지 저장
		CString szPath;
		CString szImgFileName;
		CString szFullPath;

		if (1 < g_InspectorTable[m_InspectionType].Grabber_Cnt)
		{
			szImgFileName.Format(_T("2DCAL_%s_Step%02d"), g_szParaName[nParaIdx], nCornerStep);
			szPath.Format(_T("%s\\%s\\%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode, g_szParaName[nParaIdx]);
		}
		else
		{
			szImgFileName.Format(_T("2DCAL_Step%02d"), nCornerStep);
			szPath.Format(_T("%s\\%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode);
		}

		// /Left/모듈번호/
		MakeDirectory(szPath.GetBuffer(0));

		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			szFullPath.Format(_T("%s\\%s.png"), szPath, szImgFileName);
			OnSaveImage_Gray16(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
		}

 		if (m_stOption.Inspector.bSaveImage_RGB)
 		{
 			szFullPath.Format(_T("%s\\%s.bmp"), szPath, szImgFileName);
 			OnSaveImage_RGB(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
 		}

		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szImgFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}

		// 영상 캡쳐 이력에 추가
		CString szLabel;
		szLabel.Format(_T("%02d"), nCornerStep);
		OnImage_AddHistory(nParaIdx, szLabel);
	}
	else
	{
		lReturn = RC_Grab_Err_NoSignal;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_CornerExt
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/10 - 13:04
// Desc.		: 코너점 추출
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_CornerExt(__in UINT nStepIdx, __in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// Delay Time 필요한가??

	// 영상 신호 체크
	if (OnDAQ_CheckVideoSignal(1000, nParaIdx))
	{
		// 이미지 캡쳐 (Request Capture)
		lReturn = _TI_2DCAL_CaptureImage(nCornerStep, nParaIdx);

		// * 모터가 다음 스텝으로 이동가능하다는 이벤트 발생
// 		SetEvent_ReqCapture();
// 		Sleep(300);

		if (RC_OK == lReturn)
		{
			lReturn = OnLib_2DCAL_CornerExt(nCornerStep, m_stImageBuf[nParaIdx].lpwImage_16bit, nParaIdx);
		}
	}
	else
	{
		lReturn = RC_Grab_Err_NoSignal;
	}


	// * 측정값 입력 (오류 코너포인트 스텝)
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_2D_Detect_CornerExt].vt);
// 	varResult.intVal = lReturn;
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);


	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.CamInfo[nParaIdx].nErr_CornerPtStepNo = nCornerStep + 1;

		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_ExecIntrinsic
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/10 - 13:04
// Desc.		: LGE 라이브러리 이용하여 추출된 코너점으로 결과 계산
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_ExecIntrinsic(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nLibResult = 0;
	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	lReturn = OnLib_2DCAL_ExecIntrinsic(nParaIdx, nLibResult);

	// * 측정값 입력 (Library 오류 코드)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_2D_Exec_Intrinsic].vt);
	varResult.SetString(g_szResultCode_LG_CalIntr[nLibResult], VT_BSTR);
	m_stInspInfo.CamInfo[nParaIdx].nCAL_ResultCode = nLibResult;
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// * 스텝 결과
	if (RC_OK == lReturn)
	{
		// 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/10 - 13:05
// Desc.		: 재시도 필요 없음
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[nTestItem].vt);

	switch (nTestItem)
	{
	case TI_2D_Re_KK0://TI_2D_Re_FocalLengthU:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[0];
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_KK1://TI_2D_Re_FocalLengthV:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[1];
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_KK2://TI_2D_Re_PrincipalPointU:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[2];
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_KK3://TI_2D_Re_PrincipalPointV:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[3];
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_Kc4:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKc[4];
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_R2Max://TI_2D_Re_IntrinsicFov:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fR2Max;
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_RepError://TI_2D_Re_ReprojectionError:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fRepError;
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_EvalError:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fEvalError;
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_OrgOffset:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fOrgOffset;
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_DetectionFailureCnt:
	{
		varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nDetectionFailureCnt;		// Pass: = 0
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_InvalidPointCnt:
	{
		varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nInvalidPointCnt;
		VarArray.Add(varResult);
	}
	break;

	case TI_2D_Re_ValidPointCnt:
	{
		varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nValidPointCnt;
		VarArray.Add(varResult);
	}
	break;

	default:
		break;
	}

	// 측정값 입력 및 판정
	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

//=============================================================================
// Method		: _TI_2DCAL_WriteEEPROM
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/10 - 13:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::_TI_2DCAL_WriteEEPROM(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	if (OpMode_Production == m_stInspInfo.OperateMode) // 생산 모드인 경우에만 사용
	{
		UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

		for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
		{
			lReturn = OnDAQ_EEPROM_Write(nParaIdx);

			if (RC_OK == lReturn)
				break;
		}

		// * 측정값 입력 (EEPROM 오류 코드)
		enResultCode_EEPROM nResultCode = Conv_ResultToEEPROMResult((enResultCode)lReturn); //g_szResultCode_EEPROM[nResultCode];

		COleVariant varResult;
		varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_2D_Write_EEPROM].vt);
		//varResult.intVal = nResultCode;
		varResult.SetString(g_szResultCode_EEPROM[nResultCode], VT_BSTR);
		m_stInspInfo.CamInfo[nParaIdx].nEEPROM_Result = nResultCode;
		m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	}

	// * 스텝 결과
	if (RC_OK == lReturn)
	{
		// 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		OnLog_Err(_T("2D CAL EEPROM Error"));

		// 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLib_2DCAL_SetParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/7 - 21:02
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnLib_2DCAL_SetParameter()
{
	LRESULT lReturn = RC_OK;
#if (SET_INSPECTOR == SYS_2D_CAL)
	LGCalibIntrParam stParam;
	ST_2DCal_IntrParam* pOption = &m_stInspInfo.RecipeInfo.st2D_CAL.Parameter.IntrParam;

	stParam.nImgWidth						= pOption->nImgWidth;
	stParam.nImgHeight						= pOption->nImgHeight;
	stParam.nNumofCornerX					= pOption->nNumofCornerX;
	stParam.nNumofCornerY					= pOption->nNumofCornerY;
	stParam.fPatternSizeX					= pOption->fPatternSizeX;
	stParam.fPatternSizeY					= pOption->fPatternSizeY;
	stParam.fRepThres						= pOption->fRepThres;
	stParam.nMinNumImages					= pOption->nMinNumImages;
	stParam.nMinNumValidPnts				= pOption->nMinNumValidPnts;
	stParam.fMinOriginOffset				= pOption->fMinOriginOffset;
	stParam.fMaxOriginOffset				= pOption->fMaxOriginOffset;
	stParam.fMinFocalLength					= pOption->fMinFocalLength;
	stParam.fMaxFocalLength					= pOption->fMaxFocalLength;
	stParam.stCalibInfo_default.arfKK[0]	= pOption->stCalibInfo_default.arfKK[0];
	stParam.stCalibInfo_default.arfKK[1]	= pOption->stCalibInfo_default.arfKK[1];
	stParam.stCalibInfo_default.arfKK[2]	= pOption->stCalibInfo_default.arfKK[2];
	stParam.stCalibInfo_default.arfKK[3]	= pOption->stCalibInfo_default.arfKK[3];
	stParam.stCalibInfo_default.arfKc[0]	= pOption->stCalibInfo_default.arfKc[0];
	stParam.stCalibInfo_default.arfKc[1]	= pOption->stCalibInfo_default.arfKc[1];
	stParam.stCalibInfo_default.arfKc[2]	= pOption->stCalibInfo_default.arfKc[2];
	stParam.stCalibInfo_default.arfKc[3]	= pOption->stCalibInfo_default.arfKc[3];
	stParam.stCalibInfo_default.arfKc[4]	= pOption->stCalibInfo_default.arfKc[4];
	stParam.stCalibInfo_default.fR2Max		= pOption->stCalibInfo_default.fR2Max;
	stParam.fVersion						= pOption->fVersion;

	if (FALSE == m_Lib_CalIntr.Set_Parameter(&stParam))
	{
		lReturn = RC_LIB_2DCAL_SetParam;
		OnLog_Err(_T("2D CAL DLL : Error Set Parameter"));
	}
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnLib_2DCAL_CornerExt
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCornerStep
// Parameter	: __in const unsigned short * pwInImage
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/7 - 21:02
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnLib_2DCAL_CornerExt(__in UINT nCornerStep, __in const unsigned short* pwInImage, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
#if (SET_INSPECTOR == SYS_2D_CAL)
	char szOutData[1024] = { 0, 0 };
	CFile_Etc	fileEtc;
	fileEtc.SetPtrPath(m_stInspInfo.Path.szReport);

	LGCalibRoiParam	stROI;
	stROI.nU		= m_stInspInfo.RecipeInfo.st2D_CAL.Parameter.ROI[nCornerStep].nU;
	stROI.nV		= m_stInspInfo.RecipeInfo.st2D_CAL.Parameter.ROI[nCornerStep].nV;
	stROI.nWidth	= m_stInspInfo.RecipeInfo.st2D_CAL.Parameter.ROI[nCornerStep].nW;
	stROI.nHeight	= m_stInspInfo.RecipeInfo.st2D_CAL.Parameter.ROI[nCornerStep].nH;

	if (m_Lib_CalIntr.Get_ConerPoint(pwInImage, &stROI, (float*)szOutData))
	{
		memcpy(&m_stInspInfo.CamInfo[nParaIdx].st2D_CornerPt[nCornerStep], szOutData, sizeof(ST_2DCal_CornerPt));

		fileEtc.SaveCornerPt(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_CornerPt[nCornerStep], nCornerStep);
		fileEtc.SaveCornerPtBinFile(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_CornerPt[nCornerStep], nCornerStep);
	}
	else
	{
		lReturn = RC_LIB_2DCAL_CornerExt;
		OnLog_Err(_T("2D CAL DLL : Not found Cornet Point"));
	}
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnLib_2DCAL_ExecIntrinsic
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Parameter	: __out UINT & nReturCode
// Qualifier	:
// Last Update	: 2018/3/17 - 16:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnLib_2DCAL_ExecIntrinsic(__in UINT nParaIdx, __out UINT& nReturCode)
{
	LRESULT lReturn = RC_OK;
#if (SET_INSPECTOR == SYS_2D_CAL)
	UINT nTotalStepCnt = 13;
	UINT nLoadIdx[MAX_2DCAL_Position] = { 1, };
	memset(nLoadIdx, 1, sizeof(UINT) * MAX_2DCAL_Position);

	// * 측정된 코너 포인트 데이터들 설정
	for (int nIdxStep = 0; nIdxStep < MAX_2DCAL_Position; nIdxStep++)
	{
		m_Lib_CalIntr.SetCalCornerPtData((float*)&m_stInspInfo.CamInfo[nParaIdx].st2D_CornerPt[nIdxStep], nLoadIdx, nIdxStep);
	}

	// * 코너 포인트로 Intrinsic Calibration Result 추출
	nReturCode = m_Lib_CalIntr.SetCalResultExecute(nTotalStepCnt);
	if (eLGINTRMSG_NOERR == nReturCode)
	{
		LGCalibIntrResult	stOutResult;

		m_Lib_CalIntr.GetCalResultParam(&stOutResult);

		memcpy(&m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK,	&stOutResult.arfKK,		sizeof(float)* MAX_2DCAL_Re_KK);
		memcpy(&m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKc,	&stOutResult.arfKc,		sizeof(float)* MAX_2DCAL_Re_Kc);
		memcpy(&m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arcDate, &stOutResult.arcDate,	sizeof(unsigned char)* MAX_2DCAL_arcDate);

		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fDistFOV				= stOutResult.fDistFOV;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fR2Max				= stOutResult.fR2Max;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fRepError			= stOutResult.fRepError;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fEvalError			= stOutResult.fEvalError;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fOrgOffset			= stOutResult.fOrgOffset;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nDetectionFailureCnt = stOutResult.nDetectionFailureCnt;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nInvalidPointCnt		= stOutResult.nInvalidPointCnt;
		m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nValidPointCnt		= stOutResult.nValidPointCnt;
	}
	else
	{
		lReturn = RC_LIB_2DCAL_ExecIntrinsic;
		OnLog_Err(_T("2D CAL DLL : Error Exec Intrinsic"));
	}
#endif
	return lReturn;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_2D_CAL(nParaIdx);

		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		// MES 로그 남기기
		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);
		m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_SteCAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::MES_MakeInspecData_2D_CAL(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// MES 데이터 만들기
	//m_FileMES.Reset_ItemData();
	m_FileMES.Reset();

	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);
	

	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE	= TRUE;		// 판정

	// 	* KK_0
	NAME = _T("FLengthU");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[0]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_KK0, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* KK_1
	NAME = _T("FLengthV");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[1]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_KK1, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* KK_2
	NAME = _T("OCenterX]");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[2]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_KK2, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * KK_3
	NAME = _T("OCenterY");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKK[3]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_KK3, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Kc_0
	// 	* Kc_1
	// 	* Kc_2
	// 	* Kc_3
	// 	* Kc_4
	for (UINT nIdx = 0; nIdx <= 4; nIdx++)
	{
		NAME.Format(_T("arfKc[%d]"), nIdx);
		VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arfKc[nIdx]);

		if (4 == nIdx)
		{
			pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_Kc4, nParaIdx);
			if (NULL != pMeasItem)
			{
				JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
			}
			else
			{
				JUDGE = TRUE;
			}
		}
		else
		{
			JUDGE = TRUE;
		}

		m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	}

	// 	* Dist FOV
	NAME = _T("fDistFOV");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fDistFOV);
	JUDGE = TRUE;
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* R2 Max
//	NAME = _T("fR2Max");
//	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fR2Max);
//	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_R2Max);
//	if (NULL != pMeasItem)
//	{
//		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
//	}
//	else
//	{
//	 	JUDGE = TRUE;
//	}
//	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* RepError
	NAME = _T("fRepError");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fRepError);
	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_RepError, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* EvalError
	NAME = _T("fEvalError");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fEvalError);
	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_EvalError, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* OrgOffset
	NAME = _T("fOrgOffset");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.fOrgOffset);
	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_OrgOffset, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* DetectionFailureCnt
	NAME = _T("fDetectionFailureCnt");
	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nDetectionFailureCnt);
	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_DetectionFailureCnt, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* InvalidPointCnt
	NAME = _T("fInvalidPointCnt");
	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nInvalidPointCnt);
	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_InvalidPointCnt, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* ValidPointCn
// 	NAME = _T("fValidPointCnt");
// 	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nValidPointCnt);
// 	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Re_ValidPointCnt);
// 	if (NULL != pMeasItem)
// 	{
//		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
// 	}
// 	else
// 	{
// 		JUDGE = TRUE;
// 	}
//	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* date
	NAME = _T("arcdate");
	VALUE.Empty();
	JUDGE = TRUE;
	
	CStringA szTemp;
	for (UINT nCnt = 0; nCnt < 6; nCnt++)
	{
		szTemp.AppendChar(m_stInspInfo.CamInfo[nParaIdx].st2D_Result.arcDate[nCnt]);
	}

	USES_CONVERSION;
	VALUE = CA2T(szTemp.GetBuffer(0));
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);


	// * 카메라 초기화
// 	NAME = _T("Err Camera Init Code");
// 	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].nInitialize);
//	VALUE = g_szResultCode[m_stInspInfo.CamInfo[nParaIdx].nInitialize];
// 	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Initialize_Test, nParaIdx);
// 	if (NULL != pMeasItem)
// 	{
// 		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
// 	}
// 	else
// 	{
// 		JUDGE = TRUE;
// 	}
// 	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * 코너점 추출
// 	NAME = _T("Err Corner Point Step");
//	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].nErr_CornerPtStepNo);
// 	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Detect_CornerExt, nParaIdx);
// 	if (NULL != pMeasItem)
// 	{
// 		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
// 	}
// 	else
// 	{
// 		JUDGE = TRUE;
// 	}
// 	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Calibration
// 	NAME = _T("Err Calibration Code");
// 	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.n2DCAL_Result);
//	VALUE = g_szResultCode_LG_CalIntr[m_stInspInfo.CamInfo[nParaIdx].nCAL_ResultCode];
// 	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Exec_Intrinsic, nParaIdx);
// 	if (NULL != pMeasItem)
// 	{
// 		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
// 	}
// 	else
// 	{
// 		JUDGE = TRUE;
// 	}
// 	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * EEPROM Write
// 	NAME = _T("Err EEPROM Code");
// 	VALUE.Format(_T("%d"), m_stInspInfo.CamInfo[nParaIdx].st2D_Result.nEEPROM_Result);
//	VALUE = g_szResultCode_EEPROM[m_stInspInfo.CamInfo[nParaIdx].nEEPROM_Result];
// 	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_2D_Write_EEPROM, nParaIdx);
// 	if (NULL != pMeasItem)
// 	{
// 		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
// 	}
// 	else
// 	{
// 		JUDGE = TRUE;
// 	}
// 	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);


}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);
#ifndef MOTION_NOT_USE
	
	lReturn = m_MotionSequence.OnActionStandby_2D_CAL(m_stOption.Inspector.bUseBlindShutter);
	
	if (RC_OK != lReturn)
	{
		OnLog_Err(_T("Motion : Move to Test Position Error"));

		OnAddAlarm_F(_T("Motion : Move to Test Position Error"));
	}
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToUnloadPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/1 - 19:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnMotion_UnloadProduct();
#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionLoad_2D_CAL(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		OnLog_Err(_T("Motion : Unload Product Error"));

		OnAddAlarm_F(_T("Motion : Unload Product Error"));
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_2DCAL_Test
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceX
// Parameter	: __in double dbDistanceY
// Parameter	: __in double dbDegree
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/12 - 9:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_2DCAL_Test(__in double dbDistanceX, __in double dbDistanceY, __in double dbDegree, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// 500 -> 450 -> 400 -> 350 -> 300 -> 250(rot 25도) -> 180(rot 15도)

	// * 모터 점검

	// * 로딩된 팔레트가 있는가?

	// * 로딩된 팔레트 위치 체크

	// * 정해진 거리 위치로 팔레트 이동
	lReturn = m_MotionSequence.OnActionTesting(dbDistanceX, dbDistanceY, dbDegree);
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_2DCAL_Chart_Center
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in double dbDegree
// Qualifier	:
// Last Update	: 2017/11/15 - 3:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnMotion_2DCAL_Chart_Center(__in double dbDegree)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 모터 점검

	// * 각도 한계 체크

	// * 각도에 따른 차트 회전

	//	lReturn = m_MotionSequence.OnActionChart(dbDegree);
#endif	

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	//CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);
	
	OnDIn_DetectSignal_2DCAL(byBitOffset, bOnOff);


}

 //=============================================================================
 // Method		: OnDIn_DetectSignal_2DCAL
 // Access		: virtual protected  
 // Returns		: void
 // Parameter	: __in BYTE byBitOffset
 // Parameter	: __in BOOL bOnOff
 // Qualifier	:
 // Last Update	: 2018/2/13 - 17:07
 // Desc.		:
 //=============================================================================
 void CTestManager_EQP_2DCal::OnDIn_DetectSignal_2DCAL(__in BYTE byBitOffset, __in BOOL bOnOff)
 {
	 enDI_2DCal nDI_Signal = (enDI_2DCal)byBitOffset;

	 switch (nDI_Signal)
	 {
	 case DI_2D_00_MainPower:
		 if (FALSE == bOnOff)
		 {
			 OnDIn_MainPower();
		 }
		 break;

	 case DI_2D_01_EMO:
		 if (FALSE == bOnOff)
		 {
			 OnDIn_EMO();
		 }
		 break;

	 case DI_2D_02_AreaSensor:
		 if (FALSE == bOnOff)
		 {
			 OnDIn_AreaSensor();
		 }
		 break;

	 case DI_2D_03_DoorSensor:
		 if (FALSE == bOnOff)
		 {
			 OnDOut_FluorescentLamp(TRUE);

			 OnDIn_DoorSensor();
		 }
		 else
		 {
			 OnDOut_FluorescentLamp(FALSE);
		 }
		 break;

	 case DI_2D_04_JIG_CoverCheck:
		 if (bOnOff)
		 {
			 OnDIn_JIGCoverCheck();
		 }
		 break;

	 case DI_2D_06_Start:
	 {
		 if (bOnOff)
		 {
			 //StartOperation_AutoAll();
			 StartOperation_LoadUnload(TRUE);
		 }

	 }
	 break;

	 case DI_2D_07_Stop:
	 {
		 if (bOnOff)
		 {
			 StopProcess_Test_All();
		 }
	 }
	 break;

	 default:
		 break;
	 }
 }

void CTestManager_EQP_2DCal::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();

	AfxMessageBox(_T("I/O : Main Power Down  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();

	AfxMessageBox(_T("I/O : EMO Occurred  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();

	if (IsTesting())
		AfxMessageBox(_T("I/O : Detected Area Sesnor  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_DoorSensor();

	if (IsTesting())
		AfxMessageBox(_T("I/O : Door is Opened !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	// * 검사 진행 중 커버 열리면 오류 처리
	if (IsTesting())
	{
		if (!m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
		{
			lReturn = RC_DIO_Err_JIGCorverCheck;

			OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
			OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

			StopProcess_Test_All();

			AfxMessageBox(_T("I/O : Opened JIG Cover!!"), MB_SYSTEMMODAL);
		}
	}


	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Start
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/3 - 14:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();

	


	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Stop
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/3 - 14:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/3 - 14:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();
	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	if (m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
	{
		lReturn = RC_DIO_Err_JIGCorverCheck;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);



	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/31 - 10:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_12_TowerLamp_Red, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_13_TowerLamp_Yellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_14_TowerLamp_Green, enSignalType);
		break;
	case Lamp_All:
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_12_TowerLamp_Red, enSignalType);
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_13_TowerLamp_Yellow, enSignalType);
		m_Device.DigitalIOCtrl.Set_DO_Status(DO_2D_14_TowerLamp_Green, enSignalType);
		break;
	default:
		break;
	}

	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);



	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Front:
		lReturn = OnDAQ_EEPROM_OMS_Front_2D(nIdxBrd);
		break;

 	default:
		break;
 	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_2D
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/9 - 14:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDAQ_EEPROM_OMS_Entry_2D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bReturn = TRUE;

	// Image board ID	: (address : 0x0000) 16Byte, LGIT part no (입고시 label에 쓰여지는 정보) 
	// ADDI0933 ID		: (address : 0x0010) 16Byte, chip version 정보 (chip version 변경시 갱신)
	// Sensor ID		: (address : 0x0020) 16Byte, sensor version. 정보 (sensor version 변경시 갱신)
	// Optical Center	: (address : 0x0040) x, y 4Byte * 2, 2D cal output 의 Cu, Cv값 사용
	// Checksum1		: (address : 0x00FA) 1Byte, (0x0000-0x002F) % 255

	//0x56 << 1
	CStringA szTempID = "0000000000000001";
	CStringA szTempBarcode;

	BYTE arbyBufRead[256] = { 0, };
	BYTE arbyBufWrite[256] = { 0, };

	// * Read :  Max 256 Byte -------------------------------------------------
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, 0xAC, 1, 0x00, 252, arbyBufRead))
	{
		TRACE(_T("Error : EEPROM First Read Failed\n"));
		OnLog_Err(_T("Error : EEPROM First Read Failed"));
		return RC_EEPROM_Err_Read;
	}

// 	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, 0xAC, 1, 0xFC, 4, &arbyBufRead[0xFC]))
// 	{
// 		TRACE(_T("EEPROM Read Failed : First Read Failed"));
// 		return RC_EEPROM_Err_Read;
// 	}

	// * 데이터 세팅 -----------------------------------------------------------
	memcpy(arbyBufWrite, arbyBufRead, 256);

	// 바코드
	USES_CONVERSION;
	szTempBarcode = CT2A(m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetBuffer(0));	// 16글자
	UINT nCount = szTempBarcode.GetLength();
	nCount = (16 < nCount) ? 16 : nCount;
	UINT nSN_Offset = 16 - nCount;
	//szTempBarcode += "0000000000000000";

	//memcpy(&arbyBufWrite[0x00], szTempBarcode.GetBuffer(0), 16);
	for (UINT nIdx = 0; nIdx < nSN_Offset; nIdx++)
	{
		arbyBufWrite[nIdx] = 0x00;
	}
	memcpy(&arbyBufWrite[0x00 + nSN_Offset], szTempBarcode.GetBuffer(0), nCount);
	memcpy(&arbyBufWrite[0x10], szTempID.GetBuffer(0), 16);
	memcpy(&arbyBufWrite[0x20], szTempID.GetBuffer(0), 16);

	memcpy(&arbyBufWrite[0x40], &m_stInspInfo.CamInfo[nIdxBrd].st2D_Result.arfKK[2], 4);
	memcpy(&arbyBufWrite[0x44], &m_stInspInfo.CamInfo[nIdxBrd].st2D_Result.arfKK[3], 4);

	// * CRC 체크
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufWrite, 0xFA);
	BYTE Buff[2] = { crc_uint16 & 0xff, crc_uint16 / 0x100 };
	arbyBufWrite[0xFA] = Buff[0];
	arbyBufWrite[0xFB] = Buff[1];

	// * CRC 체크 2
	crc_uint16 = 0;
	crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufWrite, 0xFC);
	Buff[0] = crc_uint16 & 0xff;
	Buff[1] = crc_uint16 / 0x100;
	arbyBufWrite[0xFC] = Buff[0];
	arbyBufWrite[0xFD] = Buff[1];
// 	if (crc_uint16 != 0x0000)
// 	{
// 		TRACE(_T("Error : EEPROM read Failed_ Second CheckSum\n"));
// 		OnLog_Err(_T("Error : EEPROM read Failed_ Second CheckSum"), Para_Left);
// 		return RC_EEPROM_Err_Read;
// 	}
	//arbyBufWrite[0xFC] = 0x00;
	//arbyBufWrite[0xFD] = 0x00;

	// * Mode
	//arbyBufWrite[0xFE] = 0x00;
	//arbyBufWrite[0xFF] = 0x00;


	// * Write : Max 1024 Byte ------------------------------------------------
	//Sleep(100);
	// Image board ID
	bReturn = m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0x00, 16, &arbyBufWrite[0x00]);
	Sleep(100);

	// ADDI0933 ID	
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0x10, 16, &arbyBufWrite[0x10]);
	Sleep(100);

	// Sensor ID	
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0x20, 16, &arbyBufWrite[0x20]);
	Sleep(100);

	// Optical Center
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0x40, 8, &arbyBufWrite[0x40]);
	Sleep(100);

	// CRC
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0xFA, 2, &arbyBufWrite[0xFA]);
	Sleep(100);

	// CRC 2
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0xFC, 2, &arbyBufWrite[0xFC]);
 	Sleep(100);
	
	// Mode
	//bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, 0xAC, 1, 0xFE, 2, &arbyBufWrite[0xFE]);
	//Sleep(100);

	if (FALSE == bReturn)
	{
		TRACE(_T("Error : EEPROM Write Failed\n"));
		OnLog_Err(_T("Error : EEPROM Write Failed"), Para_Left);
		return RC_EEPROM_Err_Write;
	}

	
	// * 카메라 리부팅 ---------------------------------------------------------
	lReturn = _TI_Cm_CameraReboot(TRUE, Para_Left);
	if (RC_OK != lReturn)
	{
		TRACE(_T("Error : Camera Reboot Failed [CH_%02d]\n"), Para_Left);
		OnLog_Err(_T("Error : Camera Reboot Failed [CH_%02d]"), Para_Left);
		//return lReturn;
		return RC_EEPROM_Err_Reboot;
	}

	// * Read -----------------------------------------------------------------
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, 0xAC, 1, 0x00, 0xFE, arbyBufRead))
	//if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, 0xAC, 1, 0x00, 0xFC, arbyBufRead))
	{
		TRACE(_T("Error : EEPROM Read Failed (Verify Data)\n"));
		OnLog_Err(_T("Error : EEPROM Read Failed (Verify Data)"));
		return RC_EEPROM_Err_Read;
	}

	// * Verify ( 0 ~ checksum2 까지) -----------------------------------------
	BOOL bVerify = TRUE;
	for (UINT nIdx = 0; nIdx < 0xFE; nIdx++)
	//for (UINT nIdx = 0; nIdx < 0xFC; nIdx++)
	{
		if (arbyBufWrite[nIdx] != arbyBufRead[nIdx])
		{
			bVerify = FALSE;
			lReturn = RC_EEPROM_Err_Verify;
			TRACE(_T("Error : EEPROM Verify Failed ( Idx[%02X] - Write : %02X, Read : %02X)\n"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			OnLog_Err(_T("Error : EEPROM Verify Failed ( Idx[%02X] - Write : %02X, Read : %02X)"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			break;
		}
	}

	if (bVerify)
	{
		TRACE(_T("EEPROM Verify Succeed!\n"));
		OnLog(_T("EEPROM Verify Succeed!"));
	}

	// 	float fOC_X = 0.0f;
	// 	float fOC_Y = 0.0f;
	// 	memcpy (&fOC_X, &arbyBufRead[0x40], 4);
	// 	memcpy (&fOC_Y, &arbyBufRead[0x44], 4);

	return lReturn;
}

LRESULT CTestManager_EQP_2DCal::OnDAQ_EEPROM_OMS_Entry_2D_V2(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bReturn = TRUE;

	// Image board ID	: (address : 0x0000) 16Byte, LGIT part no (입고시 label에 쓰여지는 정보) 
	// ADDI0933 ID		: (address : 0x0010) 16Byte, chip version 정보 (chip version 변경시 갱신)
	// Sensor ID		: (address : 0x0020) 16Byte, sensor version. 정보 (sensor version 변경시 갱신)
	// Optical Center	: (address : 0x0040) x, y 4Byte * 2, 2D cal output 의 Cu, Cv값 사용
	// Checksum1		: (address : 0x00FA) 1Byte, (0x0000-0x002F) % 255

	//0x56 << 1
	CStringA szTempID = "0000000000000001";
	CStringA szTempBarcode;

	BYTE arbyBufRead[256] = { 0, };
	BYTE arbyBufWrite[256] = { 0, };


	// * Read :  Max 256 Byte
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, 0x00, 0xFC, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : First Read Failed"));
		return RC_EEPROM_Err_Read;
	}

	// * 데이터 세팅
	memcpy(arbyBufWrite, arbyBufRead, 256);

	// 바코드
	USES_CONVERSION;
	szTempBarcode = CT2A(m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetBuffer(0));	// 16글자
	UINT nCount = szTempBarcode.GetLength();
	nCount = (g_E2ROM_Entry[E2RI_SN].DataLength < nCount) ? g_E2ROM_Entry[E2RI_SN].DataLength : nCount;
	UINT nSN_Offset = g_E2ROM_Entry[E2RI_SN].DataLength - nCount;
	
	for (UINT nIdx = 0; nIdx < nSN_Offset; nIdx++)
	{
		arbyBufWrite[nIdx] = 0x00;
	}
	memcpy(&arbyBufWrite[g_E2ROM_Entry[E2RI_SN].Address + nSN_Offset], szTempBarcode.GetBuffer(0), nCount);

	memcpy(&arbyBufWrite[g_E2ROM_Entry[E2RI_ADDI_ID].Address],		szTempID.GetBuffer(0),		g_E2ROM_Entry[E2RI_ADDI_ID].DataLength);
	memcpy(&arbyBufWrite[g_E2ROM_Entry[E2RI_Sensor_ID].Address],	szTempID.GetBuffer(0),		g_E2ROM_Entry[E2RI_Sensor_ID].DataLength);

	memcpy(&arbyBufWrite[g_E2ROM_Entry[E2RI_OC_X].Address], &m_stInspInfo.CamInfo[nIdxBrd].st2D_Result.arfKK[2], g_E2ROM_Entry[E2RI_OC_X].DataLength);
	memcpy(&arbyBufWrite[g_E2ROM_Entry[E2RI_OC_Y].Address], &m_stInspInfo.CamInfo[nIdxBrd].st2D_Result.arfKK[3], g_E2ROM_Entry[E2RI_OC_Y].DataLength);

	// * CRC 체크
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufWrite, g_E2ROM_Entry[E2RI_Checksum_1].Address);
	BYTE Buff[2] = { crc_uint16 & 0xff, crc_uint16 / 0x100 };
	arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_1].Address]		= Buff[0];
	arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_1].Address + 1]	= Buff[1];

	// * CRC 체크 2
	crc_uint16 = 0;
	crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufWrite, 0xFC);
	Buff[0] = crc_uint16 & 0xff;
	Buff[1] = crc_uint16 / 0x100;
	arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_2].Address]		= Buff[0];
	arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_2].Address + 1]	= Buff[1];
	//arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_2].Address]		= 0x00;
	//arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_2].Address + 1]	= 0x00;

	// * Write : Max 1024 Byte
	//Sleep(100);
	// Image board ID
	bReturn = m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_SN].Address, g_E2ROM_Entry[E2RI_SN].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_SN].Address]);
	Sleep(100);

	// ADDI0933 ID
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_ADDI_ID].Address, g_E2ROM_Entry[E2RI_ADDI_ID].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_ADDI_ID].Address]);
	Sleep(100);

	// Sensor ID
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_Sensor_ID].Address, g_E2ROM_Entry[E2RI_Sensor_ID].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_Sensor_ID].Address]);
	Sleep(100);

	// Optical Center
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_OC_X].Address, g_E2ROM_Entry[E2RI_OC_X].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_OC_X].Address]);
	Sleep(100);
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_OC_Y].Address, g_E2ROM_Entry[E2RI_OC_Y].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_OC_Y].Address]);
	Sleep(100);

	// CRC 1
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_Checksum_1].Address, g_E2ROM_Entry[E2RI_Checksum_1].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_1].Address]);
	Sleep(100);

	// CRC 2
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, g_E2ROM_Entry[E2RI_Checksum_2].Address, g_E2ROM_Entry[E2RI_Checksum_2].DataLength, &arbyBufWrite[g_E2ROM_Entry[E2RI_Checksum_2].Address]);
	Sleep(100);

	if (FALSE == bReturn)
	{
		TRACE(_T("EEPROM Write Failed : "));
		return RC_EEPROM_Err_Write;
	}

	// * 카메라 리부팅 ---------------------------------------------------------
	lReturn = _TI_Cm_CameraReboot(TRUE, Para_Left);
	if (RC_OK != lReturn)
	{
		TRACE(_T("Error : Camera Reboot Failed [CH_%02d]\n"), Para_Left);
		OnLog_Err(_T("Error : Camera Reboot Failed [CH_%02d]"), Para_Left);
		//return lReturn;
		return RC_EEPROM_Err_Reboot;
	}

	// * Read
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, 0x00, 0xFE, arbyBufRead))
	//if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, EEPROM_SlaveAddr_OMS_Entry, 1, 0x00, 0xFC, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : Verify Data Read Failed"));
		return RC_EEPROM_Err_Read;
	}

	// * Verify ( 0 ~ checksum1 까지)
	BOOL bVerify = TRUE;
	for (UINT nIdx = 0; nIdx < 0xFE; nIdx++)
	//for (UINT nIdx = 0; nIdx < 0xFC; nIdx++)
	{
		if (arbyBufWrite[nIdx] != arbyBufRead[nIdx])
		{
			bVerify = FALSE;
			lReturn = RC_EEPROM_Err_Verify;
			TRACE(_T("EEPROM Verify Failed : Idx[%02X] - Write : %02X, Read : %02X"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			break;
		}
	}

	// 	float fOC_X = 0.0f;
	// 	float fOC_Y = 0.0f;
	// 	memcpy (&fOC_X, &arbyBufRead[0x40], 4);
	// 	memcpy (&fOC_Y, &arbyBufRead[0x44], 4);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Front_2D
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/13 - 17:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnDAQ_EEPROM_OMS_Front_2D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);



	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
    LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

         
	// * 검사 시작
	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
	{
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
		}
	}

    return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_2DCal::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();



	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;

	//fileReport.SaveFinalizeResult2DCal(m_stInspInfo.Path.szReport, nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, &m_stInspInfo.CamInfo[nParaIdx].st2D_Result);
	fileReport.SaveFinalizeResult2DCal(m_stInspInfo.Path.szReport, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx]);
	
}

//=============================================================================
// Method		: OnJugdement_And_Report_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/5 - 17:02
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_2DCal::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

void CTestManager_EQP_2DCal::DeleteTimer_UpdateUI()
{
// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_2DCal::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnSaveWorklist()
{
// 	m_stInspInfo.WorklistInfo.Reset();
// 
// 	ST_CamInfo* pstCam = NULL;
// 
// 	CString szText;
// 	CString szTime;
// 	SYSTEMTIME lcTime;
// 
// 	GetLocalTime(&lcTime);
// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
// 
// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
// 	{
// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
// 			continue;
// 
// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
// 
// 		m_stInspInfo.WorklistInfo.Time = szTime;
// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
// 		m_stInspInfo.WorklistInfo.SWVersion;
// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
// 
// 		szText.Format(_T("%d"), nChIdx + 1);
// 		m_stInspInfo.WorklistInfo.Socket = szText;
// 
// 
// 		m_stInspInfo.WorklistInfo.MakeItemz();
// 
// 		// 파일 저장
// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
// 
// 		// UI 표시
// 		OnInsertWorklist();
// 
// 	}// End of for
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnInitialize()
{
	CTestManager_EQP::OnInitialize();

}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_2DCal::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);


}
