﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Model_h__
#define Wnd_Cfg_Model_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_Recipe.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Model
//-----------------------------------------------------------------------------
class CWnd_Cfg_Model : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Model)

public:
	CWnd_Cfg_Model();
	virtual ~CWnd_Cfg_Model();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnCbnSelendokModelType	();

	CFont			m_font_Default;

	CVGStatic		m_st_ModelCode;
	//CEdit			m_ed_ModelCode;
	CComboBox		m_cb_ModelType;

	// 전체 검사 불량 시 재시도 횟수
	CVGStatic		m_st_Test_RetryCnt;
	CComboBox		m_cb_Test_RetryCnt;

	CVGStatic		m_st_Exposure;
	CMFCMaskedEdit	m_ed_Exposure;

	//-카메라 렌즈 특성
	CVGStatic		m_st_FocalLength;
	CMFCMaskedEdit	m_ed_FocalLength;

	CVGStatic		m_st_PixelSize;
	CMFCMaskedEdit	m_ed_PixelSize;

	CVGStatic		m_st_LightDelay;
	CMFCMaskedEdit	m_ed_LightDelay;

	CVGStatic		m_st_Visel_LED;
	CMFCMaskedEdit	m_ed_Visel_LED;

	CVGStatic		m_st_CamImageType;
	CComboBox		m_cb_CamImageType;

	CVGStatic		m_st_ChangeDelay;
	CMFCMaskedEdit	m_ed_ChangeDelay;

	CVGStatic		m_st_LEDRMSCycle;
	CMFCMaskedEdit	m_ed_LEDRMSCycle;

	// 전체 검사 불량 시 Calibration Flag 변경 후 재 측정 기능 사용 여부
	CVGStatic		m_st_UseStereoCal_Flag;
	CComboBox		m_cb_UseStereoCal_Flag;


	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_2D_Cal;

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_RecipeInfo& stRecipeInfo);
	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_RecipeInfo* pRecipeInfo);

public:
	
	void		SetSystemType		(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	}

	// 모델 데이터를 UI에 표시
	void		SetRecipeInfo		(__in const ST_RecipeInfo* pRecipeInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetRecipeInfo		(__out ST_RecipeInfo& stRecipeInfo);

};

#endif // Wnd_Cfg_Model_h__


