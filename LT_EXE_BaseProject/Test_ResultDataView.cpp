#include "stdafx.h"
#include "Test_ResultDataView.h"


CTest_ResultDataView::CTest_ResultDataView()
{
}


CTest_ResultDataView::~CTest_ResultDataView()
{
}

//=============================================================================
// Method		: TestResultView_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Parameter	: __in LPVOID pParam
// Parameter	: __in enFocus_AAView enItem
// Qualifier	:
// Last Update	: 2018/3/9 - 10:06
// Desc.		:
//=============================================================================
void CTest_ResultDataView::TestResultView_Foc(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam, __in enFocus_AAView enItem)
{
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
		return;

	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_Foc[nPara].SetUIData(nTestID, pParam, enItem);
	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		if (Foc_AA_Torque == enItem)
			return;

		m_pRecipeView->m_wnd_TestResult_Foc[nPara].SetUIData(nTestID, pParam);
	}
}

void CTest_ResultDataView::TestResultView_ImgT(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam)
{
	//!SH _181106: 내용넣기
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
		return;

	//m_pRecipeView->m_wnd_TestResult_ImgT[nPara].SetUIData(nTestID, pParam);
	if (m_pMainView == NULL || m_pRecipeView == NULL)
		return;

	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_ImgT[nPara].SetUIData(nTestID, pParam);//, enItem);
	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		m_pRecipeView->m_wnd_TestResult_ImgT[nPara].SetUIData(nTestID, pParam);
	}
	
}
//=============================================================================
// Method		: TestResultView_AllReset_Foc
// Access		: public  
// Returns		: void
// Parameter	: UINT nPara
// Qualifier	:
// Last Update	: 2018/3/19 - 11:06
// Desc.		:
//=============================================================================
void CTest_ResultDataView::TestResultView_AllReset_Foc(UINT nPara)
{
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
	{
		return;
	}
	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_Foc[nPara].AllDataReset();

	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		m_pRecipeView->m_wnd_TestResult_Foc[nPara].AllDataReset();
	}
}
//=============================================================================
// Method		: TestResultView_AllReset_ImgT
// Access		: public  
// Returns		: void
// Parameter	: UINT nPara
// Qualifier	:
// Last Update	: 2018/3/19 - 11:06
// Desc.		:
//=============================================================================
void CTest_ResultDataView::TestResultView_AllReset_ImgT(UINT nPara)
{
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
	{
		return;
	}
	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_ImgT[nPara].AllDataReset();

	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		m_pRecipeView->m_wnd_TestResult_ImgT[nPara].AllDataReset();
	}
}
//=============================================================================
// Method		: TestResultView_Reset_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Qualifier	:
// Last Update	: 2018/3/9 - 10:06
// Desc.		:
//=============================================================================
void CTest_ResultDataView::TestResultView_Reset_Foc(__in UINT nTestID, __in UINT nPara)
{
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
	{
		return;
	}
	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_Foc[nPara].SetUIData_Reset(nTestID);

	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		m_pRecipeView->m_wnd_TestResult_Foc[nPara].SetUIData_Reset(nTestID);
	}
}
//=============================================================================
// Method		: TestResultView_Reset_ImgT
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Qualifier	:
// Last Update	: 2018/3/9 - 10:06
// Desc.		:
//=============================================================================
void CTest_ResultDataView::TestResultView_Reset_ImgT(__in UINT nTestID, __in UINT nPara)
{
	//Auto 창 켜져 있을 시 Main
	//Recipe창 켜져 있을 시 Recipe

	if (m_pMainView == NULL || m_pRecipeView == NULL)
	{
		return;
	}
	if (m_pMainView->IsWindowVisible())
	{
		m_pMainView->m_wnd_TestResult_ImgT[nPara].SetUIData_Reset(nTestID);

	}
	else if (m_pRecipeView->IsWindowVisible())
	{
		m_pRecipeView->m_wnd_TestResult_ImgT[nPara].SetUIData_Reset(nTestID);
	}
}
