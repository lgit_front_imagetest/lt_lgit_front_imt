﻿#ifndef TestManager_TestMES_h__
#define TestManager_TestMES_h__

#pragma once

#include <windows.h>

#include "Def_DataStruct.h"
#include "File_Mes.h"
#include "Def_Mes_Foc.h"
#include "Def_Mes_ImgT.h"
#include "Def_Mes_EachTest.h"


class CTestManager_TestMES 
{
public:
	CTestManager_TestMES();
	virtual ~CTestManager_TestMES();

	//-모든 정보
	ST_InspectionInfo *m_pInspInfo;

	void	SetPtr_InspInfo(ST_InspectionInfo* pstInfo)
	{
		if (pstInfo == NULL)
			return;

		m_pInspInfo = pstInfo;
	};

	//전류
	void Current_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Current_DataSave_ImgT		(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);

	//광축
	void OpticalCenter_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void OpticalCenter_DataSave_ImgT(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);

	//SFR
	void SFR_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void SFR_DataSave_ImgT			(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	
	//Rotate
	void Rotate_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Rotate_DataSave_ImgT		(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	
	//Ymean
	void Ymean_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Ymean_DataSave_ImgT		(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);

	//BlackSpot
	void BlackSpot_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void BlackSpot_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//LCB
	void LCB_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void LCB_DataSave_ImgT			(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//Defect_Black
	void Defect_Black_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Defect_Black_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//Defect_White
	void Defect_White_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Defect_White_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//Stain
	void Stain_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Stain_DataSave_ImgT		(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//DefectPixel
	void Defectpixel_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	void Defectpixel_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//FOV
	void FOV_DataSave_ImgT			(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData); 
	//Distortion
	void Distortion_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//DynamicBW
	void DynamicBW_DataSave_ImgT		(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//Shading
	void Shading_DataSave_ImgT	(__in UINT nParaIdx, __in ST_MES_Data_ImgT *pMesData);
	//Torque
	void Torque_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);

	//-개별 저장 
	void Current_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void OpticalCenter_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void SFR_Each_DataSave				(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Rotate_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Stain_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Shading_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void RI_Each_DataSave				(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Ymean_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void BlackSpot_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void LCB_Each_DataSave				(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Defect_Black_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Defect_White_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void FOV_Each_DataSave				(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Distortion_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DynamicBW_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
};

#endif // TestManager_TestMES_h__
