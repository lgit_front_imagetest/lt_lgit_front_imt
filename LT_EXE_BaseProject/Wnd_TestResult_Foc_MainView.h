﻿#ifndef Wnd_TestResult_Foc_MainView_h__
#define Wnd_TestResult_Foc_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"
#include "Def_Enum.h"
#include "Def_DataStruct.h"

#include "Wnd_Rst_SFR.h"
#include "Wnd_Rst_BlackSpot.h"
#include "Wnd_Rst_Current.h"
#include "Wnd_Rst_Defect_Black.h"
#include "Wnd_Rst_Defect_White.h"
#include "Wnd_Rst_LCB.h"
#include "Wnd_Rst_OpticalCenter.h"
#include "Wnd_Rst_Rotate.h"
#include "Wnd_Rst_Ymean.h"
#include "Wnd_Rst_ActiveAlign.h"
#include "Wnd_Rst_TorqueData.h"
// CWnd_TestResult_Foc_MainView
enum enTestResult_Foc_Main_Static
{
	STI_TR_Foc_Main_Title,
	STI_TR_Foc_Main_MAX,
};

static LPCTSTR	g_szTestResult_Foc_Main_Static[] =
{
	_T("TEST RESULT"),
	NULL
};

class CWnd_TestResult_Foc_MainView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_Foc_MainView)

public:
	CWnd_TestResult_Foc_MainView();
	virtual ~CWnd_TestResult_Foc_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	afx_msg void OnLbnSelChangeTest	();
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType			m_InspectionType;

	CFont					m_font;
	
	CVGStatic				m_st_Item[STI_TR_Foc_Main_MAX];

	CComboBox				m_cb_TestItem;

	int						m_iSelectTest[TI_Foc_MaxEnum];


	CWnd_Rst_SFR				m_Wnd_Rst_SFR;
	CWnd_Rst_Current			m_Wnd_Rst_Current;
	CWnd_Rst_Ymean				m_Wnd_Rst_Ymean;
	CWnd_Rst_BlackSpot			m_Wnd_Rst_BlackSpot;
	CWnd_Rst_Defect_Black		m_Wnd_Rst_Defect_Black;
	CWnd_Rst_Defect_White		m_Wnd_Rst_Defect_White;
	CWnd_Rst_LCB				m_Wnd_Rst_LCB;
	CWnd_Rst_OpticalCenter		m_Wnd_Rst_OpticalCenter;
	CWnd_Rst_Rotate				m_Wnd_Rst_Rotate;
	CWnd_Rst_ActiveAlign		m_Wnd_Rst_ActiveAlign;
	CWnd_Rst_Torque				m_Wnd_Rst_Torque;

public:

	void SelectItem				(UINT nTestItem);
	void SelectNum				(UINT nSelect);
	// 검사기 종류 설정
	void	SetSystemType	(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void AllDataReset();
	void SetUIData_Reset(int nIdx);
	void SetUIData			(__in int nIdx, __in LPVOID pParam, __in enFocus_AAView enView);
	void SetClearTab		();
	void SetAddTab			(int nIdx, int &iItemCnt);
};
#endif // Wnd_TestResult_Foc_MainView_h__
