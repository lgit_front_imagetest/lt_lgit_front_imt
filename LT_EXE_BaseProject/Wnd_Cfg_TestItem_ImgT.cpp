//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_ImgT.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_ImgT.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_ImgT.h"
#include "Def_WindowMessage.h"

// CWnd_Cfg_TestItem_ImgT

#define IDC_TAB 1000
IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_ImgT, CWnd_BaseView)

CWnd_Cfg_TestItem_ImgT::CWnd_Cfg_TestItem_ImgT()
{
	m_InspectionType = enInsptrSysType::Sys_Image_Test;
}

CWnd_Cfg_TestItem_ImgT::~CWnd_Cfg_TestItem_ImgT()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_ImgT, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()

	//	ON_EN_SETFOCUS(IDC_TAB, &CWnd_Cfg_TestItem_ImgT::OnEnSetfocusTabFocus)

END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TAB, CMFCTabCtrl::LOCATION_BOTTOM);

	m_Wnd_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_OpticalCenter.SetOverlayID(WM_SELECT_OVERLAY, Ovr_OpticalCenter);
	m_Wnd_OpticalCenter.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_SFR.SetOwner(GetOwner());
	m_Wnd_SFR.SetOverlayID(WM_SELECT_OVERLAY, Ovr_SFR);
	m_Wnd_SFR.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Defect_Black.SetOwner(GetOwner());
	m_Wnd_Defect_Black.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Defect_Black);
	m_Wnd_Defect_Black.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Defect_Black.Create(NULL, _T("Defect_Black"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Defect_White.SetOwner(GetOwner());
	m_Wnd_Defect_White.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Defect_White);
	m_Wnd_Defect_White.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Defect_White.Create(NULL, _T("Defect_White"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Rotate.SetOwner(GetOwner());
	m_Wnd_Rotate.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Rotate);
	m_Wnd_Rotate.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Rotate.Create(NULL, _T("Rotate"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Ymean.SetOwner(GetOwner());
	m_Wnd_Ymean.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Ymean);
	m_Wnd_Ymean.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Ymean.Create(NULL, _T("Ymean"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_BlackSpot.SetOwner(GetOwner());
	m_Wnd_BlackSpot.SetOverlayID(WM_SELECT_OVERLAY, Ovr_BlackSpot);
	m_Wnd_BlackSpot.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_BlackSpot.Create(NULL, _T("BlackSpot"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_LCB.SetOwner(GetOwner());
	m_Wnd_LCB.SetOverlayID(WM_SELECT_OVERLAY, Ovr_LCB);
	m_Wnd_LCB.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_LCB.Create(NULL, _T("LCB"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Current.SetOwner(GetOwner());
	m_Wnd_Current.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Current);
	m_Wnd_Current.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Current.Create(NULL, _T("Current"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	//^^ SH2_1107 추가
	m_Wnd_FOV.SetOwner(GetOwner());
	m_Wnd_FOV.SetOverlayID(WM_SELECT_OVERLAY, Ovr_FOV);											//^^ SH2 Overlay 관련 수정 필요 1107
	m_Wnd_FOV.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_FOV.Create(NULL, _T("FOV"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Distortion.SetOwner(GetOwner());
	m_Wnd_Distortion.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Distortion);
	m_Wnd_Distortion.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Distortion.Create(NULL, _T("Distortion"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	//^^ SH2_1108 추가
	m_Wnd_DynamicBW.SetOwner(GetOwner());
	m_Wnd_DynamicBW.SetOverlayID(WM_SELECT_OVERLAY, Ovr_DynamicBW);									//^^ SH2 Overlay 관련 수정 필요 1108
	m_Wnd_DynamicBW.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_DynamicBW.Create(NULL, _T("DynamicBW"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Shading.SetOwner(GetOwner());
	m_Wnd_Shading.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Shading);									//^^ SH2 Overlay 관련 수정 필요 1108
	m_Wnd_Shading.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Shading.Create(NULL, _T("Shading"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	/*
	m_Wnd_ActiveAlign.SetOwner(GetOwner());
	m_Wnd_ActiveAlign.SetOverlayID(WM_SELECT_OVERLAY, Ovr_ActiveAlign);
	m_Wnd_ActiveAlign.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_ActiveAlign.Create(NULL, _T("ActiveAlign"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Torque.SetOwner(GetOwner());
	m_Wnd_Torque.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Torque);
	m_Wnd_Torque.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Torque.Create(NULL, _T("Torque"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);
	*/
	m_wnd_TestItem_EachTest.SetCameraParaIdx(m_pnCamParaIdx);

	m_wnd_TestItem_EachTest.Create(NULL, _T("개별"), dwStyle /*| WS_BORDER*/, rectDummy, this, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int nTabW = iWidth * 4 / 5;
	m_tc_Option.MoveWindow(iLeft, iTop, nTabW, iHeight);

	iLeft += iMargin + nTabW;

	m_wnd_TestItem_EachTest.MoveWindow(iLeft, iTop, iWidth - (iMargin + nTabW), iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;

	m_tc_Option.AddTab(&m_Wnd_Current,			_T("Current					"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_OpticalCenter,	_T("OpticalCenter			"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Rotate,			_T("Rotate					"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_SFR,				_T("SFR						"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Ymean,			_T("Ymean					"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_BlackSpot,		_T("BlackSpot				"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_LCB,				_T("LCB						"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Defect_Black,		_T("Defect_Black			"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Defect_White,		_T("Defect_White			"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_FOV,				_T("FOV						"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Distortion,		_T("Distortion				"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_DynamicBW,		_T("DynamicBW				"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Shading,			_T("Shading					"), nItemCnt++, TRUE);
//	m_tc_Option.AddTab(&m_Wnd_ActiveAlign,		_T("ActiveAlign  			"), nItemCnt++, TRUE);
//	m_tc_Option.AddTab(&m_Wnd_Torque,			_T("Torque		 			"), nItemCnt++, TRUE);


}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetInitListCtrl()
{
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_wnd_TestItem_EachTest.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{
	if (pstRecipeInfo == NULL)
		return;
	
	
	m_Wnd_Current.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stCurrent);
	m_Wnd_Current.Func_CameraModel(pstRecipeInfo->ModelType);
	m_Wnd_Current.SetUpdateData();

	m_Wnd_SFR.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stSFR);
	m_Wnd_SFR.SetUpdateData();

	m_Wnd_Defect_Black.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDefect_Black);
	m_Wnd_Defect_Black.SetUpdateData();

	m_Wnd_Defect_White.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDefect_White);
	m_Wnd_Defect_White.SetUpdateData();

	m_Wnd_OpticalCenter.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stOpticalCenter);
	m_Wnd_OpticalCenter.SetUpdateData();

	m_Wnd_Rotate.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stRotation);
	m_Wnd_Rotate.SetUpdateData();

	m_Wnd_Ymean.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stYmean);
	m_Wnd_Ymean.SetUpdateData();

	m_Wnd_BlackSpot.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stBlackSpot);
	m_Wnd_BlackSpot.SetUpdateData();

	m_Wnd_LCB.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stLCB);
	m_Wnd_LCB.SetUpdateData();

	m_Wnd_FOV.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stFOV);
	m_Wnd_FOV.SetUpdateData();

	m_Wnd_Distortion.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDistortion);
	m_Wnd_Distortion.SetUpdateData();

	m_Wnd_DynamicBW.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDynamicBW);
	m_Wnd_Distortion.SetUpdateData();

	m_Wnd_Shading.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stShading);
	m_Wnd_Shading.SetUpdateData();

// 	m_Wnd_ActiveAlign.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stActiveAlign);
// 	m_Wnd_ActiveAlign.SetUpdateData();
// 
// 	m_Wnd_Torque.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stTorque);
// 	m_Wnd_Torque.SetUpdateData();
	/*
	switch (pstRecipeInfo->ModelType)
	{
	case Model_MRA2:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(1);
		break;

	case Model_IKC:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;

	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		m_Wnd_ECurrent.OnSetCameraModelType(1);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;

	default:
		break;
	}

	m_Wnd_ECurrent.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stECurrentOpt);
	m_Wnd_ECurrent.SetUpdateData();

	m_Wnd_OpticalCenter.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stOpticalCenterOpt);
	m_Wnd_OpticalCenter.SetUpdateData();

	m_Wnd_Rotate.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stRotateOpt);
	m_Wnd_Rotate.SetUpdateData();

	m_Wnd_Distortion.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDistortionOpt);
	m_Wnd_Distortion.SetUpdateData();

	m_Wnd_Fov.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stFovOpt);
	m_Wnd_Fov.SetUpdateData();

	m_Wnd_Dynamic.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDynamicOpt);
	m_Wnd_Dynamic.SetUpdateData();

	m_Wnd_DefectPixel.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDefectPixelOpt);
	m_Wnd_DefectPixel.SetUpdateData();

	m_Wnd_SFR.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stSFROpt);
	m_Wnd_SFR.SetUpdateData();

	m_Wnd_SNR_Light.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stSNR_LightOpt);
	m_Wnd_SNR_Light.SetUpdateData();

	m_Wnd_Intensity.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stIntensityOpt);
	m_Wnd_Intensity.SetUpdateData();

	m_Wnd_Shading.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stShadingOpt);
	m_Wnd_Shading.SetUpdateData();

	m_Wnd_Particle.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stParticleOpt);
	m_Wnd_Particle.SetUpdateData();

	//#ifdef SET_GWANGJU
	m_Wnd_HotPixel.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stHotPixelOpt);
	m_Wnd_HotPixel.SetUpdateData();

	m_Wnd_3D_Depth.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.st3D_DepthOpt);
	m_Wnd_3D_Depth.SetUpdateData();

	m_Wnd_FPN.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stFPNOpt);
	m_Wnd_FPN.SetUpdateData();

	m_Wnd_EEPROM_Verify.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stEEPROM_VerifyOpt);
	m_Wnd_EEPROM_Verify.SetUpdateData();
	//#endif
	m_Wnd_TemperatureSensor.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stTemperatureSensorOpt);
	m_Wnd_TemperatureSensor.SetUpdateData();

	m_Wnd_Particle_Entry.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stParticle_EntryOpt);
	m_Wnd_Particle_Entry.SetUpdateData();
	*/
	m_wnd_TestItem_EachTest.Set_RecipeInfo(pstRecipeInfo);

}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
	m_Wnd_SFR.GetUpdateData();
	m_Wnd_Defect_Black.GetUpdateData();
	m_Wnd_Defect_White.GetUpdateData();
	m_Wnd_OpticalCenter.GetUpdateData();
	m_Wnd_Rotate.GetUpdateData();
	m_Wnd_Ymean.GetUpdateData();
	m_Wnd_BlackSpot.GetUpdateData();
	m_Wnd_LCB.GetUpdateData();
	m_Wnd_Current.GetUpdateData();
	m_Wnd_FOV.GetUpdateData();
	m_Wnd_Distortion.GetUpdateData();
	m_Wnd_DynamicBW.GetUpdateData();
	m_Wnd_Shading.GetUpdateData();
	//m_Wnd_ActiveAlign.GetUpdateData();
	//m_Wnd_Torque.GetUpdateData();


}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2018/2/25 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemInfo(__in ST_RecipeInfo& stRecipeInfo, __out ST_TestItemInfo& stOutTestItemInfo)
{
	
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;
	
	for (UINT nIdx = enTestItem_ImgT::TI_ImgT_Re_ECurrent; nIdx < enTestItem_ImgT::TI_ImgT_MaxEnum; nIdx++)
	{
		pSpec = &stOutTestItemInfo.TestItemList.GetAt(nIdx);
		ASSERT(NULL != pSpec);

		// 결과 데이터가 1개 초과이고, 개별 Min Max 스펙을 가지는 경우
		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}
				
				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}

				// 항목 인덱스 증가
				++nCtrlIdx;
			}
		}
		else // 결과 데이터가 동일한 MinMax 스펙을 가지는 경우
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}
			}

			// 항목 인덱스 증가
			++nCtrlIdx;
		}
	}
	
}

//=============================================================================
// Method		: Get_TestItemMinSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemMinSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_ImgT_Re_ECurrent:
		bUseSpec = stRecipeInfo.stImageQ.stCurrent.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%.3f"), stRecipeInfo.stImageQ.stCurrent.stSpecMin[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Min[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Min[Spec_OC_X].iValue);
		break;

	case TI_ImgT_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Min[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Min[Spec_OC_Y].iValue);
		break;

	case TI_ImgT_Re_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFR.stInput[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFR.stInput[nArIdx].stSpecMin.dbValue);
		break;
	case TI_ImgT_Re_Defect_Black:
		bUseSpec = stRecipeInfo.stImageQ.stDefect_Black.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefect_Black.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Defect_White:
		bUseSpec = stRecipeInfo.stImageQ.stDefect_White.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefect_White.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Rotation:
		bUseSpec = stRecipeInfo.stImageQ.stRotation.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stRotation.stSpecMin[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_Ymean:
		bUseSpec = stRecipeInfo.stImageQ.stYmean.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stYmean.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_BlackSpot:
		bUseSpec = stRecipeInfo.stImageQ.stBlackSpot.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stBlackSpot.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_LCB:
		bUseSpec = stRecipeInfo.stImageQ.stLCB.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stLCB.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Dia:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Hor:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Ver:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].iValue);
		break;

// 	case TI_ImgT_Re_FOV:
// 		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMin[nArIdx].iValue);
// 		break;

	case TI_ImgT_Re_Distortion:
		bUseSpec = stRecipeInfo.stImageQ.stDistortion.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDistortion.stSpecMin[nArIdx].iValue);
		break;

	case TI_ImgT_Re_DynamicRange:
		bUseSpec = stRecipeInfo.stImageQ.stDynamicBW.stSpecMin[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDynamicBW.stSpecMin[nArIdx].iValue);
		break;

// 	case TI_ImgT_Re_Shading:
// 		bUseSpec = stRecipeInfo.stImageQ.stShading..bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stShading..iValue);
// 		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Get_TestItemMaxSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemMaxSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_ImgT_Re_ECurrent:
		bUseSpec = stRecipeInfo.stImageQ.stCurrent.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%.3f"), stRecipeInfo.stImageQ.stCurrent.stSpecMax[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Max[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Max[Spec_OC_X].iValue);
		break;

	case TI_ImgT_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Max[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenter.stSpec_Max[Spec_OC_Y].iValue);
		break;

	case TI_ImgT_Re_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFR.stInput[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFR.stInput[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Defect_Black:
		bUseSpec = stRecipeInfo.stImageQ.stDefect_Black.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefect_Black.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Defect_White:
		bUseSpec = stRecipeInfo.stImageQ.stDefect_White.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefect_White.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Rotation:
		bUseSpec = stRecipeInfo.stImageQ.stRotation.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stRotation.stSpecMax[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_Ymean:
		bUseSpec = stRecipeInfo.stImageQ.stYmean.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stYmean.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_BlackSpot:
		bUseSpec = stRecipeInfo.stImageQ.stBlackSpot.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stBlackSpot.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_LCB:
		bUseSpec = stRecipeInfo.stImageQ.stLCB.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stLCB.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Dia:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Hor:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_FOV_Ver:
		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].iValue);
		break;

// 	case TI_ImgT_Re_FOV:
// 		bUseSpec = stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFOV.stSpecMax[nArIdx].iValue);
// 		break;

	case TI_ImgT_Re_Distortion:
		bUseSpec = stRecipeInfo.stImageQ.stDistortion.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDistortion.stSpecMax[nArIdx].iValue);
		break;

	case TI_ImgT_Re_DynamicRange:
		bUseSpec = stRecipeInfo.stImageQ.stDynamicBW.stSpecMax[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDynamicBW.stSpecMax[nArIdx].iValue);
		break;
// 	case TI_ImgT_Re_Shading:
// 		bUseSpec = stRecipeInfo.stImageQ.stShading..bEnable;						//물어보자
// 		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stShading..iValue);
// 		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnEnSetfocusTabFocus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/7 - 19:16
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnEnSetfocusTabFocus()
{
	GetOwner()->SendNotifyMessage(WM_CHANGE_OPTIONPIC, 0, 0);
}