﻿//*****************************************************************************
// Filename	: 	File_Report.cpp
// Created	:	2016/8/5 - 17:18
// Modified	:	2016/8/5 - 17:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_Report.h"
#include "CommonFunction.h"
#include <iostream>

static LPCTSTR g_szHeaderz_2DCal[] = 
{ 
	_T("Date"), 
	_T("Barcode"), 
	_T("Focal Length U"), 
	_T("Focal Length V"), 
	_T("Optical Center X"), 
	_T("Optical Center Y"), 
	_T("Kc[0]"), 
	_T("Kc[1]"), 
	_T("Kc[2]"), 
	_T("Kc[3]"), 
	_T("Kc[4]"),
	_T("DistFov"), 
	_T("Intrinsc Fov"), 
	_T("Reprojection Error"), 
	_T("Eval Error"), 
	_T("Org Offset"),
	_T("Detection FailureCnt"), 
	_T("Invalid PointCnt"), 
	_T("Valid PointCnt"), 
	_T("Date"),
	_T("Camera Init"),
	_T("Corner Point Step"),
	_T("Calibration"),
	_T("EEPROM"),
	NULL 
};

CFile_Report::CFile_Report()
{
}


CFile_Report::~CFile_Report()
{
}

//=============================================================================
// Method		: SaveFinalizeResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szFullPath
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/11/18 - 13:46
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult( __in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString szDirPath;
	CString szUNICODE;
	CString szData;

	CFile File;
	CFileException e;
	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Header 추가
		CString szHeader;
		for (int _a = 0; _a < pszHeadArray->GetCount(); _a++)
		{
			szHeader += pszHeadArray->GetAt(_a);
			szHeader += _T(",");
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szHeader += _T("\r\n");
		szUNICODE = szHeader + szData;
		szUNICODE += _T("\r\n");
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szUNICODE = szData;
		szUNICODE += _T("\r\n");
	}

	CStringA szANSI;
	USES_CONVERSION;
	szANSI = CT2A(szUNICODE.GetBuffer(0));

	File.SeekToEnd();
	//File.Write(szBuff.GetBuffer(0), szBuff.GetLength() * sizeof(TCHAR));
	File.Write(szANSI.GetBuffer(0), szANSI.GetLength());

	File.Flush();
	File.Close();

	return TRUE;
}

//=============================================================================
// Method		: Make2DCalHeadAndData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __in const ST_2DCal_Result * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/5 - 16:19
// Desc.		:
//=============================================================================
BOOL CFile_Report::Make2DCalHeadAndData(__in CString szBarcode, __in const ST_2DCal_Result* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray != NULL);
	ASSERT(pszDataArray != NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

	//LPCTSTR szHeaderz[] = { _T("Date"), _T("Barcode"), _T("Focal Length U"), _T("Focal Length V"), _T("Optical Center X"), _T("Optical Center Y"), _T("Kc[0]"), _T("Kc[1]"), _T("Kc[2]"), _T("Kc[3]"), _T("Kc[4]"),
	//	_T("DistFov"), _T("Intrinsc Fov"), _T("Reprojection Error"), _T("Eval Error"), _T("Org Offset"), _T("Detection FailureCnt"), _T("Invalid PointCnt"), _T("Valid PointCnt"), _T("Date"), NULL };

	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_2DCal[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_2DCal[nIdx]);
	}
	
	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->arfKK[nIdx]);
		pszDataArray->Add(szItem);
	}

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
	{
		szItem.Format(_T("%e"), pstResult->arfKc[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->fDistFOV);
	pszDataArray->Add(szItem);

	// fR2Max
	szItem.Format(_T("%f"), pstResult->fR2Max);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fRepError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fEvalError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fOrgOffset);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->nDetectionFailureCnt);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->nInvalidPointCnt);
	pszDataArray->Add(szItem);

	//nValidPointCnt
	szItem.Format(_T("%d"), pstResult->nValidPointCnt);
	pszDataArray->Add(szItem);

	CStringA szTemp;
	for (UINT nIdx = 0; nIdx < 6; nIdx++)
	{
		szTemp.AppendChar(pstResult->arcDate[nIdx]);
	}
	USES_CONVERSION;
	szItem = CA2T(szTemp.GetBuffer(0));
	pszDataArray->Add(szItem);

	return TRUE;
}

//=============================================================================
// Method		: Make2DCalHeadAndData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const ST_CamInfo * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/19 - 10:39
// Desc.		:
//=============================================================================
BOOL CFile_Report::Make2DCalHeadAndData(__in const ST_CamInfo* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray != NULL);
	ASSERT(pszDataArray != NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;
	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_2DCal[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_2DCal[nIdx]);
	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), pstResult->szBarcode);
	pszDataArray->Add(szItem);

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->st2D_Result.arfKK[nIdx]);
		pszDataArray->Add(szItem);
	}

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
	{
		szItem.Format(_T("%e"), pstResult->st2D_Result.arfKc[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->st2D_Result.fDistFOV);
	pszDataArray->Add(szItem);

	// fR2Max
	szItem.Format(_T("%f"), pstResult->st2D_Result.fR2Max);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fRepError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fEvalError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fOrgOffset);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->st2D_Result.nDetectionFailureCnt);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->st2D_Result.nInvalidPointCnt);
	pszDataArray->Add(szItem);

	//nValidPointCnt
	szItem.Format(_T("%d"), pstResult->st2D_Result.nValidPointCnt);
	pszDataArray->Add(szItem);


	// * 카메라 초기화
	//szItem.Format(_T("%d"), pstResult->nInitialize);
	szItem = g_szResultCode[pstResult->nInitialize];
	pszDataArray->Add(szItem);

	// * 코너점 추출
	if (0 == pstResult->nErr_CornerPtStepNo)
	{
		szItem = _T("OK");
	}
	else
	{
		szItem.Format(_T("%d"), pstResult->nErr_CornerPtStepNo);
	}
	pszDataArray->Add(szItem);

	// * Calibration
	//szItem.Format(_T("%d"), pstResult->n2DCAL_Result);
	szItem = g_szResultCode_LG_CalIntr[pstResult->nCAL_ResultCode];
	pszDataArray->Add(szItem);

	// * EEPROM Write
	//szItem.Format(_T("%d"), pstResult->nEEPROM_Result);
	szItem = g_szResultCode_EEPROM[pstResult->nEEPROM_Result];
	pszDataArray->Add(szItem);

	CStringA szTemp;
	for (UINT nIdx = 0; nIdx < 6; nIdx++)
	{
		szTemp.AppendChar(pstResult->st2D_Result.arcDate[nIdx]);
	}
	USES_CONVERSION;
	szItem = CA2T(szTemp.GetBuffer(0));
	pszDataArray->Add(szItem);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResult2DCal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in const ST_2DCal_Result * pstResult
// Qualifier	:
// Last Update	: 2018/3/5 - 16:19
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult2DCal(__in LPCTSTR szPath, __in UINT nParaIdx, __in CString szBarcode, __in const ST_2DCal_Result* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	if (Para_Left == nParaIdx)
	{
		szFileName.Format(_T("%s_Left_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
																		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		szFileName.Format(_T("%s_Right_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
																		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make2DCalHeadAndData(szBarcode, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResult2DCal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nParaIdx
// Parameter	: __in const ST_CamInfo * pstResult
// Qualifier	:
// Last Update	: 2018/3/19 - 10:40
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult2DCal(__in LPCTSTR szPath, __in UINT nParaIdx, __in const ST_CamInfo* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	if (Para_Left == nParaIdx)
	{
		szFileName.Format(_T("%s_Left_%04d%02d%02d_%02d%02d%02d.csv"), pstResult->szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
			tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		szFileName.Format(_T("%s_Right_%04d%02d%02d_%02d%02d%02d.csv"), pstResult->szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
			tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make2DCalHeadAndData(pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}
