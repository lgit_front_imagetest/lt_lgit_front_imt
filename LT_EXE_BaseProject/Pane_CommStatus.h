﻿//*****************************************************************************
// Filename	: 	Pane_CommStatus.h
// Created	:	2014/7/5 - 10:23
// Modified	:	2016/09/21
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Pane_CommStatus_h__
#define Pane_CommStatus_h__

#pragma once

#include <afxwin.h>
#include "Def_Enum.h"
#include "VGStatic.h"

//#define		USE_LOG_WND // 테스트 용도 출력 창
#define		MAX_TEST_BUTTON_CNT		10	// 테스트 용도로 사용되는 버튼 개수

//#define		USE_VECTOR_CAN		// Vector CAN 장치 사용 할 경우

#ifdef USE_LOG_WND
#include "LogOutput.h"
#endif

//=============================================================================
// CPane_CommStatus
//=============================================================================
class CPane_CommStatus : public CMFCTasksPane
{
	DECLARE_DYNAMIC(CPane_CommStatus)

public:
	CPane_CommStatus();
	virtual ~CPane_CommStatus();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);
	afx_msg void	OnUpdateCmdUI_Test	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClickedTest		(UINT nID);

	afx_msg void	OnUpdateCmdUI_Dev	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClicked_Dev		(UINT nID);

	// UI 설정값
	UINT			m_nPaneWidth		= 140;
	UINT			m_nTestButtonCount	= 6;
	enInsptrSysType m_InsptrType		= enInsptrSysType::Sys_Focusing;

	CFont			m_Font;

#ifdef USE_LOG_WND
	CVGStatic		m_st_WarningEvent;
	CLogOutput		m_ed_WarningLog;
#endif
	
	CMFCButton		m_bn_Test[MAX_TEST_BUTTON_CNT];
	
	enum enDeviceStaticIndex
	{
		DevSI_BaseIndex			= 0,
		DevSI_PermissionMode	= 0,
		DevSI_OperateMode,
		DevSI_MES,
		DevSI_MES_Online,
		DevSI_HandyBCR,
		//DevSI_FixedBCR,
		DevSI_PCB_1,			// 1 ~ 2개
		DevSI_PCB_2,			// 1 ~ 2개
		DevSI_Light_PCB_1,		// 0 ~ 3개 : Center
		DevSI_Light_PCB_2,		// 0 ~ 3개 : Left
		DevSI_Light_PCB_3,		// 0 ~ 3개 : Right
		DevSI_LightPSU_1,
		DevSI_LightPSU_2,
		DevSI_LightPSU_3,
		DevSI_Grabber_1,		
		DevSI_Grabber_2,
		DevSI_VideoSignal_1,
		DevSI_VideoSignal_2,
		DevSI_PLC,
		DevSI_DIO,
		DevSI_Motion,
		DevSI_Torque,
		DevSI_MaxCount,
	};

	enum enDeviceButtonIndex
	{
		DEV_BN_PermissionChange,
		DEV_BN_OperateModeChange,
		DEV_BN_MES_Online,
		DEV_BN_Keyboard,
		DEV_BN_Barcode,
		DEV_BN_EquipmentInit,
		DEV_BN_MaxCount,
	};

	CVGStatic		m_st_Device[DevSI_MaxCount];
	CMFCButton		m_bn_Device[DEV_BN_MaxCount];
	
	// 테스트용 버튼 만들기
	virtual void	MakeTestButtons			();

	// 시스템 정보 표시
	virtual void	AddSystemInfo			();
	
	// 접근 권한 설정 표시
	virtual void	AddPermissionMode		();	
	// 설비 구동 모드 표시
	virtual void	Add_OperateMode			();
	
	// 주변 장치 통신 상태 표시
	virtual void	AddPCISlot				();
	virtual void	AddSerialComm			();
	virtual void	AddTCPIP				();

	// MES 상태
	virtual void	Add_MES					();
	virtual void	Add_Video				();

	// 장비제어
	virtual void	Add_EquipmentCtrl		();

	// 유틸 기능 추가
	virtual void	AddUtilities			();
	
#ifdef USE_LOG_WND
	virtual void	AddWarningStatus		();
#endif
	
	// 토크 컨트롤 표시
	virtual void	AddTorqueCtrl			();

	// EEPROM Verify 버튼 (임시 사용)
	virtual void	Add_EEPROM				();

	// 카메라 선택용
	virtual void	Add_CamSelect			();


	// 테스트용 버튼 표시
	virtual void	AddTestCtrl				();

public:

	// Pane의 가로 크기 고정
	virtual CSize	CalcFixedLayout				(__in BOOL bStretch, __in BOOL bHorz);
	// 검사기 종류 설정
	void			SetSystemType				(__in enInsptrSysType nSysType);
	// Pane 너비
	void			SetPaneWidth				(__in UINT nWidth);
	// 테스트 버튼 사용 개수
	void			SetTestButtonCount			(__in BYTE nCount);
	// 접근 권한 형태 설정
	void			SetStatus_PermissionMode	(__in enPermissionMode InspMode);
	// 설비 구동 모드
	void			SetStatus_OperateMode		(__in enOperateMode nOperMode);
	
	//-------------------------------------------------------------------------
	// ** 주변 장치 통신 상태 표시 ** 
	//-------------------------------------------------------------------------
	// 바코드 리더기 연결 상태 표시
	void			SetStatus_HandyBCR			(__in BOOL bConStatus);
	void			SetStatus_FixedBCR			(__in BOOL bConStatus);
	// 전원, 전류 측정 제어 보드 표시
	void			SetStatus_CameraBoard		(__in UINT nConStatus, __in UINT nIndex = 0);
	void			SetStatus_LightBoard		(__in UINT nConStatus, __in UINT nIndex = 0);
	void			SetStatus_LightPSU			(__in UINT nConStatus, __in UINT nIndex = 0);
	// MES 연결 상태 표시
	void			SetStatus_MES				(__in UINT nStatus);
	void			SetStatus_MES_Online		(__in UINT nStatus);
	
	// I/O 통신 연결 상태
	void			SetStatus_DIO				(__in UINT nStatus);
	void			SetStatus_Motion			(__in UINT nStatus);

	void			SetStatus_GrabBoard			(__in BOOL bConStatus, __in UINT nIndex = 0);
	void			SetStatus_VideoSignal		(__in BOOL bConStatus, __in UINT nIndex = 0);

	void			SetStatus_Torque			(__in UINT nConStatus);
};

#endif // Pane_CommStatus_h__

