#ifndef Def_Mes_ImgT_h__
#define Def_Mes_ImgT_h__

#include <afxwin.h>


#pragma pack(push,1)


static LPCTSTR g_szMESHeader_ImgT[] =
{
	_T("Current_1.8V"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("OC_Offset_X"),
	_T("OC_Offset_Y"),
	_T("SFR_0"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("Rotation"),
	_T("Distortion"),
	_T("FOV_DFOV"),
	_T("FOV_HFOV"),
	_T("FOV_VFOV"),
	_T("Dynamic_Range"),
	_T("SNR_BW"),
	_T("Ymean"),
	_T("BlackSpot"),
	_T("LCB"),
	_T("Defect_Black"),
	_T("Defect_White"),
	//_T("Shading"),   //!SH _181108: 더 많았던 걸로 기억
	_T("Shading_HorizonValue_1"),
	_T("Shading_HorizonValue_2"),
	_T("Shading_HorizonValue_3"),
	_T("Shading_HorizonValue_4"),
	_T("Shading_HorizonValue_5"),
	_T("Shading_HorizonValue_6"),
	_T("Shading_HorizonValue_7"),
	_T("Shading_HorizonValue_8"),
	_T("Shading_HorizonValue_9"),
	_T("Shading_HorizonValue_10"),
	_T("Shading_HorizonValue_11"),
	_T("Shading_HorizonValue_12"),
	_T("Shading_HorizonValue_13"),
	_T("Shading_HorizonValue_14"),
	_T("Shading_HorizonValue_15"),
	_T("Shading_HorizonValue_16"),
	_T("Shading_HorizonValue_17"),
	_T("Shading_HorizonValue_18"),
	_T("Shading_HorizonValue_19"),
	_T("Shading_VerticalValue_1"),
	_T("Shading_VerticalValue_2"),
	_T("Shading_VerticalValue_3"),
	_T("Shading_VerticalValue_4"),
	_T("Shading_VerticalValue_5"),
	_T("Shading_VerticalValue_6"),
	_T("Shading_VerticalValue_7"),
	_T("Shading_VerticalValue_8"),
	_T("Shading_VerticalValue_9"),
	_T("Shading_VerticalValue_10"),
	_T("Shading_VerticalValue_11"),
	_T("Shading_VerticalValue_12"),
	_T("Shading_VerticalValue_13"),
	_T("Shading_VerticalValue_14"),
	_T("Shading_VerticalValue_15"),
	_T("Shading_VerticalValue_16"),
	_T("Shading_VerticalValue_17"),
	_T("Shading_VerticalValue_18"),
	_T("Shading_VerticalValue_19"),
	_T("Shading_DiagonalA_1"),
	_T("Shading_DiagonalA_2"),
	_T("Shading_DiagonalA_3"),
	_T("Shading_DiagonalA_4"),
	_T("Shading_DiagonalA_5"),
	_T("Shading_DiagonalA_6"),
	_T("Shading_DiagonalA_7"),
	_T("Shading_DiagonalA_8"),
	_T("Shading_DiagonalA_9"),
	_T("Shading_DiagonalA_10"),
	_T("Shading_DiagonalA_11"),
	_T("Shading_DiagonalA_12"),
	_T("Shading_DiagonalA_13"),
	_T("Shading_DiagonalA_14"),
	_T("Shading_DiagonalA_15"),
	_T("Shading_DiagonalA_16"),
	_T("Shading_DiagonalA_17"),
	_T("Shading_DiagonalA_18"),
	_T("Shading_DiagonalA_19"),
	_T("Shading_DiagonalB_1"),
	_T("Shading_DiagonalB_2"),
	_T("Shading_DiagonalB_3"),
	_T("Shading_DiagonalB_4"),
	_T("Shading_DiagonalB_5"),
	_T("Shading_DiagonalB_6"),
	_T("Shading_DiagonalB_7"),
	_T("Shading_DiagonalB_8"),
	_T("Shading_DiagonalB_9"),
	_T("Shading_DiagonalB_10"),
	_T("Shading_DiagonalB_11"),
	_T("Shading_DiagonalB_12"),
	_T("Shading_DiagonalB_13"),
	_T("Shading_DiagonalB_14"),
	_T("Shading_DiagonalB_15"),
	_T("Shading_DiagonalB_16"),
	_T("Shading_DiagonalB_17"),
	_T("Shading_DiagonalB_18"),
	_T("Shading_DiagonalB_19"),
	NULL,
};

//항목 순서
typedef enum enMesDataIndex_ImgT
{
	MesDataIdx_ImgT_ECurrent = 0,
	MesDataIdx_ImgT_OpticalCenter,
	MesDataIdx_ImgT_SFR,
	MesDataIdx_ImgT_Rotation,
	MesDataIdx_ImgT_Distortion,
	MesDataIdx_ImgT_FOV,
	MesDataIdx_ImgT_DynamicBW,
	MesDataIdx_ImgT_Ymean,
	MesDataIdx_ImgT_BlackSpot,
	MesDataIdx_ImgT_LCB,
	MesDataIdx_ImgT_Defect_Black,
	MesDataIdx_ImgT_Defect_White,
	MesDataIdx_ImgT_Shading,
	MesDataIdx_ImgT_MAX,
};

//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_ImgT
{
	MESDataNum_ImgT_ECurrent = 1,
	MESDataNum_ImgT_OpticalCenter = 4, //!SH _181108: MES OC Offset 추가
	MESDataNum_ImgT_SFR = 30,
	MESDataNum_ImgT_Rotation = 1,
	MESDataNum_ImgT_Distortion = 1,
	MESDataNum_ImgT_FOV = 3,
	MESDataNum_ImgT_DynamicBW = 2,
	MESDataNum_ImgT_Ymean = 1,
	MESDataNum_ImgT_BlackSpot = 1,
	MESDataNum_ImgT_LCB = 1,
	MESDataNum_ImgT_Defect_Black = 1,
	MESDataNum_ImgT_Defect_White = 1,
	MESDataNum_ImgT_Shading = 76,

} MesData_DataNUM_ImgT;


typedef struct _tag_MesData_ImgT
{
	CString szMesTestData[2][MesDataIdx_ImgT_MAX];
	CString szDataName[MesDataIdx_ImgT_MAX];

	UINT nMesDataNum[MesDataIdx_ImgT_MAX];

	_tag_MesData_ImgT()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ImgT_ECurrent] = MESDataNum_ImgT_ECurrent;
		nMesDataNum[MesDataIdx_ImgT_OpticalCenter] = MESDataNum_ImgT_OpticalCenter;
		nMesDataNum[MesDataIdx_ImgT_SFR] = MESDataNum_ImgT_SFR;
		nMesDataNum[MesDataIdx_ImgT_Rotation] = MESDataNum_ImgT_Rotation;
		nMesDataNum[MesDataIdx_ImgT_Distortion] = MESDataNum_ImgT_Distortion;
		nMesDataNum[MesDataIdx_ImgT_FOV] = MESDataNum_ImgT_FOV;
		nMesDataNum[MesDataIdx_ImgT_DynamicBW] = MESDataNum_ImgT_DynamicBW;
		nMesDataNum[MesDataIdx_ImgT_Ymean] = MESDataNum_ImgT_Ymean;
		nMesDataNum[MesDataIdx_ImgT_BlackSpot] = MESDataNum_ImgT_BlackSpot;
		nMesDataNum[MesDataIdx_ImgT_LCB] = MESDataNum_ImgT_LCB;
		nMesDataNum[MesDataIdx_ImgT_Defect_Black] = MESDataNum_ImgT_Defect_Black;
		nMesDataNum[MesDataIdx_ImgT_Defect_White] = MESDataNum_ImgT_Defect_White;
		nMesDataNum[MesDataIdx_ImgT_Shading] = MESDataNum_ImgT_Shading;
		//nMesDataNum[MesDataIdx_ImgT_Torque] = MESDataNum_ImgT_Torque;

		szDataName[MesDataIdx_ImgT_ECurrent] = _T("Current");
		szDataName[MesDataIdx_ImgT_OpticalCenter] = _T("Optical Center");
		szDataName[MesDataIdx_ImgT_SFR] = _T("SFR");
		szDataName[MesDataIdx_ImgT_Rotation] = _T("Rotate");
		szDataName[MesDataIdx_ImgT_Distortion] = _T("Distortion");
		szDataName[MesDataIdx_ImgT_FOV] = _T("FOV");
		szDataName[MesDataIdx_ImgT_DynamicBW] = _T("DynamicRange_SNRBW");
		szDataName[MesDataIdx_ImgT_Ymean] = _T("Ymean");
		szDataName[MesDataIdx_ImgT_BlackSpot] = _T("BlackSpot");
		szDataName[MesDataIdx_ImgT_LCB] = _T("LCB");
		szDataName[MesDataIdx_ImgT_Defect_Black] = _T("Defect_Black");
		szDataName[MesDataIdx_ImgT_Defect_White] = _T("Defect_White");
		szDataName[MesDataIdx_ImgT_Shading] = _T("Shading");
		//szDataName[MesDataIdx_ImgT_Torque] = _T("Torque");
	};

	void Reset(UINT nCam)
	{
		for (UINT nItem = 0; nItem < MesDataIdx_ImgT_MAX; nItem++)
		{
			szMesTestData[nCam][nItem].Empty();
		}
	};

	_tag_MesData_ImgT& operator= (_tag_MesData_ImgT& ref)
	{
		for (UINT nCam = 0; nCam < 2; nCam++)
		{
			for (UINT nItem = 0; nItem < MesDataIdx_ImgT_MAX; nItem++)
			{
				szMesTestData[nCam][nItem] = ref.szMesTestData[nCam][nItem];
			}
		}

		return *this;
	};

}ST_MES_Data_ImgT, *PST_MES_Data_ImgT;


#pragma pack(pop)

#endif // Def_FOV_h__