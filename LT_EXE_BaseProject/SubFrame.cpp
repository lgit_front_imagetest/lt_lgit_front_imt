﻿//*****************************************************************************
// Filename	: SubFrame.cpp
// Created	: 2010/11/23
// Modified	: 2010/11/23 - 10:45
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// SubFrame.cpp : CSubFrame 클래스의 구현
//

#include "stdafx.h"
#include "LT MainExecution.h"
#include "resource.h"
#include "SubFrame.h"
#include "NTVisualManager.h"

#include "Def_CompileOption.h"
#include "Def_Enum.h"
#include "CommonFunction.h"
#include "SheetOption.h"
#include "Dlg_ChkPassword.h"
#include "Dialgo_Base.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define IDC_STATIS_COMM_01              3010
#define ID_VIEW_OUTPUTWND				3011

//=============================================================================
// CSubFrame
//=============================================================================
IMPLEMENT_DYNAMIC(CSubFrame, CFrameWndEx)

BEGIN_MESSAGE_MAP(CSubFrame, CFrameWndEx)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_WM_ACTIVATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_SETTINGCHANGE()
	ON_COMMAND_RANGE			(ID_TABVIEW_1, ID_TABVIEW_7, OnTabView)
	ON_UPDATE_COMMAND_UI_RANGE	(ID_TABVIEW_1, ID_TABVIEW_7, OnUpdateTabView)
	ON_MESSAGE(WM_OPTION,				OnOption)
	ON_MESSAGE(WM_LOGMSG,				OnLogMsg)
	ON_MESSAGE(WM_LOAD_COMPLETE,		OnLoadComplete)
	ON_MESSAGE(WM_ALIVE_PROCESS,		OnWatchExtProcess)
	ON_MESSAGE(WM_OPTION_CHANGED,		OnOptionChanged)
	ON_MESSAGE(WM_TEST_FUNCTION,		OnTestFunction)
	ON_MESSAGE(WM_CHANGE_VIEW,			OnChangeView)
	ON_MESSAGE(WM_PERMISSION_MODE,		OnPermissionMode)
	ON_MESSAGE(WM_MANUAL_BARCODE,		OnManualBarcode)
	ON_WM_TIMER()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

//=============================================================================
// CSubFrame 생성/소멸
//=============================================================================
//=============================================================================
// Method		: CSubFrame::CSubFrame
// Access		: public 
// Returns		: 
// Qualifier	:
// Last Update	: 2010/04/02 - 15:29
// Desc.		: 
//=============================================================================
CSubFrame::CSubFrame()
{
	m_nTabView				= (UINT)-1;
	m_hThreadStartSetting	= NULL;

	m_bShowDeviceInfoPane	= TRUE;

	m_infoMonitor.nMonitor			= 1;
	m_infoMonitor.nWidthVirtual		= 1280;
	m_infoMonitor.nHeightVirtual	= 1024;
	m_infoMonitor.nWidth			= 1280;
	m_infoMonitor.nHeight			= 1024;
	m_infoMonitor.nBitPerPixel		= 16;
	m_infoMonitor.nRefresh			= 60;
}

//=============================================================================
// Method		: CSubFrame::~CSubFrame
// Access		: public 
// Returns		: 
// Qualifier	:
// Last Update	: 2010/04/02 - 15:29
// Desc.		: 
//=============================================================================
CSubFrame::~CSubFrame()
{
	TRACE(_T("<<< Start ~CSubFrame >>> \n"));

	if (NULL != m_hThreadStartSetting)
		CloseHandle(m_hThreadStartSetting);

	TRACE(_T("<<< End ~CSubFrame >>> \n"));
}

//=============================================================================
// Method		: CSubFrame::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/04/02 - 15:29
// Desc.		: 
//=============================================================================
int CSubFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	ModifyStyle		(WS_CAPTION | FWS_ADDTOTITLE, 0);
	ModifyStyleEx	(WS_EX_CLIENTEDGE, 0);

	CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
	CMFCVisualManager::SetDefaultManager (RUNTIME_CLASS (CNTVisualManager));
	CMFCToolBarButton::m_bWrapText = FALSE;
		
	//-------------------------------------------------------------------------
	// Caption Bar
	//-------------------------------------------------------------------------
	m_wndCaptionBar.SetWM_Option(WM_OPTION);
	if (!m_wndCaptionBar.Create(this, IDR_MAINFRAME))
	{
		TRACE0("Failed to create Caption Bar \n");
		return -1;
	}

	if (afxGlobalData.fontRegular.GetSafeHandle () != NULL)
	{
		LOGFONT lf;
		ZeroMemory (&lf, sizeof (LOGFONT));
		afxGlobalData.fontRegular.GetLogFont (&lf);

		m_wndCaptionBar.SetCaptionFont (lf);
	}

	EnableDocking(CBRS_ALIGN_ANY);

	//-------------------------------------------------------------------------
	// Banner
	//-------------------------------------------------------------------------
	//m_wndBannerBar.SetButtonImage(IDB_PNG_EXIT, IDB_PNG_OPTION);	
	//m_wndBannerBar.SetWM_Option(WM_OPTION);
	if (!m_wndBannerBar.Create(this, IDI_ICON_LOGO))	
	{
		TRACE0("Failed to create Banner Bar\n");
		return -1;
	}

	if (afxGlobalData.fontRegular.GetSafeHandle () != NULL)
	{
		LOGFONT lf;
		ZeroMemory (&lf, sizeof (LOGFONT));
		
		afxGlobalData.fontRegular.GetLogFont (&lf);
		lf.lfHeight = 60;
		lf.lfWeight	= 900;

		m_wndBannerBar.SetCaptionFont (lf);
	}

	// 배너에 프로그램 이름 쓰기 --------------------------------------
	CLT_Option	ntOption;
	stOpt_Insp	stOption;
	ntOption.SetInspectorType(m_InspectionType);
	ntOption.LoadOption_Inspector(stOption);

	CString szAppName;
	szAppName.Format(_T("%s"), g_szInsptrSysType[m_InspectionType]);

	//m_wndBannerBar.SetWindowText (AfxGetAppName());
	m_wndBannerBar.SetWindowText (szAppName);

	//-------------------------------------------------------------------------
	// TabView Bar
	//-------------------------------------------------------------------------
	if (!MakeTabViewBar ())
		return -1;

	//-------------------------------------------------------------------------
	// Links Bar
	//-------------------------------------------------------------------------
	if (!m_wndLinksBar.CreateEx (this, TBSTYLE_FLAT, CBRS_SIZE_DYNAMIC | AFX_DEFAULT_TOOLBAR_STYLE, CRect (1, 1, 1, 1), AFX_IDW_TOOLBAR + 1))
	{
		TRACE0("Failed to create linksbar\n");
		return -1;
	}
	DockPane (&m_wndLinksBar);

	//-------------------------------------------------------------------------
	// 장비 정보 표시용 Pane
	//-------------------------------------------------------------------------	
	m_wndDeviceStatus.SetSystemType(m_InspectionType);
	m_wndDeviceStatus.Create (_T(""), this, CRect (0, 0, 340, 200), FALSE, IDR_MAINFRAME + 1, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT, 0, 0);
	DockPane(&m_wndDeviceStatus);

	//CLT_Option	ntOption;
	//stOpt_Insp	stOption;
	//ntOption.LoadOption_Inspector(stOption);
	m_bShowDeviceInfoPane = stOption.UseDeviceInfoPane;

	if (!m_bShowDeviceInfoPane)
		m_wndDeviceStatus.ShowPane(FALSE, 0, TRUE);	

	//-------------------------------------------------------------------------
	// 프레임의 클라이언트 영역을 차지하는 뷰를 만듭니다.
	//-------------------------------------------------------------------------
	// 주변기기 통신 연결
	//m_wndView_MainCtrl.SetSystemType(m_InspectionType);
	m_wndView_MainCtrl.SetCommPanePtr(&m_wndDeviceStatus);
	m_wndView_MainCtrl.SetOwner(this);
	if (!m_wndView_MainCtrl.Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("뷰 창을 만들지 못했습니다.\n");
		return -1;
	}	

	OnTabView (ID_TABVIEW_1);
 	
#ifndef DEVELOPMENT_MODE
 	m_wndTabViewBar.ShowButton_Exclusive(0, FALSE);	// **** 배포할때 사용해야 함
#endif

	return 0;
}

//=============================================================================
// Method		: CSubFrame::MakeTabViewBar
// Access		: protected 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/10/8 - 14:27
// Desc.		:
//=============================================================================
BOOL CSubFrame::MakeTabViewBar()
{
	m_wndTabViewBar.AddTabID (_T("Auto"),		ID_TABVIEW_1);
	m_wndTabViewBar.AddTabID (_T("Manual"),		ID_TABVIEW_2);	
	m_wndTabViewBar.AddTabID (_T("I/O"),		ID_TABVIEW_3);
	m_wndTabViewBar.AddTabID (_T("Maintenance"),ID_TABVIEW_4);
	m_wndTabViewBar.AddTabID (_T("Recipe"),		ID_TABVIEW_5);
	m_wndTabViewBar.AddTabID (_T("Alarm"),		ID_TABVIEW_6);
	m_wndTabViewBar.AddTabID (_T("Log"),		ID_TABVIEW_7);

	if (!m_wndTabViewBar.CreateEx (this, TBSTYLE_FLAT, CBRS_SIZE_FIXED | AFX_DEFAULT_TOOLBAR_STYLE, CRect (1, 1, 1, 1), AFX_IDW_TOOLBAR))
	{
		TRACE0("Failed to create TabView Bar\n");
		return FALSE;      // fail to create
	}

	DockPane (&m_wndTabViewBar);

	return TRUE;
}

//=============================================================================
// Method		: CSubFrame::PreCreateWindow
// Access		: public 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2010/04/02 - 15:29
// Desc.		: 
//=============================================================================
BOOL CSubFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;

	if(cs.hMenu != NULL)
	{
		::DestroyMenu(cs.hMenu);
		cs.hMenu = NULL;
	}

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;	
	cs.lpszClass = AfxRegisterWndClass(0);	


	BOOL bRet = FALSE;
	CString strTitle = _T("");
	bRet = strTitle.LoadString (IDR_MAINFRAME);
		
	CString strVersion = GetVersionInfo(_T("ProductVersion"));	
	CString	strDate = GetVersionInfo(_T("FileVersion"));

	CString strCaption;
	//strCaption.Format (_T("%s ( Ver %s : %s)"), strTitle.GetBuffer(), strVersion.GetBuffer(), strDate);
	strCaption.Format (_T("%s ( Ver %s : %s)"), g_szInsptrSysType[m_InspectionType], strVersion.GetBuffer(), strDate);
	CFrameWnd::SetTitle (strCaption);

	strTitle.Empty();
	strTitle.ReleaseBuffer();
	strVersion.Empty();
	strVersion.ReleaseBuffer();

	return TRUE;
}

//=============================================================================
// CSubFrame 진단
//=============================================================================
#ifdef _DEBUG
void CSubFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CSubFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG

//=============================================================================
// CSubFrame 메시지 처리기
//=============================================================================
//=============================================================================
// Method		: CSubFrame::OnSetFocus
// Access		: protected 
// Returns		: void
// Parameter	: CWnd *
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// 뷰 창으로 포커스를 이동합니다.	
	m_wndView_MainCtrl.SetFocus();
}

//=============================================================================
// Method		: CSubFrame::OnCmdMsg
// Access		: public 
// Returns		: BOOL
// Parameter	: UINT nID
// Parameter	: int nCode
// Parameter	: void * pExtra
// Parameter	: AFX_CMDHANDLERINFO * pHandlerInfo
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
BOOL CSubFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// 뷰에서 첫째 크랙이 해당 명령에 나타나도록 합니다.	
	if (m_wndView_MainCtrl.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// 그렇지 않으면 기본 처리합니다.
	return CFrameWndEx::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

//=============================================================================
// Method		: CSubFrame::OnClose
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnClose() 
{
//  	if (m_wndView_MainCtrl.IsTesting())
//  	{
//  		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."));
//  		return;
//  	}

	if (IDYES == AfxMessageBox(_T("Are you sure you want to exit the program?"), MB_YESNO))
	{
		TRACE(_T("<<< Exit Program >>> \n"));
		m_wndView_MainCtrl.FinalExitProgress();
		
		ExitProgram();
		CFrameWndEx::OnClose();
	}
}

//=============================================================================
// Method		: CSubFrame::OnActivate
// Access		: protected 
// Returns		: void
// Parameter	: UINT nState
// Parameter	: CWnd * pWndOther
// Parameter	: BOOL bMinimized
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWndEx::OnActivate(nState, pWndOther, bMinimized);

	if (m_wndCaptionBar.GetSafeHwnd () != NULL)
	{
		m_wndCaptionBar.SetParentActive (nState != WA_INACTIVE);
	}
}

//=============================================================================
// Method		: CSubFrame::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnSize(UINT nType, int cx, int cy) 
{
	SetupMemoryBitmapSize (CSize (cx, cy));

	if (m_wndCaptionBar.GetSafeHwnd () != NULL)
	{
		m_wndCaptionBar.SetParentMaximize (nType == SIZE_MAXIMIZED);
	}

	if (m_wndBannerBar.GetSafeHwnd () != NULL)
	{
		m_wndBannerBar.SetParentMaximize (nType == SIZE_MAXIMIZED);
	}

	// 그림이 깨지는 증상 때문에
	Invalidate();

	CFrameWndEx::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: CSubFrame::OnGetMinMaxInfo
// Access		: protected 
// Returns		: void
// Parameter	: MINMAXINFO FAR * lpMMI
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	CFrameWndEx::OnGetMinMaxInfo(lpMMI);

	if(m_wndTabViewBar.GetSafeHwnd () != NULL)
	{
		CRect rtRect (CPoint (0, 0), m_wndTabViewBar.CalcSize (FALSE));

		rtRect.bottom = 200;

		CalcWindowRect (rtRect, CWnd::adjustBorder);

		lpMMI->ptMinTrackSize.x = rtRect.Width ();
		lpMMI->ptMinTrackSize.y = rtRect.Height ();		
	}

	//2560 x 1080 ? 3440 x 1440 ? 3840 * 2160
	lpMMI->ptMinTrackSize.x = 1024;//440;//1920;// 2560;
	lpMMI->ptMinTrackSize.y = 768;//810;//1080;// 1080;

	lpMMI->ptMaxTrackSize.x = 1280;//440;//1920;// 2560;
	lpMMI->ptMaxTrackSize.y = 1024;//1080;// 1080;
}

//=============================================================================
// Method		: CSubFrame::OnSettingChange
// Access		: protected 
// Returns		: void
// Parameter	: UINT uFlags
// Parameter	: LPCTSTR lpszSection
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 
//=============================================================================
void CSubFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection) 
{
	CFrameWndEx::OnSettingChange(uFlags, lpszSection);

	if (m_wndCaptionBar.GetSafeHwnd () != NULL && afxGlobalData.fontRegular.GetSafeHandle () != NULL)
	{
		LOGFONT lf;
		ZeroMemory (&lf, sizeof (LOGFONT));
		afxGlobalData.fontRegular.GetLogFont (&lf);

		m_wndCaptionBar.SetCaptionFont (lf);
	}

	CRect rt;
	GetClientRect (rt);

	SetupMemoryBitmapSize (rt.Size ());
}

//=============================================================================
// Method		: CSubFrame::SetupMemoryBitmapSize
// Access		: private 
// Returns		: void
// Parameter	: const CSize & sz
// Qualifier	:
// Last Update	: 2010/04/02 - 15:28
// Desc.		: 배경으로 쓰이는 비트맵 세로 크기를 정함
//=============================================================================
void CSubFrame::SetupMemoryBitmapSize (const CSize& sz)
{
	CNTVisualManager* pManager = DYNAMIC_DOWNCAST (CNTVisualManager, CMFCVisualManager::GetInstance ());

	if (pManager != NULL)
	{
		CRect rtRes (0, 0, sz.cx, 0);

		CRect rt;

		CPane* bars[3] = {&m_wndCaptionBar, &m_wndBannerBar, &m_wndTabViewBar};

		for (long i = 0; i < 3; i++)
		{
			CPane* pBar = bars [i];

			if (pBar != NULL && pBar->GetSafeHwnd () != NULL)
			{
				pBar->GetWindowRect (rt);
				rtRes.bottom += rt.Height ();
			}
		}

		CDC* pDC = GetDC ();

		pManager->SetupMemoryBitmapSize (pDC, rtRes.Size ());

		ReleaseDC (pDC);
	}
}

//=============================================================================
// Method		: CSubFrame::OnTabView
// Access		: protected 
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2010/04/02 - 15:27
// Desc.		: 
//=============================================================================
void CSubFrame::OnTabView(UINT nID)
{
	if (m_nTabView == nID)
		return;
		
// #ifndef _DEBUG
// 	// LOT 진행 상태이면 return;
// 	if (m_wndView_MainCtrl.IsLotMode())
// 	{
// 		//if ((SUBVIEW_RECIPE == (nID - ID_TABVIEW_1)) || (SUBVIEW_IO == (nID - ID_TABVIEW_1)))
// 		if (SUBVIEW_RECIPE == (nID - ID_TABVIEW_1))
// 		{
// 			AfxMessageBox(_T("LOT 진행상태에서는 사용 할 수 없습니다."), MB_SYSTEMMODAL);
// 			return;
// 		}
// 	}
// 
// 	// 관리자 모드이면 암호를 묻지 않고
// 	if (FALSE == m_wndView_MainCtrl.IsManagerMode())
// 	{
// 		//if ((SUBVIEW_RECIPE == (nID - ID_TABVIEW_1)) || (SUBVIEW_IO == (nID - ID_TABVIEW_1)))
// 		if (SUBVIEW_RECIPE == (nID - ID_TABVIEW_1))
// 		{
// 			CDlg_ChkPassword	dlgPassword(this);
// 			if (IDCANCEL == dlgPassword.DoModal())
// 				return;
// 		}
// 	}
// #endif

	UINT nOldView = m_nTabView;

	m_nTabView = nID - ID_TABVIEW_1;
	
	// Pane 윈도우 숨김/보이기
	//ShowPaneByTabIndex(m_nTabView);

	m_wndView_MainCtrl.SwitchWindow(m_nTabView);
}

//=============================================================================
// Method		: CSubFrame::OnUpdateTabView
// Access		: protected 
// Returns		: void
// Parameter	: CCmdUI * pCmdUI
// Qualifier	:
// Last Update	: 2010/04/02 - 15:27
// Desc.		: 
//=============================================================================
void CSubFrame::OnUpdateTabView(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (TRUE);
	pCmdUI->SetCheck (pCmdUI->m_nID == (m_nTabView + ID_TABVIEW_1));
}

//=============================================================================
// Method		: CSubFrame::OnOption
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2010/5/7 - 14:21
// Desc.		: 옵션 처리
//=============================================================================
LRESULT CSubFrame::OnOption( WPARAM wParam, LPARAM lParam )
{
	// 패스워드 체크
	CDlg_ChkPassword	dlgPassword(this);
	if (IDCANCEL == dlgPassword.DoModal())
		return FALSE;

	CList<UINT, UINT> listPageID;
	UINT nIDCaption	= 0;
	for (UINT iCnt = 0; iCnt < OPT_TYPE_MAX; iCnt++) // 최대 옵션 Page 수 : 8
	{
		nIDCaption	= IDS_STR_OPT_PAGE_01 + iCnt;	// 캡션 스트링 ID
		listPageID.AddTail(nIDCaption);
	}

	CSheetOption *pPropSheet = new CSheetOption(IDS_STR_OPTION, NULL);
	// 검사기 설정 (*필수 항목)
	pPropSheet->SetInspectorType(m_InspectionType);
	pPropSheet->SetWM_ItemChanged(WM_OPTION_CHANGED);
	pPropSheet->SetRegistryPath (REG_PATH_OPTION_BASE);
	pPropSheet->InitPage(IDD_PROPPAGE_OPTION, listPageID);
	pPropSheet->DoModal();

	delete pPropSheet;
	
	return TRUE;
}

//=============================================================================
// Method		: CSubFrame::OnLogMsg
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam	-> 메세지 문자열
// Parameter	: LPARAM lParam	
//					-> HIWORD : 오류 메세지 인가?
//					-> LOWORD : 로그 종류 (기본, PLC, 관리PC 등)
// Qualifier	:
// Last Update	: 2010/10/14 - 17:38
// Desc.		: 로그 처리용
//	LOG_TAB_PLC		= 0,
//	LOG_TAB_MANPC,
//	LOG_TAB_IRDA,
//	LOG_TAB_BCR,
//=============================================================================
LRESULT CSubFrame::OnLogMsg( WPARAM wParam, LPARAM lParam )
{
	BOOL	bError = (BOOL)HIWORD(lParam);
	UINT	nType  = LOWORD(lParam);

	CString strLog = (LPCTSTR)wParam;

	AddLog (strLog, bError, nType);

	//if (NULL != (LPTSTR)wParam)
	//	delete [] (LPTSTR)wParam;

	return TRUE;
}

//=============================================================================
// Method		: CSubFrame::OnLoadComplete
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2010/10/15 - 15:24
// Desc.		:
//=============================================================================
LRESULT CSubFrame::OnLoadComplete( WPARAM wParam, LPARAM lParam )
{
	TRACE (_T("Start OnLoadComplete\n"));

	//-------------------------------------------
	// 옵션 설정 데이터 표시	
	//-------------------------------------------
	m_hThreadStartSetting = HANDLE(_beginthreadex(NULL, 0, ThreadStartSetting, this, 0, NULL));	
	
	TRACE (_T("End OnLoadComplete\n"));

	return 1;
}

//=============================================================================
// Method		: CSubFrame::AddLog
// Access		: protected 
// Returns		: void
// Parameter	: LPCTSTR lpszLog
// Parameter	: BOOL bError
// Parameter	: BYTE bySubLog
// Qualifier	:
// Last Update	: 2010/10/15 - 15:28
// Desc.		:
//=============================================================================
void CSubFrame::AddLog( LPCTSTR lpszLog, BOOL bError /*= FALSE*/, BYTE bySubLog /*= 0*/ )
{
	// UI -------------------------------------------------
	if (!m_wndView_MainCtrl.GetSafeHwnd())
		return;

	//m_wndView_MainCtrl.AddLog(lpszLog, bError, bySubLog);
}

//=============================================================================
// Method		: CSubFrame::AddLogProgramInfo
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/10/26 - 16:52
// Desc.		: 프로그램, 옵션설정 관련 정보를 로그에 추가합니다.
//=============================================================================
void CSubFrame::AddLogProgramInfo()
{
	CString strLog;

	if (m_strExecutedAppTime.IsEmpty())
	{	
		COleDateTime now = COleDateTime::GetCurrentTime ();	
		m_strExecutedAppTime = now.Format (_T("[%Y/%m/%d %H:%M:%S]"));
	}
	else
	{
		strLog.Format(_T("  Executed Time : %s"), m_strExecutedAppTime);
		AddLog (strLog);
	}

	strLog = _T("  Program Version : ") + GetVersionInfo(_T("ProductVersion"));
	AddLog (strLog);
	strLog = _T("  File Version    : ") + GetVersionInfo(_T("FileVersion"));
	AddLog (strLog);

	//-------------------------------------------
	// 옵션 설정 데이터 표시	
	//-------------------------------------------
	CLT_Option	ntOption;
	stLT_Option	stOption;
	ntOption.LoadOption_All(stOption);

}

//=============================================================================
// Method		: CSubFrame::InitProgram
// Access		: private 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/10/15 - 14:54
// Desc.		: 프로그램 시작 할 때 처리해야 할 코드
//=============================================================================
void CSubFrame::InitProgram()
{
	AddLog(_T("=================================================="));
	AddLog(_T("           LGE Camera Test System"));
	AddLog(_T("--------------------------------------------------"));
	AddLogProgramInfo();
	AddLog(_T("=================================================="));

//#ifdef USE_TEST_MODE
//	AfxMessageBox(_T("프로그램 로딩이 끝났당꼐!"));
//#endif
}

//=============================================================================
// Method		: CSubFrame::ExitProgram
// Access		: private 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/10/15 - 14:54
// Desc.		: 프로그램 종료 할 때 처리해야 할 코드
//=============================================================================
void CSubFrame::ExitProgram()
{
	AddLog(_T("=================================================="));
	AddLog(_T("                 Exit  Program"));
	AddLog(_T("--------------------------------------------------"));
	AddLogProgramInfo();
	AddLog(_T("=================================================="));
}

//=============================================================================
// Method		: ShowWindow_2ndView
// Access		: private  
// Returns		: void
// Parameter	: __in BOOL bShow
// Qualifier	:
// Last Update	: 2016/5/17 - 17:15
// Desc.		:
//=============================================================================
void CSubFrame::ShowWindow_2ndView(__in BOOL bShow)
{
	if (bShow)
	{
		// 모니터 전체 해상도로 출력
		//RECT rcDesktop;
		//HWND hWndDesktop = ::GetDesktopWindow();
		//::GetWindowRect(hWndDesktop, &rcDesktop);

		GetMonitorInformation(m_infoMonitor);

		int iMonCnt = m_infoMonitor.nMonitor;

		int iResWidth = GetSystemMetrics(SM_CXSCREEN);
		int iResHeight = GetSystemMetrics(SM_CYSCREEN);

		if (1 < iMonCnt)
		{
			RECT rt = m_infoMonitor.aryMonitors.GetAt(1).rcMonitor;
			RECT rtWork = m_infoMonitor.aryMonitors.GetAt(1).rcWork;

// 			m_wnd_2ndView.MoveWindow(rt.left, rt.top, rt.right - rt.left, rt.bottom - rt.top);
// 			m_wnd_2ndView.ShowWindow(SW_SHOW);
		}
		/*else
		{
		RECT rt = m_infoMonitor.aryMonitors.GetAt(0).rcMonitor;
		RECT rtWork = m_infoMonitor.aryMonitors.GetAt(0).rcWork;

		m_wnd_2ndView.MoveWindow(rt.left, rt.top, rt.right - rt.left, rt.bottom - rt.top);
		m_wnd_2ndView.ShowWindow(SW_SHOW);
		}*/


		// 모니터 개수 표시
		// 	CString strDevice;
		// 	for (int iCnt = 0; iCnt < iMonCnt; iCnt++)
		// 	{
		// 		strDevice = m_infoMonitor.aryMonitors.GetAt(iCnt).szDevice;
		// 		strDevice.Remove('\\');
		// 		strDevice.Remove('.');
		// 		
		// 	}	
	}
	else
	{
		//m_wnd_2ndView.ShowWindow(SW_HIDE);
	}
}

//=============================================================================
// Method		: CSubFrame::OnWatchExtProcess
// Access		: private 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2010/12/29 - 14:00
// Desc.		:
//=============================================================================
LRESULT CSubFrame::OnWatchExtProcess( WPARAM wParam, LPARAM lParam )
{
	//AddLog(_T("AppRestarter Check"));

	return TRUE;
}

//=============================================================================
// Method		: CSubFrame::LoadStartSetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2011/1/11 - 17:15
// Desc.		:
//=============================================================================
void CSubFrame::LoadStartSetting()
{
	//-------------------------------------------
	// 옵션 설정 데이터 표시	
	//-------------------------------------------
	CLT_Option	ntOption;
	stOpt_Insp	stOption;
	ntOption.LoadOption_Inspector(stOption);


	ShowWindow_2ndView(TRUE);


	// 초기 로그
	InitProgram();
	
	m_wndView_MainCtrl.InitStartProgress();


	//m_wndTabViewBar.ShowButton_Exclusive(0, FALSE);

	//Sleep(3000);

	//m_wndTabViewBar.ShowButton_Exclusive(0, TRUE);
}

//=============================================================================
// Method		: ShowPaneByTabIndex
// Access		: protected  
// Returns		: void
// Parameter	: UINT nTabIndex
// Qualifier	:
// Last Update	: 2016/10/2 - 18:50
// Desc.		:
//=============================================================================
void CSubFrame::ShowPaneByTabIndex(UINT nTabIndex)
{
	// Pane 윈도우 숨김/보이기
	BOOL bUIChanged = FALSE;
	CLT_Option	ntOption;
	stOpt_Insp	stOption;
	ntOption.LoadOption_Inspector(stOption);
	if (SUBVIEW_RECIPE == (nTabIndex))
	{
		if (stOption.UseDeviceInfoPane)
		{
			if (m_wndDeviceStatus.IsVisible())
			{
				m_wndDeviceStatus.ShowPane(FALSE, 0, FALSE);
				bUIChanged = TRUE;
			}
		}
	}
	else
	{
		if (stOption.UseDeviceInfoPane)
		{
			if (FALSE == m_wndDeviceStatus.IsVisible())
			{
				m_wndDeviceStatus.ShowPane(TRUE, 0, FALSE);
				bUIChanged = TRUE;
			}
		}
	}

	// UI가 변경 되었으면 다시 그리기
	if (bUIChanged)
	{
		CRect rc;
		GetClientRect(rc);
		SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
	}
}

//=============================================================================
// Method		: CSubFrame::ThreadStartSetting
// Access		: protected static 
// Returns		: UINT WINAPI
// Parameter	: LPVOID lParam
// Qualifier	:
// Last Update	: 2011/1/11 - 17:17
// Desc.		:
//=============================================================================
UINT WINAPI CSubFrame::ThreadStartSetting( LPVOID lParam )
{
	CSubFrame* pThis = (CSubFrame*)lParam;	

	pThis->LoadStartSetting ();

	return TRUE;
}

//=============================================================================
// Method		: CSubFrame::OnOptionChanged
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2013/2/20 - 15:07
// Desc.		:
// m_WMItemChanged -> WPARAM : Redraw Window
//					  LPARAM : Reconnect Network & Comport
//=============================================================================
LRESULT CSubFrame::OnOptionChanged( WPARAM wParam, LPARAM lParam )
{
	// UI가 변경 되었으면 다시 그림?
	BOOL bUIChanged = FALSE;

	CLT_Option	ntOption;
	stLT_Option	stOption;
	ntOption.LoadOption_All(stOption);

	m_wndView_MainCtrl.ReloadOption();

	if (m_bShowDeviceInfoPane != stOption.Inspector.UseDeviceInfoPane)
	{
		m_bShowDeviceInfoPane = stOption.Inspector.UseDeviceInfoPane;
		m_wndDeviceStatus.ShowPane(m_bShowDeviceInfoPane, 0, TRUE);

		bUIChanged = TRUE;
	}

	// UI가 변경 되었으면 다시 그리기
	if (bUIChanged)
	{
		CRect rc;
		GetClientRect(rc);
		SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
	}

	return 1;
}

//=============================================================================
// Method		: CSubFrame::OnTestFunction
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2013/6/11 - 20:03
// Desc.		:
//=============================================================================
LRESULT CSubFrame::OnTestFunction( WPARAM wParam, LPARAM lParam )
{
	UINT nTestNo = (UINT)wParam;

	m_wndView_MainCtrl.Test_Process(nTestNo);

	return 1;
}

//=============================================================================
// Method		: CSubFrame::OnChangeView
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2013/6/18 - 19:13
// Desc.		:
//=============================================================================
LRESULT CSubFrame::OnChangeView( WPARAM wParam, LPARAM lParam )
{
	UINT nView			= (UINT)wParam + ID_TABVIEW_1;
	UINT nEquipIndex	= (UINT)lParam;	

	OnTabView(nView);	

	return TRUE;
}

//=============================================================================
// Method		: OnPermissionMode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/9/27 - 20:01
// Desc.		: 검사 권한 모드 변경 알림
//=============================================================================
LRESULT CSubFrame::OnPermissionMode(WPARAM wParam, LPARAM lParam)
{
	enPermissionMode enAcessMode = (enPermissionMode)wParam;

	//if (Permission_Operator == m_wndView_MainCtrl.GetPermissionMode())
	if (Permission_Operator == enAcessMode)
	{
		m_wndTabViewBar.ShowButton_Exclusive(0, FALSE);
	}
	else
	{
		m_wndTabViewBar.ShowButton_Exclusive(0, TRUE);
	}

	//m_wndView_MainCtrl.SetPermissionMode(enAcessMode);

	return TRUE;
}

//=============================================================================
// Method		: OnManualBarcode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/11/10 - 13:24
// Desc.		:
//=============================================================================
LRESULT CSubFrame::OnManualBarcode(WPARAM wParam, LPARAM lParam)
{
	m_wndView_MainCtrl.ManualBarcode();

	return TRUE;
}

//=============================================================================
// Method		: SetSystemType
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/10/11 - 16:39
// Desc.		:
//=============================================================================
void CSubFrame::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

