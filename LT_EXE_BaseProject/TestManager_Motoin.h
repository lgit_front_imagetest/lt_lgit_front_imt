#ifndef TestManager_Motoin_h__
#define TestManager_Motoin_h__

#pragma once

#include "MotionManager.h"
#include "DigitalIOCtrl.h"
#include "Def_DataStruct.h"
#include "Def_ResultCode_Cm.h"
#include "Def_Motion.h"
#include "Def_Digital_IO.h"

#include "Def_Enum_Cm.h"

#include "LT_Option.h"
#include "WLog.h"

class CTestManager_Motoin
{
public:
	CTestManager_Motoin();
	virtual	~CTestManager_Motoin();

	// LOG 메시지 
	CWLog			m_Log;
	HWND			m_hOwnerWnd;

	// LOG 메시지 
	void SetLogMsgID(HWND hOwnerWnd, UINT nWM_ID, UINT nLogType = 0)
	{
		m_hOwnerWnd = hOwnerWnd;
		m_Log.SetOwner(m_hOwnerWnd, nWM_ID);
		m_Log.SetLogType(nLogType);
	}

	// 도어센서 기능 추가 필요
	void SetLTOption(stLT_Option* pstOption)
	{
		if (pstOption == NULL)
			return;

		m_pOption = pstOption;
	};

	void SetPtr_Device(__in CMotionManager*	pstDevice, __in CDigitalIOCtrl* pstDIO)
	{
		if (NULL == pstDevice)
			return;

		m_pDevice	= pstDevice;
		m_pDIO		= pstDIO;
	}

	void SetPtr_TeachInfo(__in ST_TeachInfo* pstTeachInfo)
	{
		m_pstTeachInfo = pstTeachInfo;
	}

	void SetSystemType(__in enInsptrSysType enType)
	{
		m_SysType = enType;
	}

	void SetModelType(__in enModelType nModelType)
	{
		m_ModelType = nModelType;
	}

	LRESULT		MotorAllEStop					();

	// 전체 모터 사용 가능 여부 확인
	LRESULT		MotorGetStatus					();

	// 메뉴얼 모터 원점
	LRESULT		MotorManualOrigin				(__in int iAxis);

	// 전체 모터 원점 시작
	LRESULT		MotorAllOriginStart				();	

	// 장비별 모터 원점 루틴 (Product 가 없는 경우)
	LRESULT		MotorSysOriginSeq					(enInsptrSysType SysType);

	// 원점 루틴
	LRESULT		MotorOrigin_2DCal				();
	LRESULT		MotorOrigin_Foc					();
	LRESULT		MotorOrigin_ImgT();

	// 전체 원점 중지
	LRESULT		MotorAllOriginStop					();

	// 모터 개별 제어 
	LRESULT		MotorMoveAxis		 				(__in int iAxis, __in double dbPos);

	LRESULT		IsReadyOk							(__in UINT nPort, __in BOOL bStatus = TRUE);

	LRESULT		OnActionTesting						(__in double dbDistanceX, __in double dbDistanceY, __in double dbDegreeR);
	LRESULT		OnActionTesting_2D_CAL				(__in double dbDistanceX, __in double dbDistanceY, __in double dbDegreeR);

	LRESULT		OnActionTesting_ImgT_Change			(__in UINT nParaIdx = 0);
	LRESULT		OnActionTesting_ImgT_Chart			(__in UINT nParaIdx = 0);
	LRESULT		OnActionTesting_ImgT_Par			(__in UINT nParaIdx = 0);
	LRESULT		OnActionTesting_ImgT_Y				(__in double dbDistanceY, __in BOOL bUseShutter = TRUE);

	LRESULT		OnActionTesting_Foc_Fix				(__in BOOL bOnOff, __in UINT nParaID = 0);
	LRESULT		OnActionTesting_Foc_Gripper			(__in BOOL bOnOff);
	LRESULT		OnActionTesting_Foc_Stage			(__in BOOL bUpDn );
	LRESULT		OnActionTesting_Foc_Particle		(__in BOOL bInOut);
	LRESULT		OnActionTesting_Foc_Driver			(__in BOOL bUpDn);
		LRESULT		OnActionTesting_Foc_Collet		(__in BOOL bUpDn);

	LRESULT		OnActionStandby						();
	LRESULT		OnActionStandby_2D_CAL				(__in BOOL bUseShutter = TRUE);
	LRESULT		OnActionStandby_ImgT				(__in BOOL bUseShutter = TRUE);

	LRESULT		OnActionLoad						();
	LRESULT		OnActionLoad_2D_CAL					(__in BOOL bUseShutter = TRUE);
	LRESULT		OnActionLoad_IgmT					(__in BOOL bUseShutter = TRUE);

	LRESULT		OnActionInit_Foc					();
	LRESULT		OnActionLoad_Foc					();
	LRESULT		OnActionUnLoad_Foc					();

	LRESULT		OnActionLampControl					(__in long lPort, __in enum_IO_SignalType enType);

	LRESULT		OnActionTesting_OC_Adj				(__in int iOffsetX, __in int iOffsetY, __in double dbPixelSensor);
	LRESULT		OnActionTesting_Rotate_Adj			(__in double dbDegree, __in double dbPixelSensor);


private:
	UINT	m_nStagePoint;

	stLT_Option*			m_pOption;
	CMotionManager*			m_pDevice			= NULL;
	CDigitalIOCtrl*			m_pDIO				= NULL;
	enInsptrSysType			m_SysType;
	enModelType				m_ModelType;

	ST_TeachInfo*			m_pstTeachInfo;
};

#endif // TestManager_Motoin_h__

