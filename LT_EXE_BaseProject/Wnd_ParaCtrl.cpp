// Wnd_ParaCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ParaCtrl.h"

// CWnd_ParaCtrl
#define		IDC_EDIT_TESTFUNC		2000

#define		IDC_BTN_TESTFUNC		3000

IMPLEMENT_DYNAMIC(CWnd_ParaCtrl, CVGGroupWnd)

CWnd_ParaCtrl::CWnd_ParaCtrl()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_SEMIBOLD,			// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_nParaIdx = 0;
}

CWnd_ParaCtrl::~CWnd_ParaCtrl()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ParaCtrl, CVGGroupWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_TESTFUNC, IDC_BTN_TESTFUNC + ManuP_Max_Num, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_ParaCtrl 메시지 처리기입니다.
int CWnd_ParaCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CVGGroupWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Static & Button
	for (UINT nIdx = 0; nIdx < ManuP_Max_Num; nIdx++)
	{
		m_stStatusName[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stStatusName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stStatusName[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		m_stStatusName[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
		m_stStatusName[nIdx].SetFont(&m_font);

		m_btnFuncCtrl[nIdx].Create(g_szManuParaName[nIdx], dwStyle, rectDummy, this, IDC_BTN_TESTFUNC + nIdx);
	}
	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/12/13 - 18:58
// Desc.		:
//=============================================================================
void CWnd_ParaCtrl::OnSize(UINT nType, int cx, int cy)
{
	CVGGroupWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iLeftMargin		= 7;
	int iRightMargin	= 7;
	int iTopMargin		= 27;
	int iBottomMargin	= 7;

	int iSpacing = 5;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;

	int iBottom = cy - iBottomMargin;
	int iWidth = cx - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;

	int iDataWidth = (int)(iWidth * 0.7);
	int iBtnWidth  = iWidth - iDataWidth - iSpacing;

	int iCtrlHeight = iHeight / ManuP_Max_Num;

	int iDataLeft = iLeft + iBtnWidth + iSpacing;

	for (UINT nIdx = 0; nIdx < ManuP_Max_Num; nIdx++)
	{
		m_btnFuncCtrl[nIdx].MoveWindow(iLeft, iTop, iBtnWidth, iCtrlHeight);
		m_stStatusName[nIdx].MoveWindow(iDataLeft, iTop, iDataWidth, iCtrlHeight);
		iTop += iCtrlHeight;
	}
}

BOOL CWnd_ParaCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CVGGroupWnd::PreCreateWindow(cs);
}

void CWnd_ParaCtrl::OnRangeCmds(__in UINT nID)
{
	UINT nFunc = nID - IDC_BTN_TESTFUNC;
	LRESULT lReturn = GetParent()->GetParent()->SendMessage(WM_MANAUL_SEQUENCE, (WPARAM)m_nParaIdx, (LPARAM)nFunc);
}