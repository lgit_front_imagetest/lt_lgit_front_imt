﻿//*****************************************************************************
// Filename	: 	TestManager_Test.cpp
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Test.h"
#include "CommonFunction.h"

#define		Center_Auto_Detect	1				// (기본 : 사용)

CTestManager_Test::CTestManager_Test()
{
}

CTestManager_Test::~CTestManager_Test()
{
}

//=============================================================================
// Method		: CenterPointFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_OpticalCenter_Opt stTestOpt
// Parameter	: __out ST_OpticalCenter_Data & stTestData
// Parameter	: __in int iCenterX
// Parameter	: __in int iCenterY
// Parameter	: __in UINT nParaID
// Qualifier	:
// Last Update	: 2018/3/15 - 11:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::CenterPointFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data_Foc &stTestData, __in int iCenterX /*= 0*/, __in int iCenterY /*= 0*/, __in UINT nParaID /*= 0*/)
{
	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int iBright = stTestOpt.nBrightness;
	int iColor = stTestOpt.nMarkColor;
	int nSharpness = 0;

	int iPosX = 0;
	int iPosY = 0;

	CRect rtTest = stTestOpt.rtROI;

	//중심점을 찾는다
	stTestData.nDetectResult = m_LT_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, rtTest, iBright, iPosX, iPosY, iColor, nSharpness);

	//광축 Pixel값이 나옴. [ nPosX, nPosY ]
	if (TRUE == stTestData.nDetectResult)
	{
		//-영상에서 그려질 위치
		stTestData.nPixX = iPosX;
		stTestData.nPixY = iPosY;

		//카메라 상태 별로 값 변경.
		CamType_CenterPoint(iImgW, iImgH, iPosX, iPosY);

		stTestData.nResultPosX = iPosX + stTestOpt.iOffsetX;
		stTestData.nResultPosY = iPosY + stTestOpt.iOffsetY;

		stTestData.iStandPosDevX = stTestData.nResultPosX - stTestOpt.nStandarX;
		stTestData.iStandPosDevY = stTestData.nResultPosY - stTestOpt.nStandarY;

		stTestData.nDetectResult = RC_OK;
	}
	else
	{
		stTestData.nResultPosX = iPosX;
		stTestData.nResultPosY = iPosY;

		stTestData.iStandPosDevX = 0;
		stTestData.iStandPosDevY = 0;

		stTestData.nDetectResult = RC_Err_TestFail;

		lReturn = RC_Detect;
	}

	BOOL bMinUseX = stTestOpt.stSpec_Min[Spec_OC_X].bEnable;
	BOOL bMinUseY = stTestOpt.stSpec_Min[Spec_OC_Y].bEnable;
	BOOL bMaxUseX = stTestOpt.stSpec_Max[Spec_OC_X].bEnable;
	BOOL bMaxUseY = stTestOpt.stSpec_Max[Spec_OC_Y].bEnable;

	int iMinDevX = stTestOpt.stSpec_Min[Spec_OC_X].iValue;
	int iMinDevY = stTestOpt.stSpec_Min[Spec_OC_Y].iValue;
	int iMaxDevX = stTestOpt.stSpec_Max[Spec_OC_X].iValue;
	int iMaxDevY = stTestOpt.stSpec_Max[Spec_OC_Y].iValue;

	// 판정
	stTestData.nResultX = Get_MeasurmentData(bMinUseX, bMaxUseX, iMinDevX, iMaxDevX, stTestData.iStandPosDevX);
	stTestData.nResultY = Get_MeasurmentData(bMinUseY, bMaxUseY, iMinDevY, iMaxDevY, stTestData.iStandPosDevY);

	stTestData.nResult = stTestData.nResultX & stTestData.nResultY & stTestData.nDetectResult;

	return lReturn;
}

//=============================================================================
// Method		: CenterPointFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_OpticalCenter_Opt stTestOpt
// Parameter	: __out ST_OpticalCenter_Data & stTestData
// Parameter	: __in int iCenterX
// Parameter	: __in int iCenterY
// Parameter	: __in UINT nParaID
// Qualifier	:
// Last Update	: 2018/3/15 - 11:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::CenterPointFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data_IQ &stTestData, __in int iCenterX /*= 0*/, __in int iCenterY /*= 0*/, __in UINT nParaID /*= 0*/)
{
	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int iBright = stTestOpt.nBrightness;
	int iColor = stTestOpt.nMarkColor;
	int nSharpness = 0;

	int iPosX = 0;
	int iPosY = 0;

	CRect rtTest = stTestOpt.rtROI;

	//중심점을 찾는다
	stTestData.nDetectResult = m_LT_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, rtTest, iBright, iPosX, iPosY, iColor, nSharpness);

	//광축 Pixel값이 나옴. [ nPosX, nPosY ]
	if (TRUE == stTestData.nDetectResult)
	{
		//-영상에서 그려질 위치
		stTestData.nPixX = iPosX;
		stTestData.nPixY = iPosY;

		//카메라 상태 별로 값 변경.
		CamType_CenterPoint(iImgW, iImgH, iPosX, iPosY);

		stTestData.nResultPosX = iPosX + stTestOpt.iOffsetX;
		stTestData.nResultPosY = iPosY + stTestOpt.iOffsetY;

		stTestData.iStandPosDevX = stTestData.nResultPosX - stTestOpt.nStandarX;
		stTestData.iStandPosDevY = stTestData.nResultPosY - stTestOpt.nStandarY;

		stTestData.nDetectResult = RC_OK;
	}
	else
	{
		stTestData.nResultPosX = iPosX;
		stTestData.nResultPosY = iPosY;

		stTestData.iStandPosDevX = 0;
		stTestData.iStandPosDevY = 0;

		stTestData.nDetectResult = RC_Err_TestFail;

		lReturn = RC_Detect;
	}

	BOOL bMinUseX = stTestOpt.stSpec_Min[Spec_OC_X].bEnable;
	BOOL bMinUseY = stTestOpt.stSpec_Min[Spec_OC_Y].bEnable;
	BOOL bMaxUseX = stTestOpt.stSpec_Max[Spec_OC_X].bEnable;
	BOOL bMaxUseY = stTestOpt.stSpec_Max[Spec_OC_Y].bEnable;

	int iMinDevX = stTestOpt.stSpec_Min[Spec_OC_X].iValue;
	int iMinDevY = stTestOpt.stSpec_Min[Spec_OC_Y].iValue;
	int iMaxDevX = stTestOpt.stSpec_Max[Spec_OC_X].iValue;
	int iMaxDevY = stTestOpt.stSpec_Max[Spec_OC_Y].iValue;

	// 판정
	stTestData.nResultX = Get_MeasurmentData(bMinUseX, bMaxUseX, iMinDevX, iMaxDevX, stTestData.iStandPosDevX);
	stTestData.nResultY = Get_MeasurmentData(bMinUseY, bMaxUseY, iMinDevY, iMaxDevY, stTestData.iStandPosDevY);

	stTestData.nResult = stTestData.nResultX & stTestData.nResultY & stTestData.nDetectResult;

	return lReturn;
}
//=============================================================================
// Method		: CamType_CenterPoint
// Access		: public  
// Returns		: void
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __out int & nPosX
// Parameter	: __out int & nPosY
// Qualifier	:
// Last Update	: 2018/2/27 - 9:23
// Desc.		:
//=============================================================================
void CTestManager_Test::CamType_CenterPoint(__in int iImgW, __in int iImgH, __out int &nPosX, __out  int &nPosY)
{
	int ICamType = m_nCameraModel;
	int IChangePosX = 0;
	int IChangePosY = 0;

	switch (ICamType)
	{
		// 좌우반전
	case CAM_STATE_MIRROR:
		IChangePosX = nPosX;
		IChangePosY = nPosY;
		break;

		// 상하반전
	case CAM_STATE_FLIP:
		IChangePosX = iImgW - nPosX;
		IChangePosY = iImgH - nPosY;
		break;

		// 오리지널
	case CAM_STATE_ORIGINAL:
		IChangePosX = iImgW - nPosX;
		IChangePosY = nPosY;
		break;

		// 로테이트
	case CAM_STATE_ROTATE:
		IChangePosX = nPosX;
		IChangePosY = iImgH - nPosY;
		break;

	default:
		break;
	}

	nPosX = IChangePosX;
	nPosY = IChangePosY;
}


//=============================================================================
// Method		: LGIT_Func_Chart
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Chart stOption
// Parameter	: __out ST_Result_Chart & stResult
// Qualifier	:
// Last Update	: 2018/8/23 - 16:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Chart(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Chart stOption, __out ST_Result_Chart& stResult)
{
	LRESULT	lReturn = RC_OK;

	int iBright = stOption.nBrightness;
	int iColor = stOption.nMarkColor;
	int nSharpness = 0;

	int iCenterX = 0;
	int iCenterY = 0;

	lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.rtROI, stResult.rtROI);

	if (RC_OK == lReturn)
	{
		stResult.bDetect = m_LT_CircleDetect.CircleDetect_Test(pImage8bit, iImageW, iImageH, stResult.rtROI, iBright, iCenterX, iCenterY, iColor, nSharpness);
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_SFR
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_SFR stOption
// Parameter	: __out ST_Result_SFR & stResult
// Qualifier	:
// Last Update	: 2018/8/7 - 0:03
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_SFR(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption, __out ST_Result_SFR_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	/*vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	m_LG_Algorithm.LoadImageData(ibuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");*/

	// 자동 ROI에 사용될 영상 데이터
	IplImage *lpAutoImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 3);
	memcpy(lpAutoImage->imageData, pImage8bit, lpAutoImage->widthStep * lpAutoImage->height);

	// 나중에 꼭 바꾸자...
	OnTestProcess_SFRROI(lpAutoImage, stOption, stResult);

	// 자동 ROI에 사용된 영상 데이터 해제
	cvReleaseImage(&lpAutoImage);

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (TRUE == stOption.stInput[nROI].bEnable)
		{
			// 2. SFR Config
			//m_LG_Algorithm.LGIT_SFR_Config(stOption.stInput[nROI].rtROI.Width(), stOption.stInput[nROI].rtROI.Height(), stOption.dbMaxEdgeAngle, stOption.dbPixelSize, (ESFRAlgorithmType)stOption.eType, (ESFRAlgorithmMethod)stOption.eMethod, stOption.bEnableLog);// , (ESFRFrequencyUnit)stOption.eConverse);
			m_LG_Algorithm.LGIT_SFR_Config(stOption.stInput[nROI].rtROI.Width(), stOption.stInput[nROI].rtROI.Height(), stOption.dbMaxEdgeAngle, stOption.dbPixelSize, (ESFRAlgorithmType)stOption.eType, (ESFRAlgorithmMethod)stOption.eMethod, stOption.bEnableLog, (ESFRFrequencyUnit)stOption.eConverse);

			// 3. SFR DATA
			//	m_LG_Algorithm.LGIT_SFR_Func(ibuffer.data(), iImageW, iImageH, stResult.rtROI[nROI], stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);
			m_LG_Algorithm.LGIT_SFR_Func(pImageRaw, iImageW, iImageH, stResult.rtROI[nROI], stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);

			stResult.dbValue[nROI] += stOption.stInput[nROI].dbOffset;

			// 판정
			BOOL	bMinUse = stOption.stInput[nROI].stSpecMin.bEnable;
			BOOL	bMaxUse = stOption.stInput[nROI].stSpecMax.bEnable;
			double dbMinDev = stOption.stInput[nROI].stSpecMin.dbValue;
			double dbMaxDev = stOption.stInput[nROI].stSpecMax.dbValue;

			stResult.bResult[nROI] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbValue[nROI]);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_SFR
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_SFR stOption
// Parameter	: __out ST_Result_SFR & stResult
// Qualifier	:
// Last Update	: 2018/8/7 - 0:03
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_SFR(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption, __out ST_Result_SFR_FOC& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(ibuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	// 자동 ROI에 사용될 영상 데이터
	IplImage *lpAutoImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 3);
	memcpy(lpAutoImage->imageData, pImage8bit, lpAutoImage->widthStep * lpAutoImage->height);

	// 나중에 꼭 바꾸자...
	OnTestProcess_SFRROI(lpAutoImage, stOption, stResult);

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (TRUE == stOption.stInput[nROI].bEnable)
		{
			// 2. SFR Config
			m_LG_Algorithm.LGIT_SFR_Config(stOption.stInput[nROI].rtROI.Width(), stOption.stInput[nROI].rtROI.Height(), stOption.dbMaxEdgeAngle, stOption.dbPixelSize, (ESFRAlgorithmType)stOption.eType, (ESFRAlgorithmMethod)stOption.eMethod, stOption.bEnableLog, (ESFRFrequencyUnit)stOption.eConverse);

			// 3. SFR DATA
			//m_LG_Algorithm.LGIT_SFR_Func(ibuffer.data(), iImageW, iImageH, stResult.rtROI[nROI], stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);
			m_LG_Algorithm.LGIT_SFR_Func(pImageRaw, iImageW, iImageH, stResult.rtROI[nROI], stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);

			stResult.dbValue[nROI] += stOption.stInput[nROI].dbOffset;

			// 판정
			BOOL	bMinUse = stOption.stInput[nROI].stSpecMin.bEnable;
			BOOL	bMaxUse = stOption.stInput[nROI].stSpecMax.bEnable;
			double dbMinDev = stOption.stInput[nROI].stSpecMin.dbValue;
			double dbMaxDev = stOption.stInput[nROI].stSpecMax.dbValue;

			stResult.bResult[nROI] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbValue[nROI]);
		}
	}

	// 자동 ROI에 사용된 영상 데이터 해제
	cvReleaseImage(&lpAutoImage);

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_DynamicBW
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_DynamicBW stOption
// Parameter	: __out ST_Result_DynamicBW & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Rotation(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation_IQ& stResult)
{
	stResult.Reset();
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//  	vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(ibuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");
	// 1. 자동ROI
	//for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
	//{
	//	lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, stResult.ptROI[nROI]);//!SH _180904: Point 버전 필요
	//}
	int iROITestNum = stOption.nFiducialMarkNum;
	for (UINT nROI = 0; nROI < iROITestNum; nROI++)
	{
		if (stOption.stInput[nROI].bEnable)
		{
			CRect tempROI(0, 0, 0, 0);
			lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, tempROI);
			lReturn = LURI_Func_DetectROI_White(pImage8bit, iImageW, iImageH, tempROI, stResult.ptROI[nROI], stOption.stInput[nROI].nMarkColor);
		}
	}

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	//RECT pPoitnTemp[ROI_Rotation_Max];

	//Set_Rotation_ROI_Array(stResult, pPoitnTemp);

	// 2. Rotation Config
	m_LG_Algorithm.LGIT_Rotation_Config(stOption.nROIBoxSize, stOption.nMaxROIBoxSize, stOption.dRadius, stOption.dRealGapX, stOption.dRealGapY, stOption.nFiducialMarkNum, stOption.nFiducialMarkType, stOption.dModuleChartDistance, stOption.nDistortionAlrotithmType);

	//// 3. Rotation DATA
	//m_LG_Algorithm.LGIT_Rotation_Func(ibuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.dbRotation);
	m_LG_Algorithm.LGIT_Rotation_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbRotation);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Rotation].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Rotation].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Rotation].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Rotation].dbValue;

	stResult.dbRotation *= stOption.dbOffsetRot;

	stResult.bRotation = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbRotation);


	if (stResult.bRotation == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_Distortion(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Distortion_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(ibuffer.data(), "C:\\ProgramSetting\\Image\image\\Capatue_Distortion_0916_180054.raw");
	// 1. 자동ROI
	for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
	{
		CRect tempROI(0, 0, 0, 0);
		lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, tempROI);//!SH _180904: Point 버전 필요
		lReturn = LURI_Func_DetectROI_White(pImage8bit, iImageW, iImageH, tempROI, stResult.ptROI[nROI], stOption.stInput[nROI].nMarkColor);
	}

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	/*RECT pPoitnTemp[ROI_Rotation_Max];*/

	//Set_Rotation_ROI_Array(stResult, pPoitnTemp);

	// 2. Rotation Config
	m_LG_Algorithm.LGIT_Rotation_Config(stOption.nROIBoxSize, stOption.nMaxROIBoxSize, stOption.dRadius, stOption.dRealGapX, stOption.dRealGapY, stOption.nFiducialMarkNum, stOption.nFiducialMarkType, stOption.dModuleChartDistance, stOption.nDistortionAlrotithmType);

	//// 3. Rotation DATA
	//m_LG_Algorithm.LGIT_Rotation_Func(ibuffer.data(), iImageW, iImageH, pPoitnTemp, stResult.dbDistortion);
	m_LG_Algorithm.LGIT_Distortion_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbDistortion);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Rotation].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Rotation].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Rotation].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Rotation].dbValue;

	stResult.dbDistortion *= stOption.dbOffsetRot;

	stResult.bDistortion = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDistortion);


	if (stResult.bDistortion == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_FOV(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_FOV stOption, __out ST_Result_FOV_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(ibuffer.data(), "C:\\ProgramSetting\\Image\image\\Capatue_Distortion_0916_180054.raw");
	// 1. 자동ROI
	for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
	{
		CRect tempROI(0, 0, 0, 0);
		lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, tempROI);//!SH _180904: Point 버전 필요
		lReturn = LURI_Func_DetectROI_White(pImage8bit, iImageW, iImageH, tempROI, tempROI, stOption.stInput[nROI].nMarkColor);
	}

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	/*RECT pPoitnTemp[ROI_Rotation_Max];*/

	//Set_Rotation_ROI_Array(stResult, pPoitnTemp);

	// 2. Rotation Config
	m_LG_Algorithm.LGIT_Rotation_Config(stOption.nROIBoxSize, stOption.nMaxROIBoxSize, stOption.dRadius, stOption.dRealGapX, stOption.dRealGapY, stOption.nFiducialMarkNum, stOption.nFiducialMarkType, stOption.dModuleChartDistance, stOption.nDistortionAlrotithmType);

	//// 3. Rotation DATA
	//m_LG_Algorithm.LGIT_Rotation_Func(ibuffer.data(), iImageW, iImageH, pPoitnTemp, stResult.dbDFOV);
	m_LG_Algorithm.LGIT_FOV_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbDFOV, stResult.dbHFOV, stResult.dbVFOV);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Rotation].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Rotation].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Rotation].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Rotation].dbValue;

	stResult.dbDFOV *= stOption.dbOffsetRot;

	stResult.bDFOV = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDFOV);


	if (stResult.bDFOV == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_Rotation
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Rotation stOption
// Parameter	: __out ST_Result_Rotation_Foc & stResult
// Qualifier	:
// Last Update	: 2018/10/24 - 10:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Rotation(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation_Foc& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> ibuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(ibuffer.data(), "D:\\Capatue_Rotation_1114_195728.raw");

	int iROITestNum = stOption.nFiducialMarkNum;
	// 1. 자동ROI
	//for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
	for (UINT nROI = 0; nROI < iROITestNum; nROI++)
	{
		if (stOption.stInput[nROI].bEnable)
		{
			CRect tempROI(0, 0, 0, 0);
			lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, tempROI);
			lReturn = LURI_Func_DetectROI_White(pImage8bit, iImageW, iImageH, tempROI, stResult.ptROI[nROI],stOption.stInput[nROI].nMarkColor);
			//lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, stResult.ptROI[nROI]);
		}
	}

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 	RECT pPoitnTemp[ROI_Rotation_Max];
	// 
	// 	Set_Rotation_ROI_Array(stResult, pPoitnTemp);

	stOption.dRadius = 0.7;

	// 2. Rotation Config
	m_LG_Algorithm.LGIT_Rotation_Config(stOption.nROIBoxSize, stOption.nMaxROIBoxSize, stOption.dRadius, stOption.dRealGapX, stOption.dRealGapY, stOption.nFiducialMarkNum, stOption.nFiducialMarkType, stOption.dModuleChartDistance, stOption.nDistortionAlrotithmType);

	//// 3. Rotation DATA
	//m_LG_Algorithm.LGIT_Rotation_Func(ibuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.dbRotation);
	m_LG_Algorithm.LGIT_Rotation_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbRotation);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Rotation].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Rotation].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Rotation].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Rotation].dbValue;

	stResult.dbRotation *= stOption.dbOffsetRot;

	stResult.bRotation = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbRotation);


// 	if (stResult.bRotation == FALSE)
// 	{
// 		lReturn = RC_Err_TestFail;
// 	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_DynamicBW
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_DynamicBW stOption
// Parameter	: __out ST_Result_DynamicBW & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_DynamicBW(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption, __out ST_Result_DynamicBW_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 1. 자동ROI
	for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
	{
		if (TRUE == stOption.stInput[nROI].bEnable)
		{
			//lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].ptROI, stResult.rtROI[nROI]);//!SH _180904: Point 버전 필요
		}
	}

	//@SH _180809: Raw Data로 데이터 볼떄 씀, 최종 빌드시 삭제 예정
	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	POINT pPoitnTemp[ROI_DnyBW_Max];

	//!SH _180809: Data Log 필요할 수 있을것 같아서 만들어만 둠. 현재는 쓰지는 않음

	Set_Dinamic_ROI_Array(stOption, pPoitnTemp);

	// 2. DynamicBW Config
	m_LG_Algorithm.LGIT_DynamicBW_Config(stOption.iMaxROIWidth, stOption.iMaxROIHeight, stOption.iLscBlockSize, stOption.iMaxROICount, stOption.dbSNRThreshold, stOption.dbDRThreshold, pPoitnTemp);// , stOption.bEnableLog);
	//
	//// 3. DynamicBW DATA
	m_LG_Algorithm.LGIT_DynamicBW_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbVarianceValue, stResult.dbAverageValue, stResult.dbSNR_BW, stResult.dbDynamic);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Dynamic].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Dynamic].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Dynamic].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Dynamic].dbValue;

	stResult.dbDynamic *= stOption.dbOffsetDR;

	stResult.bDynamic = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDynamic);

	bMinUse = stOption.stSpecMin[Spec_SNR_BW].bEnable;
	bMaxUse = stOption.stSpecMax[Spec_SNR_BW].bEnable;
	dbMinDev = stOption.stSpecMin[Spec_SNR_BW].dbValue;
	dbMaxDev = stOption.stSpecMax[Spec_SNR_BW].dbValue;

	stResult.dbSNR_BW *= stOption.dbOffsetSNR;

	stResult.bSNR_BW = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbSNR_BW);

	if (stResult.bDynamic == FALSE || stResult.bSNR_BW == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_DynamicBW(__in LPBYTE pImage8bit, __in LPBYTE pImageWhite, __in LPBYTE pImageGray, __in LPBYTE pImageBlack, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption, __out ST_Result_DynamicBW_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 1. 자동ROI
	for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
	{
		//if (TRUE == stOption.stInput[nROI].bEnable)
		//{
			
			stResult.ptROI[nROI] = stOption.stInput[nROI].ptROI;
	//		//lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].ptROI, stResult.rtROI[nROI]);//!SH _180904: Point 버전 필요
		//}
	}

	//@SH _180809: Raw Data로 데이터 볼떄 씀, 최종 빌드시 삭제 예정
	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	POINT pPoitnTemp[ROI_DnyBW_Max];

	Set_Dinamic_ROI_Array(stOption, pPoitnTemp);

	// 2. DynamicBW Config
	m_LG_Algorithm.LGIT_DynamicBW_Config(stOption.iMaxROIWidth, stOption.iMaxROIHeight, stOption.iLscBlockSize, stOption.iMaxROICount, stOption.dbSNRThreshold, stOption.dbDRThreshold, pPoitnTemp);// , stOption.bEnableLog);
	//
	//// 3. DynamicBW DATA
	m_LG_Algorithm.LGIT_DynamicBW_Func(pImageWhite, pImageGray, pImageBlack, iImageW, iImageH, stResult.ptROI, stResult.dbVarianceValue, stResult.dbAverageValue, stResult.dbSNR_BW, stResult.dbDynamic);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Dynamic].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Dynamic].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Dynamic].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Dynamic].dbValue;

	stResult.dbDynamic *= stOption.dbOffsetDR;

	stResult.bDynamic = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDynamic);

	bMinUse = stOption.stSpecMin[Spec_SNR_BW].bEnable;
	bMaxUse = stOption.stSpecMax[Spec_SNR_BW].bEnable;
	dbMinDev = stOption.stSpecMin[Spec_SNR_BW].dbValue;
	dbMaxDev = stOption.stSpecMax[Spec_SNR_BW].dbValue;

	stResult.dbSNR_BW *= stOption.dbOffsetSNR;

	stResult.bSNR_BW = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbSNR_BW);

	if (stResult.bDynamic == FALSE || stResult.bSNR_BW == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Shading
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Shading stOption
// Parameter	: __out ST_Result_Shading & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Shading(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption, __out ST_Result_Shading_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int iOffsetX = 0;
	int iOffsetY = 0;

	// 1. 자동ROI
	// 	for (UINT nROI = 0; nROI < ROI_Shading_Max; nROI++)
	// 	{
	// 		if (TRUE == stOption.stInput[nROI].bEnable)
	// 		{
	// 			lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, stResult.rtROI[nROI]);
	// 		}
	// 	}
	//vector<BYTE> vFrameBuffer(iImageW * (iImageH+4) * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\System\\Image\\No Barcode\\Capatue_Ymean_0809_211152.raw");
	
	POINT ptROICenterHor[19];// = { { 32, 240 }, { 64, 240 }, { 96, 240 }, { 128, 240 }, { 160, 240 }, { 192, 240 }, { 224, 240 }, { 256, 240 }, { 288, 240 }, { 320, 240 }, { 352, 240 }, { 384, 240 }, { 416, 240 }, { 448, 240 }, { 480, 240 }, { 512, 240 }, { 544, 240 }, { 576, 240 }, { 608, 240 } }; // Center point of ROI region
	POINT ptROICenterVer[19];// = { { 320, 24 }, { 320, 48 }, { 320, 72 }, { 320, 96 }, { 320, 120 }, { 320, 144 }, { 320, 168 }, { 320, 192 }, { 320, 216 }, { 320, 240 }, { 320, 264 }, { 320, 288 }, { 320, 312 }, { 320, 336 }, { 320, 360 }, { 320, 384 }, { 320, 408 }, { 320, 432 }, { 320, 456 } };
	POINT ptROICenterDiaA[19];// = { { 32, 24 }, { 64, 48 }, { 96, 72 }, { 128, 96 }, { 160, 120 }, { 192, 144 }, { 224, 168 }, { 256, 192 }, { 288, 216 }, { 320, 240 }, { 352, 264 }, { 384, 288 }, { 416, 312 }, { 448, 336 }, { 480, 360 }, { 512, 384 }, { 544, 408 }, { 576, 432 }, { 608, 456 } }; // Center point of ROI region
	POINT ptROICenterDiaB[19];// = { { 608, 24 }, { 576, 48 }, { 544, 72 }, { 512, 96 }, { 480, 120 }, { 448, 144 }, { 416, 168 }, { 384, 192 }, { 352, 216 }, { 320, 240 }, { 288, 264 }, { 256, 288 }, { 224, 312 }, { 192, 336 }, { 160, 360 }, { 128, 384 }, { 96, 408 }, { 64, 432 }, { 32, 456 } }; // Center point of ROI region
	double dHorThd[19]; //= { 35.0, 55.0, 70.0, 80.0, 85.0, 90.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 90.0, 85.0, 80.0, 70.0, 55.0, 35.0 };
	double dVerThd[19];//= { 45.0, 60.0, 75.0, 80.0, 85.0, 95.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 95.0, 85.0, 80.0, 75.0, 60.0, 45.0 };
	double dDiaThd[19]; //= { 1.0, 15.0, 40.0, 70.0, 85.0, 90.0, 100.0, 105.0, 110.0, 110.0, 110.0, 105.0, 100.0, 90.0, 85.0, 70.0, 40.0, 15.0, 1.0 };

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	//// 2. Shading Config
	m_LG_Algorithm.LGIT_Shading_Config(stOption.iMaxROIWidth, stOption.iMaxROIHeight, stOption.iMaxROICount, stOption.iNormalizeIndex,
		stOption.stInputHor[0].stSpecMin.dbValue, stOption.stInputHor[0].stSpecMax.dbValue,
		stOption.stInputVer[0].stSpecMin.dbValue, stOption.stInputVer[0].stSpecMax.dbValue,
		stOption.stInputDirA[0].stSpecMin.dbValue, stOption.stInputDirA[0].stSpecMax.dbValue);// , stOption.bEnableLog);
	//m_LG_Algorithm.LGIT_Shading_Config_Array(stOption.dbHorThreshold, stOption.dbVerThreshold, stOption.dbDiaThreshold, stOption.ptHorROI, stOption.ptVerROI, stOption.ptDiaAROI, stOption.ptDiaBROI, stOption.iMaxROICount);
	Set_Shading_ROI_Array(stOption, dHorThd, dVerThd, dDiaThd, ptROICenterHor, ptROICenterVer, ptROICenterDiaA, ptROICenterDiaB);

	m_LG_Algorithm.LGIT_Shading_Config_Array(dHorThd, dVerThd, dDiaThd, ptROICenterHor, ptROICenterVer, ptROICenterDiaA, ptROICenterDiaB, 19);
	//
	//// 3. Shading DATA
	////@SH _180808: CenterPoint, bLGITShadingResult는 값은 받아오지만 쓰지는 않음.
	double CenterResult;
	bool   bLGITShadingResult[4][ROI_Shading_Max];



	m_LG_Algorithm.LGIT_Shading_Func(pImageRaw, iImageW, iImageH, CenterResult,
		stResult.ptHorROI, stResult.ptVerROI, stResult.ptDiaAROI, stResult.ptDiaBROI,
		stResult.dbHorizonValue, stResult.dbVerticalValue, stResult.dbDiaAValue, stResult.dbDiaBValue,
		bLGITShadingResult[0], bLGITShadingResult[1], bLGITShadingResult[2], bLGITShadingResult[3]);

	// 판정
	BOOL	bMinUse;
	BOOL	bMaxUse;
	double dbMinDev;
	double dbMaxDev;


	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
	{
		bMinUse = stOption.stInputHor[nldx].stSpecMin.bEnable;
		bMaxUse = stOption.stInputHor[nldx].stSpecMax.bEnable;
		dbMinDev = stOption.stInputHor[nldx].stSpecMin.dbValue;
		dbMaxDev = stOption.stInputHor[nldx].stSpecMax.dbValue;
		stResult.bHorizonResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbHorizonValue[nldx]);
	}

	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
	{
		bMinUse = stOption.stInputVer[nldx].stSpecMin.bEnable;
		bMaxUse = stOption.stInputVer[nldx].stSpecMax.bEnable;
		dbMinDev = stOption.stInputVer[nldx].stSpecMin.dbValue;
		dbMaxDev = stOption.stInputVer[nldx].stSpecMax.dbValue;
		stResult.bVerticalResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbVerticalValue[nldx]);
	}
	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
	{
		bMinUse = stOption.stInputDirA[nldx].stSpecMin.bEnable;
		bMaxUse = stOption.stInputDirA[nldx].stSpecMax.bEnable;
		dbMinDev = stOption.stInputDirA[nldx].stSpecMin.dbValue;
		dbMaxDev = stOption.stInputDirA[nldx].stSpecMax.dbValue;
		stResult.bDiaAResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDiaAValue[nldx]);
	}
	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
	{
		bMinUse = stOption.stInputDirB[nldx].stSpecMin.bEnable;
		bMaxUse = stOption.stInputDirB[nldx].stSpecMax.bEnable;
		dbMinDev = stOption.stInputDirB[nldx].stSpecMin.dbValue;
		dbMaxDev = stOption.stInputDirB[nldx].stSpecMax.dbValue;
		stResult.bDiaBResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDiaBValue[nldx]);
	}


	return lReturn;
}



//=============================================================================
// Method		: LGIT_Func_Shading
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Shading stOption
// Parameter	: __out ST_Result_Shading & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Rllumination(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rllumination stOption, __out ST_Result_Rllumination& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int iOffsetX = 0;
	int iOffsetY = 0;

	// 1. 자동ROI
	//vector<BYTE> vFrameBuffer(iImageW * (iImageH+4) * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\System\\Image\\No Barcode\\Capatue_Ymean_0809_211152.raw");

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	//// 2. Shading Config
	m_LG_Algorithm.LGIT_RI_Config(stOption.dBoxField, stOption.nBoxSize, stOption.dSpecRIcornerMax, stOption.dSpecRIcornerMin, stOption.dSpecRIminMax, stOption.dSpecRIminMin, stOption.b8BitUse);// , stOption.bEnableLog);

	//// 3. Shading DATA
	////@SH _180808: CenterPoint, bLGITShadingResult는 값은 받아오지만 쓰지는 않음.
	//double CenterResult;
	//bool   bLGITShadingResult[4][ROI_Shading_Max];

	//m_LG_Algorithm.LGIT_RelativeIllumination_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.ptRIROI, stResult.dRICorner, stResult.dRIMin, stResult.dRIValue, stOption.dbCorneroffset, stOption.dbMinoffset, stResult.stRILogData);
	m_LG_Algorithm.LGIT_RelativeIllumination_Func(pImageRaw, iImageW, iImageH, stResult.ptRIROI, stResult.dRICorner, stResult.dRIMin, stResult.dRIValue, stOption.dbCorneroffset, stOption.dbMinoffset, stResult.stRILogData);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_RI_Coner].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_RI_Coner].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_RI_Coner].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_RI_Coner].dbValue;

	/*	stResult.dbDynamic *= stOption.dbOffsetDR;*/

	stResult.dRIResult[0] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dRICorner);

	bMinUse = stOption.stSpecMin[Spec_RI_Min].bEnable;
	bMaxUse = stOption.stSpecMax[Spec_RI_Min].bEnable;
	dbMinDev = stOption.stSpecMin[Spec_RI_Min].dbValue;
	dbMaxDev = stOption.stSpecMax[Spec_RI_Min].dbValue;

	//stResult.dbSNR_BW *= stOption.dbOffsetSNR;

	stResult.dRIResult[1] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dRIMin);

	if (stResult.dRIResult[1] == FALSE || stResult.dRIResult[0] == FALSE)
	{
		lReturn = RC_TestSkip;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Ymean(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption, __out ST_Result_Ymean_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;
	// 
	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_Ymean_Config(stOption.iDefectBlockSize, stOption.iEdgeSize, stOption.fCenterThreshold, stOption.fEdgeThreshold, stOption.fCornerThreshold, stOption.iLscBlockSize,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		double pCircleMaxValue = 0.0;

		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_Ymean_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.nCircleCount, pCircleMaxValue, stResult.pCircleMaxPoint,
			stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
		/* __out int& nCircleCount, __out double& pCircleMaxValue, __out CPoint& pCircleMaxPoint,
		__out int& ocx, __out int& ocy, __out int& radx, __out int& rady)*/
	}
	else
	{
		double CenterValue;
		double EdgeValue;
		double CornerValue;

		// 3. Ymean DATA
		// 		m_LG_Algorithm.LGIT_Ymean_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount,
		// 			stResult.nCenterCount, CenterValue, stResult.pCenterMaxPoint,
		// 			stResult.nEdgeCount, EdgeValue, stResult.pEdgeMaxPoint,
		// 			stResult.nCornerCount, CornerValue, stResult.pCornerMaxPoint);
		m_LG_Algorithm.LGIT_Ymean_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount,
			stResult.nCenterCount, CenterValue, stResult.pCenterMaxPoint,
			stResult.nEdgeCount, EdgeValue, stResult.pEdgeMaxPoint,
			stResult.nCornerCount, CornerValue, stResult.pCornerMaxPoint);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_Ymean_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bYmeanResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bYmeanResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_Ymean(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption, __out ST_Result_Ymean_Foc& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_Ymean_Config(stOption.iDefectBlockSize, stOption.iEdgeSize, stOption.fCenterThreshold, stOption.fEdgeThreshold, stOption.fCornerThreshold, stOption.iLscBlockSize,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		double pCircleMaxValue;

		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_Ymean_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.nCircleCount, pCircleMaxValue, stResult.pCircleMaxPoint,
			stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
		/* __out int& nCircleCount, __out double& pCircleMaxValue, __out CPoint& pCircleMaxPoint,
		__out int& ocx, __out int& ocy, __out int& radx, __out int& rady)*/
	}
	else
	{
		double CenterValue;
		double EdgeValue;
		double CornerValue;

		// 3. Ymean DATA
		// 		m_LG_Algorithm.LGIT_Ymean_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount,
		// 			stResult.nCenterCount, CenterValue, stResult.pCenterMaxPoint,
		// 			stResult.nEdgeCount, EdgeValue, stResult.pEdgeMaxPoint,
		// 			stResult.nCornerCount, CornerValue, stResult.pCornerMaxPoint);
		m_LG_Algorithm.LGIT_Ymean_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount,
			stResult.nCenterCount, CenterValue, stResult.pCenterMaxPoint,
			stResult.nEdgeCount, EdgeValue, stResult.pEdgeMaxPoint,
			stResult.nCornerCount, CornerValue, stResult.pCornerMaxPoint);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_Ymean_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bYmeanResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bYmeanResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_BlackSpot(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;
	// 
	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_BlackSpot_Config(stOption.nBlockWidth, stOption.nBlockHeight, stOption.nClusterSize, stOption.nDefectInCluster, stOption.dDefectRatio, stOption.nMaxSingleDefectNum,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		double pCircleMaxValue = 0.0;

		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_BlackSpot_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		//	m_LG_Algorithm.LGIT_BlackSpot_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
		m_LG_Algorithm.LGIT_BlackSpot_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_BlackSpot_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bBlackSpotResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bBlackSpotResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_BlackSpot(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot_Foc& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_BlackSpot_Config(stOption.nBlockWidth, stOption.nBlockHeight, stOption.nClusterSize, stOption.nDefectInCluster, stOption.dDefectRatio, stOption.nMaxSingleDefectNum,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_BlackSpot_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		//	m_LG_Algorithm.LGIT_BlackSpot_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
		m_LG_Algorithm.LGIT_BlackSpot_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_BlackSpot_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bBlackSpotResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bBlackSpotResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_LCB(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_LCB_Config(stOption.dCenterThreshold, stOption.dEdgeThreshold, stOption.dCornerThreshold, stOption.nMaxSingleDefectNum, stOption.nMinDefectWidthHeight,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_LCB_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		//	m_LG_Algorithm.LGIT_LCB_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
		m_LG_Algorithm.LGIT_LCB_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);

	}

	// 판정
	for (int nidx = 0; nidx < Spec_LCB_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bLCBResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bLCBResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_LCB(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB_Foc& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_LCB_Config(stOption.dCenterThreshold, stOption.dEdgeThreshold, stOption.dCornerThreshold, stOption.nMaxSingleDefectNum, stOption.nMinDefectWidthHeight,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_LCB_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		//	m_LG_Algorithm.LGIT_LCB_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
		m_LG_Algorithm.LGIT_LCB_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_LCB_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bLCBResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bLCBResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_DefectBlack(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black_FOC& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectBlack_Config(stOption.nBlockSize, stOption.dbNormalDefectRatio, stOption.dbVeryDefectRatio, stOption.nMaxNormalDefectNum, stOption.nMaxVeryDefectNum, 0);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	int veryDefect = 0;
	//m_LG_Algorithm.LGIT_DefectBlack_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.nDefect_BlackCount, veryDefect);
	m_LG_Algorithm.LGIT_DefectBlack_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_BlackCount, veryDefect);


	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_BlackResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_BlackCount);
	}

	if (stResult.bDefect_BlackResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_DefectBlack(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectBlack_Config(stOption.nBlockSize, stOption.dbNormalDefectRatio, stOption.dbVeryDefectRatio, stOption.nMaxNormalDefectNum, stOption.nMaxVeryDefectNum, 0);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	int veryDefect = 0;
	//m_LG_Algorithm.LGIT_DefectBlack_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.nDefect_BlackCount, veryDefect);
	m_LG_Algorithm.LGIT_DefectBlack_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_BlackCount, veryDefect);


	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_BlackResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_BlackCount);
	}

	if (stResult.bDefect_BlackResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_DefectWhite(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White_FOC& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectWhite_Config(stOption.nBlockSize, stOption.dbNormalDefectRatio, stOption.dbVeryDefectRatio, stOption.nMaxNormalDefectNum, stOption.nMaxVeryDefectNum, 0);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	int veryDefect = 0;
	//m_LG_Algorithm.LGIT_DefectWhite_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.nDefect_WhiteCount, veryDefect);
	m_LG_Algorithm.LGIT_DefectWhite_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_WhiteCount, veryDefect);


	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_WhiteResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_WhiteCount);
	}

	if (stResult.bDefect_WhiteResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
LRESULT CTestManager_Test::LGIT_Func_DefectWhite(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White_IQ& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 	vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	// 	m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "C:\\ProgramSetting\\Image\\image\\Capatue_Distortion_0916_180054.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectWhite_Config(stOption.nBlockSize, stOption.dbNormalDefectRatio, stOption.dbVeryDefectRatio, stOption.nMaxNormalDefectNum, stOption.nMaxVeryDefectNum, 0);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	int veryDefect = 0;
	//m_LG_Algorithm.LGIT_DefectWhite_Func(vFrameBuffer.data(), iImageW, iImageH, stResult.ptROI, stResult.nDefect_WhiteCount, veryDefect);
	m_LG_Algorithm.LGIT_DefectWhite_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_WhiteCount, veryDefect);


	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_WhiteResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_WhiteCount);
	}

	if (stResult.bDefect_WhiteResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: Get_MeasurmentData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in double dbMinSpec
// Parameter	: __in double dbMaxSpec
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
bool CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpec, __in double dbValue)
{
	bool bMinResult = true;
	bool bMaxResult = true;

	if (TRUE == bMinUse)
	{
		bMinResult = (dbMinSpec <= dbValue) ? true : false;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (dbMaxSpec >= dbValue) ? true : false;
	}

	return bMinResult & bMaxResult;
}

//=============================================================================
// Method		: Get_MeasurmentData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in int iMinSpec
// Parameter	: __in int iMaxSpec
// Parameter	: __in int iValue
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
bool CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue)
{
	bool bMinResult = true;
	bool bMaxResult = true;

	if (TRUE == bMinUse)
	{
		bMinResult = (iMinSpec <= iValue) ? true : false;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (iMaxSpec >= iValue) ? true : false;
	}

	return bMinResult & bMaxResult;
}

//=============================================================================
// Method		: LURI_Func_DetectROI_Ver1
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in CRect rtOption
// Parameter	: __out CRect & rtResult 
// Qualifier	:
// Last Update	: 2018/8/9 - 9:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LURI_Func_DetectROI_Ver1(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult)
{
	int iHulfW = iImageW / 2;
	int iHulfH = iImageH / 2;

	// 영상 사이즈에 따라 영역 변경하여 사용 (전체 이미지에서 선정)
	int iFindW = iImageW / 5;
	int iFindH = iImageH / 5;

	// 영역 설정
	CRect rtROI;

	rtROI.left = iImageW / 4;
	rtROI.right = iImageW - (iImageW / 4);
	rtROI.top = iImageH / 4;
	rtROI.bottom = iImageH - (iImageH / 4);

	int iBright = 0;
	int iColor = MarkCol_Black;
	int nSharpness = 0;

	int iCenterX = 0;
	int iCenterY = 0;

	if (FALSE == m_LT_CircleDetect.CircleDetect_Test(pImageBuf, iImageW, iImageH, rtROI, iBright, iCenterX, iCenterY, iColor, nSharpness))
	{
		return RC_Detect;
	}

	// ROI Offset
	int iOffsetX = iCenterX - iHulfW;
	int iOffsetY = iCenterY - iHulfH;

	rtResult.left = rtOption.left + iOffsetX;
	rtResult.right = rtOption.right + iOffsetX;
	rtResult.top = rtOption.top + iOffsetY;
	rtResult.bottom = rtOption.bottom + iOffsetY;

	if (rtResult.left < 0 || rtResult.left > iImageW)
		return RC_Detect;

	if (rtResult.right < 0 || rtResult.right > iImageW)
		return RC_Detect;

	if (rtResult.top < 0 || rtResult.top > iImageW)
		return RC_Detect;

	if (rtResult.bottom < 0 || rtResult.bottom > iImageW)
		return RC_Detect;

	return RC_OK;
}

LRESULT CTestManager_Test::LURI_Func_DetectROI_White(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult, __in UINT nMarkColor)
{
	int iHulfW = iImageW / 2;
	int iHulfH = iImageH / 2;

	// 영상 사이즈에 따라 영역 변경하여 사용 (전체 이미지에서 선정)
	int iFindW = iImageW / 5;
	int iFindH = iImageH / 5;

	// 영역 설정
	//!SH _181115: LGIT Rotation 알고리즘 수행하기 위해 만듬. 기존의 알고리즘에서 사용되는 것을 고치기 시간이 부족하여 만듬. 추후 파라미터 넣는 방식으로 변경예정
	CRect rtROI;

	//rtROI.left = iImageW / 4;
	//rtROI.right = iImageW - (iImageW / 4);
	//rtROI.top = iImageH / 4;
	//rtROI.bottom = iImageH - (iImageH / 4);

	rtROI = rtOption;

	int iBright = 0;
	int iColor = nMarkColor;
	int nSharpness = 0;

	int iCenterX = 0;
	int iCenterY = 0;

	if (FALSE == m_LT_CircleDetect.CircleDetect_Test(pImageBuf, iImageW, iImageH, rtROI, iBright, iCenterX, iCenterY, iColor, nSharpness))
	{
		return RC_Detect;
	}

	// ROI Offset
	int iOffsetX = iCenterX;//- iHulfW;
	int iOffsetY = iCenterY;// -iHulfH;

	rtResult.left = iOffsetX - rtOption.Width() / 2;
	rtResult.right = iOffsetX + rtOption.Width() / 2;
	rtResult.top = iOffsetY - rtOption.Height() / 2;
	rtResult.bottom = iOffsetY + rtOption.Height() / 2;

	if (rtResult.left < 0 || rtResult.left > iImageW)
		return RC_Detect;

	if (rtResult.right < 0 || rtResult.right > iImageW)
		return RC_Detect;

	if (rtResult.top < 0 || rtResult.top > iImageW)
		return RC_Detect;

	if (rtResult.bottom < 0 || rtResult.bottom > iImageW)
		return RC_Detect;

	return RC_OK;
}

void CTestManager_Test::OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR_FOC &stTestData)
{
	int AutoROI_Thres = 0;
	if (pFrameImage == NULL)
		return;

	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	CvSeq *pContour_Center = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pContoresImage = NULL;
	IplImage* eig_image = NULL;
	IplImage* temp_image = NULL;

	// 광축을 구해서 Offset 추가
	CvPoint Offset_for_Auto;
	Offset_for_Auto.x = 0;
	Offset_for_Auto.y = 0;

	cvCvtColor(pFrameImage, pSrcImage, CV_RGB2GRAY);

	cvAdaptiveThreshold(pSrcImage, pBinaryImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 201, 5);

	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);
	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	pContour_Center = pContour;

	//광축 구하기
	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	nCenterPoint.x = 0;
	nCenterPoint.y = 0;
	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	int iStartX = pSrcImage->width / 4;
	int iStartY = pSrcImage->height / 4;

	int iEndX = pSrcImage->width * 3 / 4;
	int iEndY = pSrcImage->height * 3 / 4;

	for (; pContour_Center != 0; pContour_Center = pContour_Center->h_next)
	{
		rect = cvContourBoundingRect(pContour_Center, 1);
		area = cvContourArea(pContour_Center, CV_WHOLE_SEQ);
		arcCount = cvArcLength(pContour_Center, CV_WHOLE_SEQ, -1);
		circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

		rtCenterX = rect.x + rect.width / 2;
		rtCenterY = rect.y + rect.height / 2;

		StartPoint.x = rect.x;
		StartPoint.y = rect.y;

		EndPoint.x = rect.x + rect.width;
		EndPoint.y = rect.y + rect.height;

		if (rect.x > iStartX && rect.x + rect.width < iEndX
			&& rect.y > iStartY && rect.y + rect.height < iEndY)
		{
			dbDist = GetDistance(pSrcImage->width / 2, pSrcImage->height / 2, rtCenterX, rtCenterY);

			//원에 가깝고
			if (circularity > 0.3)
			{
				// 이미지 중심에서 제일 가까운 오브젝트 탐색
				if (dbMin_Dist > dbDist)
				{
					dbMin_Dist = dbDist;

					nCenterPoint.x = rtCenterX;
					nCenterPoint.y = rtCenterY;

					StartPoint.x = nCenterPoint.x - rect.width / 2;
					StartPoint.y = nCenterPoint.y - rect.height / 2;

					EndPoint.x = nCenterPoint.x + rect.width / 2;
					EndPoint.y = nCenterPoint.y + rect.height / 2;

				}
			}
		}
	}

	if (pSrcImage->width < nCenterPoint.x || 0 >= nCenterPoint.x)
	{
		nCenterPoint.x = pSrcImage->width / 2;
	}

	if (pSrcImage->height < nCenterPoint.y || 0 >= nCenterPoint.y)
	{
		nCenterPoint.y = pSrcImage->height / 2;
	}

	Offset_for_Auto.x = nCenterPoint.x - (pSrcImage->width / 2);
	Offset_for_Auto.y = nCenterPoint.y - (pSrcImage->height / 2);

	// 샤프니스 확인 후 자동 ROI 진입
	BOOL bAutoROI = TRUE;

	if (bAutoROI == TRUE) //광축 틀어진 만큼 ROI 값 보정
	{
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			if (stOpt.stInput[i].bEnable)
			{
				stTestData.rtROI[i].left = stOpt.stInput[i].rtROI.left + Offset_for_Auto.x;
				stTestData.rtROI[i].top = stOpt.stInput[i].rtROI.top + Offset_for_Auto.y;
				stTestData.rtROI[i].right = stOpt.stInput[i].rtROI.right + Offset_for_Auto.x;
				stTestData.rtROI[i].bottom = stOpt.stInput[i].rtROI.bottom + Offset_for_Auto.y;
			}

		}
	}
	else // ROI 마다 자동으로 찾아버리기
	{
		CvRect rcContour;
		CRect rtROI[ROI_SFR_Max];
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			CvPoint Corr_Center;
			if (stOpt.stInput[i].bEnable)
			{
				int _W = stOpt.stInput[i].rtROI.right - stOpt.stInput[i].rtROI.left;
				int _H = stOpt.stInput[i].rtROI.bottom - stOpt.stInput[i].rtROI.top;
				CvSeq *pContour_Tmp = pContour;
				rtROI[i] = stOpt.stInput[i].rtROI;
				CvPoint ROICenterPoint;
				ROICenterPoint.x = (rtROI[i].left + rtROI[i].right) / 2 + Offset_for_Auto.x;
				ROICenterPoint.y = (rtROI[i].top + rtROI[i].bottom) / 2 + Offset_for_Auto.y;
				// ROI에 근접한 Contours 구하기
				double Old_Dist = 99999;
				double Max_Size = 0;

				CvRect Near_ContP;
				BOOL TRUE_Flag = FALSE;
				for (; pContour_Tmp != 0; pContour_Tmp = pContour_Tmp->h_next)
				{
					rcContour = cvBoundingRect(pContour_Tmp, 1);
					double Size_Rect = rcContour.width * rcContour.height;
					double Dist_Contour2ROI = GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);
					if (rcContour.height > _H && rcContour.width > _W)
					{
						if (Old_Dist > Dist_Contour2ROI && rcContour.width < pFrameImage->width / 2 && rcContour.height < pFrameImage->height / 2)
						{

							Old_Dist = Dist_Contour2ROI;
							Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
							Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
							Near_ContP.width = rcContour.width + 10;
							Near_ContP.height = rcContour.height + 10;
							TRUE_Flag = TRUE;
						}
					}

				}
				if (TRUE_Flag == FALSE)
				{
					TRACE(_T("Near_Point가 없습니다.\n"));
					//cvReleaseMemStorage(&pContourStorage);
					//cvReleaseImage(&pSrcImage);
					//cvReleaseImage(&pBinaryImage);

					stTestData.rtROI[i].left = stOpt.stInput[i].rtROI.left;
					stTestData.rtROI[i].top = stOpt.stInput[i].rtROI.top;
					stTestData.rtROI[i].right = stOpt.stInput[i].rtROI.right;
					stTestData.rtROI[i].bottom = stOpt.stInput[i].rtROI.bottom;


				}
				else{
					// 근접한 Contours 이미지 따기
					pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
					eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					if (Near_ContP.x < 0)
						Near_ContP.x = 0;
					if (Near_ContP.y < 0)
						Near_ContP.y = 0;
					if (Near_ContP.x + Near_ContP.width > pFrameImage->width - 1)
						Near_ContP.x = pFrameImage->width - Near_ContP.width - 1;
					if (Near_ContP.y + Near_ContP.height > pFrameImage->height - 1)
						Near_ContP.y = pFrameImage->height - Near_ContP.height - 1;
					cvSetImageROI(pSrcImage, Near_ContP);
					cvCopy(pSrcImage, pContoresImage);
					cvResetImageROI(pSrcImage);
					// 스무딩 처리
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);
					//cvSmooth(pContoresImage, pContoresImage);
					cvNormalize(pContoresImage, pContoresImage, 0, 255, CV_MINMAX);
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);

					int MAX_CORNERS = 2000;
					CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];
					CvPoint2D32f* corners_Final = new CvPoint2D32f[MAX_CORNERS];

					int Corners_Cnt = 0;
					// 코너 찾기
					cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.14, 50.0, 0, 5, 0, 0.04);
					for (int j = 0; j < MAX_CORNERS; j++)
					{
						if (!(corners[j].x > pContoresImage->width / 2 - 10 && corners[j].x < pContoresImage->width / 2 + 10 &&
							corners[j].y > pContoresImage->height / 2 - 10 && corners[j].y < pContoresImage->height / 2 + 10))
						{
							corners_Final[Corners_Cnt] = corners[j];
							cvCircle(pContoresImage, cvPoint((int)corners[j].x, (int)corners[j].y), 4, CV_RGB(255, 255, 255), 1);
							Corners_Cnt++;
						}
					}

					// 월드좌표 변환
					for (int j = 0; j < Corners_Cnt; j++)
					{
						corners_Final[j].x += Near_ContP.x;
						corners_Final[j].y += Near_ContP.y;
					}
					double Dist_1st = 0.0;
					CvPoint Dist_1stPoint;
					double Dist_2rd = 0.0;
					CvPoint Dist_2rdPoint;

					// W가 큰 ROI 일때 (가로SFR) => X축, Y축에 대한 거리 추출 가중치가 달라짐
					if (rtROI[i].Width() > rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y /*- (float)Pointy_Mul*/));
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].y < Dist_1stPoint.y + _H && corners_Final[k].y > Dist_1stPoint.y - _H))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y) /*- (float)Pointy_Mul)*/);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					// H가 큰 ROI 일때 (세로SFR)
					else if (rtROI[i].Width() < rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x)/*+ (float)Pointy_Mul)*/, (int)corners_Final[k].y);
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].x <= Dist_1stPoint.x + _W && corners_Final[k].x >= Dist_1stPoint.x - _W))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x /*+ Pointy_Mul*/), (int)corners_Final[k].y);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					Corr_Center.x = (int)((Dist_1stPoint.x + Dist_2rdPoint.x) / 2.0);
					Corr_Center.y = (int)((Dist_1stPoint.y + Dist_2rdPoint.y) / 2.0);
					//cvCircle(pContoresImage, Corr_Center, 4, CV_RGB(255, 255, 255), 1);

					// 보정값 넣기
					stTestData.rtROI[i].left = Corr_Center.x - (_W / 2);
					stTestData.rtROI[i].top = Corr_Center.y - (_H / 2);
					stTestData.rtROI[i].right = stTestData.rtROI[i].left + _W;
					stTestData.rtROI[i].bottom = stTestData.rtROI[i].top + _H;

					delete[] corners;
					delete[] corners_Final;

					cvReleaseImage(&eig_image);
					cvReleaseImage(&temp_image);
					cvReleaseImage(&pContoresImage);
					//cvRectangle(pSrcImage, cvPoint(stTestData.rtROI[i].left, stTestData.rtROI[i].top), cvPoint(stTestData.rtROI[i].right, stTestData.rtROI[i].bottom),CV_RGB(200,200,200),2 );

				}

			}
		}
	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);
}


//=============================================================================
// Method		: OnTestProcess_SFRROI
// Access		: protected  
// Returns		: void
// Parameter	: __in IplImage * pFrameImage
// Parameter	: __in ST_UI_SFR stOpt
// Parameter	: __out ST_Result_SFR & stTestData
// Qualifier	:
// Last Update	: 2018/10/12 - 13:30
// Desc.		:
//=============================================================================
void CTestManager_Test::OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR_IQ &stTestData)
{
	int AutoROI_Thres = 0;
	if (pFrameImage == NULL)
		return;

	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	CvSeq *pContour_Center = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pContoresImage = NULL;
	IplImage* eig_image = NULL;
	IplImage* temp_image = NULL;

	// 광축을 구해서 Offset 추가
	CvPoint Offset_for_Auto;
	Offset_for_Auto.x = 0;
	Offset_for_Auto.y = 0;

	cvCvtColor(pFrameImage, pSrcImage, CV_RGB2GRAY);

	//cvThreshold(pSrcImage, pBinaryImage, 0, 255, CV_THRESH_OTSU); // 20180305 HTH Modify
	cvAdaptiveThreshold(pSrcImage, pBinaryImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 201, 5);

	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);
	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	//cvFindContours(pBinaryImage, pContourStorage, &pContour_Center, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	pContour_Center = pContour;

	//광축 구하기
	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	nCenterPoint.x = 0;
	nCenterPoint.y = 0;
	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	int iStartX = pSrcImage->width / 4;
	int iStartY = pSrcImage->height / 4;

	int iEndX = pSrcImage->width * 3 / 4;
	int iEndY = pSrcImage->height * 3 / 4;

	for (; pContour_Center != 0; pContour_Center = pContour_Center->h_next)
	{
		rect = cvContourBoundingRect(pContour_Center, 1);
		area = cvContourArea(pContour_Center, CV_WHOLE_SEQ);
		arcCount = cvArcLength(pContour_Center, CV_WHOLE_SEQ, -1);
		circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

		rtCenterX = rect.x + rect.width / 2;
		rtCenterY = rect.y + rect.height / 2;

		StartPoint.x = rect.x;
		StartPoint.y = rect.y;

		EndPoint.x = rect.x + rect.width;
		EndPoint.y = rect.y + rect.height;

		if (rect.x > iStartX && rect.x + rect.width < iEndX
			&& rect.y > iStartY && rect.y + rect.height < iEndY)
		{
			dbDist = GetDistance(pSrcImage->width / 2, pSrcImage->height / 2, rtCenterX, rtCenterY);

			//원에 가깝고
			if (circularity > 0.3)
			{
				// 이미지 중심에서 제일 가까운 오브젝트 탐색
				if (dbMin_Dist > dbDist)
				{
					//cvRectangleR(pSrcImage, rect, CV_RGB(255, 255, 255), 3);

					dbMin_Dist = dbDist;

					nCenterPoint.x = rtCenterX;
					nCenterPoint.y = rtCenterY;

					StartPoint.x = nCenterPoint.x - rect.width / 2;
					StartPoint.y = nCenterPoint.y - rect.height / 2;

					EndPoint.x = nCenterPoint.x + rect.width / 2;
					EndPoint.y = nCenterPoint.y + rect.height / 2;

				}
			}
		}
	}

	if (pSrcImage->width < nCenterPoint.x || 0 >= nCenterPoint.x)
	{
		nCenterPoint.x = pSrcImage->width / 2;
	}

	if (pSrcImage->height < nCenterPoint.y || 0 >= nCenterPoint.y)
	{
		nCenterPoint.y = pSrcImage->height / 2;
	}

	Offset_for_Auto.x = nCenterPoint.x - (pSrcImage->width / 2);
	Offset_for_Auto.y = nCenterPoint.y - (pSrcImage->height / 2);

	// ROI 마다 자동으로 찾아버리기
	CvRect rcContour;
	CRect rtROI[ROI_SFR_Max];
	for (int i = 0; i < ROI_SFR_Max; i++)
	{
		CvPoint Corr_Center;
		if (stOpt.stInput[i].bEnable)
		{
			int _W = stOpt.stInput[i].rtROI.right - stOpt.stInput[i].rtROI.left;
			int _H = stOpt.stInput[i].rtROI.bottom - stOpt.stInput[i].rtROI.top;
			CvSeq *pContour_Tmp = pContour;
			rtROI[i] = stOpt.stInput[i].rtROI;
			CvPoint ROICenterPoint;
			ROICenterPoint.x = (rtROI[i].left + rtROI[i].right) / 2 + Offset_for_Auto.x;
			ROICenterPoint.y = (rtROI[i].top + rtROI[i].bottom) / 2 + Offset_for_Auto.y;
			// ROI에 근접한 Contours 구하기
			double Old_Dist = 99999;
			double Max_Size = 0;

			CvRect Near_ContP;
			BOOL TRUE_Flag = FALSE;
			for (; pContour_Tmp != 0; pContour_Tmp = pContour_Tmp->h_next)
			{
				rcContour = cvBoundingRect(pContour_Tmp, 1);
				double Size_Rect = rcContour.width * rcContour.height;
				double Dist_Contour2ROI = GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);
				if (rcContour.height > _H && rcContour.width > _W)
				{
					if (Old_Dist > Dist_Contour2ROI && rcContour.width < pFrameImage->width / 2 && rcContour.height < pFrameImage->height / 2)
					{

						Old_Dist = Dist_Contour2ROI;
						Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
						Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
						Near_ContP.width = rcContour.width + 10;
						Near_ContP.height = rcContour.height + 10;
						TRUE_Flag = TRUE;
					}
				}

			}
			if (TRUE_Flag == FALSE)
			{
				TRACE(_T("Near_Point가 없습니다.\n"));
				cvReleaseMemStorage(&pContourStorage);
				cvReleaseImage(&pSrcImage);
				cvReleaseImage(&pBinaryImage);

				stTestData.rtROI[i].left = stOpt.stInput[i].rtROI.left;
				stTestData.rtROI[i].top = stOpt.stInput[i].rtROI.top;
				stTestData.rtROI[i].right = stOpt.stInput[i].rtROI.right;
				stTestData.rtROI[i].bottom = stOpt.stInput[i].rtROI.bottom;


			}
			else
			{
				// 근접한 Contours 이미지 따기
				pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
				eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
				temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
				if (Near_ContP.x < 0)
					Near_ContP.x = 0;
				if (Near_ContP.y < 0)
					Near_ContP.y = 0;
				if (Near_ContP.x + Near_ContP.width > pFrameImage->width - 1)
					Near_ContP.x = pFrameImage->width - Near_ContP.width - 1;
				if (Near_ContP.y + Near_ContP.height > pFrameImage->height - 1)
					Near_ContP.y = pFrameImage->height - Near_ContP.height - 1;
				cvSetImageROI(pSrcImage, Near_ContP);
				cvCopy(pSrcImage, pContoresImage);
				cvResetImageROI(pSrcImage);
				// 스무딩 처리
				cvDilate(pContoresImage, pContoresImage, 0, 3);
				cvErode(pContoresImage, pContoresImage, 0, 3);
				//cvSmooth(pContoresImage, pContoresImage);
				cvNormalize(pContoresImage, pContoresImage, 0, 255, CV_MINMAX);
				cvDilate(pContoresImage, pContoresImage, 0, 3);
				cvErode(pContoresImage, pContoresImage, 0, 3);

				int MAX_CORNERS = 2000;
				CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];
				CvPoint2D32f* corners_Final = new CvPoint2D32f[MAX_CORNERS];

				int Corners_Cnt = 0;
				// 코너 찾기
				cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.14, 50.0, 0, 5, 0, 0.04);
				for (int j = 0; j < MAX_CORNERS; j++)
				{
					if (!(corners[j].x > pContoresImage->width / 2 - 10 && corners[j].x < pContoresImage->width / 2 + 10 &&
						corners[j].y > pContoresImage->height / 2 - 10 && corners[j].y < pContoresImage->height / 2 + 10))
					{
						corners_Final[Corners_Cnt] = corners[j];
						cvCircle(pContoresImage, cvPoint((int)corners[j].x, (int)corners[j].y), 4, CV_RGB(255, 255, 255), 1);
						Corners_Cnt++;
					}
				}

				// 월드좌표 변환
				for (int j = 0; j < Corners_Cnt; j++)
				{
					corners_Final[j].x += Near_ContP.x;
					corners_Final[j].y += Near_ContP.y;
				}
				double Dist_1st = 0.0;
				CvPoint Dist_1stPoint;
				double Dist_2rd = 0.0;
				CvPoint Dist_2rdPoint;

				// W가 큰 ROI 일때 (가로SFR) => X축, Y축에 대한 거리 추출 가중치가 달라짐
				if (rtROI[i].Width() > rtROI[i].Height())
				{
					// 1st
					double Max_Tmp = 99999;
					for (int k = 0; k < Corners_Cnt; k++)
					{
						int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
						Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y /*- (float)Pointy_Mul*/));
						if (Max_Tmp > Dist_1st)
						{
							Max_Tmp = Dist_1st;
							Dist_1stPoint.x = (int)corners_Final[k].x;
							Dist_1stPoint.y = (int)corners_Final[k].y;
						}
					}
					// 2rd
					Max_Tmp = 99999;
					for (int k = 0; k < Corners_Cnt; k++)
					{
						// 1st Point 를 제외한 가장 가까운 Point = 2rd
						if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].y < Dist_1stPoint.y + _H && corners_Final[k].y > Dist_1stPoint.y - _H))
						{
							// 가중치를 적용한 거리 계산
							int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
							Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y) /*- (float)Pointy_Mul)*/);
							if (Max_Tmp > Dist_2rd)
							{
								Max_Tmp = Dist_2rd;
								Dist_2rdPoint.x = (int)corners_Final[k].x;
								Dist_2rdPoint.y = (int)corners_Final[k].y;
							}
						}
					}
				}
				// H가 큰 ROI 일때 (세로SFR)
				else if (rtROI[i].Width() < rtROI[i].Height())
				{
					// 1st
					double Max_Tmp = 99999;
					for (int k = 0; k < Corners_Cnt; k++)
					{
						int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
						Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x)/*+ (float)Pointy_Mul)*/, (int)corners_Final[k].y);
						if (Max_Tmp > Dist_1st)
						{
							Max_Tmp = Dist_1st;
							Dist_1stPoint.x = (int)corners_Final[k].x;
							Dist_1stPoint.y = (int)corners_Final[k].y;
						}
					}
					// 2rd
					Max_Tmp = 99999;
					for (int k = 0; k < Corners_Cnt; k++)
					{
						// 1st Point 를 제외한 가장 가까운 Point = 2rd
						if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].x <= Dist_1stPoint.x + _W && corners_Final[k].x >= Dist_1stPoint.x - _W))
						{
							// 가중치를 적용한 거리 계산
							int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
							Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x /*+ Pointy_Mul*/), (int)corners_Final[k].y);
							if (Max_Tmp > Dist_2rd)
							{
								Max_Tmp = Dist_2rd;
								Dist_2rdPoint.x = (int)corners_Final[k].x;
								Dist_2rdPoint.y = (int)corners_Final[k].y;
							}
						}
					}
				}
				Corr_Center.x = (int)((Dist_1stPoint.x + Dist_2rdPoint.x) / 2.0);
				Corr_Center.y = (int)((Dist_1stPoint.y + Dist_2rdPoint.y) / 2.0);
				//cvCircle(pContoresImage, Corr_Center, 4, CV_RGB(255, 255, 255), 1);

				// 보정값 넣기
				stTestData.rtROI[i].left = Corr_Center.x - (_W / 2);
				stTestData.rtROI[i].top = Corr_Center.y - (_H / 2);
				stTestData.rtROI[i].right = stTestData.rtROI[i].left + _W;
				stTestData.rtROI[i].bottom = stTestData.rtROI[i].top + _H;

				delete[] corners;
				delete[] corners_Final;

				cvReleaseImage(&eig_image);
				cvReleaseImage(&temp_image);
				cvReleaseImage(&pContoresImage);
				//cvRectangle(pSrcImage, cvPoint(stTestData.rtROI[i].left, stTestData.rtROI[i].top), cvPoint(stTestData.rtROI[i].right, stTestData.rtROI[i].bottom),CV_RGB(200,200,200),2 );

			}
		}
	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);
}

//=============================================================================
// Method		: GetDistance
// Access		: protected  
// Returns		: double
// Parameter	: __in int x1
// Parameter	: __in int y1
// Parameter	: __in int x2
// Parameter	: __in int y2
// Qualifier	:
// Last Update	: 2018/8/11 - 14:50
// Desc.		:
//=============================================================================
double CTestManager_Test::GetDistance(__in int x1, __in int y1, __in int x2, __in int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

//=============================================================================
// Method		: Set_Shading_ROI_Array
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_UI_Shading & stOption
// Parameter	: __out double * pdHorThres
// Parameter	: __out double * pdVerThres
// Parameter	: __out double * pdDiaThres
// Parameter	: __out POINT * ppHorizonPoint
// Parameter	: __out POINT * ppVerticalPoint
// Parameter	: __out POINT * ppDiaAPoint
// Parameter	: __out POINT * ppDiaBPoint
// Qualifier	:
// Last Update	: 2018/8/11 - 14:48
// Desc.		:
//=============================================================================
void CTestManager_Test::Set_Shading_ROI_Array(__in ST_UI_Shading & stOption, __out double * pdHorThres, __out double * pdVerThres, __out double * pdDiaThres, __out POINT * ppHorizonPoint, __out POINT * ppVerticalPoint, __out POINT * ppDiaAPoint, __out POINT * ppDiaBPoint)
{
	for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
	{
		ppHorizonPoint[iROI].x = stOption.stInputHor[iROI].ptROI.x;
		ppHorizonPoint[iROI].y = stOption.stInputHor[iROI].ptROI.y;

		ppVerticalPoint[iROI].x = stOption.stInputVer[iROI].ptROI.x;
		ppVerticalPoint[iROI].y = stOption.stInputVer[iROI].ptROI.y;

		ppDiaAPoint[iROI].x = stOption.stInputDirA[iROI].ptROI.x;
		ppDiaAPoint[iROI].y = stOption.stInputDirA[iROI].ptROI.y;

		ppDiaBPoint[iROI].x = stOption.stInputDirB[iROI].ptROI.x;
		ppDiaBPoint[iROI].y = stOption.stInputDirB[iROI].ptROI.y;

		pdHorThres[iROI] = stOption.stInputHor[iROI].dbThreshold;
		pdVerThres[iROI] = stOption.stInputVer[iROI].dbThreshold;
		pdDiaThres[iROI] = stOption.stInputDirA[iROI].dbThreshold;
	}
}

//=============================================================================
// Method		: Set_Dinamic_ROI_Array
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_UI_DynamicBW & stOption
// Parameter	: __out POINT * ppDinamicBWPoint
// Qualifier	:
// Last Update	: 2018/8/11 - 14:47
// Desc.		:
//=============================================================================
void CTestManager_Test::Set_Dinamic_ROI_Array(__in ST_UI_DynamicBW & stOption, __out POINT * ppDinamicBWPoint)
{
	for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
	{
		ppDinamicBWPoint[iROI].x = stOption.stInput[iROI].ptROI.x;
		ppDinamicBWPoint[iROI].y = stOption.stInput[iROI].ptROI.y;
	}
}

//=============================================================================
// Method		: Set_Dinamic_ROI_Array
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_UI_DynamicBW & stOption
// Parameter	: __out POINT * ppDinamicBWPoint
// Qualifier	:
// Last Update	: 2018/8/11 - 14:47
// Desc.		:
//=============================================================================
void CTestManager_Test::Set_Rotation_ROI_Array(__in ST_Result_Rotation_Foc & stOption, __out RECT * ppRotationRect)
{
	for (int iROI = 0; iROI < ROI_Rotation_Max; iROI++)
	{
		ppRotationRect[iROI].left = stOption.ptROI[iROI].left;
		ppRotationRect[iROI].right = stOption.ptROI[iROI].right;
		ppRotationRect[iROI].top = stOption.ptROI[iROI].top;
		ppRotationRect[iROI].bottom = stOption.ptROI[iROI].bottom;
	}
}