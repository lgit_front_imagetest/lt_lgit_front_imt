﻿//*****************************************************************************
// Filename	: 	Wnd_MasterView.h
// Created	:	2016/10/29 - 16:47
// Modified	:	2016/10/29 - 16:47
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_MasterView_h__
#define Wnd_MasterView_h__

#pragma once

#include "List_Error.h"
#include "VGStatic.h"

//-----------------------------------------------------------------------------
// CWnd_MasterView
//-----------------------------------------------------------------------------
class CWnd_MasterView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MasterView)

public:
	CWnd_MasterView();
	virtual ~CWnd_MasterView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CFont	m_font_Default;
	CFont	m_font_Data;

	CVGStatic		m_st_Title;
	CVGStatic		m_stCount;


public:

	HANDLE			m_hExternalExitEvent = NULL;	// 외부의 통신 접속 쓰레드 종료 이벤트
	void			SetExitEvent(HANDLE hExitEvent);
	BOOL			m_bFlag_Counter = FALSE;
	HANDLE			m_hThr_Counter = NULL;	// PLC 모니터링 쓰레드 핸들	
	DWORD			m_dwCounterMonCycle = 50;	// 모니터링 주기
	static UINT WINAPI	Thread_CounterMon(__in LPVOID lParam);

	UINT m_nStartTime;

	UINT nCountTime;
	void TimeSetting(UINT nTime){
		nCountTime = nTime;
	};
	BOOL OnCounterCheck();
	BOOL Start_CounterCheck_Mon();
	BOOL Stop_CounterCheck_Mon();
};

#endif // Wnd_MasterView_h__

