//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_Foc.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_Foc_h__
#define Wnd_Cfg_TestItem_Foc_h__

#pragma once


#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_TestItem_Cm.h"
#include "Def_DataStruct.h"

#include "Wnd_Cfg_SFR.h"
#include "Wnd_Cfg_Defect_Black.h"
#include "Wnd_Cfg_Defect_White.h"
#include "Wnd_Cfg_OpticalCenter.h"
#include "Wnd_Cfg_Rotate.h"
#include "Wnd_Cfg_Ymean.h"
#include "Wnd_Cfg_BlackSpot.h"
#include "Wnd_Cfg_LCB.h"
#include "Wnd_Cfg_Current.h"
#include "Wnd_Cfg_ActiveAlign.h"
#include "Wnd_Cfg_Torque.h"

#include "Wnd_Cfg_TestItem_EachTest.h"
#include "Def_WindowMessage.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TestItem_Foc
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestItem_Foc : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem_Foc)

public:
	CWnd_Cfg_TestItem_Foc();
	virtual ~CWnd_Cfg_TestItem_Foc();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnNMClickTestItem	(NMHDR *pNMHDR, LRESULT *pResult);

	// 검사기 설정
	enInsptrSysType						m_InspectionType = enInsptrSysType::Sys_Focusing;

	CMFCTabCtrl							m_tc_Option;


	CWnd_Cfg_SFR						m_Wnd_SFR;
	CWnd_Cfg_Defect_Black				m_Wnd_Defect_Black;
	CWnd_Cfg_Defect_White				m_Wnd_Defect_White;
	CWnd_Cfg_OpticalCenter				m_Wnd_OpticalCenter;
	CWnd_Cfg_Rotate						m_Wnd_Rotate;
	CWnd_Cfg_Ymean						m_Wnd_Ymean;
	CWnd_Cfg_BlackSpot					m_Wnd_BlackSpot;
	CWnd_Cfg_LCB						m_Wnd_LCB;
	CWnd_Cfg_Current					m_Wnd_Current;
	CWnd_Cfg_ActiveAlign				m_Wnd_ActiveAlign;
	CWnd_Cfg_Torque						m_Wnd_Torque;

	CWnd_Cfg_TestItem_EachTest			m_wnd_TestItem_EachTest;

	void		SetSysAddTabCreate		();
	void		SetInitListCtrl			();

public:

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// 저장된 Test Item Info 데이터 불러오기
	void		Set_RecipeInfo			(__in  ST_RecipeInfo* pstRecipeInfo);
	void		Get_RecipeInfo			(__out ST_RecipeInfo& stOutRecipInfo);

	void		Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo);

	void		Get_TestItemInfo		(__in  ST_RecipeInfo& stRecipeInfo, __out ST_TestItemInfo& stOutTestItemInfo);
	void		Get_TestItemMinSpec		(__in  ST_RecipeInfo& stRecipeInfo, __in enTestItem_Focusing enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec);
	void		Get_TestItemMaxSpec		(__in  ST_RecipeInfo& stRecipeInfo, __in enTestItem_Focusing enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec);

	afx_msg void OnEnSetfocusTabFocus();

	UINT *m_pnCamParaIdx;

	void	SetCameraParaIdx(__in UINT *pstCamParaIdx)
	{
		if (NULL == pstCamParaIdx)
			return;

		m_pnCamParaIdx = pstCamParaIdx;
	};

};
#endif // Wnd_Cfg_TestItem_h__


