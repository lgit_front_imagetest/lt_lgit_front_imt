﻿// Wnd_Origin.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Origin.h"

// CWnd_Origin

IMPLEMENT_DYNAMIC(CWnd_Origin, CWnd)

CWnd_Origin::CWnd_Origin()
{
	m_pstDevice			 = NULL;
	m_hTimerQueue		 = NULL;
	m_hTimer_SensorCheck = NULL;
	m_nAxisNum			 = 0;
	m_nAxisMax			 = 0;
	m_bFlag_Timer		 = TRUE;

	VERIFY(m_font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	CreateTimerQueue_Mon();
	CreateTimerSensorCheck();

}

CWnd_Origin::~CWnd_Origin()
{
	m_font.DeleteObject();
	DeleteTimerQueue_Mon();
}

BEGIN_MESSAGE_MAP(CWnd_Origin, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BN_OR_STOP + BN_ORI_START,	OnBnClickedBnStart		)
	ON_BN_CLICKED(IDC_BN_OR_STOP + BN_ORI_STOP,		OnBnClickedBnStop		)
	ON_BN_CLICKED(IDC_BN_OR_STOP + BN_ORI_EXIT,		OnBnClickedBnExit		)
	ON_COMMAND_RANGE(IDC_ST_OR_AXIS_NAME,	IDC_ST_OR_SEN_AMP - 1,	OnAxisSelect	)
	ON_COMMAND_RANGE(IDC_ST_OR_SEN_AMP,		IDC_ST_OR_SEN_POS - 1,	OnAxisAmpCtr	)
	ON_COMMAND_RANGE(IDC_ST_OR_SEN_ALRAM,	IDC_ST_OR_SEN_MAXNUM,	OnAxisAlramCtr	)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_Origin::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < BN_ORI_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szOriginButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OR_STOP + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < ST_MT_OR_MAXNUM; nIdx++)
	{
		m_st_AxisName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_AxisName[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_AxisName[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_AxisName[nIdx].Create(g_szMotionOriginStatusName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ST_MT_OR_MAXNUM; nIdx++)
	{
		m_nAxisMax = m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt;

		if (m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt == 0)
		{
			m_nAxisMax = 4;
		}

		for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
		{
			m_st_AxisSen[nAxis][ST_MT_OR_POS_NAME].SetFont_Gdip(L"Arial", 12.0F);
			m_st_AxisSen[nAxis][nIdx].SetStaticStyle(CVGStatic::StaticStyle_GroupHeader);

			if (nIdx == ST_MT_OR_AXIS_NAME)
			{
				CString strName;

				if (m_pstDevice->m_AllMotorData.pMotionParam == NULL)
					strName.Empty();
				else
					strName = m_pstDevice->m_AllMotorData.pMotionParam[nAxis].szAxisName;

				m_st_AxisSen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
				m_st_AxisSen[nAxis][nIdx].Create(strName, dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_OR_AXIS_NAME + nAxis + nIdx * MAX_OR_AIXS);
			}
			else
			{
				m_st_AxisSen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
				m_st_AxisSen[nAxis][nIdx].Create(_T(""), dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_OR_AXIS_NAME + nAxis + nIdx * MAX_OR_AIXS);
			}
		}
	}

	m_st_AxisSen[0][ST_MT_OR_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	m_Group_Nmae.SetTitle(L"MOTOR ORIGIN MONITERING");

	if (!m_Group_Nmae.Create(_T("MOTOR ORIGIN MONITERING"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_Origin::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	cx = (int)(cx * MT_OR_WIDTH_OFFSET);

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = 0;
	int iTop = 0;
	int iWidth = cx;
	int iHeight = cy;
	int iStaticW = ((iWidth - 15) - ((iSpacing * (ST_MT_OR_MAXNUM - 1)))) / (ST_MT_OR_MAXNUM + 3);

	int iStNameH = 28;
	int iEdNameH = 28;
	int iStSensorH = 0;
	int iHeightTemp = 0;
	int iStTitleNameH = 35;

	int iAxisCnt = 0;

	// 초기화
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		for (UINT nIdx = 0; nIdx < ST_MT_OR_MAXNUM; nIdx++)
			m_st_AxisSen[nAxis][nIdx].MoveWindow(iLeft, iTop, 0, 0);

		if (TRUE == m_pstDevice->GetAxisUseStatus(nAxis))
		{
			iAxisCnt++;
		}
	}

	// 초기화
	for (UINT nAxis = 0; nAxis < (UINT)iAxisCnt; nAxis++)
	{
		for (UINT nIdx = 0; nIdx < ST_MT_OR_MAXNUM; nIdx++)
			m_st_AxisSen[nAxis][nIdx].MoveWindow(iLeft, iTop, 0, 0);
	}

	m_Group_Nmae.MoveWindow(iLeft, iTop, iWidth, iHeight);
	iTop += iSpacing * 6;

	iLeft = iMargin;
	m_st_AxisName[ST_MT_OR_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStNameH);
	iLeft += iStaticW * 2 + iSpacing;

	m_st_AxisName[ST_MT_OR_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStNameH);
	iLeft += iStaticW * 1 + iSpacing;

	m_st_AxisName[ST_MT_OR_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStNameH);
	iLeft += iStaticW * 3 + iSpacing;

	for (UINT nIdx = ST_MT_OR_ORI_NAME; nIdx < ST_MT_OR_MAXNUM; nIdx++)
	{
		m_st_AxisName[nIdx].MoveWindow(iLeft, iTop, iStaticW, iStNameH);
		iLeft += iStaticW + iSpacing;
	}

	iTop += iStNameH + iSpacing;
	UINT nCnt = 0;

	for (UINT nAxis = 0; nAxis < (UINT)iAxisCnt; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis) == TRUE)
			nCnt++;
	}

	iHeightTemp = iHeight - iTop;
	iStSensorH = (int)((iHeightTemp * MT_OR_HEIGHT_OFFSET) - (iSpacing * nCnt)) / (nCnt + 1);

	for (UINT nAxis = 0; nAxis < (UINT)iAxisCnt; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis) == TRUE)
		{
			iLeft = iMargin;

			m_st_AxisSen[nAxis][ST_MT_OR_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStSensorH);
			iLeft += iStaticW * 2 + iSpacing;

			m_st_AxisSen[nAxis][ST_MT_OR_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStSensorH);
			iLeft += iStaticW * 1 + iSpacing;

			m_st_AxisSen[nAxis][ST_MT_OR_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStSensorH);
			iLeft += iStaticW * 3 + iSpacing;

			for (UINT nIdx = ST_MT_OR_ORI_NAME; nIdx < ST_MT_OR_MAXNUM; nIdx++)
			{
				m_st_AxisSen[nAxis][nIdx].MoveWindow(iLeft, iTop, iStaticW, iStSensorH);
				iLeft += iStaticW + iSpacing;
			}

			iTop += iStSensorH + iSpacing;
		}
	}

	int iBtnW = (iWidth - 15 - iSpacing * BN_ORI_MAXNUM) / BN_ORI_MAXNUM;

	iLeft = iMargin;

	for (UINT nIdx = 0; nIdx < BN_ORI_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].MoveWindow(iLeft, iTop, iBtnW, iStSensorH - 15);
		iLeft += iBtnW + iSpacing;
	}

}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/11/8 - 9:41
// Desc.		:
//=============================================================================
void CWnd_Origin::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 1050;
	lpMMI->ptMaxTrackSize.y = m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt * 80;

	lpMMI->ptMinTrackSize.x = 1050;
	lpMMI->ptMinTrackSize.y = m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt * 80;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnSensorComds
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:09
// Desc.		:
//=============================================================================
void CWnd_Origin::OnAxisSelect(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	m_nAxisNum = nID - IDC_ST_OR_AXIS_NAME;

	for (UINT nIdx = 0; nIdx < (UINT)m_nAxisMax; nIdx++)
		m_st_AxisSen[nIdx][ST_MT_OR_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_AxisSen[m_nAxisNum][ST_MT_OR_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
}

//=============================================================================
// Method		: OnAxisAmpCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_Origin::OnAxisAmpCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_OR_SEN_AMP;

	if (m_pstDevice->GetAmpStatus(nAxis))
		m_pstDevice->SetAmpCtr(nAxis, OFF);
	else
		m_pstDevice->SetAmpCtr(nAxis, ON);
}

//=============================================================================
// Method		: OnAxisAlramCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_Origin::OnAxisAlramCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_OR_SEN_ALRAM;

	m_pstDevice->SetAlarmClear(nAxis);
}

//=============================================================================
// Method		: OnBnClickedBnStart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/7 - 21:11
// Desc.		:
//=============================================================================
void CWnd_Origin::OnBnClickedBnStart()
{
	m_bn_Item[BN_ORI_START].EnableWindow(FALSE);

	GetOwner()->SendNotifyMessageW(WM_MOTOR_ORIGIN, 0, 0);

	return;
}

//=============================================================================
// Method		: OnBnClickedBnStop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 11:11
// Desc.		:
//=============================================================================
void CWnd_Origin::OnBnClickedBnStop()
{
	m_bn_Item[BN_ORI_START].EnableWindow(TRUE);

	m_pstDevice->SetAllOriginStop();
}

//=============================================================================
// Method		: OnBnClickedBnExit
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/7 - 21:11
// Desc.		:
//=============================================================================
void CWnd_Origin::OnBnClickedBnExit()
{
	m_bResult = FALSE;

	EndModalLoop(TRUE);
	ShowWindow(SW_HIDE);

	CWnd::OnClose();
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_Origin::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: TimerRoutine_SensorCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_Origin::TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_Origin* pThis = (CWnd_Origin*)lpParam;

	pThis->OnMonitorSensorCheck();
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
void CWnd_Origin::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_Origin::DeleteTimerQueue_Mon()
{
	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_Origin::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_Origin::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_Origin::CreateTimerSensorCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimer_SensorCheck)
		if (!CreateTimerQueueTimer(&m_hTimer_SensorCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_SensorCheck, (PVOID)this, 500, 150, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_Origin::DeleteTimerSensorCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_SensorCheck, NULL))
			m_hTimer_SensorCheck = NULL;
		else
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::DeleteTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: OnMonitorSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_Origin::OnMonitorSensorCheck()
{
	// 모터 Amp 센서
	GetMotorAmpStatus();

	// 모터 원점 센서
	GetMotorOriginStatus();

	// 모터 동작 상태
	GetMotorMotionStatus();

	// 모터 펄스 값
	GetMotorCurrentPosStatus();

	// 모터 +/- 리미트 센서
	GetMotorLimitStatus();

	// 모터 홈 센서
	GetMotorHomeStatus();

	// 모터 알람 센서
	GetMotorAlarmStatus();

	// 원점 완료 체크 함수 
	IsOrigin();
}

//=============================================================================
// Method		: GetMotorAmpStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:14
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorAmpStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetAmpStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_OR_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_OR_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorCurrentPosStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:18
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorCurrentPosStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	CString strPos;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			strPos.Format(_T("%.0f"), m_pstDevice->GetCurrentPos(nAxis));
			m_st_AxisSen[nAxis][ST_MT_OR_POS_NAME].SetText(strPos);
		}
	}
}

//=============================================================================
// Method		: GetMotorMotionStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 8:04
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorMotionStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetMotionStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_OR_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_OR_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorOriginStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:25
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorOriginStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetOriginStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_OR_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_OR_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorLimitStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:28
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorLimitStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// + 센서 UNUSE 일 경우
			if (m_pstDevice->GetPosSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_OR_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetPosSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_OR_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_OR_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}

		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// - 센서 UNUSE 일 경우
			if (m_pstDevice->GetNegSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_OR_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetNegSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_OR_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_OR_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorHomeStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorHomeStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// HOME 센서 UNUSE 일 경우
			if (m_pstDevice->GetHomeSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_OR_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetHomeSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_OR_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
				else
					m_st_AxisSen[nAxis][ST_MT_OR_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorAlarmStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_Origin::GetMotorAlarmStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// ALRAM 센서 UNUSE 일 경우
			if (m_pstDevice->GetAlramSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_OR_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetAlarmSenorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_OR_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_OR_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: IsOrigin
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/8 - 12:29
// Desc.		:
//=============================================================================
void CWnd_Origin::IsOrigin()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;
	
	BOOL bOrigin = FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetOriginStatus(nAxis))
			{
				bOrigin = TRUE;
			}
			else
			{
				bOrigin = FALSE;
				break;
			}
		}
	}

	if (TRUE == bOrigin)
	{
		if (TRUE == m_bFlag_Timer)
		{
			m_bFlag_Timer = FALSE;
			m_bResult = TRUE;

			Sleep(500);
			EndModalLoop(TRUE);
			ShowWindow(SW_HIDE);

			CWnd::OnClose();
		}
	}
}

//=============================================================================
// Method		: SetUpdataAxisName
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 13:24
// Desc.		:
//=============================================================================
void CWnd_Origin::SetUpdataAxisName()
{
	CString strName;

	for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
	{
		strName = m_pstDevice->m_AllMotorData.pMotionParam[nAxis].szAxisName;
		m_st_AxisSen[nAxis][ST_MT_OR_AXIS_NAME].SetWindowText(strName);
	}

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/4 - 17:18
// Desc.		:
//=============================================================================
void CWnd_Origin::SetUpdataDataReset(UINT nAxis)
{
	for (UINT nIdx = 0; nIdx < (UINT)m_nAxisMax; nIdx++)
	{
		m_st_AxisSen[nIdx][ST_MT_OR_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);
	}

	m_st_AxisSen[nAxis][ST_MT_OR_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	// AXIS NAME
	SetUpdataAxisName();
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Origin::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		for (UINT nAxis = 0; nAxis < (UINT)m_nAxisMax; nAxis++)
		{
			m_pstDevice->SetOriginReset(nAxis);
		}
	}
}

//=============================================================================
// Method		: DoModal
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/11/8 - 9:38
// Desc.		:
//=============================================================================
BOOL CWnd_Origin::DoModal()
{
	EnableWindow(TRUE);
	m_bFlag_Timer = TRUE;

	if (GetParent())
	{
		RunModalLoop(MLF_SHOWONIDLE);
		DestroyWindow();
	}

	return m_bResult;
}
