﻿//*****************************************************************************
// Filename	: Wnd_ManualView.cpp
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// Wnd_ManualView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ManualView.h"
#include "resource.h"

#define		IDC_BTN_COLUMN			2000

#define		IDC_BTN_PARA			7000

static LPCTSTR g_szParaPort[] =
{
	_T("LEFT"),
	_T("RIGHT"),
	NULL
};

//=============================================================================
// CWnd_ManualView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_ManualView, CWnd_BaseView)

CWnd_ManualView::CWnd_ManualView()
{
	m_nPort = Para_Left;

	VERIFY(m_font_Default.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
	
	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_InspMode		= Permission_Operator;
	m_pstInspInfo	= NULL;
	m_pDevice		= NULL;
}

CWnd_ManualView::~CWnd_ManualView()
{
	TRACE(_T("<<< Start ~CWnd_ManualView >>> \n"));
	
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ManualView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE	()
	ON_COMMAND_RANGE(IDC_BTN_COLUMN, IDC_BTN_COLUMN + MAX_STEP_COUNT, OnCmdRanges)
	ON_COMMAND_RANGE(IDC_BTN_PARA, IDC_BTN_PARA + Para_MaxEnum, OnRangePort)
	ON_MESSAGE(WM_MANAUL_SEQUENCE, OnMsgManualSequence)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


//=============================================================================
// CWnd_ManualView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_ManualView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_ManualView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	m_wndLayout.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, 10);

	// * 검사 항목별 결과값 표시
	switch (m_InspectionType)
	{
	case Sys_Focusing:
		m_wnd_TestResult.Set_TestItemCount(9);
		break;

	case Sys_2D_Cal:
		m_wnd_TestResult.Set_TestItemCount(30);
		break;
	case Sys_Image_Test:
		m_wnd_TestResult.Set_TestItemCount(20);
		break;

	default:
		break;
	}

	for (UINT nIdex = 0; nIdex < Para_MaxEnum; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szParaPort[nIdex], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_PARA + nIdex);
		m_bn_Item[nIdex].SetMouseCursorHand();
		m_bn_Item[nIdex].SetImage(IDB_UNCHECKED_16);
		m_bn_Item[nIdex].SetCheckedImage(IDB_CHECKED_16);
		m_bn_Item[nIdex].SizeToContent();
		m_bn_Item[nIdex].SetFont(&m_Font);
	}

	m_bn_Item[Para_Left].SetCheck(1);

	m_wnd_TestResult.SetOwner(GetParent());
	m_wnd_TestResult.SetColumnBtnUse(TRUE);
	m_wnd_TestResult.SetBtnResourceID(IDC_BTN_COLUMN);
	m_wnd_TestResult.SetSystemType(m_InspectionType);
	m_wnd_TestResult.Set_TestInfo(&m_pstInspInfo->RecipeInfo.StepInfo, &m_pstInspInfo->RecipeInfo.TestItemInfo);
	m_wnd_TestResult.Create(NULL, _T(""), dwStyle, rectDummy, this, 11);

	m_st_frameZone.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	m_st_frameSatus.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	m_st_frameInfo.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);

	m_st_Judgment.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);
	m_st_Judgment.SetTextRenderingHint(TextRenderingHint::TextRenderingHintAntiAliasGridFit);
	m_st_Judgment.SetFont_Gdip(L"Arial", 32.0F);
	m_st_Judgment.Create(_T("Judgment"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	return 0;
}

//=============================================================================
// Method		: CWnd_ManualView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_ManualView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 5;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft			= iMagrin;
	int iTop			= iMagrin;
	int iWidth			= cx - iMagrin - iMagrin;
	int iHeight			= cy - iMagrin - iMagrin;

	int iHalfWidth		= (iWidth - iCateSpacing) * 75 / 100;

	int iStatusHeight	= 70;
	int iLayOutHeight	= (int)((iHeight - iCateSpacing - iCateSpacing) * 0.3);
	int iTestListHeight = iHeight - iCateSpacing - iCateSpacing - iStatusHeight - iLayOutHeight;

	int iTempWidth = iWidth - iCateSpacing - iHalfWidth;

	if (Sys_2D_Cal == m_InspectionType)
	{
		// Status
		/*iTop += iBtnHeight;*/
		MoveWindow_Status(iLeft, iTop, iHalfWidth, iStatusHeight);

		// LayOutTest
		iTop += iStatusHeight + iCateSpacing;
		MoveWindow_Monitoring(iLeft, iTop, iHalfWidth, iLayOutHeight);

		// TestList
		iTop += iLayOutHeight + iCateSpacing;
		MoveWindow_TestList(iLeft, iTop, iHalfWidth, iTestListHeight);

		// TestInfo
		iLeft += iHalfWidth + iCateSpacing;
		iTop = iMagrin;
		MoveWindow_Info(iLeft, iTop, iTempWidth, iHeight);
	}
	else// 파라 1개
	{
		int iStatusHeight	= (iHeight - iCateSpacing - iCateSpacing) / 8;
		int iLayOutHeight	= (iHeight - iCateSpacing - iCateSpacing) * 3 / 8;
		int iTestListHeight = iHeight - iCateSpacing - iCateSpacing - iStatusHeight - iLayOutHeight;


		// Status
		MoveWindow_Status(iLeft, iTop, iHalfWidth, iStatusHeight);

		// LayOutTest
		iTop += iStatusHeight + iCateSpacing;
		MoveWindow_Monitoring(iLeft, iTop, iHalfWidth, iLayOutHeight);

		// TestList
		iTop += iLayOutHeight + iCateSpacing;
		MoveWindow_TestList(iLeft, iTop, iHalfWidth, iTestListHeight);

		// TestInfo
		iLeft += iHalfWidth + iCateSpacing;
		iTop = iMagrin;
		MoveWindow_Info(iLeft, iTop, iTempWidth, iHeight);
	}
}

//=============================================================================
// Method		: MoveWindow_Monitoring
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_ManualView::MoveWindow_Monitoring(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iLeft	= x;
	int iTop	= y;
	int iWidth	= nWidth - 10;	
	int iHeight = nHeight - 10;

	m_wndLayout.MoveWindow(x, y, nWidth, nHeight);
}

void CWnd_ManualView::MoveWindow_TestList(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	m_wnd_TestResult.MoveWindow(x, y, nWidth, nHeight);
}

//=============================================================================
// Method		: MoveWindow_Info
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_ManualView::MoveWindow_Info(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iMagrin = 5;
	int iSpacing = 5;
	int iCateSpacing = 5;

	int iLeft = x + iMagrin;
	int iTop = y + iMagrin;
	int iWidth = nWidth - iMagrin - iMagrin;
	int iHeight = nHeight - iMagrin - iMagrin;

	if (Sys_2D_Cal == m_InspectionType || (Sys_Image_Test == m_InspectionType))
	{
		int iBtnHeight = 80;
		int iBtnWidth = (iWidth - iSpacing) / 2;

		m_bn_Item[Para_Left].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

		iLeft += iBtnWidth + iSpacing;
		m_bn_Item[Para_Right].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);
	}
	else
	{
		m_bn_Item[Para_Left].MoveWindow(0, 0, 0, 0);
		m_bn_Item[Para_Right].MoveWindow(0, 0, 0, 0);
	}

	m_st_frameInfo.MoveWindow(x, y, nWidth, nHeight);
}

//=============================================================================
// Method		: MoveWindow_Status
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_ManualView::MoveWindow_Status(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing = 5;

	int iLeft = x;
	int iTop = y;

	m_st_Judgment.MoveWindow(iLeft, iTop, nWidth, nHeight);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:59
// Desc.		:
//=============================================================================
void CWnd_ManualView::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

	m_wndLayout.SetParaCount(g_InspectorTable[m_InspectionType].Grabber_Cnt);
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_ManualView::SetPermissionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		m_InspMode = Permission_Operator;
		break;

	case Permission_Manager:
	case Permission_Administrator:
		m_InspMode = Permission_Administrator;
		break;

	default:
		break;
	}
}

void CWnd_ManualView::OnRangePort(__in UINT nID)
{
	UINT nFunc = nID - IDC_BTN_PARA;

	m_nPort = nFunc;

	if (Para_Left == nFunc)
	{
		m_bn_Item[Para_Right].SetCheck(0);
	}
	else
	{
		m_bn_Item[Para_Left].SetCheck(0);
	}
}

void CWnd_ManualView::OnCmdRanges(__in UINT nID)
{
	CString sz;

	LRESULT lReturn = RC_OK;

	UINT nParaIdx = m_nPort;
	UINT nIdx = nID - IDC_BTN_COLUMN;

// 	sz.Format(_T("%d"), nID);
// 	AfxMessageBox(sz);

	lReturn = GetParent()->SendMessage(WM_MANAUL_TESTITEM, nParaIdx, nIdx);
}

void CWnd_ManualView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		m_wnd_TestResult.Set_TestInfo(&m_pstInspInfo->RecipeInfo.StepInfo, &m_pstInspInfo->RecipeInfo.TestItemInfo);
	}
}

LRESULT CWnd_ManualView::OnMsgManualSequence(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn;
	lReturn = GetParent()->SendMessage(WM_MANAUL_SEQUENCE, wParam, lParam);
	return lReturn;
}
