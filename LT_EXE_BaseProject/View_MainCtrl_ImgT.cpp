﻿//*****************************************************************************
// Filename	: View_MainCtrl_ImgT.cpp
// Created	: 2010/11/26
// Modified	: 2016/07/21
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// View_MainCtrl_ImgT.cpp : CView_MainCtrl_ImgT 클래스의 구현
//

#include "stdafx.h"
#include "resource.h"

#include "View_MainCtrl_ImgT.h"
#include "CommonFunction.h"
#include "Pane_CommStatus.h"
#include "File_Recipe.h"
#include "File_Report.h"
#include "File_Maintenance.h"
#include "Dlg_ChkPassword.h"

#include <strsafe.h>
#include <iphlpapi.h>
#include <icmpapi.h>

#pragma comment(lib, "iphlpapi.lib")

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//=============================================================================
// CView_MainCtrl_ImgT 생성자
//=============================================================================
CView_MainCtrl_ImgT::CView_MainCtrl_ImgT()
{
	InitConstructionSetting();
}

//=============================================================================
// CView_MainCtrl_ImgT 소멸자
//=============================================================================
CView_MainCtrl_ImgT::~CView_MainCtrl_ImgT()
{
	TRACE(_T("<<< Start ~CView_MainCtrl_ImgT >>> \n"));


	DeleteSplashScreen();

	TRACE(_T("<<< End ~CView_MainCtrl_ImgT >>> \n"));
}


BEGIN_MESSAGE_MAP(CView_MainCtrl_ImgT, CView_MainCtrl)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE	(WM_LOGMSG,				OnLogMsg)
	ON_MESSAGE	(WM_LOGMSG_PLC,			OnLogMsg)
	ON_MESSAGE	(WM_LOGMSG_TESTER,		OnLogMsg)
	ON_MESSAGE	(WM_TEST_START,			OnTestStart)
	ON_MESSAGE	(WM_TEST_STOP,			OnTestStop)
	ON_MESSAGE	(WM_TEST_INIT,			OnTestInit)
	ON_MESSAGE	(WM_TEST_COMPLETED,		OnTestCompleted)
	ON_MESSAGE	(WM_MES_COMM_STATUS,	OnCommStatus_MES)
	ON_MESSAGE	(WM_MES_RECV_BARCODE,	OnRecvMES)
	ON_MESSAGE	(WM_PERMISSION_MODE,	OnSwitchPermissionMode)
	ON_MESSAGE	(WM_MES_ONLINE_MODE,	OnSwitchMESOnlineMode)
	ON_MESSAGE	(WM_CHANGED_MODEL,		OnChangeRecipe)
	ON_MESSAGE	(WM_MANUAL_DEV_CTRL,	OnDeviceCtrl)
	ON_MESSAGE	(WM_RECV_BARCODE,		OnRecvBarcode)
	ON_MESSAGE	(WM_CAMERA_CHG_STATUS,	OnCameraChgStatus)
	ON_MESSAGE	(WM_CAMERA_RECV_VIDEO,	OnCameraRecvVideo)
	ON_MESSAGE	(WM_RECV_DIO_BIT,		OnRecvDIOMon)
	ON_MESSAGE	(WM_RECV_DIO_FST_READ,	OnRecvDIOFirstRead)
	ON_MESSAGE	(WM_RECV_MAIN_BRD_ACK,	OnRecvMainBrd)
	ON_MESSAGE	(WM_CHANGED_MOTOR,		OnChangeMotor)
	ON_MESSAGE	(WM_MOTOR_ORIGIN,		OnMotorOrigin)
	ON_MESSAGE	(WM_CHANGED_MAINTENANCE,OnChangeMaintenance)
	ON_MESSAGE	(WM_MANAUL_SEQUENCE,	OnManualSequence)
	ON_MESSAGE	(WM_MANAUL_TESTITEM,	OnManualTestItem)
	ON_MESSAGE	(WM_MANAUL_CANCOMM,		OnManualCanComm)	
	ON_MESSAGE	(WM_MANAUL_CANCOMMPG2,	OnManualCanCommPg2)
	ON_MESSAGE	(WM_MANAUL_CANCOMMPG3,	OnManualCanCommPg3)
	ON_MESSAGE	(WM_MANAUL_CANCOMMPG4,	OnManualCanCommPg4)
	ON_MESSAGE	(WM_CONSUMABLES_RESET,	OnConsumableReset)
	//ON_MESSAGE	(WM_RECV_TORQUE_ACK,	OnRecvTorque)
	ON_MESSAGE	(WM_CYLINGER_MANUL,		OnManualCylinger)
END_MESSAGE_MAP()


//=============================================================================
// CView_MainCtrl_ImgT 메시지 처리기
//=============================================================================

//=============================================================================
//=============================================================================
// Method		: CView_MainCtrl_ImgT::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
int CView_MainCtrl_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	m_wnd_MainView.SetSystemType(m_InspectionType);
// 	m_wnd_ManualView.SetSystemType(m_InspectionType);
// 	m_wnd_ManualView.SetPtrInspectionInfo(&m_stInspInfo);
	m_wnd_RecipeView.SetPtr_ImageMode(&m_stImageMode);

	if (CView_MainCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// 초기 세팅
	CreateSplashScreen (this, IDB_BITMAP_Luritech);
	InitUISetting ();
	InitDeviceSetting();

	return 0;
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CView_MainCtrl::OnSize(nType, cx, cy);
}


//=============================================================================
// Method		: CView_MainCtrl_ImgT::OnLogMsg
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam	-> 메세지 문자열
// Parameter	: LPARAM lParam	
//					-> HIWORD : 오류 메세지 인가?
//					-> LOWORD : 로그 종류 (기본, PLC, 관리PC 등)
// Qualifier	:
// Last Update	: 2010/10/14 - 17:38
// Desc.		: 로그 처리용
//	LOG_TAB_PLC		= 0,
//	LOG_TAB_MANPC,
//	LOG_TAB_IRDA,
//	LOG_TAB_BCR,
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnLogMsg( WPARAM wParam, LPARAM lParam )
{
	BOOL	bError = (BOOL)HIWORD(lParam);
	UINT	nType  = LOWORD(lParam);

	if (NULL == (LPCTSTR)wParam)
	{
		return FALSE;
	}
	
	AddLog((LPCTSTR)wParam, bError, nType);

	return TRUE;
}

//=============================================================================
// Method		: OnLogMsg_PLC
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/31 - 17:50
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnLogMsg_PLC(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage (m_hOwnerWnd, m_nWM_LOG, (WPARAM)lpszLog, (LPARAM)MAKELONG((WORD)m_nDeviceType, (WORD)FALSE));

	BOOL	bError = (BOOL)HIWORD(lParam);
	UINT	nType = LOWORD(lParam);

	if (NULL == (LPCTSTR)wParam)
	{
		return FALSE;
	}

	AddLog((LPCTSTR)wParam, bError, nType);

	return TRUE;
}

//=============================================================================
// Method		: OnTestStart
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:12
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnTestStart(WPARAM wParam, LPARAM lParam)
{
	UINT nParaIdx = (UINT)wParam;

// 	if (MAX_SITE_CNT <= nParaIdx)
// 	{
// 		nParaIdx = 0;
// 	}

	AddLog(_T("Start Inspection"));

	if (FALSE == IsTesting())
	{
		StartOperation_LoadUnload(TRUE);
	}
	return TRUE;
}

//=============================================================================
// Method		: OnTestStop
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:16
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnTestStop(WPARAM wParam, LPARAM lParam)
{
	// 진행 중인 모든 작업 중지
	if (IsTesting())
	{
		StopProcess_Test_All();
		Delay(500);
	}

	return 1;
}

//=============================================================================
// Method		: OnTestInit
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/11/11 - 1:58
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnTestInit(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		return FALSE;
	}

// 	if (IDYES == AfxMessageBox(_T("데이터를 초기화 하시겠습니까?"), MB_YESNO))
// 	{
// 		// 알람 초기화
// 
// 		// 데이터 초기화
// 	}

	return TRUE;
}

//=============================================================================
// Method		: OnTestCompleted
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/30 - 13:43
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnTestCompleted(WPARAM wParam, LPARAM lParam)
{
	// 최종 검사 판정 업데이트?
	OnJugdement_And_Report();
	return TRUE;
}

//=============================================================================
// Method		: OnSwitchPermissionMode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/29 - 16:46
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnSwitchPermissionMode(WPARAM wParam, LPARAM lParam)
{
 	if (IsTesting())
 	{
 		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
 		return FALSE;
 	}

	enPermissionMode InspMode = (enPermissionMode)wParam;

	m_stInspInfo.PermissionMode = InspMode;

	m_wnd_MainView.SetPermissionMode(InspMode);

	// MainFrm으로 권한 변경 통보
	GetParent()->SendMessage(WM_PERMISSION_MODE, (WPARAM)InspMode, 0);

	return TRUE;
}

//=============================================================================
// Method		: OnSwitchMESOnlineMode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/3/1 - 10:26
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnSwitchMESOnlineMode(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		return FALSE;
	}

	enMES_Online MESOnlineMode = (enMES_Online)wParam;

	// MES로 Online 상태 변경

	OnSetStatus_MES_Online(MESOnlineMode);

	return TRUE;
}

//=============================================================================
// Method		: OnChangeRecipe
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/25 - 18:31
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnChangeRecipe(WPARAM wParam, LPARAM lParam)
{
 	if (IsTesting())
 	{
 		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
 		return FALSE;
 	}

	//DEBUG_ONLY();
	
	// 모델 파일에서 모델 정보 불러오기
	CString strModel = (LPCTSTR)wParam;
	BOOL bNotifyModelView = (BOOL)lParam;
	
	LoadRecipeInfo(strModel, bNotifyModelView);

	return TRUE;
}

//=============================================================================
// Method		: OnCommStatus_MES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnCommStatus_MES(WPARAM wParam, LPARAM lParam)
{
	UINT	nDevice = (UINT)wParam;
	UINT	nStatus = (UINT)lParam;

	OnSetStatus_MES(nStatus);

//  	switch (nStatus)
//  	{
//  	case enTCPIPConnectStatus::COMM_CONNECTED:
//  		break;
//  
//  	case enTCPIPConnectStatus::COMM_CONNECTED_SYNC_OK:
//  		SetMESOnlineMode(enMES_Online::MES_Online);
//  		break;
//  
//  	case enTCPIPConnectStatus::COMM_DISCONNECT:
//  	case enTCPIPConnectStatus::COMM_CONNECT_DROP:
//  	case enTCPIPConnectStatus::COMM_CONNECT_ERROR:
//  		SetMESOnlineMode(enMES_Online::MES_Offline);
//  		break;
//  	}

	return 0;
}

//=============================================================================
// Method		: OnRecvMES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnRecvMES(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage(m_hOwnerWnd, m_WM_Recv, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
	ST_LG_MES_Protocol	m_stRecvProtocol;
	m_stRecvProtocol.SetRecvProtocol((const char*)wParam, (DWORD)lParam);

	USES_CONVERSION;
	m_stInspInfo.szBarcodeBuf.Format(_T("%s"), A2T(m_stRecvProtocol.szLotID));
	UINT nRecvLotTryCnt = atoi(m_stRecvProtocol.szLotTryCount.GetBuffer());
	m_stRecvProtocol.szLotTryCount.ReleaseBuffer();
	m_stRecvProtocol.szProtocol.ReleaseBuffer();

	AddLog_F(_T("MES Recieved -> Barcode : %s, Try : %s"), m_stRecvProtocol.szLotID, m_stRecvProtocol.szLotTryCount);

	// 리셋 데이터
	//OnResetInfo_StartTest();
	if (TRUE == m_stInspInfo.CamInfo[Para_Left].szBarcode.IsEmpty())
	{
		OnResetInfo_Loading();
	}

	// 16자리 체크
// 	if (32 < m_stInspInfo.szBarcodeBuf.GetLength())
// 	{
// 		m_stInspInfo.ResetBarcodeBuffer();
// 
// 		OnAddAlarm(_T("Can't Use Barcode"));
// 		AfxMessageBox(_T("Can't Use Barcode"), MB_SYSTEMMODAL);
// 		return -1;
// 	}

	// UI에 표시
	TRACE(_T("Barcode : %s\n"), m_stInspInfo.szBarcodeBuf);

	OnSet_Barcode(m_stInspInfo.szBarcodeBuf, nRecvLotTryCnt, Para_Left);

	return 0;
}

//=============================================================================
// Method		: OnDeviceCtrl
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/8/10 - 10:42
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnDeviceCtrl(WPARAM wParam, LPARAM lParam)
{
	//GetOwner()->SendNotifyMessage(WM_MANUAL_DEV_CTRL, (WPARAM)nChIdx, (LPARAM)nBnIdx);

	UINT nChIdx = (UINT)wParam;
	UINT nBnIdx = (UINT)lParam;

	Manual_DeviceControl(nChIdx, nBnIdx);

	return TRUE;
}

//=============================================================================
// Method		: OnRecvBarcode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/10/21 - 17:33
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnRecvBarcode(WPARAM wParam, LPARAM lParam)
{
	// ::SendNotifyMessage(m_hOwnerWnd, m_WM_Ack, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
#ifdef USE_BARCODE_SCANNER
	CStringA szTemp = (char*)wParam;
	DWORD dwLength = (DWORD)lParam;

	szTemp.Remove('\r');
	szTemp.Remove('\n');

	USES_CONVERSION;
	CString szBarcode = A2T(szTemp.GetBuffer());
	szTemp.ReleaseBuffer();

	TRACE(_T("BCR Barcode : %s (Length : %d)\n"), szBarcode, dwLength);
	AddLog_F(_T("BCR Barcode : %s (Length : %d)\n"), szBarcode, dwLength);

	// 리셋 데이터
	OnResetInfo_Loading();

	// 16자리 체크
	if (32 < m_stInspInfo.szBarcodeBuf.GetLength())
	{
		m_stInspInfo.ResetBarcodeBuffer();

		OnAddAlarm(_T("Can't Use Barcode"));
		AfxMessageBox(_T("Can't Use Barcode"), MB_SYSTEMMODAL);
		return -1;
	}

	// UI에 표시
	OnSet_BarcodeWithDialog(szBarcode);
#endif

	return TRUE;
}

//=============================================================================
// Method		: OnCameraChgStatus
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/2/1 - 13:13
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnCameraChgStatus(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage(m_hWndOwner, m_nWM_ChgStatus, (WPARAM)m_nBoardNumber, (LPARAM)m_stStatus.bSignal);

	UINT nBoardNumber	= (UINT)wParam;
	BOOL bSignal		= (BOOL)lParam;

	// On -> Off
	OnSetStatus_VideoSignal(bSignal, nBoardNumber);

	if (FALSE == bSignal)
	{
		DisplayVideo_NoSignal(nBoardNumber);
	}

	return 1;
}

//=============================================================================
// Method		: OnCameraRecvVideo
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/2/1 - 13:13
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnCameraRecvVideo(WPARAM wParam, LPARAM lParam)
{
	UINT nBoardNumber	= (UINT)wParam;

	ST_VideoRGB* pRGB = m_Device.DAQ_LVDS.GetRecvVideoRGB(nBoardNumber);
	LPBYTE pRGBDATA = m_Device.DAQ_LVDS.GetRecvRGBData(nBoardNumber);

	DisplayVideo(nBoardNumber, pRGBDATA, pRGB->m_dwSize, pRGB->m_dwWidth, pRGB->m_dwHeight);	

	return 1;
}

//=============================================================================
// Method		: OnRecvDIOMon
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/2/1 - 17:18
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnRecvDIOMon(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage(m_hOwnerWnd, m_nWMBitChanged, (WPARAM)nIdx, (LPARAM)m_warReadData[nIdx]);

	UINT	nReadType = (UINT)wParam;
	DWORD64	dwReadData = (DWORD)lParam;

	if (CDigitalIOCtrl::ReadIdx_DI == nReadType)
	{
		m_Device.DigitalIOCtrl.Get_DI_Status_All(dwReadData);
		if (dwReadData != m_stInspInfo.dwDI)
		{
			m_stInspInfo.dwDI = dwReadData;
			BOOL bOnOff = FALSE;
			for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
			{
				bOnOff = dwReadData >> nOffset & 0x0000000000000001;

				if (bOnOff != m_stInspInfo.byDIO_DI[nOffset])
				{
					m_stInspInfo.byDIO_DI[nOffset] = bOnOff;

					OnDIn_DetectSignal(nOffset, bOnOff);
				}
			}

			// UI 갱신
			m_wnd_IOView.Set_IO_DI_Data(m_stInspInfo.byDIO_DI, MAX_DIGITAL_IO);
		}
	}
	else if (CDigitalIOCtrl::ReadIdx_DO == nReadType)
	{
		m_Device.DigitalIOCtrl.Get_DO_Status_All(dwReadData);
		if (dwReadData != m_stInspInfo.dwDO)
		{
			m_stInspInfo.dwDO = dwReadData;
			BOOL bOnOff = FALSE;
			for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
			{
				bOnOff = dwReadData >> nOffset & 0x0000000000000001;

				if (bOnOff != m_stInspInfo.byDIO_DO[nOffset])
				{
					m_stInspInfo.byDIO_DO[nOffset] = bOnOff;
				}
			}

			// UI 갱신
			m_wnd_IOView.Set_IO_DO_Data(m_stInspInfo.byDIO_DO, MAX_DIGITAL_IO);
		}
	}

	return 1;
}

//=============================================================================
// Method		: OnRecvDIOFirstRead
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/2/1 - 17:18
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnRecvDIOFirstRead(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage(m_hOwnerWnd, m_nWMFirstRead, (WPARAM)nBlockIdx, (LPARAM)m_warReadData[nBlockIdx]);

	UINT	nReadType = (UINT)wParam;
	DWORD64	dwReadData = (DWORD64)lParam;

	if (CDigitalIOCtrl::ReadIdx_DI == nReadType)
 	{
		m_Device.DigitalIOCtrl.Get_DI_Status_All(dwReadData);

 		m_stInspInfo.dwDI = dwReadData;
 
		for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
 		{
 			m_stInspInfo.byDIO_DI[nOffset] = dwReadData >> nOffset & 0x00000001;
 		}
 
 		// UI 갱신
		m_wnd_IOView.Set_IO_DI_Data((LPBYTE)m_stInspInfo.byDIO_DI, MAX_DIGITAL_IO);
 	}
	else if (CDigitalIOCtrl::ReadIdx_DO == nReadType)
 	{
		m_Device.DigitalIOCtrl.Get_DO_Status_All(dwReadData);
		m_stInspInfo.dwDO = dwReadData;
 
		for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
 		{
 			m_stInspInfo.byDIO_DO[nOffset] = dwReadData >> nOffset & 0x00000001;
 		}
 
 		// UI 갱신
		m_wnd_IOView.Set_IO_DO_Data((LPBYTE)m_stInspInfo.byDIO_DO, MAX_DIGITAL_IO);
 	}

	return 1;
}

//=============================================================================
// Method		: OnRecvMainBrd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/1/24 - 11:21
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnRecvMainBrd(WPARAM wParam, LPARAM lParam)
{
	//::SendNotifyMessage(m_hOwnerWnd, m_WM_ID_ACK, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);

	return 0;
}

//=============================================================================
// Method		: OnMotorOrigin
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/11/7 - 21:35
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnMotorOrigin(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.MotorAllOriginStart();
#endif

	AfxMessageBox(g_szResultCode[lReturn]);

	return lReturn;
}

//=============================================================================
// Method		: OnChangeMotor
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/2 - 14:19
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnChangeMotor(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		return FALSE;
	}

	//DEBUG_ONLY();

	// 모델 파일에서 모델 정보 불러오기
	CString strMotor = (LPCTSTR)wParam;
	BOOL bNotifyModelView = (BOOL)lParam;

	LoadMotorInfo(strMotor, bNotifyModelView);

	return 0;
}

//=============================================================================
// Method		: OnChangeMaintenance
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/12 - 14:28
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnChangeMaintenance(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		return FALSE;
	}

	//DEBUG_ONLY();

	// 모델 파일에서 모델 정보 불러오기
	CString strMaintenance = (LPCTSTR)wParam;
	BOOL bNotifyModelView = (BOOL)lParam;

	LoadMaintenanceInfo(strMaintenance, bNotifyModelView);

	return 0;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/12/10 - 10:20
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualSequence(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	UINT		nParaID		= (UINT)wParam;
	enParaManual nFuncID	= (enParaManual)lParam;

	lReturn = __super::OnManualSequence(nParaID, nFuncID);
	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/12/10 - 10:20
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualTestItem(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	UINT		nParaID = (UINT)wParam;
	UINT		nTestItem = (UINT)lParam;

	lReturn = __super::OnManualTestItem(nParaID, nTestItem);
	return lReturn;
}

//=============================================================================
// Method		: OnManualCanComm
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/11/15 - 4:11
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualCanComm(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnManualCanCommPg2
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/12/10 - 10:20
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualCanCommPg2(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnManualCanCommPg3
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/11/18 - 16:54
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualCanCommPg3(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnManualCanCommPg4
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/12/7 - 10:58
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualCanCommPg4(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnManualCylinger
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/3/8 - 14:24
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnManualCylinger(WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturn = RC_OK;

	enCYL_Focusing enCylinder = (enCYL_Focusing)wParam;
	UINT nParaID = (UINT)lParam;
#ifndef MOTION_NOT_USE			// 1109 SH2 임의모션삭제
	switch (enCylinder)
	{
	case CYL_Fo_FixOn:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(ON, nParaID);
		break;
	case CYL_Fo_FixOff:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(OFF);
		break;
	case CYL_Fo_StageUp:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Gripper(OFF);
		
		if (RC_OK != lReturn)
			return lReturn;

		lReturn = m_MotionSequence.OnActionTesting_Foc_Stage(UP);
		break;
	case CYL_Fo_StageDown:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Gripper(OFF);

		if (RC_OK != lReturn)
			return lReturn;

		lReturn = m_MotionSequence.OnActionTesting_Foc_Stage(DN);
		break;
	case CYL_Fo_GripperOn:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Gripper(ON);
		break;
	case CYL_Fo_GripperOff:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Gripper(OFF);
		break;
	case CYL_Fo_ParticleIn:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Particle(ON);
		break;
	case CYL_Fo_ParticleOut:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Particle(OFF);
		break;
	case CYL_Fo_DriveUp:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Driver(UP);
		break;
	case CYL_Fo_DriveDown:
		lReturn = m_MotionSequence.OnActionTesting_Foc_Gripper(OFF);

		if (RC_OK != lReturn)
			return lReturn;

		lReturn = m_MotionSequence.OnActionTesting_Foc_Stage(DN);

		if (RC_OK != lReturn)
			return lReturn;

		lReturn = m_MotionSequence.OnActionTesting_Foc_Driver(DN);
		break;
	default:
		break;
	}
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnConsumableReset
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/3/5 - 9:41
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl_ImgT::OnConsumableReset(WPARAM wParam, LPARAM lParam)
{
	UINT nIndex = (UINT)wParam;

	if (IDYES == AfxMessageBox(_T("Are you sure you want to Reset Count?"), MB_YESNO))
	{
		// 패스워드 체크
		CDlg_ChkPassword	dlgPassword(this);
		if (IDCANCEL == dlgPassword.DoModal())
			return FALSE;

		Reset_ConsumCount(nIndex);
	}

	return 1;
}
// 
// //=============================================================================
// // Method		: OnRecvTorque
// // Access		: protected  
// // Returns		: LRESULT
// // Parameter	: WPARAM wParam
// // Parameter	: LPARAM lParam
// // Qualifier	:
// // Last Update	: 2018/3/8 - 8:28
// // Desc.		:
// //=============================================================================
// LRESULT CView_MainCtrl_ImgT::OnRecvTorque(WPARAM wParam, LPARAM lParam)
// {
// 	ST_TorqueMonitor stTorque = m_Device.Torque.GetAckProtocol();
// 
// 	char Buffer[10] = { 0, };
// 	memcpy(Buffer, stTorque.ConvTorque, sizeof(stTorque.ConvTorque));
// 
// 	switch (stTorque.FastenLoosen)
// 	{
// 	case '0': // 풀림
// 
// 		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_20_Screw_A))
// 		{
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Tor_A]  = 0.0;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.enStatus[Spec_Tor_A] = Torque_Rele;
// 		}
// 
// 		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_21_Screw_B))
// 		{
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Tor_B]  = 0.0;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.enStatus[Spec_Tor_B] = Torque_Rele;
// 		}
// 
// 	//	if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_23_Screw_C))
// 	//	{
// 	//		m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stFocus.stTorqueData.dbValue[Spec_Tor_C]  = 0.0;
// 	//		m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stFocus.stTorqueData.enStatus[Spec_Tor_C] = Torque_Rele;
// 	//	}
// 
// 	//	if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_22_Screw_D))
// 	//	{
// 	//		m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stFocus.stTorqueData.dbValue[Spec_Tor_D]  = 0.0;
// 	//		m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stFocus.stTorqueData.enStatus[Spec_Tor_D] = Torque_Rele;
// 	//	}
// 
// 		break;
// 
// 	case '1':	// 체결
// 
// 		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_20_Screw_A))
// 		{
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Tor_A] = atof(Buffer) * 0.01;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Cnt_A] += 1.0;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.enStatus[Spec_Tor_A] = Torque_Lock;
// 		}
// 
// 		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_21_Screw_B))
// 		{
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Tor_B] = atof(Buffer) * 0.01;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.dbValue[Spec_Cnt_B] += 1.0;
// 			m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.stTorque.enStatus[Spec_Tor_B] = Torque_Lock;
// 		}
// 
// 		break;
// 
// 	default:
// 		break;
// 	}
// 
// 	return 0;
// }

//=============================================================================
// Method		: OnInitLogFolder
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/19 - 15:04
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnInitLogFolder()
{
	// 로그 처리
	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_logFile.SetPath(m_stInspInfo.Path.szLog, _T("Inspector"));

	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_Log_ErrLog.SetPath(m_stInspInfo.Path.szLog, _T("Error"));

	m_Log_ErrLog.SetLogFileName_Prefix(_T("Err"));

}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::InitConstructionSetting
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/13 - 15:13
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitConstructionSetting()
{
	// 프로그램 폴더 구하기
	TCHAR szExePath[MAX_PATH] = {0};	
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];	
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s (szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);	
	
	m_stInspInfo.Path.szProgram.		Format(_T("%s%s"), drive, dir);
	m_stInspInfo.Path.szLog	.			Format(_T("%s%sLOG\\"), drive, dir);
	m_stInspInfo.Path.szReport.			Format(_T("%s%sReport\\"), drive, dir);
	if (m_bUseForcedModel)
	{
		m_stInspInfo.Path.szRecipe.Format(_T("%s%sRecipe\\%s\\"), drive, dir, g_szModelFolder[m_nModelType]);
	}
	else
	{
		m_stInspInfo.Path.szRecipe.Format(_T("%s%sRecipe\\"), drive, dir);
	}
	m_stInspInfo.Path.szConsumables.	Format(_T("%s%sConsumables\\"), drive, dir);
	m_stInspInfo.Path.szMotor.			Format(_T("%s%sMotor\\"), drive, dir);
	m_stInspInfo.Path.szMaintenance.	Format(_T("%s%sMaintenance\\"), drive, dir);
	m_stInspInfo.Path.szImage.			Format(_T("%s%sImage\\"), drive, dir);
	m_stInspInfo.Path.szI2c.			Format(_T("%s%sI2c\\"), drive, dir);
	
	OnLoadOption();

	MakeSubDirectory(m_stInspInfo.Path.szReport);
	MakeSubDirectory(m_stInspInfo.Path.szRecipe);
	MakeSubDirectory(m_stInspInfo.Path.szConsumables);
	MakeSubDirectory(m_stInspInfo.Path.szMotor);
	MakeSubDirectory(m_stInspInfo.Path.szMaintenance);
	MakeSubDirectory(m_stInspInfo.Path.szImage);
	MakeSubDirectory(m_stInspInfo.Path.szI2c);

	OnInitLogFolder();

	m_wnd_IOView.SetPtr_Device(&m_Device);

#ifndef MOTION_NOT_USE
	m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor);
	m_Device.MotionManager.SetAllMotorOpen();
#endif

	m_wnd_MaintenanceView.SetPtrInspectionInfo(&m_stInspInfo);
	m_wnd_MaintenanceView.SetPath(m_stInspInfo.Path.szMotor, m_stInspInfo.Path.szMaintenance);
	m_wnd_MaintenanceView.SetPtr_Device(&m_Device);
	
	m_wnd_MainView.SetPtrInspectionInfo(&m_stInspInfo);

	m_wnd_RecipeView.SetPtr_Device(&m_Device);
	m_wnd_RecipeView.SetPtr_CameraInfo(m_stInspInfo.CamInfo);
	m_wnd_RecipeView.SetPtrImageCaptureMode(&m_bPicCaptureMode);
	m_wnd_RecipeView.SetPtrImageCaptureFile(&m_szImageFileName);
	m_wnd_RecipeView.SetPath(m_stInspInfo.Path.szRecipe, m_stInspInfo.Path.szConsumables, m_stInspInfo.Path.szImage);

#ifndef MOTION_NOT_USE
	m_MotionSequence.SetLTOption(&m_stOption);
	m_MotionSequence.SetPtr_Device(&m_Device.MotionManager, &m_Device.DigitalIOCtrl);
	m_MotionSequence.SetPtr_TeachInfo(&m_stInspInfo.MaintenanceInfo.stTeachInfo);
#endif

	m_Test_ResultDataView.SetPtr_MainView(&m_wnd_MainView);
	m_Test_ResultDataView.SetPtr_RecipeView(&m_wnd_RecipeView);

	m_TestMgr_TestMes.SetPtr_InspInfo(&m_stInspInfo);
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::InitUISetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/1/2 - 16:23
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitUISetting()
{
	CReg_InspInfo regInfo;
	DWORD dwValue = 0;

	// 레지스트리에 변경사항 불러오기
	DWORD dwChkUsableCh = 0;
	m_regInspInfo.LoadSelectedCam(dwChkUsableCh);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_stInspInfo.bTestEnable[nIdx] = (BOOL)((dwChkUsableCh >> nIdx) & 0x00000001);
	}
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::InitDeviceSetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2012/12/17 - 17:53
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitDeviceSetting()
{
	InitDevicez(GetSafeHwnd());
}

//=============================================================================
// Method		: OnSetStatus_MES
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nCommStatus
// Qualifier	:
// Last Update	: 2017/9/13 - 17:39
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_MES(__in UINT nCommStatus)
{
	__super::OnSetStatus_MES(nCommStatus);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_MES(nCommStatus);
}

//=============================================================================
// Method		: OnSetStatus_MES_Online
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nOnlineMode
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_MES_Online(__in UINT nOnlineMode)
{
	__super::OnSetStatus_MES_Online(nOnlineMode);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_MES_Online(nOnlineMode);
}

//=============================================================================
// Method		: OnSetStatus_HandyBCR
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_HandyBCR(__in UINT nConnect)
{
	__super::OnSetStatus_HandyBCR(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_HandyBCR(nConnect);
}
 
//=============================================================================
// Method		: OnSetStatus_Camera_Brd
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_Camera_Brd(__in UINT nConnect, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_Camera_Brd(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_CameraBoard(nConnect, nIdxBrd);
}

//=============================================================================
// Method		: OnSetStatus_LightBrd
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_LightBrd(__in UINT nConnect, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_LightBrd(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_LightBoard(nConnect, nIdxBrd);
}

//=============================================================================
// Method		: OnSetStatus_LightPSU
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_LightPSU(__in UINT nConnect, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_LightPSU(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_LightPSU(nConnect, nIdxBrd);
}

//=============================================================================
// Method		: OnSetStatus_Motion
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nCommStatus
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_Motion(__in UINT nCommStatus)
{
	__super::OnSetStatus_Motion(nCommStatus);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_DIO(nCommStatus);
	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Motion(nCommStatus);
}

//=============================================================================
// Method		: OnSetStatus_Indicator
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_Indicator(__in UINT nConnect, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_Indicator(nConnect, nIdxBrd);

	//((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Indicator(nConnect, nIdxBrd);
}

//=============================================================================
// Method		: OnSetStatus_Torque
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2018/3/8 - 8:36
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_Torque(__in UINT nConnect)
{
	__super::OnSetStatus_Torque(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Torque(nConnect);
}

//=============================================================================
// Method		: OnSetStatus_GrabBoard
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_GrabBoard(__in UINT nConnect, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_GrabBoard(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_GrabBoard(nConnect, nIdxBrd);
}

//=============================================================================
// Method		: OnSetStatus_VideoSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT bSignalStatus
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/9/28 - 11:16
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_VideoSignal(__in UINT bSignalStatus, __in UINT nIdxBrd /*= 0*/)
{
	__super::OnSetStatus_VideoSignal(bSignalStatus, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_VideoSignal(bSignalStatus, nIdxBrd);
}

//=============================================================================
// Method		: OnSet_BarcodeWithDialog
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/10/20 - 21:20
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSet_BarcodeWithDialog(__in LPCTSTR szBarcode)
{
	__super::OnSet_BarcodeWithDialog(szBarcode);

	if (NULL != m_pdlgBarcode)
	{
		m_pdlgBarcode->SetBarcodeType(enBarcodeType::Barcode_SN);

		m_pdlgBarcode->ShowWindow(SW_SHOW);

		if (m_pdlgBarcode->InsertBarcode(szBarcode))
		{
			m_stInspInfo.szBarcodeBuf = m_pdlgBarcode->GetBarcode();

			//m_wnd_MainView.Insert_ScanBarcode(m_stInspInfo.szBarcodeBuf);

			AddLog_F(_T("Barcode : %s"), m_stInspInfo.szBarcodeBuf);
			OnSet_Barcode(m_stInspInfo.szBarcodeBuf);
		}
	}
}

//=============================================================================
// Method		: OnAddAlarmInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2017/9/20 - 20:46
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnAddAlarmInfo(__in enResultCode nResultCode)
{
// 	ST_ErrorInfo stErrInfo;
 
// 	stErrInfo.lCode = lErrorCode;
// 	stErrInfo.nType = 0;
// 	GetLocalTime(&stErrInfo.tmTime);
// 	stErrInfo.szDesc = g_szErrorCode_H_Desc[lErrorCode];

//	m_wnd_AlarmView.InsertErrorInfo(&stErrInfo);

	__super::OnAddAlarmInfo(nResultCode);
}

//=============================================================================
// Method		: OnAddAlarm
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szAlarm
// Qualifier	:
// Last Update	: 2017/12/4 - 16:11
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnAddAlarm(__in LPCTSTR szAlarm)
{
	__super::OnAddAlarm(szAlarm);

	m_wnd_MainView.Set_Alarm(szAlarm);

	OnLog_Err(szAlarm);
}


//=============================================================================
// Method		: OnAddAlarm_F
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szAlarm
// Parameter	: ...
// Qualifier	:
// Last Update	: 2017/12/12 - 20:33
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnAddAlarm_F(__in LPCTSTR szAlarm, ...)
{
	__try
	{
		TCHAR szBuffer[4096] = { 0, };

		size_t cb = 0;
		va_list args;
		va_start(args, szAlarm);
		::StringCchVPrintfEx(szBuffer, 4096, NULL, &cb, 0, szAlarm, args);
		va_end(args);

		OnAddAlarm(szBuffer);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CView_MainCtrl_ImgT::OnAddAlarm_F()\n"));
	}
}

//=============================================================================
// Method		: OnResetAlarm
// Access		: virtual protected  
// Returns		: void
// Parameter	: void
// Qualifier	:
// Last Update	: 2017/12/10 - 15:55
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetAlarm()
{
	__super::OnResetAlarm();

	m_wnd_MainView.Reset_Alarm();
}

//=============================================================================
// Method		: OnSet_CycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/11 - 21:01
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSet_CycleTime()
{
	__super::OnSet_CycleTime();

	m_wnd_MainView.UpdateCycleTime();
}

//=============================================================================
// Method		: OnUpdate_EquipmentInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 20:51
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnUpdate_EquipmentInfo()
{
	__super::OnUpdate_EquipmentInfo();

	m_wnd_MainView.UpdateEquipmentInfo();
}

//=============================================================================
// Method		: OnUpdate_ElapTime_TestUnit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/21 - 16:05
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnUpdate_ElapTime_TestUnit(__in UINT nParaIdx /*= 0*/)
{
	m_wnd_MainView.UpdateElapTime_TestUnit(nParaIdx);
}

void CView_MainCtrl_ImgT::OnUpdate_ElapTime_Cycle()
{	
	m_wnd_MainView.UpdateElapTime_Cycle();
}

void CView_MainCtrl_ImgT::OnUpdate_TestReport(__in UINT nParaIdx /*= 0*/)
{
	m_wnd_MainView.Update_TestReport(nParaIdx);
}

//=============================================================================
// Method		: OnSet_TestProgress
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestProcess nProcess
// Qualifier	:
// Last Update	: 2016/5/29 - 16:19
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSet_TestProgress(__in enTestProcess nProcess)
{
	__super::OnSet_TestProgress(nProcess);

	m_wnd_MainView.UpdateTestProgress();
}

void CView_MainCtrl_ImgT::OnSet_TestProgress_Unit(__in enTestProcess nProcess, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_TestProgress_Unit(nProcess, nParaIdx);

	m_wnd_MainView.UpdateTestProgress_Unit(nParaIdx);
}

void CView_MainCtrl_ImgT::OnSet_TestProgressStep(__in UINT nTotalStep, __in UINT nProgStep)
{
	__super::OnSet_TestProgressStep(nTotalStep, nProgStep);

	m_wnd_MainView.SetTestProgressStep(nTotalStep, nProgStep);
}

// void CView_MainCtrl_ImgT::OnSet_TestResult(__in enTestResult nResult)
// {
// 	__super::OnSet_TestResult(nResult);
// 
// 	m_wnd_MainView.UpdateTestResult();
// }

void CView_MainCtrl_ImgT::OnSet_TestResult_Unit(__in enTestResult nResult, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_TestResult_Unit(nResult, nParaIdx);

	m_wnd_MainView.SetTestResult_Unit(nParaIdx, nResult);
}

void CView_MainCtrl_ImgT::OnSet_ResultCode_Unit(__in LRESULT nResultCode, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_ResultCode_Unit(nResultCode, nParaIdx);

	m_wnd_MainView.SetTestResultCode_Unit(nParaIdx, nResultCode);
}

void CView_MainCtrl_ImgT::OnSet_TestStepSelect(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_TestStepSelect(nStepIdx, nParaIdx);

	m_wnd_MainView.SetTestStep_Select(nStepIdx, nParaIdx);
}

void CView_MainCtrl_ImgT::OnSet_TestStepResult(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_TestStepResult(nStepIdx, nParaIdx);
	
	m_wnd_MainView.SetTestStep_Result(nStepIdx, (enTestResult)m_stInspInfo.GetTestItemMeas(nStepIdx, nParaIdx)->nJudgmentAll, nParaIdx);
	//m_wnd_MainView.SetTestStep_Result(nStepIdx, (enTestResult)m_stInspInfo.CamInfo[nParaIdx].TestInfo.TestMeasList[nStepIdx].nJudgmentAll, nParaIdx);
}

void CView_MainCtrl_ImgT::OnSet_InputTime()
{
	__super::OnSet_InputTime();

	m_wnd_MainView.UpdateInputTime();
}

void CView_MainCtrl_ImgT::OnSet_InputTime__Unit(__in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_InputTime__Unit(nParaIdx);

	m_wnd_MainView.UpdateInputTime();
}

void CView_MainCtrl_ImgT::OnSet_BeginTestTime(__in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_BeginTestTime(nParaIdx);

}

void CView_MainCtrl_ImgT::OnSet_OutputTime()
{
	__super::OnSet_OutputTime();

}

void CView_MainCtrl_ImgT::OnSet_GraphSFR(__in double dbCurrentSFR, __in double dbMaxSFR)
{
	m_wnd_MainView.UpdateGraphData(dbCurrentSFR, dbMaxSFR);
}

void CView_MainCtrl_ImgT::OnSet_GraphReset()
{
	m_wnd_MainView.UpdateGraphReset();
}

//=============================================================================
// Method		: OnSet_Barcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nRetryCnt
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/3 - 19:58
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSet_Barcode(__in LPCTSTR szBarcode, __in UINT nRetryCnt /*= 0*/, __in UINT nParaIdx /*= 0*/)
{
	__super::OnSet_Barcode(szBarcode, nRetryCnt, nParaIdx);

	m_wnd_MainView.SetBarcode(szBarcode, nRetryCnt, nParaIdx);
}

// void CView_MainCtrl_ImgT::OnSet_VCSEL_Status(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
// {
// 	__super::OnSet_VCSEL_Status(bOn, nParaIdx);
// }

void CView_MainCtrl_ImgT::OnDIO_UpdateDInSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_wnd_IOView.Set_IO_DI_OffsetData(byBitOffset, bOnOff);
}

void CView_MainCtrl_ImgT::OnDIO_UpdateDOutSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_wnd_IOView.Set_IO_DO_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: DisplayVideo
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in LPBYTE lpbyRGB
// Parameter	: __in DWORD dwRGBSize
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Qualifier	:
// Last Update	: 2018/2/1 - 13:15
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::DisplayVideo(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight)
{
	IplImage *Testimage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);

	memcpy(Testimage->imageData, lpbyRGB, Testimage->imageSize);

	switch (m_nWndIndex)
	{
	case SUBVIEW_AUTO:
		DisplayVideo_Overlay(m_stInspInfo.nTestPara, m_stInspInfo.RecipeInfo.nOverlayItem, Testimage);
		m_wnd_MainView.ShowVideo(nChIdx, (LPBYTE)Testimage->imageData, nWidth, nHeight);
		break;

	case SUBVIEW_MAINTENANCE:
		m_wnd_MaintenanceView.ShowVideo(nChIdx, (LPBYTE)Testimage->imageData, nWidth, nHeight);
		break;

	case SUBVIEW_RECIPE:
		if (m_stImageMode.eImageMode == ImageMode_LiveCam)
		{
			m_wnd_RecipeView.ShowVideo_Overlay_ImgT(m_stInspInfo.nTestPara, Testimage, nWidth, nHeight);
		}
		break;

	default:
		break;
	}

	if (m_bPicCaptureMode)
	{
		CString strFile;
		strFile.Format(_T("%s_Pic.png"), m_stInspInfo.CamInfo[m_stInspInfo.nTestPara].stImageQ.szFileName);
		cvSaveImage(CT2A(strFile), Testimage);
		m_bPicCaptureMode = FALSE;
	}

	cvReleaseImage(&Testimage);
}

//=============================================================================
// Method		: DisplayVideo_LastImage
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2018/3/18 - 13:01
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::DisplayVideo_LastImage(__in UINT nChIdx)
{

}

//=============================================================================
// Method		: DisplayVideo_NoSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2018/3/18 - 13:02
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::DisplayVideo_NoSignal(__in UINT nChIdx)
{
	for (UINT nIdx = 0; nIdx < Para_MaxEnum; nIdx++)
	{
		m_wnd_MainView.NoSignal_Ch(nIdx);
		m_wnd_RecipeView.NoSignal_Ch(nIdx);
		m_wnd_MaintenanceView.NoSignal_Ch(nIdx);
	}
}

//=============================================================================
// Method		: DisplayVideo_Overlay
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in enOverlayItem enItem
// Parameter	: __inout IplImage * TestImage
// Qualifier	:
// Last Update	: 2018/2/23 - 10:08
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::DisplayVideo_Overlay(__in UINT nChIdx, __in enOverlayItem enItem, __inout IplImage *TestImage)
{
	if (NULL == TestImage)
		return;

	m_OverlayProc.SetTestMode(TRUE);

	switch (enItem)
	{
	case Ovr_OpticalCenter:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stOpticalCenter, m_stInspInfo.CamInfo[nChIdx].stImageQ.stOpticalCenter);
		break;
	case Ovr_Current:
		m_OverlayProc.Overlay_Current(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stCurrent, m_stInspInfo.CamInfo[nChIdx].stImageQ.stCurrent);
		break;
	case Ovr_SFR:
		m_OverlayProc.Overlay_SFR(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stSFR, m_stInspInfo.CamInfo[nChIdx].stImageQ.stSFR);
		break;
	case Ovr_Rotate:
		m_OverlayProc.Overlay_Rotation(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stRotation, m_stInspInfo.CamInfo[nChIdx].stImageQ.stRotate);
		break;
	case Ovr_Ymean:
		m_OverlayProc.Overlay_Ymean(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stYmean, m_stInspInfo.CamInfo[nChIdx].stImageQ.stYmean);
		break;
	case Ovr_BlackSpot:
		m_OverlayProc.Overlay_BlackSpot(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stBlackSpot, m_stInspInfo.CamInfo[nChIdx].stImageQ.stBlackSpot);
		break;
	case Ovr_LCB:
		m_OverlayProc.Overlay_LCB(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stLCB, m_stInspInfo.CamInfo[nChIdx].stImageQ.stLCB);
		break;
	case Ovr_Defect_Black:
		m_OverlayProc.Overlay_Defect_Black(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stDefect_Black, m_stInspInfo.CamInfo[nChIdx].stImageQ.stDefect_Black);
		break;
	case Ovr_Defect_White:
		m_OverlayProc.Overlay_Defect_White(TestImage, m_stInspInfo.RecipeInfo.stImageQ.stDefect_White, m_stInspInfo.CamInfo[nChIdx].stImageQ.stDefect_White);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnImage_AddHistory
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in LPCTSTR szTitle
// Qualifier	:
// Last Update	: 2018/2/23 - 9:58
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnImage_AddHistory(__in UINT nChIdx, __in LPCTSTR szTitle)
{
	if (nullptr != m_stImageBuf[nChIdx].lpbyImage_8bit)
	{
		m_wnd_MainView.Add_ImageHistory(nChIdx, szTitle, m_stImageBuf[nChIdx].lpbyImage_8bit, m_stImageBuf[nChIdx].dwWidth, m_stImageBuf[nChIdx].dwHeight);
	}
	
	//m_wnd_MainView.Add_ImageHistory_Index();
	//m_stImageBuf.lpwImage_16bit
}

//=============================================================================
// Method		: OnImage_SetHistory
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in UINT nHistoryIndex
// Parameter	: __in LPCTSTR szTitle
// Qualifier	:
// Last Update	: 2018/2/23 - 17:48
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnImage_SetHistory(__in UINT nChIdx, __in UINT nHistoryIndex, __in LPCTSTR szTitle)
{
	if (nullptr != m_stImageBuf[nChIdx].lpbyImage_8bit)
	{
		m_wnd_MainView.Set_ImageHistory(nChIdx, szTitle, nHistoryIndex, m_stImageBuf[nChIdx].lpbyImage_8bit, m_stImageBuf[nChIdx].dwWidth, m_stImageBuf[nChIdx].dwHeight);
	}
}

//=============================================================================
// Method		: OnHidePopupUI
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/22 - 23:03
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnHidePopupUI()
{
	__super::OnHidePopupUI();

}

//=============================================================================
// Method		: OnReset_CamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:34
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	__super::OnReset_CamInfo(nParaIdx);

	//m_wnd_MainView.ResetInfo_Loading(nParaIdx);
}

//=============================================================================
// Method		: OnReset_CamInfo_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/18 - 13:02
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnReset_CamInfo_All()
{
	__super::OnReset_CamInfo_All();
}

//=============================================================================
// Method		: OnResetInfo_Loading
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 22:53
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetInfo_Loading()
{
	__super::OnResetInfo_Loading();

	m_wnd_MainView.ResetInfo_Loading();
}

//=============================================================================
// Method		: OnResetInfo_StartTest
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 22:35
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetInfo_StartTest(__in UINT nParaIdx /*= 0*/)
{
	__super::OnResetInfo_StartTest(nParaIdx);

	m_wnd_MainView.ResetInfo_StartTest(nParaIdx);	
}

//=============================================================================
// Method		: OnResetInfo_Unloading
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 16:41
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetInfo_Unloading()
{
	__super::OnResetInfo_Unloading();

	m_wnd_MainView.ResetInfo_Unloading();
}

//=============================================================================
// Method		: OnResetInfo_Measurment
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/16 - 11:56
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetInfo_Measurment(__in UINT nParaIdx /*= 0*/)
{
	__super::OnResetInfo_Measurment(nParaIdx);

	m_wnd_MainView.ResetInfo_Measurment(nParaIdx);
}

//=============================================================================
// Method		: OnInsertWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:13
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnInsertWorklist()
{
	//m_wnd_MaintenanceView.InsertWorklist(&m_stInspInfo.WorklistInfo);
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:14
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSaveWorklist()
{
	__super::OnSaveWorklist();

	// 파일 저장
	//OnMES_FinalResult();
}

//=============================================================================
// Method		: OnLoadWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/11 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnLoadWorklist()
{

}

//=============================================================================
// Method		: OnUpdateYield
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/12 - 15:00
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnUpdateYield(__in UINT nParaIdx /*= 0*/)
{
	__super::OnUpdateYield();

	// UI 갱신
	m_wnd_MainView.UpdateYield();
}

//=============================================================================
// Method		: OnLoadYield
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 14:28
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnLoadYield()
{
	__super::OnLoadYield();

	m_wnd_MainView.UpdateYield();
}

//=============================================================================
// Method		: OnSetStatus_ConsumablesInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/14 - 10:02
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetStatus_ConsumInfo()
{
	m_wnd_MainView.UpdatePogoCount();
}

void CView_MainCtrl_ImgT::OnSetStatus_ConsumInfo(__in UINT nItemIdx)
{
	m_wnd_MainView.UpdatePogoCount(nItemIdx);
}

//=============================================================================
// Method		: OnResetYieldCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/21 - 11:07
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnResetYieldCycleTime()
{
	m_stInspInfo.YieldInfo.Reset();
	m_stInspInfo.CycleTime.Reset();

	m_wnd_MainView.UpdateYield();
	m_wnd_MainView.UpdateCycleTime();
}

//=============================================================================
// Method		: LoadRecipeInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szRecipe
// Parameter	: BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2016/5/28 - 14:50
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::LoadRecipeInfo(__in LPCTSTR szRecipe, BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szRecipe, szRecipe, RECIPE_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Model..."));
	}

	// 모델 변경
	m_stInspInfo.RecipeInfo.szRecipeFile = szRecipe;
	m_stInspInfo.RecipeInfo.szRecipeFullPath = strFullPath;

	// 파일 불러오기
	CFile_Recipe		fileRecipe;
	fileRecipe.SetSystemType(m_InspectionType);

 	if (fileRecipe.Load_RecipeFile(strFullPath, m_stInspInfo.RecipeInfo))
 	{
		// 선택한 모델 레지스트리에 저장
		//m_regInspInfo.SaveSelectedModel(m_stInspInfo.RecipeInfo.szRecipeFile, m_stInspInfo.RecipeInfo.szModelCode);
		OnSave_SelectedRecipe();	// 2018.8.21 레시피별 폴더 구분

		// 스텝정보, 검사 항목 스펙 데이터 갱신
		m_stInspInfo.UpdateTestInfo();

		// DAQ 설정
		OnDAQ_SetOption(m_stInspInfo.RecipeInfo.ModelType);

		// Motion 설정
		OnMotion_SetOption_Model(m_stInspInfo.RecipeInfo.ModelType);

		// UI 갱신
		m_wnd_MainView.UpdateRecipeInfo();

		// 카메라 검사 데이터 초기화
		OnReset_CamInfo_All();
		m_tm_Test.Func_CameraModel(m_stInspInfo.RecipeInfo.ModelType);

		// 레시피 설정 윈도우로 모델 변경 알림 (프로그램 시작시 사용)
		if (bNotifyModelWnd)
			m_wnd_RecipeView.SetRecipeFile(m_stInspInfo.RecipeInfo.szRecipeFile);		

		// 포고 카운트 설정
		Load_ConsumInfo();

		m_OverlayProc.SetModelType(m_stInspInfo.RecipeInfo.ModelType);

		// 모델 정보 불러오기 완료
		strLog.Format(_T("Recipe File load completed. [File: %s]"), m_stInspInfo.RecipeInfo.szRecipeFile);
		AddLog(strLog);
 	}
 	else
 	{
		if (!bNotifyModelWnd)
		{
			ShowSplashScreen(FALSE);
		}

 		strLog.Format(_T("Cannot load the Model File. [File: %s.luri]"), szRecipe);
 		AddLog(strLog);
		strLog.Format(_T("Cannot load the Model File.\r\nFile: %s"), strFullPath);
 		AfxMessageBox(strLog, MB_SYSTEMMODAL);
 		return FALSE;
 	}

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen(FALSE);
	}

	for (int t = 0; t < 2; t++)
	{
		m_wnd_MainView.m_wnd_TestResult_ImgT[t].SetClearTab();
	}

	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;

	// * 설정된 스텝 진행
	int nCNT = 0;

	for (UINT nCh = 0; nCh < USE_CHANNEL_CNT; nCh++)
	{
		nCNT = 0;

		for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
		{
			m_wnd_MainView.m_wnd_TestResult_ImgT[nCh].SetAddTab(m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nTestItem, nCNT);
		}

		m_wnd_MainView.m_wnd_TestResult_ImgT[nCh].SelectItem(0);
	}

	//----------------------------
	// LOG Model Info
	//----------------------------
	// Model
	// Modle Code
	//----------------------------
	

	return TRUE;
}

//=============================================================================
// Method		: InitLoadRecipeInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/11 - 15:09
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitLoadRecipeInfo()
{
	CString strModelFile;
	CString strModelCode;
	//if (m_regInspInfo.LoadSelectedModel(strModelFile, strModelCode))
	if (OnLoad_SelectedRecipe(strModelFile, strModelCode)) // 2018.8.21 레시피별 폴더 구분
	{
		m_stInspInfo.RecipeInfo.szRecipeFile = strModelFile;
		LoadRecipeInfo(strModelFile);
	}
	else
	{
		AfxMessageBox(_T("Please set the model data."), MB_SYSTEMMODAL);
	}
}


//=============================================================================
// Method		: LoadMotorInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMotor
// Parameter	: __in BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2017/10/2 - 14:04
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::LoadMotorInfo(__in LPCTSTR szMotor, __in BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szMotor, szMotor, MOTOR_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Motor..."));
	}

	// 모터 변경
	m_stInspInfo.RecipeInfo.szMotorFile = szMotor;

#ifndef MOTION_NOT_USE
	m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor, m_stInspInfo.RecipeInfo.szMotorFile);

	// 파일 불러오기
	if (m_Device.MotionManager.LoadMotionInfo())
	{
		// 선택한 모델 레지스트리에 저장
		m_regInspInfo.SaveSelectedMotor(m_stInspInfo.RecipeInfo.szMotorFile);

		// UI 갱신
		m_wnd_MaintenanceView.UpdateMotorInfo(szMotor);

		// 모델 정보 불러오기 완료
		strLog.Format(_T("Motor File load completed. [File: %s]"), m_stInspInfo.RecipeInfo.szMotorFile);
		AddLog(strLog);
	}
	else
	{
		if (!bNotifyModelWnd)
		{
			ShowSplashScreen(FALSE);
		}

		strLog.Format(_T("Cannot load the Motor File. [File: %s.luri]"), szMotor);
		AddLog(strLog);
		strLog.Format(_T("Cannot load the Motor File.\r\nFile: %s"), strFullPath);
		AfxMessageBox(strLog, MB_SYSTEMMODAL);
		return FALSE;
	}
#endif

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen(FALSE);
	}

	//----------------------------
	// LOG Model Info
	//----------------------------
	// Model
	// Modle Code
	//----------------------------


	return TRUE;
}


//=============================================================================
// Method		: InitLoaMotorInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/2 - 14:04
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitLoadMotorInfo()
{
	CString strMotorFile;

	if (m_regInspInfo.LoadSelectedMotor(strMotorFile))
	{
		m_stInspInfo.RecipeInfo.szMotorFile = strMotorFile;
		LoadMotorInfo(strMotorFile);
	}
	else
	{
		AfxMessageBox(_T("Please set the motor data."), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: LoadMaintenanceInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMaintenance
// Parameter	: __in BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2017/9/29 - 17:04
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::LoadMaintenanceInfo(__in LPCTSTR szMaintenance, __in BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기
 	CString strFullPath;
 	CString strRomFullPath;
 	CString strLog;
 
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szMaintenance, szMaintenance, MAINTENANCE_FILE_EXT);
 
 	if (!bNotifyModelWnd)
 	{
 		ShowSplashScreen();
 		m_wndSplash.SetText(_T("Changing Maintenance..."));
 	}
 
 	// 유지 변경
 	m_stInspInfo.MaintenanceInfo.szMaintenanceFile = szMaintenance;
	m_stInspInfo.MaintenanceInfo.szMaintenanceFullPath = strFullPath;
 
 	// 파일 불러오기
	CFile_Maintenance m_fileMaintenance;
	if (m_fileMaintenance.LoadMaintenanceFile(strFullPath, m_stInspInfo.MaintenanceInfo))
 	{
		OnLightPSU_PowerOnOff(ON);

 		// 선택한 유지 레지스트리에 저장
 		m_regInspInfo.SaveSelectMaintenance(m_stInspInfo.MaintenanceInfo.szMaintenanceFile);
 
		// UI 갱신
		m_wnd_MaintenanceView.UpdateMaintenanceInfo(szMaintenance);

 		// 유지 정보 불러오기 완료
 		strLog.Format(_T("Maintenance File load completed. [File: %s]"), m_stInspInfo.MaintenanceInfo.szMaintenanceFile);
 		AddLog(strLog);
 	}
 	else
 	{
 		if (!bNotifyModelWnd)
 		{
 			ShowSplashScreen(FALSE);
 		}
 
 		strLog.Format(_T("Cannot load the Maintenance File. [File: %s.luri]"), szMaintenance);
 		AddLog(strLog);
 		strLog.Format(_T("Cannot load the Maintenance File.\r\nFile: %s"), strFullPath);
 		AfxMessageBox(strLog, MB_SYSTEMMODAL);
 		return FALSE;
 	}
 
 	if (!bNotifyModelWnd)
 	{
 		ShowSplashScreen(FALSE);
 	}

	return TRUE;
}

//=============================================================================
// Method		: InitLoadMaintenanceInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/29 - 16:50
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitLoadMaintenanceInfo()
{
	CString szMaintenanceFile;

	if (m_regInspInfo.LoadSelectMaintenance(szMaintenanceFile))
	{
		m_stInspInfo.MaintenanceInfo.szMaintenanceFile = szMaintenanceFile;
		LoadMaintenanceInfo(szMaintenanceFile);
	}
	else
	{
		AfxMessageBox(_T("Please set the Maintenance data."), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: Manual_DeviceControl
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in UINT nBnIdx
// Qualifier	:
// Last Update	: 2017/1/22 - 13:36
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::Manual_DeviceControl(__in UINT nChIdx, __in UINT nBnIdx)
{
	
}

//=============================================================================
// Method		: OnSetOverlayInfo_ImgT
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT enTestItem
// Parameter	: __in UINT nParaItem
// Qualifier	:
// Last Update	: 2018/3/18 - 13:02
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetOverlayInfo_ImgT(__in UINT enTestItem, __in UINT nParaItem /*= 0*/)
{
	m_stInspInfo.nTestPara = nParaItem;

	switch (enTestItem)
	{

	case TI_ImgT_Fn_ECurrent:
	case TI_ImgT_Re_ECurrent:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Current;
		break;
	case TI_ImgT_Fn_OpticalCenter:
	case TI_ImgT_Re_OpticalCenterX:
	case TI_ImgT_Re_OpticalCenterY:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_OpticalCenter;
		break;
	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Re_SFR:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_SFR;
		break;
	case TI_ImgT_Fn_Rotation:
	case TI_ImgT_Re_Rotation:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Rotate;
		break;
	case TI_ImgT_Fn_Ymean:
	case TI_ImgT_Re_Ymean:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Ymean;
		break;
	case TI_ImgT_Fn_BlackSpot:
	case TI_ImgT_Re_BlackSpot:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_BlackSpot;
		break;
	case TI_ImgT_Fn_LCB:
	case TI_ImgT_Re_LCB:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_LCB;
		break;
	case TI_ImgT_Fn_Defect_Black:
	case TI_ImgT_Re_Defect_Black:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Defect_Black;
		break;
	case TI_ImgT_Fn_Defect_White:
	case TI_ImgT_Re_Defect_White:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Defect_White;
		break;
	case TI_ImgT_Fn_FOV:
	case TI_ImgT_Re_FOV_Dia:
	case TI_ImgT_Re_FOV_Hor:
	case TI_ImgT_Re_FOV_Ver:
		 m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_FOV;
		 break;
	case TI_ImgT_Fn_Distortion:
	case TI_ImgT_Re_Distortion:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Distortion;
		break;
	case TI_ImgT_Fn_DynamicBW:
	case TI_ImgT_Re_DynamicRange:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_DynamicBW;
		break;
	case TI_ImgT_Fn_Shading:
	case TI_ImgT_Re_Shading:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_Shading;
		break;
	default:
		m_stInspInfo.RecipeInfo.nOverlayItem = Ovr_MaxEnum;
		break;
	}

	m_wnd_RecipeView.SetPtr_OverInfo(m_stInspInfo.RecipeInfo.nOverlayItem);
}

//=============================================================================
// Method		: OnSetCamerParaSelect
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/16 - 16:22
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnSetCamerParaSelect(__in UINT nParaIdx /*= 0*/)
{
	CView_MainCtrl::OnSetCamerParaSelect(nParaIdx);
	//m_wnd_MainView.Set_CameraSelect(nParaIdx);
}

//=============================================================================
// Method		: SetSystemType
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:53
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetSystemType(__in enInsptrSysType nSysType)
{
	__super::SetSystemType(nSysType);

	m_wnd_MainView.SetSystemType(nSysType);
	m_wnd_RecipeView.SetSystemType(nSysType);
	m_wnd_IOView.SetSystemType(nSysType);
	m_wnd_MaintenanceView.SetSystemType(nSysType);

#ifndef MOTION_NOT_USE
	m_MotionSequence.SetSystemType(nSysType);
#endif
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::AddLog
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR lpszLog
// Parameter	: BOOL bError
// Parameter	: UINT nLogType
// Parameter	: BOOL bOnlyLogType
// Qualifier	:
// Last Update	: 2013/1/16 - 15:39
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::AddLog(LPCTSTR lpszLog, BOOL bError /*= FALSE*/, UINT nLogType /*= LOGTYPE_NORMAL*/, BOOL bOnlyLogType /*= FALSE*/)
{
	if (!GetSafeHwnd())
		return;

	if (NULL == lpszLog)
		return;

	__try
	{
		TCHAR		strTime[255] = { 0 };
		UINT_PTR	nLogSize = _tcslen(lpszLog) + 255;
		LPTSTR		lpszOutLog = new TCHAR[nLogSize];
		SYSTEMTIME	LocalTime;

		// **** 시간 추가 ****
		GetLocalTime(&LocalTime);
		StringCbPrintf(strTime, sizeof(strTime), _T("[%02d:%02d:%02d.%03d] "), LocalTime.wHour, LocalTime.wMinute, LocalTime.wSecond, LocalTime.wMilliseconds);

		// 파일 처리 ------------------------------------------------
		StringCbPrintf(lpszOutLog, nLogSize, _T("%s%s \r\n"), strTime, lpszLog);

		if (bError)
			m_Log_ErrLog.LogWrite(lpszOutLog);

		// UI 처리 --------------------------------------------------
		m_wnd_LogView.AddLog(lpszOutLog, bError, nLogType, RGB(0, 0, 0));
		m_logFile.LogWrite(lpszOutLog);

		delete[] lpszOutLog;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CView_MainCtrl_ImgT::AddLog () \n"));
	}
}


//=============================================================================
// Method		: CView_MainCtrl_ImgT::SwitchWindow
// Access		: public 
// Returns		: UINT
// Parameter	: UINT nIndex
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		: 자식 윈도우 전환하는 함수
// MainView에서 선택된 검사기 번호를 다른 윈도우로 넘긴다.
//=============================================================================
UINT CView_MainCtrl_ImgT::SwitchWindow(UINT nIndex)
{
	return CView_MainCtrl::SwitchWindow(nIndex);
	
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::SetCommPanePtr
// Access		: public 
// Returns		: void
// Parameter	: CWnd * pwndCommPane
// Qualifier	:
// Last Update	: 2013/7/16 - 16:51
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetCommPanePtr(CWnd* pwndCommPane)
{
	CView_MainCtrl::SetCommPanePtr(pwndCommPane);
}

//=============================================================================
// Method		: ReloadOption
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2014/8/3 - 22:27
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::ReloadOption()
{
	stLT_Option tempOpt = m_stOption;

	OnLoadOption();

	BOOL bChanged = FALSE;

	// MES 주소 변경
	//m_stOption.MES.Address.dwAddress;
	//m_stOption.MES.Address.dwPort;
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::InitStartProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2014/7/5 - 10:49
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::InitStartProgress()
{
	//CView_MainCtrl::InitStartProgress();

	ShowSplashScreen();

	m_wndSplash.SetText(_T("Connecting Devices"));

	// 주변 장치 연결
	__try
	{
		ConnectDevicez();
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : InitStartProgress ()"));
	}

	//m_wndSplash.SetText(_T("Loading Recipe.."));
	m_wndSplash.SetText(_T("Loading existing inspection information ..."));

	// 기본 : Online 모드
	//SetMESOnlineMode(enMES_Online::MES_Offline);
	SetMESOnlineMode(enMES_Online::MES_Online);

	// 모델 정보 로드
	InitLoadRecipeInfo();
	InitLoadMaintenanceInfo();
	InitLoadMotorInfo();
	OnLightBrd_Volt_PowerOn(12);

	Sleep(500);

	// 검사 가능 상태로 변경
	m_bFlag_ReadyTest = TRUE;

	ShowSplashScreen(FALSE);

	// KHO KHO KHO 
#ifndef USE_TEST_MODE	

	if (TRUE == InitStartDeviceProgress())
	{
		m_bFlag_ReadyTest = TRUE;
	}
	else
	{
		m_bFlag_ReadyTest = FALSE;
	}

#endif

}

//=============================================================================
// Method		: InitStartDeviceProgress
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/12 - 22:11
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::InitStartDeviceProgress()
{
	m_Device.DigitalIOCtrl.Start_Monitoring();

	Sleep(200);

	if (FALSE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_00_MainPower))
	{
		AfxMessageBox(_T("[ERR] MAIN POWER OFF"));
		return FALSE;
	}

	if (FALSE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_01_EMO))
	{
		AfxMessageBox(_T("[ERR] EMO STATE Check"));
		return FALSE;
	}

	OnDOut_FluorescentLamp(OFF);

#ifndef MOTION_NOT_USE	
 	// 모터 원점
 	if (m_Device.MotionManager.m_AllMotorData.pMotionParam != NULL)
 	{
 		if (FALSE == MotorOrigin())
 		{
			OnDOut_StartLamp(FALSE);
			OnDOut_StopLamp(FALSE);
			OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
 			return FALSE;
 		}
 	}
#endif

	OnDOut_StartLamp(TRUE);
	OnDOut_StopLamp(FALSE);
	OnDOut_TowerLamp(enLampColor::Lamp_Red, FALSE);
	OnDOut_TowerLamp(enLampColor::Lamp_Yellow, FALSE);
	OnDOut_TowerLamp(enLampColor::Lamp_Green, TRUE);

	StartThread_Monitoring();

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::FinalExitProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2016/06/13
// Desc.		: 프로그램 종료시 처리해야 할 코드들..
//=============================================================================
void CView_MainCtrl_ImgT::FinalExitProgress()
{
	// 검사 불가 상태로 변경
	m_bFlag_ReadyTest = FALSE;

	TRACE(_T("Set Exit Program External Event\n"));
	m_bExitFlag = TRUE;

	if (FALSE == SetEvent(m_hEvent_ProgramExit))
	{
		TRACE(_T("Set Exit Program External Event 실패!!\n"));
	}

	OnLightBrd_PowerOff();
	OnLightPSU_PowerOnOff(OFF);

	// 보드 전원
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].Grabber_Cnt; nIdx++)
	{
		OnDOut_BoardPower(FALSE, nIdx);
	}

	OnDOut_StartLamp(FALSE);
	OnDOut_StopLamp(FALSE);
	OnDOut_TowerLamp(enLampColor::Lamp_All, FALSE);
	OnDOut_FluorescentLamp(OFF);

	OnShowSplashScreen(TRUE, _T("Quiting program"));

	// 주변 장치 연결 해제
	DisconnectDevicez();

	// 종료
	OnShowSplashScreen(TRUE, _T("-- Quit --"));

	Sleep(300);
	ShowSplashScreen(FALSE);
	TRACE(_T("- End ExitProgramCtrl -\n"));
}

//=============================================================================
// Method		: ManualBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/10 - 13:28
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::ManualBarcode()
{
	CDlg_Barcode	dlgBarcode;

	dlgBarcode.SetBarcodeType(enBarcodeType::Barcode_SN);

	if (IDOK == dlgBarcode.DoModal())
	{
		// 테스트 관련 UI 초기화
		OnResetInfo_Loading();

		m_stInspInfo.szBarcodeBuf = dlgBarcode.GetBarcode();

		//m_wnd_MainView.Insert_ScanBarcode(m_stInspInfo.szBarcodeBuf);

		AddLog_F(_T("Manual Barcode : %s"), m_stInspInfo.szBarcodeBuf);

		OnSet_Barcode(m_stInspInfo.szBarcodeBuf, 0);
	}

	//((CPane_CommStatus*)m_pwndCommPane)->Set_Barcode(szBarcode);
}

//=============================================================================
// Method		: MotorOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/11/7 - 21:55
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::MotorOrigin()
{
	BOOL  bReslut = FALSE;

#ifndef MOTION_NOT_USE
	CWnd_Origin*	pWnd_Origin;
	pWnd_Origin = new CWnd_Origin;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);

	pWnd_Origin->SetInspectorType(m_InspectionType);
	pWnd_Origin->SetOwner(this);
	pWnd_Origin->SetPtr_Device(&m_Device.MotionManager, &m_Device.DigitalIOCtrl);
	pWnd_Origin->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	pWnd_Origin->EnableWindow(TRUE);
	pWnd_Origin->CenterWindow();	

	if (pWnd_Origin->DoModal() == TRUE)
		bReslut = TRUE;
	else
		bReslut = FALSE;

	ShowSplashScreen(FALSE);

	delete pWnd_Origin;
#endif

	return bReslut;
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/12/16 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	__super::SetPermissionMode(nAcessMode);
	m_wnd_MainView.SetPermissionMode(nAcessMode);
}

//=============================================================================
// Method		: SetMESOnlineMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enMES_Online nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/1 - 10:31
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetMESOnlineMode(__in enMES_Online nOnlineMode)
{
	__super::SetMESOnlineMode(nOnlineMode);
	OnSetStatus_MES_Online(nOnlineMode);
}
//=============================================================================
// Method		: SetMESOnlineMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enMES_Online nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/1 - 10:31
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetMoterMoniter(__in CString szDistance,__in CString szChartTX, __in CString szChartTZ)
{
	m_wnd_MainView.SetMoterMoniter(szDistance, szChartTX, szChartTZ);
}

//=============================================================================
// Method		: ChangeMESOnlineMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enMES_Online nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/1 - 10:31
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::ChangeMESOnlineMode(__in enMES_Online nOnlineMode)
{
	if (m_stInspInfo.MESOnlineMode != nOnlineMode)
	{
		if (nOnlineMode == MES_Offline)
		{
			// 통신을 끊는다.
			ConnectMES(FALSE);
		}
		else
		{
			// 통신을 연결한다.
			ConnectMES(TRUE);
		}
	}
}

//=============================================================================
// Method		: SetOperateMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/17 - 10:36
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::SetOperateMode(__in enOperateMode nOperMode)
{
	if (FALSE == IsTesting())
	{
		CTestManager_EQP::SetOperateMode(nOperMode);

		((CPane_CommStatus*)m_pwndCommPane)->SetStatus_OperateMode(nOperMode);

		m_wnd_MainView.SetOperateMode(nOperMode);
	}
	else
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Set Operate Mode Error : Inspection is in progress."));
		AfxMessageBox(_T("Inspection is in progress. \r\n\r\nPlease wait until the Inspection is finished."), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: OnManual_OneItemTest
// Access		: public  
// Returns		: void
// Parameter	: UINT nStepIdx
// Parameter	: UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/25 - 11:17
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnManual_OneItemTest(UINT nStepIdx, UINT nParaIdx)
{
	if (m_wnd_RecipeView.IsWindowVisible())
	{
		m_wnd_RecipeView.OnChangeOptionPic(1, 0);
	}
	StartOperation_Manual(nStepIdx, nParaIdx);
}

//=============================================================================
// Method		: EquipmentInit
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nCondition
// Qualifier	:
// Last Update	: 2018/3/11 - 22:13
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::EquipmentInit(__in UINT nCondition /*= 0*/)
{
	if (IsTesting())
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Inspection is in progress."));
		AfxMessageBox(_T("Inspection is in progress. \r\n\r\nPlease wait until the Inspection is finished."), MB_SYSTEMMODAL);
		return; //RC_AlreadyTesting;
	}

	// 확인
	if (IDYES == AfxMessageBox(_T("Are you sure you want to Initialize?"), MB_YESNO))
	{
		OnShowSplashScreen(TRUE, _T("Equipment Initialize"));

		// 그래버 영상 캡쳐 Off, 보드 전원 Off
		for (UINT nParaIdx = 0; nParaIdx < g_InspectorTable[m_InspectionType].Grabber_Cnt; nParaIdx++)
		{
			// * Capture Off
			OnDAQ_CaptureStop(nParaIdx);

			// * Camera Power Off
			OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off, nParaIdx);
		}

		ShowSplashScreen(FALSE);

		if (TRUE == InitStartDeviceProgress())
		{
			m_bFlag_ReadyTest = TRUE;
			m_stInspInfo.bForcedStop = FALSE;
		}
		else
		{
			m_bFlag_ReadyTest = FALSE;
		}
	}
}

//=============================================================================
// Method		: OnChangeTheCamera
// Access		: virtual public  
// Returns		: void
// Parameter	: BOOL bMode
// Parameter	: UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/16 - 16:22
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnChangeTheCamera(BOOL bMode, UINT nParaIdx)
{
	m_wnd_MainView.Foc_ChangeCameraMode(bMode, nParaIdx);
}

//=============================================================================
// Method		: OnChangeTheCameraStop
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/16 - 16:22
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::OnChangeTheCameraStop()
{
	m_wnd_MainView.Foc_ChangeCameraStop();
}

//=============================================================================
// Method		: OnFoc_ChangeCamFlag
// Access		: virtual public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/16 - 16:23
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl_ImgT::OnFoc_ChangeCamFlag()
{
	return m_wnd_MainView.Foc_ChangeCamFlag();
}

//=============================================================================
// Method		: CView_MainCtrl_ImgT::Test_Process
// Access		: public 
// Returns		: void
// Parameter	: UINT nTestNo
// Qualifier	:
// Last Update	: 2014/7/10 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl_ImgT::Test_Process( UINT nTestNo )
{
	switch (nTestNo)
	{
	case 0:
	{
			  _TI_Cm_Initialize(0, 0);
	}
		break;

	case 1:
	{
// 			  int iImageW = 0;
// 			  int iImageH = 0;
// 
// 			  if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, 0, iImageW, iImageH, m_stInspInfo.CamInfo[0].stIRImage.szFileName, TRUE, _T("Distortion")))
// 			  {
// 				  //return lReturn;
// 			  }
	}
		break;

	case 2:
	{
			  UINT iImageW = 0;
			  UINT iImageH = 0;

			  if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, 0, iImageW, iImageH, m_stInspInfo.CamInfo[0].stIRImage.szFileName, TRUE, _T("Distortion")))
			  {
				  //return lReturn;
			  }
	}
		break;

	case 3:
	{
	}
		break;

	case 4:
	{
	}
		break;

	case 5:
	{
	}
		break;

	case 6:
	{

	}
		break;

	case 7:
	{

	}
		break;
	case 8:
	{

	}
		break;
	case 9:
	{

	}
		break;
	case 10:
	{
		
	}
		break;
	default:
		break;
	}
}


