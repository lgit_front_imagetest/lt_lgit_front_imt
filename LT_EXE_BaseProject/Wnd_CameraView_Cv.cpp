// Wnd_CameraView_Cv.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CameraView_Cv.h"


// CWnd_CameraView_Cv

IMPLEMENT_DYNAMIC(CWnd_CameraView_Cv, CWnd_BaseView)

CWnd_CameraView_Cv::CWnd_CameraView_Cv()
{
	//-영상
	m_pBitmapInfo = (BITMAPINFO *) new BYTE[sizeof(BITMAPINFO)+255 * sizeof(RGBQUAD)];
	m_pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_pBitmapInfo->bmiHeader.biPlanes = 1;
	m_pBitmapInfo->bmiHeader.biBitCount = 8;
	m_pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	m_pBitmapInfo->bmiHeader.biSizeImage = 0;
	m_pBitmapInfo->bmiHeader.biXPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biClrUsed = 0;
	m_pBitmapInfo->bmiHeader.biClrImportant = 0;
	for (int i = 0; i < 256; i++)
	{
		m_pBitmapInfo->bmiColors[i].rgbBlue = (BYTE)i;
		m_pBitmapInfo->bmiColors[i].rgbGreen = (BYTE)i;
		m_pBitmapInfo->bmiColors[i].rgbRed = (BYTE)i;
		m_pBitmapInfo->bmiColors[i].rgbReserved = 0;
	}
	m_pBitmapInfo->bmiHeader.biWidth = 0;
	m_pBitmapInfo->bmiHeader.biHeight = 0;

}

CWnd_CameraView_Cv::~CWnd_CameraView_Cv()
{
	if (m_pBitmapInfo) delete m_pBitmapInfo;
}


BEGIN_MESSAGE_MAP(CWnd_CameraView_Cv, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CWnd_CameraView_Cv 메시지 처리기입니다.



int CWnd_CameraView_Cv::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();



	m_CameraFrame.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_CameraFrame.SetColorStyle(CVGStatic::ColorStyle_Default);

	m_CameraFrame.SetFont_Gdip(L"Arial", 25.0F);
	m_CameraFrame.Create(_T("CAM"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	CameraMode(0);
	return 0;
}

void CWnd_CameraView_Cv::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	if (bShow)
	{


	}


}
void CWnd_CameraView_Cv::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);
	if ((cx == 0) && (cy == 0))
		return;

	int iLeftMargin = 7;
	int iRightMargin = 7;
	int iTopMargin = 27;
	int iBottomMargin = 7;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;
	int iBottom = cy - iBottomMargin;
	int iWidth = cx - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;

	int camFramH = iHeight - 32;
	m_CameraFrame.MoveWindow(0, 0, cx, cy);

}

void CWnd_CameraView_Cv::Display_NoImage(){

	//Delay(100);
	CRect rt;
	GetClientRect(rt);
	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB = new CBitmap;

	pcDC = this->GetDC();
	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC, rt.Width(), rt.Height());
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);

	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);

	//----
	CBrush NewBrush;
	NewBrush.CreateStockObject(BLACK_BRUSH);
	CBrush* pOldBrush = pcMemDC->SelectObject(&NewBrush);

	pcMemDC->SetBkMode(TRANSPARENT);

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(0, 0, 0);
	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pcMemDC->SelectObject(&NewPen);



	pcMemDC->Rectangle(CRect(0, 0, rt.Width(), rt.Height()));

	CFont font;
	font.CreatePointFont(200, _T("Arial"));
	pcMemDC->SelectObject(&font);
	pcMemDC->SetTextAlign(TA_LEFT | TA_BASELINE);
	CString strText = _T("No Signal");
	pcMemDC->SetTextColor(RGB(255, 0, 0));
	pcMemDC->TextOut(20, 40, strText, strText.GetLength());
	font.DeleteObject();

	pcMemDC->SelectObject(pOldBrush);
	pcMemDC->SelectObject(pOldPen);


	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), pcMemDC->m_hDC, 0, 0, rt.Width(), rt.Height(), SRCCOPY);

	this->ReleaseDC(pcDC);
	delete pcDIB;
	delete pcMemDC;

}

void CWnd_CameraView_Cv::CameraMode(UINT nMode){

	if (nMode == 1)
	{
		m_CameraFrame.ShowWindow(SW_HIDE);
	}
	else{
		m_CameraFrame.ShowWindow(SW_SHOW);

	}

}

void CWnd_CameraView_Cv::ShowVideo(__in INT iChIdx, __in  IplImage *TestImage, __in DWORD dwWidth, __in DWORD dwHeight){


	int nWidth = dwWidth;
	int nHeight = dwHeight;
	m_pBitmapInfo->bmiHeader.biWidth = nWidth;  // Width = Pitch(bytes) divided by the number of bytes per pixel
	m_pBitmapInfo->bmiHeader.biHeight = -nHeight;
	
	CRect rt;
	CDC* pDC = m_CameraFrame.GetDC();
	m_CameraFrame.GetClientRect(&rt);
	::SetStretchBltMode(pDC->GetSafeHdc(), COLORONCOLOR);
	StretchDIBits(pDC->GetSafeHdc(), 0, 0, rt.Width(), rt.Height(), 0, 0, dwWidth, dwHeight, TestImage->imageData, m_pBitmapInfo, DIB_RGB_COLORS, SRCCOPY);
	ReleaseDC(pDC);
}