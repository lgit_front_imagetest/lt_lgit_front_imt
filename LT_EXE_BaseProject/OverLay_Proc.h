﻿#ifndef Overlay_Proc_h__
#define Overlay_Proc_h__

#pragma once

typedef enum enOverlayMode
{
	OvrMode_LINE,		// 직선
	OvrMode_RECTANGLE,	// 직사각형
	OvrMode_CIRCLE,		// 원
	OvrMode_TXT,		// 글씨
	OvrMode_MaxNum,
};

#include "cv.h"
#include "highgui.h"
#include "Def_DataStruct.h"

//#include "Def_UI_DynamicBW.h"
//#include "Def_UI_Current.h"
//#include "Def_UI_Shading.h"
//#include "Def_UI_OpticalCenter.h"

// COverlay_Proc
class COverlay_Proc
{

public:
	COverlay_Proc();
	virtual ~COverlay_Proc();

	void SetModelType	(__in enModelType nModelType)
	{
		m_eModelType = nModelType;
	}

	void SetTestMode	(__in BOOL bTest)
	{
		m_bTest = bTest;
	}
	void	Overlay_LockingScrew		(__inout IplImage* lpImage, __in ST_UI_Torque			stOption, __in ST_Result_Torque_Foc			stData);
	void	Overlay_ReleaseScrew		(__inout IplImage* lpImage, __in ST_UI_Torque			stOption, __in ST_Result_Torque_Foc			stData);
	void	Overlay_Chart				(__inout IplImage* lpImage, __in ST_UI_Chart		stOption, __in ST_Result_Chart stResult);
	void	Overlay_Current				(__inout IplImage* lpImage, __in ST_UI_Current		stOption, __in ST_Result_Current_IQ stResult);
	void	Overlay_Current				(__inout IplImage* lpImage, __in ST_UI_Current		stOption, __in ST_Result_Current_FOC stResult);
	void	Overlay_SFR					(__inout IplImage* lpImage, __in ST_UI_SFR			stOption, __in ST_Result_SFR_IQ stResult);
	void	Overlay_SFR					(__inout IplImage* lpImage, __in ST_UI_SFR			stOption, __in ST_Result_SFR_FOC stResult);
//  void	Overlay_DynamicBW			(__inout IplImage* lpImage, __in ST_UI_DynamicBW	stOption, __in ST_Result_DynamicBW stResult);
// 	void	Overlay_Shading				(__inout IplImage* lpImage, __in ST_UI_Shading		stOption, __in ST_Result_Shading stResult);
	void	Overlay_Ymean				(__inout IplImage* lpImage, __in ST_UI_Ymean		stOption, __in ST_Result_Ymean_IQ stResult);
	void	Overlay_Ymean				(__inout IplImage* lpImage, __in ST_UI_Ymean		stOption, __in ST_Result_Ymean_Foc stResult);
	void	Overlay_LCB					(__inout IplImage* lpImage, __in ST_UI_LCB		stOption, __in ST_Result_LCB_IQ stResult);
	void	Overlay_LCB					(__inout IplImage* lpImage, __in ST_UI_LCB		stOption, __in ST_Result_LCB_Foc stResult);
	void	Overlay_BlackSpot			(__inout IplImage* lpImage, __in ST_UI_BlackSpot		stOption, __in ST_Result_BlackSpot_IQ stResult);
	void	Overlay_BlackSpot			(__inout IplImage* lpImage, __in ST_UI_BlackSpot		stOption, __in ST_Result_BlackSpot_Foc stResult);
	void	Overlay_RI					(__inout IplImage* lpImage, __in ST_UI_Rllumination		stOption, __in ST_Result_Rllumination stResult);
	void	Overlay_Defect_Black		(__inout IplImage* lpImage, __in ST_UI_Defect_Black		stOption, __in ST_Result_Defect_Black_IQ stResult);
	void	Overlay_Defect_Black		(__inout IplImage* lpImage, __in ST_UI_Defect_Black		stOption, __in ST_Result_Defect_Black_FOC stResult);
	void	Overlay_Defect_White		(__inout IplImage* lpImage, __in ST_UI_Defect_White		stOption, __in ST_Result_Defect_White_IQ stResult);
	void	Overlay_Defect_White		(__inout IplImage* lpImage, __in ST_UI_Defect_White		stOption, __in ST_Result_Defect_White_FOC stResult);
	void	Overlay_Rotation			(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Rotation_IQ stResult);
	void	Overlay_Rotation			(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Rotation_Foc stResult);
	void	Overlay_FOV					(__inout IplImage* lpImage, __in ST_UI_FOV stOption, __in ST_Result_FOV_IQ stResult);
	void	Overlay_Distortion			(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Distortion_IQ stResult);
	void	Overlay_DynamicBW			(__inout IplImage* lpImage, __in ST_UI_DynamicBW stOption, __in ST_Result_DynamicBW_IQ stResult);
	void	Overlay_Shading				(__inout IplImage* lpImage, __in ST_UI_Shading stOption, __in ST_Result_Shading_IQ stResult);
	void	Overlay_OpticalCenter		(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt	stOption, __in ST_OpticalCenter_Data_IQ stData);
	void	Overlay_OpticalCenter		(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt	stOption, __in ST_OpticalCenter_Data_Foc stData);
	void	Overlay_PreFocusing			(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data_Foc stData);

protected:

	enModelType		m_eModelType	= Model_OMS_Front;
	BOOL			m_bTest			= FALSE;

	void	Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor = RGB(255, 255, 0), __in int iLineSize = 1, __in double dbFontSize = 1.0, __in CString szText = _T(""));

};


#endif // Overlay_Proc_h__
