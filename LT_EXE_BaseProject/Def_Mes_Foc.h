﻿#ifndef Def_Mes_Foc_h__
#define Def_Mes_Foc_h__

#include <afxwin.h>


#pragma pack(push,1)


//!SH _181108: MES OC Offset 추가
static LPCTSTR g_szMESHeader_Foc[] =
{
	_T("Current_1.8V"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("OC_Offset_X"),
	_T("OC_Offset_Y"),
	_T("SFR_0"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("Rotation"),
	_T("Ymean"),
	_T("BlackSpot"),
	_T("LCB"),
	_T("Defect_Black"),
	_T("Defect_White"),
	_T("Torque_Value_A"),
	_T("Torque_Value_B"),
	_T("Torque_Count_A"),
	_T("Torque_Count_B"),
	NULL,
};

//항목 순서
typedef enum enMesDataIndex_Foc
{
	MesDataIdx_Foc_ECurrent = 0,
	MesDataIdx_Foc_OpticalCenter,
	MesDataIdx_Foc_SFR,
	MesDataIdx_Foc_Rotation,
	MesDataIdx_Foc_Ymean,
	MesDataIdx_Foc_BlackSpot,
	MesDataIdx_Foc_LCB,
	MesDataIdx_Foc_Defect_Black,
	MesDataIdx_Foc_Defect_White,
	MesDataIdx_Foc_Torque,
	MesDataIdx_Foc_MAX,
};

//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_Foc 
{
	MESDataNum_Foc_ECurrent			= 1,
	MESDataNum_Foc_OpticalCenter	= 4,//!SH _181108: MES OC Offset 추가
	MESDataNum_Foc_SFR				= 30,
	MESDataNum_Foc_Rotation			= 1,
	MESDataNum_Foc_Ymean			= 1,
	MESDataNum_Foc_BlackSpot		= 1,
	MESDataNum_Foc_LCB				= 1,
	MESDataNum_Foc_Defect_Black		= 1,
	MESDataNum_Foc_Defect_White		= 1,
	MESDataNum_Foc_Torque			= 4,

} MesData_DataNUM_Foc;


typedef struct _tag_MesData_Foc
{
	CString szMesTestData[2][MesDataIdx_Foc_MAX];
	CString szDataName[MesDataIdx_Foc_MAX];

	UINT nMesDataNum[MesDataIdx_Foc_MAX];

	_tag_MesData_Foc()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_Foc_ECurrent		] = MESDataNum_Foc_ECurrent;
		nMesDataNum[MesDataIdx_Foc_OpticalCenter] = MESDataNum_Foc_OpticalCenter;
		nMesDataNum[MesDataIdx_Foc_SFR			] = MESDataNum_Foc_SFR;
		nMesDataNum[MesDataIdx_Foc_Rotation		] = MESDataNum_Foc_Rotation;
		nMesDataNum[MesDataIdx_Foc_Ymean		] = MESDataNum_Foc_Ymean;
		nMesDataNum[MesDataIdx_Foc_BlackSpot	] = MESDataNum_Foc_BlackSpot;
		nMesDataNum[MesDataIdx_Foc_LCB			] = MESDataNum_Foc_LCB;
		nMesDataNum[MesDataIdx_Foc_Defect_Black	] = MESDataNum_Foc_Defect_Black;
		nMesDataNum[MesDataIdx_Foc_Defect_White	] = MESDataNum_Foc_Defect_White;
		nMesDataNum[MesDataIdx_Foc_Torque		] = MESDataNum_Foc_Torque;

		szDataName[MesDataIdx_Foc_ECurrent		] = _T("Current");
		szDataName[MesDataIdx_Foc_OpticalCenter	] = _T("Optical Center");
		szDataName[MesDataIdx_Foc_SFR			] = _T("SFR");
		szDataName[MesDataIdx_Foc_Rotation		] = _T("Rotate");
		szDataName[MesDataIdx_Foc_Ymean			] = _T("Ymean");
		szDataName[MesDataIdx_Foc_BlackSpot		] = _T("BlackSpot");
		szDataName[MesDataIdx_Foc_LCB			] = _T("LCB");
		szDataName[MesDataIdx_Foc_Defect_Black	] = _T("Defect_Black");
		szDataName[MesDataIdx_Foc_Defect_White	] = _T("Defect_White");
		szDataName[MesDataIdx_Foc_Torque		] = _T("Torque");
	};

	void Reset(UINT nCam)
	{
		for (UINT nItem = 0; nItem < MesDataIdx_Foc_MAX; nItem++)
		{
			szMesTestData[nCam][nItem].Empty();
		}
	};

	_tag_MesData_Foc& operator= (_tag_MesData_Foc& ref)
	{
		for (UINT nCam = 0; nCam < 2; nCam++)
		{
			for (UINT nItem = 0; nItem < MesDataIdx_Foc_MAX; nItem++)
			{
				szMesTestData[nCam][nItem] = ref.szMesTestData[nCam][nItem];
			}
		}
		
		return *this;
	};

}ST_MES_Data_Foc, *PST_MES_Data_Foc;


#pragma pack(pop)

#endif // Def_FOV_h__