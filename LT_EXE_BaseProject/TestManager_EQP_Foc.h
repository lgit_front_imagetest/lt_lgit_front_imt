﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_Foc.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_Foc_h__
#define TestManager_EQP_Foc_h__

#pragma once

#include "TestManager_EQP.h"
#include "TestManager_TestMES.h"
//-----------------------------------------------------------------------------
// CTestManager_EQP_Foc
//-----------------------------------------------------------------------------
class CTestManager_EQP_Foc : public CTestManager_EQP
{
public:
	CTestManager_EQP_Foc();
	virtual ~CTestManager_EQP_Foc();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);


	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload		(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection		(__in UINT nParaIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual		(__in UINT nParaIdx = 0);

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload			();
	virtual void	StopProcess_Test_All			();
	virtual void	StopProcess_InspManual			(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload		(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All		(__in UINT nParaIdx = 0);
	virtual void	AutomaticProcess_Test_InspManual(__in UINT nParaIdx = 0);

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload			(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload			(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload				(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All				(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All				(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All				(__in UINT nParaIdx = 0);
	
	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual		(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual		(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product					();
	virtual LRESULT Unload_Product					();

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment				(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_Focusing				(__in UINT nParaIdx = 0);

	
	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem		(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);

	virtual LRESULT	StartTest_Focusing_TestItem		(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 공용 검사항목
	virtual	LRESULT _TI_Cm_Initialize				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize_Error			(__in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurCurrent			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_Cm_CaptureImage				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT	_TI_Cm_CameraRegisterSet		(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraAlphaSet			(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraPowerOnOff			(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	BOOL _TI_Get_ImageBuffer_CaptureImage			(enImageMode eImageMode , __in UINT nParaIdx , __out UINT &nImgW, __out UINT &nImgH, __out CString &strTestFileName, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	
	// Focusing 검사항목
	virtual LRESULT	_TI_Foc_Mot_StageLoad			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_StageUnLoad			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_LockingCheck		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_ReleaseTorque		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_ParticleLenzIn		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_ParticleLenzOut		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Mot_LockingTorque		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	
	virtual LRESULT	_TI_Foc_Pro_PreFocus			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_ActiveAlign			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_OpticalCenter		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_Current				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_SFR					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_Rotation			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_Particle			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_DefectPixel			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_Ymean				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_BlackSpot			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_LCB					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_DefectBlack			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_DefectWhite			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	
	virtual LRESULT _TI_Foc_JudgeResult				(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);

	virtual LRESULT	_TI_Foc_AA_OpticalCenter		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_AA_Rotate				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_AA_ScrewLocking			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_AA_Data					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Foc_Pro_Torque				(__in UINT nParaIdx = 0);
	
	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_MakeInspecData_Foc			(__in UINT nParaIdx = 0);

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual LRESULT	OnMotion_MoveToTestPos			(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos		(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin					();
	virtual LRESULT	OnMotion_LoadProduct			();
	virtual LRESULT	OnMotion_UnloadProduct			();
	virtual LRESULT	OnMotion_SteCal_Test			(__in UINT nParaIdx = 0);

	// 제품 재검사를 위해서 측정 데이터 및 UI 초기화
	virtual void	OnResetInfo_Measurment			(__in UINT nParaIdx = 0);

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal				(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_Foc			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal				();

	virtual LRESULT	OnDIn_MainPower					();
	virtual LRESULT	OnDIn_EMO						();
	virtual LRESULT	OnDIn_AreaSensor				();
	virtual LRESULT	OnDIn_DoorSensor				();
	virtual LRESULT	OnDIn_JIGCoverCheck				();
	virtual LRESULT	OnDIn_Start						();
	virtual LRESULT	OnDIn_Stop						();
	virtual LRESULT	OnDIn_CheckSafetyIO				();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus		();
		
	virtual LRESULT	OnDOut_BoardPower				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp			(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp				(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer					(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_EEPROM_Write				(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Entry_Foc		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Front_Foc		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_MRA2_Foc			(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_IKC_Foc			(__in UINT nIdxBrd = 0);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	
	virtual void	OnSet_GraphSFR					(__in double dbCurrentSFR, __in double dbMaxSFR){};
	virtual void	OnSet_GraphReset				(){};

	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	
	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnSaveWorklist					();
	
	//-----------------------------------------------------
	// Overlay Info
	//-----------------------------------------------------
	virtual void	OnSetOverlayInfo_Foc			(__in UINT enTestItem, __in UINT nParaItem = 0){};

	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------
	virtual void	SetLoopItem_Info				(__in UINT nTestItemID, __in enTestLoop enUse);
	virtual BOOL	GetLoopItem_Info				(__in UINT nTestItemID);

	virtual void	OnChangeTheCamera				(__in BOOL bMode, __in UINT nParaIdx){};
	virtual void	OnChangeTheCameraStop			(){};
	virtual BOOL	OnFoc_ChangeCamFlag				(){return FALSE;};

	// Loop Flag 변수
	BOOL				m_bFlag_LoopItem[TI_Foc_MaxEnum];

	// 카메라 변경 처리 변수
	BOOL				m_bFlag_CameraChange	= FALSE;

	// 유저 Stop 처리 변수
	BOOL				m_bFlag_UserStop		= FALSE;

public:
	
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);

	void TestResultView								(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam, __in enFocus_AAView enItem = Foc_AA_MaxNum);
	void TestResultView_Reset						(__in UINT nTestID, __in UINT nPara);

	virtual void OnReset_CamInfo(__in UINT nParaIdx = 0);
	void EachTest_ResultDataSave(UINT nTestID, UINT nPara);

	ST_MES_Data_Foc m_stMesData;
	CTestManager_TestMES m_TestMgr_TestMes; 
};

#endif // TestManager_EQP_Foc_h__

